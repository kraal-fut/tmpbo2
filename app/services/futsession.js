import Ember from 'ember';

const { service } = Ember.inject;
const { alias } = Ember.computed;

export default Ember.Service.extend({
    session: service('session'),
    store: service(),

    data: alias('session.data.authenticated.data'),

    user: alias('data.user'),
    userSettings: alias('data.userSettings'),
    appSettings: alias('data.appSettings'),

    isAuthenticated: alias('session.isAuthenticated'),

    settings: Ember.computed('appSettings.@each', 'userSettings.@each', {
        get() {
            let cl = Ember.copy(this.get('appSettings'), true);
            return Ember.merge(cl, this.get('userSettings'));
        }
    })
});
