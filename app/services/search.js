import Ember from 'ember';

export default Ember.Service.extend({
    searchPlaceholder: 'Find orders',
    handler: undefined,

    setHandler(handler) {
        this.set('handler', handler);
    },

    unsetHandler() {
        this.set('handler', undefined);
    },

    hasHandler() {
        return typeof this.get('handler') !== 'undefined';
    },

    callhandler(query) {
        if (this.hasHandler()) {
            return this.get('handler').search(query);
        }
    }
});
