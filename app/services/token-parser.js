import Ember from 'ember';

let tokenMap = {
    sex: 'order.sex',
    achternaam: 'order.DelName',
    name: 'order.name',
    phone: 'order.phone',
    lines_cancelled: undefined,
    lines_added: undefined,
    lines_added_free: undefined,
    klantnummer: 'order.RegId',
    regid: 'order.RegId',
    order: 'order.Ordernumber',
    ordernummer: 'order.Ordernumber',
    paymentmethod: 'order.PaymentMethod',
    totaltopay: 'order.TotalToPay',
    discount: 'order.Discount',
    barcode: 'order.barcode',
    barcode_TNT: 'order.barcode',
    DelZip: 'order.DelZip',
    dzip: 'order.DelZip'
};

let tokensToStrip = [
    'sex', 'lines_cancelled', 'lines_added', 'lines_added_free', 'password'
];

export default Ember.Service.extend({
    parse(input, model) {
        if (!model) {
            throw Error('You cannot use a tokenparser without a model to get data from');
        }
        if (!input) {
            return input;
        }

        let matches = input.match(/%%([a-zA-Z_])+%%/gi);

        if (!matches) {
            return input;
        }

        matches.forEach((match)=> {
            let varName = match.match(/([a-zA-Z_]+)/gi)[0];

            if (tokensToStrip.contains(varName)) {
                input = input.replace(match, '');
                return;
            }

            if (tokenMap[varName] && model.get(tokenMap[varName]) !== undefined) {
                input = input.replace(match, model.get(tokenMap[varName]));
            }
        });

        return input;
    }
});
