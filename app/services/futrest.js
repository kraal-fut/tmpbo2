import Ember from 'ember';
import {request} from 'ic-ajax';

let modelUrlMap = {
    'product/product': '/products'
};

export default Ember.Service.extend({
    loadData(url, id, params, entityName) {
        if (id) {
            url += '/' + id;
        }

        return this.doRequest('GET', url, undefined, params)
            .then(response => {
                if (entityName && response[entityName]) {
                    return response[entityName];
                }

                return response;
            }).catch(response => {
                if (response.jqXHR) {
                    throw response.jqXHR;
                }
                throw response;
            });
    },

    deleteRequest(url, params) {
        return this.doRequest('DELETE', url, params);
    },

    storeData(url, data, params, method) {
        if (params) {
            url += params;
        }

        return this.doRequest(method, url, data);
    },

    doRequest(type, resource, body = undefined, params = undefined) {
        let url = '/api/v3' + resource;
        if (params) {
            url += '?' + this.encodeToUrl(params);
        }
        let data = JSON.stringify(body);

        return request({
            type,
            url,
            data,
            contentType: 'application/json'
        });
    },

    encodeToUrl(params) {
        return Object.keys(params).map(k => {
            return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]);
        }).join('&');
    },

    load(url, params, entityName) {
        return this.loadData(url, null, params, entityName);
    },

    store(url, data, method) {
        return this.storeData(url, data, null, method);
    },

    loadModel(type, Model, ids){
        let url = modelUrlMap[type];
        if (ids) {
            url += '/';
            if (ids.length) {
                url += ids.join(',');
            } else {
                url += ids;
            }
        }

        return this.load(url).then(response => {
            if (typeof Model === 'undefined') {
                return response;
            }
            return response.map(obj => {
                return Model.create(obj);
            });
        });
    },

    parseError(response, model = undefined, notifier = undefined) {
        let error;

        if (response.jqXHR) {
            response = response.jqXHR;
        }

        try {
            error = JSON.parse(response.responseText);

            if (Ember.isNone(error.errors)) {
                error = error.error.message;
            } else {
                if (response.status === 422) {
                    if (Ember.tryInvoke(model, 'setServerValidationErrors', [error.errors])) {
                        return error.errors;
                    } else {
                        error = "Could not save, maybe not everything is filled in correctly?";
                        console.error(error, response);
                    }
                }
            }

        } catch (ignore) {
            error = response.responseText;
        }

        if (notifier) {
            notifier.notify({text: error, options: {status: 'error', delay: 0}}, true);
        }

        return error;
    },

    notifyFailedAjax(action = 'Save') {
        this.notifications.addNotification({
            message: action + ' ran into an unkown error!',
            type: 'error',
            autoClear: true,
            clearDuration: 1200
        });
    },

    notifySuccessAjax(action = 'Save') {
        this.notifications.addNotification({
            message: action + 'd successfully!',
            type: 'success',
            autoClear: true,
            clearDuration: 1200
        });
    }
});
