import Ember from 'ember';

export default Ember.Service.extend({
    forceHtml: false,

    notify(notifyEvent, forceHtml) {
        if (_.isString(notifyEvent)) {
            notifyEvent = {text: notifyEvent};
        }
        notifyEvent = notifyEvent || {};
        notifyEvent.options = notifyEvent.options || {};

        if (this.forceHtml || forceHtml) {
            return this.notifyHtml(notifyEvent);
        }

        return this.desktopNotify(notifyEvent);
    },

    desktopNotify(notifyEvent) {
        const notifier = this;

        const sendDesktopNotify = function () {
            let notification = new window.Notification('Futurumshop BO2', {
                body: notifyEvent.text,
                icon: '/apple-touch-icon.png'
            });

            setTimeout(() => {
                notification.close();
            }, notifyEvent.options.delay || 5000);
        };

        if (window.Notification.permission === 'granted') {
            sendDesktopNotify();
            return;
        }

        if (window.Notification.permission !== 'denied') {
            window.Notification.requestPermission(permission => {
                if (permission === 'granted') {
                    sendDesktopNotify();
                } else {
                    notifier.desktopNotify(notifyEvent);
                }
            });
            return;
        }
        this.desktopNotify(notifyEvent);
    },

    notifyHtml(notifyEvent) {
        let clear = typeof notifyEvent.options.delay === 'undefined' ? 1800 : notifyEvent.options.delay;

        clear = clear === 0 ? undefined : clear;

        this.notifications.addNotification({
            message: notifyEvent.text,
            type: notifyEvent.options.status || notifyEvent.status || 'info',
            autoClear: true,
            clearDuration: clear
        });
    }
});
