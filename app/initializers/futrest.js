import Ember from 'ember';

export function initialize(container, application) {
    //Allow setting a data-toggle attribute on links, this is a bit needed for bootstrap nav links
    Ember.LinkComponent.reopen({
        attributeBindings: ['data-toggle', 'data-target']
    });

    ['controller', 'component', 'route', 'router'].forEach(function (injectionTarget) {
        application.inject(injectionTarget, 'futRest', 'service:futrest');
        application.inject(injectionTarget, 'notifier', 'service:notifier');
        application.inject(injectionTarget, 'searchSvc', 'service:search');
    });

    application.inject('controller', 'session', 'service:futsession');
    application.inject('component', 'session', 'service:futsession');
}

export default {
    name: 'futrest',
    initialize: initialize
};
