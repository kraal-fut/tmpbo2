import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        authenticate() {
            this.get('session').authenticate(
                'authenticator:futurumshop',
                this.getProperties('identification', 'password')
            );
        }
    }
});
