import Ember from 'ember';

export default Ember.Controller.extend({
    pagination: Ember.computed(function(){
        return this.controllerFor('shop/faqs').get('pagination');
    })
});
