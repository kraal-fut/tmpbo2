import Ember from 'ember';

export default Ember.Controller.extend( {
    imageHost: Ember.computed.alias('session.appSettings.BASE_URL')
} );