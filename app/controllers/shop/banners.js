import Ember from 'ember';
import Paginated from 'bo2/mixins/paginated';

export default Ember.Controller.extend(
    Paginated,
    {
        bannerTypes: [
            'top',
            'header',
            'theme',
            'themeSmall',
            'highlight'
        ]
    }
);
