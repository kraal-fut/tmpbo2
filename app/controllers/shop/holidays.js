import Ember from 'ember';
import Paginated from 'bo2/mixins/paginated';

export default Ember.Controller.extend(
    Paginated,
    {}
);
