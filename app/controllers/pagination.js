import Ember from 'ember';

export default Ember.Controller.extend({
    state: Ember.Object.create({itemsPerPage: 10}),
    perPage: Ember.computed.oneWay('state.itemsPerPage'),
    totalPages: Ember.computed.oneWay('state.totalPages'),
    currentPage: Ember.computed.oneWay('state.page'),

    doTransition(newPage = undefined, perPage = undefined) {
        this.transitionToRoute({
            queryParams: {
                page: newPage || this.get('currentPage'),
                limit: perPage || this.get('perPage')
            }
        });
    }
});
