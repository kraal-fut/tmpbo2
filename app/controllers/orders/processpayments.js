import Ember from 'ember';

export default Ember.Controller.extend({
    sortProperties: ['valid'],
    lineDelim: /\r\n|\r|\n/g,

    sortedOrders: Ember.computed.sort('model', 'sortProperties'),

    bo1url: Ember.computed.alias('session.appSettings.BO1_URL'),

    greenOrders: Ember.computed('model.@each.paymenttype', 'model.@each.valid', function () {
        return this.get('model').filter(function (order) {
            return order.paymenttype === 'C' && order.valid === 1;
        }).get('length');
    }),

    redOrders: Ember.computed('model.@each.paymenttype', 'model.@each.valid', function () {
        return this.get('model').filter(function (order) {
            return order.paymenttype === 'C' && order.valid === 0;
        }).get('length');
    }),

    blackOrders: Ember.computed('model.@each.paymenttype', function () {
        return this.get('model').filterBy('paymenttype', 'D').get('length');
    }),

    ordersToProcess: Ember.computed('model.@each.process', function () {
        return this.get('model').filterBy('process', true);
    }),

    hasOrdersToProcess: Ember.computed('ordersToProcess', function () {
        return this.get('ordersToProcess.length');
    })
});
