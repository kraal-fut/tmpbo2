import Ember from 'ember';

export default Ember.Controller.extend({
    queryParams: ['template', 'backUrl'],
    template: null,
    backUrl: null,
    TokenParser: Ember.inject.service(),

    addresses: Ember.computed('model.order.email','model.order.DelEmail', function() {
        return Ember.A([
            this.get('model.order.email'),
            this.get('model.order.DelEmail')
        ]).uniq();
    }),

    body: Ember.computed('model.mail.body', {
        set(field, value) {
            let parsed = this.get('TokenParser').parse(value, this.get('model'));
            this.set('model.mail.body', parsed);
            return parsed;
        },
        get() {
            return this.get('TokenParser').parse(this.get('model.mail.body'), this.get('model'));
        }
    }),

    subject: Ember.computed('model.mail.subject', {
        set(field, value) {
            let parsed = this.get('TokenParser').parse(value, this.get('model'));
            this.set('model.mail.subject', parsed);
            return parsed;
        },
        get() {
            return this.get('TokenParser').parse(this.get('model.mail.subject'), this.get('model'));
        }
    })
});
