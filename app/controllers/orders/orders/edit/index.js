import Ember from 'ember';

export default Ember.Controller.extend({
    edit: Ember.inject.controller('orders/orders/edit'),
    automails: Ember.computed.alias('edit.automails')
});
