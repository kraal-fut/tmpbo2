import Ember from 'ember';
import ItpakketModel from 'bo2/models/order/itpakket';

export default Ember.Controller.extend({
    imports: Ember.A([]),
    errors: Ember.A([]),

    lineDelim: /(3STQOV[0-9]{7,9}[\S\s]+?)FuturumShop.com bestelling/g,

    noExportable: Ember.computed.not('hasExportable'),

    hasExportable: Ember.computed('model', function () {
        return this.get('model.length') > 0;
    }),

    exportableOrders: Ember.computed('model', function () {
        return this.get('model.length');
    }),

    parseData(orders, lines) {
        _.each(orders, order => {
            try {

                var m = ItpakketModel.create({});
                m.parseModelLine(order.split(/\n/g));

                lines.pushObject(m);

            } catch (Exception) {
                if (Exception.message === "Cannot read property '1' of null") {
                    return false;
                }
            }
        });
    },

    actions: {
        imported: function (importeds) {
            this.parseData(importeds, this.get('imports'));
        }
    }
});
