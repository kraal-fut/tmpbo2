import Ember from 'ember';
import EditableMixin from 'bo2/mixins/editable';

export default Ember.Controller.extend(EditableMixin, {
    fomenu: Ember.computed('id', function () {
        return this.parentController.get('model.fomenu');
    }),

    actions: {
        popup() {
            return this.get('editing');
        }
    }
});
