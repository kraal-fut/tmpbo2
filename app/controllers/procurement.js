import Ember from 'ember';

export default Ember.Controller.extend({
    init() {
        this._super();
        this.get('searchSvc').setHandler(this);
    },

    search(query) {
        this.transitionToRoute('procurement.orders.index', {queryParams: {q: query}});
    },

    unsetSearchHandler() {
        this.get('searchSvc').unsetHandler();
    }
});
