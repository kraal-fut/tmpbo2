import Ember from 'ember';

export default Ember.Controller.extend(Ember.SortableMixin, {
    sortProperties: ['name:asc'],

    sortedList: Ember.computed.sort('model', 'sortProperties'),

    actions: {
        sortBy(property) {
            var prop = this.get('sortProperties')[0].split(':')[0];
            var order = this.get('sortProperties')[0].split(':')[1];
            if (property === prop) {
                order = order === 'asc' ? 'desc' : 'asc';
            } else {
                prop = property;
                order = 'asc';
            }

            this.set('sortProperties', [prop + ':' + order]);
        },

        timePicked(from, to) {
            this.get('model').setProperties({from, to});
        }
    }
});
