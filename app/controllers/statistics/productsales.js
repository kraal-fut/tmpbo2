import Ember from 'ember';

export default Ember.Controller.extend({
    searchQuery: undefined,
    actions: {
        search(query) {
            if (Ember.isNone(query)) {
                this.set('model.searchQuery', undefined);
            } else {
                this.set('model.searchQuery', query);
            }
        },

        clearSearch() {
            this.set('model.searchQuery', undefined);
            this.set('searchQuery', undefined);
        },

        timePicked(from, to) {
            this.get('model').setProperties({from, to});
        }
    }
});
