import Ember from 'ember';

export default Ember.Controller.extend({
    searchQuery: undefined,
    domain: Ember.computed(() => {
        return [
            {fieldname: 'revHist', key: 'Revenue', color: '#ff7f0e', area: true},
            {fieldname: 'nrOfOrdersHist', key: 'Number of orders', color: '#2CDE2C', area: false},
            {fieldname: 'avgRevHist', key: 'Average Revenue', color: '#ff0000', area: false},
            {fieldname: 'averageOrderMargeHist', key: 'Average Marge', color: '#2CA02C', area: false}
        ];
    }),

    actions: {
        search(query) {
            if (Ember.isNone(query)) {
                this.set('model.searchQuery', undefined);
            } else {
                this.set('model.searchQuery', query);
            }
        },

        clearSearch() {
            this.set('model.searchQuery', undefined);
            this.set('searchQuery', undefined);
        },

        timePicked(from, to) {
            this.get('model').setProperties({from, to});
        }
    }
});
