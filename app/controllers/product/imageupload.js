import Ember from 'ember';
import UploadModel from 'bo2/models/product/imageupload';

export default Ember.Controller.extend({
    checkFiles(files) {
        var models = this.get('model');
        models.pushObjects(_.map(files, this.extractDataFromFilename));

        var ids = models.getEach('prodlevid').uniq().join(',');
        var that = this;

        this.futRest.loadData('/productdescription/' + ids + '/images?limit=' + ( models.get('length') + 1 ))
            .then(response => {
                if (_.isObject(response)) {
                    if (!_.isUndefined(response.ProductImage)) {
                        return response.ProductImage;
                    }
                    response = [response];
                }
                return response;
            }).then(response => {
                _.each(response, that.findMatchingUploadModel.bind(that));
                return response;
            }).then(response => {
                let notFounds = _.difference(models.getEach('prodlevid'), response.getEach('id'));

                models.filter((model) => {
                    return notFounds.contains(model.get('prodlevid'));
                }).setEach('invalid', true);

                return models;
            }).catch(() => {
                models.setEach('invalid', true);
            });
    },

    extractDataFromFilename(file) {
        var fullFileName = file.name;
        var fileExt = fullFileName.split('.').pop().toLowerCase();
        var fileName = fullFileName.substr(0, ( fullFileName.length - fileExt.length ) - 1);

        var extraPos = fileName.indexOf('_extra');
        var isExtra = extraPos !== -1;
        var prodlevid = fileName.substr(0, isExtra ? extraPos : undefined);

        return UploadModel.create({
            invalid: false,
            prodlevid: prodlevid,
            filename: fullFileName,
            fileExt: fileExt,
            isExtra: isExtra,
            file: file
        }).readFile();
    },

    findMatchingUploadModel(foundAtServer) {
        var newFile = _.reduce(foundAtServer.images, (memo, image) => {
            return parseInt(image.filename, 10) > memo ? parseInt(image.filename, 10) : memo;
        }, 0);

        this.get('model')
            .filterBy('prodlevid', foundAtServer.id)
            .forEach(imageToUploadModel => {
                imageToUploadModel.set('images', foundAtServer.images);
                imageToUploadModel.setVariationInfo(foundAtServer.relatedProducts);
                newFile = imageToUploadModel.setFileName(newFile);
            });
    },

    actions: {
        dropped(event) {
            this.checkFiles(event.originalEvent.dataTransfer.files);
        }
    }
});
