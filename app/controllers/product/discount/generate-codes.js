import Ember from 'ember';

export default Ember.Controller.extend({
    example: Ember.computed(
        'model.optional_arguments.prefix',
        'model.optional_arguments.codelength',
        'model.optional_arguments.postfix',
        function () {
            let oa = Ember.Object.create(this.get('model.optional_arguments'));
            let pre = oa.get('prefix');
            let codelength = parseInt(oa.get('codelength'), 10);
            let post = oa.get('postfix');
            let code = Math.random().toString(36).substring(2, codelength + 2);

            return pre + '' + code + '' + post;
        }
    )
});
