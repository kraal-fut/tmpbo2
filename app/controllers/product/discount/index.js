import Ember from 'ember';
import Paginated from 'bo2/mixins/paginated';

export default Ember.Controller.extend(
    Paginated,
    {
        isLoadingCode: false,
        searchedForCode: false,

        foundCode: Ember.Object.create({}),
        hasFoundCode: Ember.computed.and('foundCode.id', 'searchedForCode'),
        noFoundCode: Ember.computed.not('hasFoundCode'),
        isUsed: Ember.computed.bool('foundCode.used'),

        loadingCode(isLoading) {
            this.set('isLoadingCode', isLoading);
        },

        loadCode(code) {
            if (Ember.isNone(code)) {
                return;
            }
            this.loadingCode(true);

            return this.futRest.loadData('/couponcodes/' + code).then(foundCode => {
                this.get('foundCode').setProperties(foundCode.CouponCode[0]);
                this.set('couponcode', this.get('foundCode.code'));

                return this.get('foundCode');
            }, response => {
                this.loadingCode(false);
                if (response.error && response.status === 404) {
                    this.set('foundCode.id', false);
                }
            }).finally(()=> {
                this.loadingCode(false);
                this.set('searchedForCode', true);
            });
        },

        invalidate(code) {
            code.set('used', 1);
            return this.futRest.storeData('/couponcodes/' + code.id, code, null, 'PUT');
        },

        actions: {
            invalidate(code) {
                this.invalidate(code);
            }
        }
    }
);
