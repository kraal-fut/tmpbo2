import Ember from 'ember';

export default Ember.Controller.extend({
    mailBody: Ember.computed('model.mail_body', 'model.hasEmptyMailBody', 'session.userSettings.emailSignature', {
        get: function () {
            if (this.get('model.hasEmptyMailBody')) {
                return this.get('session.userSettings.emailSignature');
            }
            return this.get('model.mail_body');
        },
        set: function (key, value) {
            this.set('email_answer', value);
            return value;
        }
    })
});
