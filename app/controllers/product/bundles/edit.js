import Ember from 'ember';

export default Ember.Controller.extend({
    discountTypes: ['euro', 'percentage'],
    categories: [],
    vendors: [],

    init() {
        this._super();
        Ember.$('body').on('paste', '.pastable', Ember.$.proxy(this.pasteItems, this));
    },

    pasteItems(event) {
        event = event.originalEvent;
        var $target = Ember.$(event.target);
        var relation = $target.parents('tbody').data('relation');

        var pastedText;

        if (window.clipboardData && window.clipboardData.getData) { // IE
            pastedText = window.clipboardData.getData('Text');
        } else if (event.clipboardData && event.clipboardData.getData) {
            pastedText = event.clipboardData.getData('text/plain');
        }

        var res = pastedText.split(/\s*[\s,]\s*/);

        var value = Ember.$.trim(res.shift());

        if (value !== '') {
            $target.val(value);
        }

        var key = $target.attr('name');

        _.each(res, function (item) {
            if (item !== '') {
                var model = {};
                model[key] = item;
                this.get('model.' + relation).pushObject(model);
            }
        }, this);

        return false;
    },

    getCategories(controller) {
        this.futRest.loadData('/frontofficemenus/subs').then(function (categories) {
            return categories;
        }).then(function (categories) {
            controller.get('categories').pushObjects(categories.frontoffice_menu_pseudo);
        });
    },

    getVendors(controller) {
        this.futRest.loadData('/vendors', null, {
            limit: 1000,
            sort: JSON.stringify({"name": "ASC"})
        }).then(function (vendors) {
            return vendors;
        }).then(function (vendors) {
            controller.get('vendors').pushObjects(vendors.vendor);
        });
    }
});
