import Ember from 'ember';
import Paginated from 'bo2/mixins/paginated';

export default Ember.Controller.extend(
    Paginated,
    {
        queryParams: ['q'],
        q: null,

        init() {
            this._super();
            this.get('searchSvc').setHandler(this);
        },

        search(query) {
            this.transitionToRoute('product.bundles.index', {queryParams: {q: query}});
        },

        unsetSearchHandler() {
            this.get('searchSvc').unsetHandler();
        }
    });
