import Ember from 'ember';

export default Ember.Controller.extend({

    modalValue: Ember.computed('fieldName', 'model', {
        get() {
            return this.model.get(this.get('fieldName'));
        },
        set(key, value) {
            this.model.set(this.get('fieldName'), value);
            return value;
        }
    })
});
