import Ember from 'ember';

export default Ember.Controller.extend({
    modalValue: Ember.computed('fieldName', 'model', {
        get() {
            return this.model.get(this.get('fieldName'));
        },
        set(key, value) {
            this.model.set(this.get('fieldName'), value);
            return value;
        }
    }),

    removeImage(model, data, filename) {
        model.get('Image').forEach( (item) => {
            if (item.filename === filename) {
                item.setProperties(data);
            }
        });
    },

    actions: {
        deleteImage(model, image) {
            if (!confirm('Are you sure you want to remove this picture?')) {
                return false;
            }
            if (typeof image.RawData !== 'undefined') {
                return false; // Don't know the data
            }

            var filename = image.filename;
            var data = {
                filename: filename,
                prodlevid: image.prodlevid,
                removed: true
            };

            if (!model.get('oldSituation') && confirm('Do you want to remove it from the disk?')) {
                data.removedFromDisk = true;
            }

            this.removeImage(model, data, filename);
        }
    }
});
