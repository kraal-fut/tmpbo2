import Ember from 'ember';
import OrderModel from 'bo2/models/procurement/order';
const { alias } = Ember.computed;

export default Ember.Controller.extend({
    firstRow: alias('model.firstObject'),

    totalValue: Ember.computed('model.@each.pprice', 'model.@each.advice', function () {
        return this.get('model').reduce((memo, line) => {
            return memo + ( line.pprice * line.advice );
        }, 0);
    }),

    selectedValue: Ember.computed('selecteds.@each.pprice', 'selecteds.@each.advice', function () {
        return this.get('selecteds').reduce((memo, line) => {
            return memo + ( line.pprice * line.advice );
        }, 0);
    }),

    totalAdvice: Ember.computed('model.@each.advice', function () {
        return this.get('model').reduce((memo, line) => {
            return memo + line.advice;
        }, 0);
    }),

    selectedAdvice: Ember.computed('selecteds.@each.advice', function () {
        return this.get('selecteds').reduce((memo, line) => {
            return memo + line.advice;
        }, 0);
    }),

    supplier: alias('firstRow.supplier.name'),
    supplier_id: alias('firstRow.supplier.id'),

    selecteds: Ember.computed.filterBy('model', 'selected', true),
    selectedCount: alias('selecteds.length'),
    hasSelecteds: Ember.computed.bool('selecteds.length'),
    noSelecteds: Ember.computed.not('hasSelecteds'),

    createOrder(supplierId, lines) {
        return new Ember.RSVP.Promise((resolve, reject) => {
            if (!lines) {
                reject(new Error('nothing selected'));
                return;
            }

            resolve(OrderModel.create({
                supplier_id: supplierId,
                lines: lines.map(line => {
                    let data = line.getProperties(['prodlevid', 'prodid', 'pprice', 'vendor', 'specification']);
                    data['qty'] = line.get('advice');
                    data['metadata'] = line.getProperties(['pr', 'freestock', 'min', 'max']);
                    return data;
                })
            }));
        });
    },
    actions: {
        selectAll() {
            this.get('model').setEach('selected', !this.get('hasSelecteds'));
        }
    }
});
