import Ember from 'ember';

let sum = (count, sum) => {
    return sum + count;
};

export default Ember.Controller.extend({
    NumberOfSuppliers: Ember.computed.alias('model.length'),

    TotalProductCount: Ember.computed('model.@each.productcount', function() {
        return this.get('model').getEach('productcount').reduce(sum, 0);
    }),

    totalAdviceValue: Ember.computed('model.@each.pprice', function() {
        return this.get('model').getEach('pprice').reduce(sum, 0.0);
    }),

    selecteds: Ember.computed.filterBy('model', 'selected', true),
    selectedCount: Ember.computed.alias('selecteds.length'),

    hasSelecteds: Ember.computed.bool('selecteds.length'),
    noSelecteds: Ember.computed.not('hasSelecteds'),

    selectAll() {
        this.get('model').setEach('selected', !this.get('hasSelecteds'));
    },

    createDrafts() {
        let selecteds = this.get('selecteds').getEach('id');
        return this.futRest.store('/procurement/orders', {'fromfullsupplieradvices': selecteds}, 'POST');
    },

    actions: {
        selectAll() {
            this.selectAll();
        },

        createDrafts() {
            this.createDrafts().then(()=> {
                this.get('target.router').refresh();
            });
        }
    }
});
