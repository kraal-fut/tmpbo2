import Ember from 'ember';
import Product from 'bo2/models/product/product';

export default Ember.Controller.extend({
    allowedStocks: Ember.A([]),
    mailOpen: Ember.computed.equal('model.status', 'draft'),
    mailBody: Ember.computed('model.mail_body', 'model.hasEmptyMailBody', 'session.userSettings.emailSignature', function () {
        if (this.get('model.hasEmptyMailBody')) {
            this.set('model.mail_body', this.get('session.userSettings.emailSignature'));
        }
        return this.get('model.mail_body');
    }),

    selecteds: Ember.computed.filterBy('model.lines', 'selected', true),
    selectedCount: Ember.computed.alias('selecteds.length'),
    hasSelecteds: Ember.computed.bool('selecteds.length'),

    getTotal: Ember.computed('model.totalValue', 'model.total', 'model.lines', function () {
        if (this.get('model.lines')) {
            return this.get('model.totalValue');
        }
        return this.get('model.total');
    }),

    retreiveStockData(futRest, prodlevid) {
        return futRest.load('/products/' + prodlevid, {"fields['products']": 'ProdLevId,Stock'}, 'products')
            .then(response => response.Stock);
    },

    actions: {
        requestDelete(line) {
            if (line.get('wantsToDelete')) {
                line.toggleProperty('wantsToDelete');
                return;
            }

            line.set('stockLoading', true);

            this.retreiveStockData(this.futRest, line.get('prodlevid')).then(stock => {
                line.set('currentStock', stock);
                line.toggleProperty('wantsToDelete');
            }).catch(e => {
                this.notifier.notify('Could not retreive current stock of ' + line.get('prodlevid'), true);
                console.error(e);
            }).finally(()=> {
                line.set('stockLoading', false);
            });
        },

        performDelete(line) {
            let p = Product.create({prodlevid: line.get('prodlevid'), Stock: line.get('currentStock')});

            p.updateStock(this.futRest).then(()=> {
                this.notifier.notify('Stock of ' + line.get('prodlevid') + ' is updated');
                this.get('model.lines').removeObject(line);
            });
        },

        selectAll() {
            this.get('model.lines').setEach('selected', !this.get('hasSelecteds'));
        },

        deleteSelected() {
            let selected = this.get('selecteds');

            if (confirm('Delete all ' + selected.get('length') + ' selected lines?')) {
                this.get('model.lines').removeObjects(selected);
            }
        }
    }
});
