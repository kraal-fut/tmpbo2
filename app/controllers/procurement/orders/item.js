import Ember from 'ember';

export default Ember.Controller.extend({
    getSearched: Ember.computed('parentController.q', function () {
        let q = this.get('parentController.q');
        let withProdLevId = this.get('model.lines').filterBy('prodlevid', q);
        let withProdId = this.get('model.lines').filterBy('prodid', q);
        return withProdLevId.concat(withProdId);
    }),

    isSearched: Ember.computed('getSearched', function () {
        return !Ember.isEmpty(this.get('getSearched'));
    })
});
