import Ember from 'ember';

export default Ember.Controller.extend({
    queryParams: ['q', 'status'],
    q: null,
    status: 'allopen',
    itemController: 'procurement/orders/item',
    searchPlaceholder: 'Find Procurement order',

    filters: [
        {id: 'allopen', label: 'All Open'},
        {id: 'all', label: 'All'},
        {id: 'draft', label: 'Drafts'},
        {id: 'sent', label: 'Sent'},
        {id: 'received', label: 'Received'},
        {id: 'closed', label: 'Closed'}
    ],

    variousChanged: function () {
        this.transitionToRoute('procurement.orders.index');
    }.observes('status'),

    actions: {
        selectAll() {
            this.setEach('model.selected', !this.isAny('model.selected', true));
        },

        closeOrder(order) {
            if (order.get('canBeClosed') || confirm('This order is not officially ready to be closed, proceed anyway?')) {
                order.set('status', 'closed');
                return true;
            }
            return false;
        }
    }
});
