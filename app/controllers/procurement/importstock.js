import Ember from 'ember';
import Product from 'bo2/models/product/product';

export default Ember.Controller.extend({
    columnMap: {'0': 'prodlevid', '1': 'stock'},
    imports: Ember.A([]),
    allowedstocks: Ember.A([]),
    fieldDelim: '\t',

    performImport() {
        let toSave = this.imports.filterBy('isAllowed', true);

        let promises = toSave.map(prod => {
            return prod.updateStock(this.futRest).then(()=> {
                this.imports.removeObject(prod);
                return prod;
            });
        });

        return Ember.RSVP.allSettled(promises);
    },

    processImporteds(importeds) {
        this.imports.pushObjects(importeds.map(prod => Product.create(prod)));

        this.imports.forEach(prod => {
            prod.set('isAllowed', this.get('allowedstocks').contains(prod.get('stock')));
        });
    },

    retreiveStockData() {
        let prodlevs = this.imports.mapBy('prodlevid').join(',');
        return this.futRest.load('/products/' + prodlevs, {"fields['products']": 'ProdLevId,Stock'}, 'products');
    },

    actions: {
        imported(importeds) {
            this.processImporteds(importeds);

            this.retreiveStockData().then(prods => {
                prods.forEach(prod => {
                    this.imports.findBy('prodlevid', prod.ProdLevId).set('currentStock', prod.Stock);
                });
            });
        },

        saveStocks() {
            this.performImport().then(()=> {
                this.notifications.addNotification({
                    message: 'Stocks imported successfully!',
                    type: 'success',
                    autoClear: true,
                    clearDuration: 1200
                });
            });
        }
    }
});
