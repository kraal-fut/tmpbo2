import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';
var fR;

export default Ember.Object.extend(JsonableMixin, {
    orders: undefined,
    order: undefined,
    customerid: undefined,
    date: undefined,
    amount: undefined,
    description: undefined,
    valid: 0,
    process: false,
    bankaccount: '',
    bic: '',
    name: '',
    city: 'unknown',
    checkPayment: Ember.computed.equal('paymenttype', 'D'),

    shouldSendMail: Ember.computed('pin', 'creditcard', 'american', function () {
        return !_.isEmpty(this.get('pin')) || !_.isEmpty(this.get('creditcard')) || !_.isEmpty(this.get('american'));
    }),

    updateOrder(futRest) {
        fR = futRest;
        var data = {
            id: this.get('ordernumber'),
            oldstatus: this.get('status'),
            newstatus: this.get('newstatus'),
            bankaccount: this.get('bankaccount'),
            bic: this.get('bic'),
            City: this.get('city'),
            name: this.get('name')
        };

        return fR
            .storeData('/orders/' + this.get('ordernumber') + '/status?action=processpayment', data, null, 'PUT')
            .then(this.createIncident.bind(this), this.handleError.bind(this));
    },

    createIncident() {
        var data = {
            Ordernumber: this.get('ordernumber'),
            RegId: this.get('customerid'),
            Text: this.get('text')
        };
        return fR
            .storeData('/orders/' + this.get('ordernumber') + '/incidents', data, null, 'POST')
            .then(() => {
                this.set('failed', false);
                return this;
            }, this.handleError.bind(this));
    },

    getByBarcode(barcode) {
        fR.loadData('/paymentorders/barcode/' + barcode).then((ordernumber) => {
            this.set('order', ordernumber);
        });
    },

    handleError(error) {
        var message = error.responseText;

        if (!_.isUndefined(error.responseJSON.error.message)) {
            message = error.responseJSON.error.message;
        }

        this.set('error', message);
        this.set('valid', false);
        return this;
    }
});
