import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';

export default Ember.Object.extend(JsonableMixin, {
    parseModelLine(lines) {
        this.getTracknoDateAndName(lines);

        this.getOrderNoAndColli(lines);

        this.getShipcodeAndDetail(lines);

        this.getInsuranceAndAddress(lines);

        this.getWeightRembAndZip(lines);

        this.getSupplierOrCustomerShipment();
    },

    getTracknoDateAndName(lines) {
        var result = lines[0].match(/(3STQOV[0-9]{7,9})\s+([0-9]{1,2}-[0-9]{2}-[0-9]{4})\s+(.*)\s+/);
        this.setProperties({
            trackno: result[1].replace(/\s{2,}/g, ' '),
            date: result[2].replace(/\s{2,}/g, ' '),
            name: result[3].replace(/\s{2,}/g, ' ')
        });
    },

    getOrderNoAndColli(lines) {
        var result = lines[1].match(/[\w\s]+\s+([0-9]+?x?)\s+([0-9]+)/i);
        if (_.isNull(result)) {
            result = ['', '', ''];
        }
        this.setProperties({
            orderno: result[1].replace(/\s{2,}/g, ' ').replace('x', ''),
            colli: result[2].replace(/\s{2,}/g, ' ')
        });
    },
    getShipcodeAndDetail(lines) {
        var result = lines[2].split(/\s-\s/);
        this.setProperties({
            shipcode: result[0].replace(/\s+/m, ''),
            shipdetail: result[1].replace(/\s{2,}/g, ' ')
        });
    },

    getInsuranceAndAddress(lines) {
        var insuranceamount;
        var address;
        if (lines[3].match(/EUR/)) {
            var result = lines[3].match(/EUR ([0-9\.]+,[0-9]+)\s+(.+)/);
            insuranceamount = result[1];
            address = result[2];
        } else {
            insuranceamount = '';
            address = lines[3];
        }
        this.setProperties({
            insuranceamount: insuranceamount.replace(/\s{2,}/g, ' '),
            address: address.replace(/\s{2,}/g, ' ')
        });
    },

    getWeightRembAndZip(lines) {
        var weight;
        var rembamount;
        var zipetc;
        var result;
        if (lines[4].match(/EUR [0-9]+,[0-9]+/)) {
            result = lines[4].match(/([0-9]+,[0-9]+)\s+EUR ([0-9]+,[0-9]+)\s+(.+)/);
            weight = result[1];
            rembamount = result[2];
            zipetc = result[3];
        } else {
            result = lines[4].match(/([0-9]+,[0-9]{3,})\s+(.+)/);
            weight = result[1];
            rembamount = '';
            if (lines[5].match(/EUR [0-9]+,[0-9]+/)) {
                rembamount = lines[5].match(/EUR [0-9]+,[0-9]+/)[0].replace(/\s*EUR\s*/, '');
            }
            zipetc = result[2];
        }

        this.setProperties({
            weight: weight.replace(/\s{2,}/g, ' '),
            rembamount: rembamount.replace(/\s{2,}/g, ' '),
            zipetc: zipetc.replace(/\s{2,}/g, ' ')
        });
    },

    getSupplierOrCustomerShipment() {
        var receiver = 'S';
        var order = false;
        if (this.get('orderno')) {
            receiver = 'C';
            order = true;
        }
        this.setProperties({receiver: receiver, order: order});
    },

    updateOrder(errors, futRest) {
        var ordernumber = this.get('orderno');

        if (_.isUndefined(ordernumber)) {
            return false;
        }

        var url = '/itorders/' + ordernumber + 'x/import';
        var data = {
            'barcode': this.get('trackno'),
            'oldStatus': 12,
            'newStatus': 60,
            'action': 'itpakket'
        };

        return this.sendPostData(url, data, errors, futRest);
    },

    sendPostData(url, data, errors, futRest) {
        var that = this;

        return futRest.store(url, data, 'POST').catch(function (response) {
            var message = response.responseText;

            if (!_.isUndefined(response.responseJSON.error) && !_.isUndefined(response.responseJSON.error.message)) {
                message = response.responseJSON.error.message;
            }
            errors.pushObject(that.get('orderno') + 'x: ' + message);
        });
    }
});
