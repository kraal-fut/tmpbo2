import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';

var statuses = {
    0: 'Incomplete',
    10: 'Complete',
    11: 'Waiting for receiving',
    12: 'Received',
    9999: 'Archived'
};

export default Ember.Object.extend(JsonableMixin, {
    statusText: Ember.computed('status', function () {
        return statuses[this.get('status')];
    }),

    isReceived: Ember.computed.gte('status', 12)
});
