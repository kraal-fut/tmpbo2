import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';

export default Ember.Object.extend(JsonableMixin, {});
