import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';

export default Ember.Object.extend(JsonableMixin, {
    received: Ember.Object.create({}),
    ourData: Ember.Object.create({}),
    regDate: Ember.computed('received.registrationDate', function () {
        return moment(this.get('received.registrationDate')).format('DD-MM-YYYY HH:mm');
    }),

    orderAmount: Ember.computed.alias('received.orderAmount'),
    total: Ember.computed.alias('ourData.total'),

    safe: Ember.computed('orderAmount', 'total', function () {
        return this.get('orderAmount') === this.get('total');
    }),

    warning: Ember.computed.not('safe'),

    danger: Ember.computed('ourData.status', function () {
        return this.get('ourData.status') !== 60;
    }),

    label: Ember.computed('safe', 'warning', 'danger', function () {
        if (this.get('danger')) {
            return 'label-danger';
        }
        if (this.get('warning')) {
            return 'label-warning';
        }
        if (this.get('safe')) {
            return 'label-success';
        }
    })
});
