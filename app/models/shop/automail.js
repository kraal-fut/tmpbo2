import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';
import ValidatableMixin from 'bo2/mixins/validatable';

export default Ember.Object.extend(JsonableMixin, ValidatableMixin, {
    validations: {
        subject: {required: true, min: 5},
        body: {required: true, min: 20},
        fromwho: {required: true, email: true},
        reply_to: {required: true, email: true}
    }
});
