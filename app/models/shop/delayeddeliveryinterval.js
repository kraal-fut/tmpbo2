import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';
import ValidatableMixin from 'bo2/mixins/validatable';

export default Ember.Object.extend(JsonableMixin, ValidatableMixin, {
    validations: {
        time_start: {required: true},
        time_end: {required: true},
        shipment_code: {required: true}
    }
});
