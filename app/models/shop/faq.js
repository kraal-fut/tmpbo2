import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';

export default Ember.Object.extend(JsonableMixin, {
    answer: '',
    category: {title:"", queue:"", image:""},

    categoryObserver: Ember.observer('category.title', function() {
        this.set('category.queue', '');
        this.set('category.image', '');
    })
});
