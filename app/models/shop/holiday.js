import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';

export default Ember.Object.extend(JsonableMixin, {
    defaultKeys: {
        type: 'holiday',
        texts: {
            instock: '',
            bikeinstock: '',
            title: '',
            toppanel: ''
        }
    },

    setDefaults() {
        this.setProperties(this.defaultKeys);
        return this;
    }
});
