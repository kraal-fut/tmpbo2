import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';
import ValidatableMixin from 'bo2/mixins/validatable';

export default Ember.Object.extend(JsonableMixin, ValidatableMixin, {
    validations: {
        'type': {required: true},
        'name': {required: true},
        'queue': {required: true},
        'costs': {required: true}
    }
});