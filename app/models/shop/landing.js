import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';
import ENV from 'bo2/config/environment';

export default Ember.Object.extend(JsonableMixin, {
    headerChanged: Ember.observer('header', function() {
        this.set('footer', this.get('header'));
    }),

    type: Ember.computed('themepage', 'vendorpage', {
        get() {
            if (this.get('themepage')) {
                return 'themepage';
            }
            if (this.get('vendorpage')) {
                return 'vendorpage';
            }
            return 'landingpage';
        },

        set(key, value) {
            if (value === 'themepage') {
                this.set('themepage', true);
                this.set('vendorpage', false);
            }
            if (value === 'vendorpage') {
                this.set('themepage', false);
                this.set('vendorpage', true);
            }
            if (value === 'landingpage') {
                this.set('themepage', false);
                this.set('vendorpage', false);
            }
            return value;
        }
    }),

    previewLink: Ember.computed('type', function () {
        let type;
        let specifier;

        if (this.get('type') === 'themepage') {
            type = 'theme';
            specifier = '&element=' + this.get('name');
        } else if (this.get('type') === 'vendorpage') {
            type = 'vendor';
            specifier = '&name=' + this.get('name');
        } else {
            type = 'html';
            specifier = '/' + this.get('_id');
        }

        return ENV.APP.fo2url + '?action=' + type + specifier + '&preview=true';
    })
});
