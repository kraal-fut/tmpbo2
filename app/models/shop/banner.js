import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';

export default Ember.Object.extend(JsonableMixin, {
    defaultKeys: {
        category: '',
        showOn: [{
            id: 0,
            type: 0,
            position: 0
        }]
    },

    setDefaults() {
        this.setProperties(this.defaultKeys);
        return this;
    }
});
