import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';
import ProductImagesMixin from 'bo2/mixins/productimages';

export default Ember.Object.extend(JsonableMixin, ProductImagesMixin, {
    Images: Ember.computed('Image', function () {
        return this.getImages(this.get('Image')).sortBy('id');
    }),

    updateImages() {
        this.set('Image', this.get('Images').sortBy('id').map((image)=>{
            return Ember.Object.create({
                prodlevid: image.get('prodlevid'),
                filename: image.get('filename')
            });
        }));
        return this;
    },

    variations: Ember.computed.alias('relatedProducts.variations'),
    hasVariations: Ember.computed.bool('variations.length')
});
