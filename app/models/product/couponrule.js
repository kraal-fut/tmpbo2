import Ember from 'ember';
import Jsonable from 'bo2/mixins/jsonable';

const couponRuleArguments = function(type) {
    const Arguments = {
        0: undefined,
        1: undefined,
        2: {
            options: [
                {label: 'Procent', value: 'procent'},
                {label: 'Euro', value: 'euro'}
            ]
        },
        4: {label: 'Euro'},
        8: {label: 'Euro'},
        128: {buttons: true},
        256: undefined,
        512: undefined,
        1024: {input: true, label: 'Quantity'}
    };

    return Arguments[type];
};

export default Ember.Object.extend(Jsonable, {
    validations: {

    },

    type: Ember.computed('coupon_rule_type', function () {
        let couponRuleType = this.get('coupon_rule_type');
        if (Ember.isNone(couponRuleType)) {
            return '';
        }

        let type = this.get('couponRuleTypes').filter(obj => {
            return obj.value === parseInt(couponRuleType);
        });

        if (Ember.isEmpty(type)) {
            return '';
        }
        return _.first(type).label;
    }),

    couponRuleArgument: Ember.computed('coupon_rule_type', function () {
        return couponRuleArguments(this.get('coupon_rule_type'));
    }),

    hasArguments: Ember.computed('couponRuleArgument', function () {
        return !Ember.isNone(this.get('couponRuleArgument'));
    }),

    isSelectableArgument: Ember.computed('couponRuleArgument', function () {
        let arg = this.get('couponRuleArgument');
        return !!(_.isObject(arg) && arg.options);
    }),

    selectOptions: Ember.computed('isSelectableArgument', function () {
        if (!this.get('isSelectableArgument')) {
            return [];
        }
        return this.get('couponRuleArgument.options');
    }),

    isInputArgument: Ember.computed('couponRuleArgument', function () {
        return !!this.get('couponRuleArgument.input');
    }),

    inputLabel: Ember.computed('couponRuleArgument.label', function () {
        if (!this.get('couponRuleArgument.label')) {
            return '';
        }
        return this.get('couponRuleArgument.label');

    }),

    hasButtons: Ember.computed('coupon_rule_type', 'couponRuleArgument', function () {
        let optional = this.get('couponRuleArgument');
        return _.isObject(optional) && !Ember.isEmpty(optional.buttons);
    }),

    isMultipleCodes: Ember.computed('coupon_rule_type', function () {
        return this.get('coupon_rule_type') === 128;
    }),

    ruleTypeChanged: Ember.observer('coupon_rule_type', function () {
        if (this.get('isMultipleCodes') && Ember.isEmpty(this.get('optional_arguments'))) {
            this.set('optional_arguments', Ember.Object.create({}));
        } else {
            this.set('optional_arguments', undefined);
        }
    }),

    couponRuleTypes: Ember.computed(function () {
        return [
            {label: 'Code', value: 1},
            {label: 'Discount', value: 2},
            {label: 'Minimum order', value: 4},
            {label: 'Maximum order', value: 8},
            {label: 'Multiple Codes', value: 128},
            {label: 'Max code usage', value: 256},
            {label: 'Vendor', value: 512},
            {label: 'Free product', value: 1024}
        ];
    })
});
