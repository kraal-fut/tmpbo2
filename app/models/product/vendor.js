import Ember from 'ember';
import ValidatableMixin from 'bo2/mixins/validatable';

export default Ember.Object.extend(ValidatableMixin, {
    hasNoSeoText: Ember.computed.empty('info'),
    hasSeoText: Ember.computed.not('hasNoSeoText')
});
