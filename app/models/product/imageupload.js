import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';

export default Ember.Object.extend(JsonableMixin, {
    images: [],
    isExtra: false,
    hasVariations: false,
    isMain: false,
    allVariations: false,

    notExtraButHasMain: Ember.computed('isExtra', 'images', 'images.[]', function () {
        return !this.get('isExtra') && _.reduce(this.get('images'), function (memo, model) {
                if (memo || parseInt(model.filename, 10) === 1) {
                    memo = true;
                }
                return memo;
            }, false);
    }),

    action: Ember.computed('invalid', 'isExtra', 'notExtraButHasMain', function () {
        if (this.get('invalid')) {
            return 'none';
        }

        var action = 'create';

        if (this.get('isExtra')) {
            action = 'add';
        } else if (this.get('notExtraButHasMain')) {
            action = 'overwrite';
        }

        return action;
    }),

    readFile() {
        var reader = new FileReader();

        var f = this.get('file');

        reader.onload = _.bind(function (e) {
            this.fileSetter(f, e, this, 'RawData');
        }, this);

        reader.readAsDataURL(f);

        return this;
    },

    setFileName(oldFile) {
        if (this.get('isExtra')) {
            oldFile++;
            if (oldFile === 1) {
                oldFile++;
            }
            this.set('filename', oldFile + '.' + this.get('fileExt'));
        } else {
            this.set('filename', '1.' + this.get('fileExt'));
        }

        return oldFile;
    },

    fileSetter(file, event, context, fieldName) {
        this.set(fieldName, {
            ImageFilename: file.name,
            Image: event.target.result
        });
    },

    setVariationInfo(related) {
        var hasVariations = false;
        if (!_.isUndefined(related)) {
            hasVariations = !!related.variations;
        }

        var isMain = true;
        if (hasVariations && related.main !== this.get('prodlevid')) {
            isMain = false;
        }

        this.set('hasVariations', hasVariations);
        this.set('isMain', isMain);
        this.set('allVariations', isMain);
    }
});
