import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';

export default Ember.Object.extend(JsonableMixin, {
    stock: Ember.Object.create({}),

    updateStock(futRest) {
        let url = '/products/' + this.get('prodlevid');
        return futRest.store(url, {stock: {stock: this.get('Stock')}}, 'PUT');
    }

});
