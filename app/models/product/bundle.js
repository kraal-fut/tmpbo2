import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';
import ValidatableMixin from 'bo2/mixins/validatable';

export default Ember.Object.extend(JsonableMixin, ValidatableMixin, {
    validations: {
        name: {required: true, min: 10},
        discount: {required: true},
        discountType: {required: true}
    }
});
