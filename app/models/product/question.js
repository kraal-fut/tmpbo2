import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';
import ValidatableMixin from 'bo2/mixins/validatable';

export default Ember.Object.extend(ValidatableMixin, {
    hasEmptyMailBody: Ember.computed.empty('email_answer'),
    setData: function() {
        this.set('AnswerDateTime', moment().format('YYYY-MM-DD HH:mm:ss'));
        this.set('is_answered', true);
        if(this.get('email_input')) {
            this.set('answer_type', 1);
        }
    },
    checkEmailInput: function() {
        if (this.get('email_input') && Ember.isEmpty(this.get('user_email'))) {
            alert('Please enter an e-mail address');
            return false;
        }
    }
});
