import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';
import ValidatableMixin from 'bo2/mixins/validatable';

export default Ember.Object.extend(JsonableMixin, ValidatableMixin, {
    validations: {
        name: {required: true, min: 3},
        active_start_date: {pattern: /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/, niceName: 'Active from'},
        active_end_date: {pattern: /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/, niceName: 'Active until'}
    },
    totalCodes: Ember.computed.sum('ActiveCodeCount', 'UsedCodeCount'),

    hasMultipleCodes: Ember.computed('coupon_rule.@each.isMultipleCodes', function () {
        if (!this.get('coupon_rule')) {
            return false;
        }
        return this.get('coupon_rule').isAny('isMultipleCodes', true);
    })
});
