import Ember from 'ember';

export default Ember.Object.extend({
    prevQty: null,

    setPreviousQty: function () {
        this.set('prevQty', this.get('qty'));
    }.on('init'),

    isChanged: Ember.computed('qty', 'prevQty', function () {
        return parseInt(this.get('qty'), 10) !== parseInt(this.get('prevQty'), 10);
    }),

    wantsToDelete: false
});
