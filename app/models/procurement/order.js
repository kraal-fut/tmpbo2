import Ember from 'ember';
import JsonableMixin from 'bo2/mixins/jsonable';
import ValidatableMixin from 'bo2/mixins/validatable';

export default Ember.Object.extend(JsonableMixin, ValidatableMixin, {
    lines: [],

    getSearched: Ember.computed('parentController.q', function () {
        let q = this.get('controller');
        let withProdLevId = this.get('lines').filterBy('prodlevid', q);
        let withProdId = this.get('lines').filterBy('prodid', q);
        return withProdLevId.concat(withProdId);
    }),

    isSearched: Ember.computed('getSearched', function () {
        return !Ember.isEmpty(this.get('getSearched'));
    }),

    validations: {
        'mail_to': {required: true},
        'mail_subject': {required: true, min: 5},
        'mail_body': {required: true, min: 20}
    },

    canBeClosed: Ember.computed.equal('status', 'received'),
    isClosed: Ember.computed.equal('status', 'closed'),
    isNotClosed: Ember.computed.not('isClosed'),
    isSent: Ember.computed.equal('status', 'sent'),
    isReceived: Ember.computed.equal('status', 'received'),
    isSendOrReceived: Ember.computed.or('isSent', 'isReceived'),
    isNotSent: Ember.computed.not('isSent'),
    hasEmptyMailBody: Ember.computed.empty('mail_body'),

    totalValue: Ember.computed('lines.@each.pprice', 'lines.@each.qty', function () {
        return this.get('lines').reduce(function (memo, line) {
            return parseInt(memo, 10) + parseInt(line.pprice * line.qty, 10);
        }, 0);
    }),

    allQty: Ember.computed('lines.@each.qty', 'totalQty', function () {
        return this.get('lines').getEach('qty') || this.get(' totalQty');
    }),

    totalQty: Ember.computed('lines.@each.qty', function () {
        return this.get('lines').reduce(function (memo, line) {
            return parseInt(memo, 10) + parseInt(line.qty, 10);
        }, 0);
    }),

    allScanned: Ember.computed('lines.@each.scanned_qty', function () {
        return this.get('lines').getEach('scanned_qty');
    }),

    setAsNew: function () {
        if (this.get('status') === 'draft') {
            this.set('status', 'new');
        }
        return this;
    }
});
