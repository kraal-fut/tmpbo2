import Ember from 'ember';
import DS from 'ember-data';
import ENV from 'bo2/config/environment';

const {attr, hasMany} = DS;

export default DS.Model.extend({
    prodlevid: attr('string'),
    vendor: attr('string'),
    name: attr('string'),
    euprice: attr('number'),
    description: attr('string'),
    multiImages: hasMany('product/image', {async: false}),

    hasImage: Ember.computed.bool('mainImage'),

    mainImage: Ember.computed('image', 'multiImages.[]', function () {
        if (!this.get('multiImages.length')) {
            return false;
        }

        let firstImage = this.get('multiImages.firstObject');
        let filename = firstImage.get('filename');
        let prodlevid = firstImage.get('prodlevid');

        return ENV.APP.fo2url + `/img/products/${prodlevid}/medium/${filename}`;
    })
});
