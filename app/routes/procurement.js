import Ember from 'ember';
import CanSearchMixin from 'bo2/mixins/cansearch';

export default Ember.Route.extend(
    CanSearchMixin,
    {
        searchPlaceholder: 'Find Procurement order'
    }
);
