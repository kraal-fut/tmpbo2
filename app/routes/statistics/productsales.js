import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';
import Loader from 'bo2/utils/statistics/ProductDataLoader';

export default FutEmberRoute.extend({
    session: Ember.inject.service('futsession'),

    model() {
        let index = this.get('session.settings.ELASTICSEARCHORDERINDEX') || 'futurum2_nl';
        let elUrl = this.get('session.settings.ELASTICSEARCHURL');

        let loader = Loader.create({index: index}).connect(elUrl);
        return loader.loadData();
    }
});
