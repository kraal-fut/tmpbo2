import Ember from 'ember';

import FutEmberRoute from 'bo2/routes/fut-ember-route';
import Loader from 'bo2/utils/statistics/loader';

export default FutEmberRoute.extend(
    {
        session: Ember.inject.service('futsession'),

        dataUrl(params) {
            return this._super(params, '/vendors/bundles');
        },

        setupController(controller, model) {
            this._super(controller, model);

            let index = this.get('session.settings.ELASTICSEARCHORDERINDEX') || 'futurum2_nl';
            let elUrl = this.get('session.settings.ELASTICSEARCHURL');

            this.loader = Loader.create({onlyCounts: true, index}).connect(elUrl);

            this.loader.getPopupularBundles().then(bundleData => {
                if (Ember.isEmpty(bundleData)) {
                    this.notifier.notify({text:'No valid bundle data found', status:'warning'}, true);
                    console.error('No valid bundle data found');
                    return [];
                }
                const parseBundles = (bundles) => {
                    if (Ember.isArray(bundles)) {
                        let addCounts = item => {
                            let countData = bundles.findBy('_id', item._id);
                            if (Ember.isNone(countData)) {
                                return false;
                            }
                            countData.count = item.count;
                            return countData;
                        };

                        controller.set('popularBundles', bundleData.map(addCounts).filter(Boolean));
                    } else {
                        return {count: 0};
                    }
                };
                let idPart = '/' + bundleData.mapBy('_id').join(',');
                return this.futRest
                    .loadData('/bundles' + idPart)
                    .then(parseBundles);
            });
        }
    }
);
