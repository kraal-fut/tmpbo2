import Ember from 'ember';

import FutEmberRoute from 'bo2/routes/fut-ember-route';
import Loader from 'bo2/utils/statistics/OrderDataLoader';

export default FutEmberRoute.extend({
    loader: null,
    session: Ember.inject.service('futsession'),

    setupController(controller, model) {
        this._super(controller, model);

        this.futRest.load('/coupons/list').then(coupons => {
            let ar = {};

            coupons.forEach(item => {
               ar[item.id] = item.name;
            });
            controller.set('coupons', ar);
        });
    },

    model() {
        let index = this.get('session.settings.ELASTICSEARCHORDERINDEX') || 'futurum2_nl';
        let elUrl = this.get('session.settings.ELASTICSEARCHURL');

        this.loader = Loader.create({index}).connect(elUrl);
        this.loader.loadData();
        return this.loader;
    }
});
