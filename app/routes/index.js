import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';

export default FutEmberRoute.extend({
    dataUrl(params) {
        let url = '/bo2daystats';

        return this._super(params, url);
    },

    parse(data) {
        return Ember.Object.create(data);
    }
});
