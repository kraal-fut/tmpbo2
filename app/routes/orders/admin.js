import FutEmberRoute from 'bo2/routes/fut-ember-route';

export default FutEmberRoute.extend({
    baseUrl: '/api/v3/admin/reports',

    model() {
        var maxDate = moment().format('DD-MM-YYYY');
        var to = moment().subtract(1, 'months').endOf('month');
        var from = moment().subtract(1, 'months').startOf('month');
        return [{
            'name': 'German orders',
            'from': from,
            'to': to,
            'maxDate': maxDate,
            'url': '/germanyorders'
        }, {
            'name': 'Belgium Creditcard & Paypal orders',
            'from': from,
            'to': to,
            'maxDate': maxDate,
            'url': '/belgiumcreditcards'
        }, {
            'name': 'Austria orders',
            'from': from,
            'to': to,
            'maxDate': maxDate,
            'url': '/austriaorders'
        }, {
            'name': 'Employee orders',
            'from': from,
            'to': to,
            'maxDate': maxDate,
            'url': '/employeeorders'
        }];
    },

    actions: {
        exportReport(model) {
            if (model.to.diff(model.from, 'months') > 3) {
                this.get('notifier').notify('You can not export more then 3 months at a time');
                return false;
            }
            var url = this.baseUrl + model.url + '.csv';
            var from = model.from.format('DD-MM-YYYY');
            var to = model.to.format('DD-MM-YYYY');
            window.location = url + '?from=' + from + '&to=' + to;
        }
    }
});
