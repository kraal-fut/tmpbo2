import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import SortableMixin from 'bo2/mixins/sortable';
import RetourModel from 'bo2/models/order/retour';

export default PageableRoute.extend(
    SortableMixin,
    FilterableMixin,
    {
        entityName: 'retours',

        map(order) {
            return RetourModel.create(order);
        },

        dataUrl(params) {

            if (params.find) {
                this.filters['ordernumber'] = parseInt(params.find, 10);
                this.serverQueryParams.find = JSON.stringify(this.filters);
            }

            return this._super(params, '/retours');
        },

        actions: {
            toggleItems(retour) {
                retour.toggleProperty('itemsVisible');
            },
            setReceived(retour) {
                if (retour.get('isReceived')) {
                    return;
                }

                retour.set('status', 12);

                return this.get('futRest').store(this.dataUrl() + '/' + retour.get('_id'), retour.getJSON(), 'PUT');
            }
        }
    }
);
