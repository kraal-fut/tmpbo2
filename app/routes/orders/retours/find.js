import FutEmberRoute from 'bo2/routes/fut-ember-route';

export default FutEmberRoute.extend({
        model(params) {
            this.transitionTo('orders.retours.index', {queryParams: {find: params.id}});
        }
    }
);
