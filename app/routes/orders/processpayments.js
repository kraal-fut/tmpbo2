import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';
import PaymentModel from 'bo2/models/order/processpayment';
import LineParser from 'bo2/utils/LineParser';

export default FutEmberRoute.extend({
        entityName: 'payments',

        model() {
            return Ember.A([]);
        },

        processPayments() {
            let accepteds = this.get('controller.ordersToProcess');

            return Ember.RSVP.allSettled(accepteds.map((accepted) => {
                return accepted.updateOrder(this.futRest);
            }));
        },

        ordersUpdated(orders) {
            orders.removeObjects(orders.filterBy('failed', false));

            if (this.get('controller.hasOrdersToProcess')) {
                this.ordersFailed();
            } else {
                this.get('notifier').notify('All selected orders are processed');
            }
        },

        ordersFailed() {
            this.get('notifier').notify('Not all selected orders are processed');
        },

        addLine(line, lines) {
            if (line === '' || line.match('Boekdatum')) {
                return false;
            }

            var m;
            var error = false;

            try {
                m = PaymentModel.create({line: line});
                lines.pushObject(m);
                return new LineParser(m, this.notifier).parseLine();
            } catch (e) {
                m.setProperties({description: line, valid: 0, process: false, error: e.message});
                error = true;
            }
        },

        actions: {
            imported(lines) {
                lines.forEach(line => {
                    this.addLine(line, this.currentModel);
                });
            },
            clear() {
                this.currentModel.clear();
            },
            processOrders(orders) {
                if (!this.get('controller.hasOrdersToProcess')) {
                    this.notifier.notify('No orders are selected to process', true);
                    return false;
                }

                this.processPayments(orders).then(() => {
                    this.ordersUpdated(orders);
                }).catch(() => {
                    this.ordersFailed();
                });
            }
        }
    }
);
