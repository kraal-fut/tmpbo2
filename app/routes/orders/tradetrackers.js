import Ember from 'ember';
import Pageable from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import TradetrackerModel from 'bo2/models/order/tradetracker';

export default Pageable.extend(
    FilterableMixin,
    {
        entityName: 'orders',

        map( model ) {
            return TradetrackerModel.create( model );
        },

        dataUrl( params ) {
            return this._super( params, '/tradetrackers' );
        },

        getChecked() {
            return this.currentModel.filterBy( 'selected', true );
        },

        sendApproveChecked() {
            var promises = [];

            this.getChecked().each( ( accepteds ) => {
                promises.push( accepteds.sendApprove() );
            } );

            return Ember.RSVP.all( promises );
        },

        /**
         *
         * @returns {*|!Promise.<!Array>}
         */
        sendModifyChecked() {
            var promises = [];

            this.getChecked().each( ( toModify ) => {
                promises.push( toModify.sendModify() );
            } );

            return Ember.RSVP.all( promises );
        },

        sendRejectChecked: function( rejectionreason ) {
            return new Ember.RSVP.Promise( ( resolve, reject ) => {
                if ( !rejectionreason ) {
                    reject( new Error( 'Should choose a rejection reason' ) );
                }

                var promises = [];

                this.getChecked().each( ( rejected ) => {
                    promises.push( rejected.sendReject( rejectionreason ) );
                } );

                resolve( Ember.RSVP.all( promises ) );
            } );
        },
        actions: {
            selectAll() {
                var hasSelected = this.currentModel.isAny( 'selected', true );
                this.currentModel.setEach( 'selected', !hasSelected );
            },

            accept( model ) {
                if ( model ) {
                    model.set( 'newStatus', 'accepted' );
                    return this.storeModel( model );
                }

                return this.sendApproveChecked();
            },
            modify( model ) {
                if ( model ) {

                    model.set( 'newStatus', 'accepted' );

                    if ( model.get( 'ourData' ) ) {
                        model.set( 'newAmount', model.get( 'ourData.total' ) );
                    }

                    return this.storeModel( model );
                }

                return this.sendModifyChecked();
            },
            reject( model ) {
                var rejectionReason = prompt( 'Reason for rejection' );

                if ( model ) {
                    model.set( 'newStatus', 'rejected' );
                    model.set( 'rejectReason', rejectionReason );

                    return this.storeModel( model );
                }

                return this.sendRejectChecked( rejectionReason );
            }
        }
    }
);
