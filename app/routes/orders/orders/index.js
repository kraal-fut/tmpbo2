import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import SortableMixin from 'bo2/mixins/sortable';
import OrderModel from 'bo2/models/order/order';

export default PageableRoute.extend(
    FilterableMixin,
    SortableMixin,
    {
        queryParams: {
            q: {refreshModel: true, scope: 'controller'}
        },

        entityName: 'orders',

        afterModel(orders) {
            if (orders.get('length') === 1) {
                this.transitionTo('orders.orders.edit', orders.get('firstObject'));
            }
        },

        map(order) {
            return OrderModel.create(order);
        },

        dataUrl(params) {
            if (params.q) {
                return this._super(params, '/orders/search?q=' + params.q);
            }
            return this._super(params, '/orders');
        },

        actions: {}
    }
);
