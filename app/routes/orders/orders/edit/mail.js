import Ember from 'ember';

export default Ember.Route.extend({
    model(params) {
        let modelObj = Ember.Object.create({
            order: this.modelFor('orders.orders.edit'),
            mail: Ember.Object.create({})
        });

        if (params.template) {
            return this.futRest
                .loadData('/automails', params.template, null, 'automail').then(mail => {
                    modelObj.set('mail', Ember.Object.create(mail));
                    modelObj.set('mail.towho', modelObj.get('order.DelEmail'));
                    return modelObj;
                }).catch(e => {
                    console.error('Exception while loading automail:', e);
                    return modelObj;
                });
        } else {
            return modelObj;
        }

    },
    redirectBack() {
        let backUrl = this.get('controller.backUrl');

        if (backUrl) {
            window.location = 'https://' + backUrl.replace(/.*?:\/\//g, "");
        } else {
            window.history.go(-1);
        }
    },
    actions: {
        send(mail, order, subject, body) {
            let ordernumber = order.get('Ordernumber');
            let data = {
                fromwho: mail.get('fromwho'),
                towho: mail.get('towho'),
                subject: subject,
                body: body
            };

            this.futRest.store('/orders/'+ ordernumber + '/mails', data, 'POST').then(()=>{
                this.redirectBack();
            });
        },
        cancel() {
            this.redirectBack();
        }
    }
});
