import FutEmberRoute from 'bo2/routes/fut-ember-route';
import OrderModel from 'bo2/models/order/order';

export default FutEmberRoute.extend({
    entityName: 'order',
    setupController(controller, model) {
        this._super(controller, model);

        this.futRest.loadData('/automails/list').then(automaildata => {
            controller.set('automails.content', automaildata);
        });
    },

    dataUrl(params) {
        return '/orders/' + params[this.idAttribute];
    },

    parse(data) {
        let d = this._super(data);
        return OrderModel.create(d);
    },

    actions: {
        makeMail(template) {
            this.transitionTo('orders.orders.edit.mail', {queryParams: {template: template}});
        }
    }
});
