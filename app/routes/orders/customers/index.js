import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import CustomerModel from 'bo2/models/order/customer';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'customer',

        map(model) {
            return CustomerModel.create(model);
        },

        dataUrl(params) {
            return this._super(params, '/customers');
        }
    }
);
