import FutEmberRoute from 'bo2/routes/fut-ember-route';
import Customer from 'bo2/models/order/customer';

export default FutEmberRoute.extend({
    dataUrl(params) {
        return '/customers/' + params[this.idAttribute];
    },

    parse(data) {
        return Customer.create(data);
    },

    actions: {}
});
