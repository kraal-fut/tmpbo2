import FutEmberRoute from 'bo2/routes/fut-ember-route';
import ItpakketModel from 'bo2/models/order/itpakket';

export default FutEmberRoute.extend({
    entityName: 'orders',
    reloadInterval: undefined,

    setupController(controller, model) {
        this._super(controller, model);
        this.reloadInterval = setInterval(this.reloadModel.bind(this), 10000);
    },

    resetController(controller, isExiting) {
        if (isExiting) {
            clearInterval(this.reloadInterval);
        }
    },

    reloadModel: function () {
        this.model().then(model => {
            this.set('controller.model', model);
        });
    },

    map(order) {
        return ItpakketModel.create(order);
    },

    dataUrl(params) {
        return this._super(params, '/itorders?find={"status":11}&limit=100');
    },

    actions: {
        importOrders(imports) {
            imports.filterBy('order', true).forEach(order => {
                order.updateOrder(this.get('controller.errors'), this.futRest).then(() => {
                    imports.removeObject(order);
                });
            });
        },
        exportOrders() {
            window.location = '/api/v3/itorders/export.txt?find={"status":11}&limit=100';
        },
        downloadLast() {
            window.location = '/api/v3/itorders/download';
        }
    }
});
