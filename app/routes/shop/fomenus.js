import FutEmberRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import FoMenuModel from 'bo2/models/shop/fomenu';

export default FutEmberRoute.extend(
    FilterableMixin,
    {
        entityName: 'frontoffice_menu_pseudo',

        serverQueryParams: {
            limit: 100,
            parent_id: {}
        },

        map(menu) {
            return FoMenuModel.create(menu);
        },

        dataUrl(params) {
            return this._super(params, '/frontofficemenus');
        },

        actions: {
            save(model) {
                this.storeModel(model);
            },
            getChildren(id) {
                this.send('filter', 'parent_id', {'eq': id});
            },
            editMenu(id) {
                this.transitionTo('shop.fomenu.edit', id);
            }
        }
    }
);
