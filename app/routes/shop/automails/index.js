import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import AutomailModel from 'bo2/models/shop/automail';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'automails',

        map(order) {
            return AutomailModel.create(order);
        },

        dataUrl(params) {
            return this._super(params, '/automails');
        },

        actions: {
            save(mail) {
                this.storeModel(mail);
            },
            add() {
                let m = AutomailModel.create();
                this.currentModel.unshiftObject(m);
                this.transitionTo('shop.automails.edit', m);
            }
        }
    }
);
