import FutEmberRoute from 'bo2/routes/fut-ember-route';
import AutomailModel from 'bo2/models/shop/automail';

export default FutEmberRoute.extend({
    entityName: 'automails',

    dataUrl(params) {
        var url = '/automails';

        if (params) {
            url += '/' + params['id'];
        }

        return this._super(params, url);
    },

    parse(data) {
        return AutomailModel.create(data);
    },

    actions: {
        save(mail) {
            mail.validate().then(()=> {
                this.storeModel(mail).then(()=> {
                    this.transitionTo('shop.automails.index');
                });
            }).catch(e => {
                console.error('The model is invalid', e);
            });
        },
        cancel() {
            this.transitionTo('shop.automails.index');
        }
    }
});
