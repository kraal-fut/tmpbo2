import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';
import ShippingMethod from 'bo2/models/shop/shippingmethod';
import DelayedDeliveryInterval from 'bo2/models/shop/delayeddeliveryinterval';

export default FutEmberRoute.extend( {
    entityName: 'shippingmethods',
    shippingMethodsUrl: '/shippingmethods',
    delayedDeliveryIntervalUrl: '/delayeddeliveryintervals',
    excludeDatesUrl: '/excludedates',

    model(params) {
        return Ember.RSVP.hash({
            shippingmethods: this._super(params),
            delayeddeliveryintervals: this.getDelayedDeliveryIntervals(),
            excludedates: this.getExcludeDates()
        });
    },

    getDelayedDeliveryIntervals() {
        return this.futRest
            .loadData(this.get('delayedDeliveryIntervalUrl'))
            .then(response => {
                var data = response.delayed_delivery_intervals;
                if (!Ember.isArray(data)) {
                    data = [data];
                }

                data = _.map(data, delayeddeliveryinterval => DelayedDeliveryInterval.create(delayeddeliveryinterval));

                return Ember.A(data);
            });
    },

    getExcludeDates() {
        return this.futRest
            .loadData(this.get('excludeDatesUrl'))
            .then(response => {
                var data = response.excludedates;
                if (!Ember.isArray(data)) {
                    data = [data];
                }
                data = _.map(data, (date, index) => {
                    return Ember.Object.create({id: index, date: date});
                });

                return Ember.A(data);
            });
    },

    map(order) {
        return ShippingMethod.create(order);
    },

    dataUrl(params) {
        var url = this.shippingMethodsUrl;
        return this._super(params, url);
    },

    actions: {
        addShippingmethod() {
            this.currentModel.shippingmethods.unshiftObject(ShippingMethod.create());
        },

        saveShippingMethod(model) {
            var url = this.shippingMethodsUrl;

            return model.validate({}).then(model => {
                this.storeModel(model, url);
            }).catch(e => {
                console.error('The model is invalid', e);
            });
        },

        deleteShippingmethod(model) {
            var url = this.shippingMethodsUrl;
            if (confirm('This will permanently delete this shippingmethod, are you sure?')) {
                this.deleteModel(model, url);
            }
        },

        addDelayedDeliveryInterval() {
            this.currentModel.delayeddeliveryintervals.unshiftObject(new DelayedDeliveryInterval());
        },

        saveDelayedDeliveryInterval(model) {
            var url = this.delayedDeliveryIntervalUrl;

            return model.validate({}).then(model => {
                this.storeModel(model, url);
            }).catch(e => {
                console.error('The model is invalid', e);
            });
        },

        deleteDelayedDeliveryInterval(model) {
            var url = this.delayedDeliveryIntervalUrl;
            if (confirm('This will permanently delete this delayed delivery interval, are you sure?')) {
                this.deleteModel(model, url);
            }
        },

        addExcludeDate() {
            this.currentModel.excludedates.unshiftObject(new Ember.Object({}));
        },

        saveExcludeDate(model) {
            var url = this.excludeDatesUrl;
            this.storeModel(model, url);
        },

        deleteExcludeDate(model) {
            let url = this.get('excludeDatesUrl') + '/' + model.get('date');
            this.futRest.deleteRequest(url).then(response => {
                this.refresh();
                return response;
            });
        }
    }
} );
