import Ember from 'ember';
import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import ArticleModel from 'bo2/models/shop/article';
import FoMenuModel from 'bo2/models/shop/fomenu';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'news_articles',

        model(params) {
            return Ember.RSVP.hash({
                articles: this._super(params),
                fomenu: this.getFomenu()
            });
        },

        map(article) {
            return ArticleModel.create(article);
        },

        dataUrl(params) {
            return this._super(params, '/newsarticles');
        },

        getFomenu() {
            return this.futRest
                .loadData('/frontofficemenus', undefined, {limit: 100})
                .then(response => {
                    let data = response.frontoffice_menu_pseudo;
                    if (!Ember.isArray(data)) {
                        data = [data];
                    }

                    data = _.map(data, menu => {
                        return FoMenuModel.create(menu);
                    });

                    return Ember.A(data);
                });
        },

        actions: {
            save(article) {
                this.storeModel(article);
            },
            add() {
                this.currentModel.articles.unshiftObject(ArticleModel.create({}));
            },
            getChildren(id, model) {
                var ids = [id];

                if (typeof model === 'undefined') {
                    this.send('removeFilter', 'category_id');
                    return;
                }

                _.map(model.children, child => {
                    ids.push(child.frontoffice_menu_id);
                }, ids);

                this.send('filter', 'category_id', {'in': ids});
            }
        }
    }
);
