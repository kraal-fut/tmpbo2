import FutEmberRoute from 'bo2/routes/fut-ember-route';
import ArticleModel from 'bo2/models/shop/article';
import Ember from 'ember';
import FoMenuModel from 'bo2/models/shop/fomenu';

export default FutEmberRoute.extend({
    entityName: 'frontoffice_menu_pseudo',

    getFomenu() {
        return this.futRest
            .loadData('/frontofficemenus', {limit: 100})
            .then(response => {
                let data = response.frontoffice_menu_pseudo;
                if (!Ember.isArray(data)) {
                    data = [data];
                }

                data = _.map(data, menu => {
                    return FoMenuModel.create(menu);
                });

                return Ember.A(data);
            });
    },

    setupController(controller, model) {
        this._super(controller, model);
        this.getFomenu().then(fomenu => {
            controller.set('fomenu', fomenu);
        });
    },

    parse(data) {
        return ArticleModel.create(data);
    },

    dataUrl(params) {
        let url = '/newsarticles';

        if (params) {
            url += '/' + params['id'];
        }

        return this._super(params, url);
    },

    actions: {
        save(model) {
            this.storeModel(model);
        },
        cancel() {
            this.transitionTo('shop.articles.index');
        }
    }
});
