import Ember from 'ember';
import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import HighlightCategoryModel from 'bo2/models/shop/highlightcategory';
import FoMenuModel from 'bo2/models/shop/fomenu';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'frontofficemenuhighlight',

        model(params) {
            return Ember.RSVP.hash({
                highlighted: this._super(params),
                fomenu: this.getFomenu()
            });
        },

        map(order) {
            return HighlightCategoryModel.create(order);
        },

        dataUrl(params) {
            return this._super(params, '/frontofficemenuhighlights');
        },

        getFomenu() {
            return this.futRest
                .loadData('/frontofficemenus', {limit: 100})
                .then(response => {
                    let data = response.frontoffice_menu_pseudo;
                    if (!Ember.isArray(data)) {
                        data = [data];
                    }

                    return Ember.A(_.map(data, menu => FoMenuModel.create(menu)));
                });
        },

        actions: {
            save(highlight) {
                this.storeModel(highlight);
            },
            deleteHighlight(highlight) {
                if (confirm('This will permanently delete this highlighted menu item, are you sure?')) {
                    this.deleteModel(highlight);
                }
            },
            addHighlight() {
                this.currentModel.unshiftObject(new HighlightCategoryModel({}));
            }
        }
    }
);
