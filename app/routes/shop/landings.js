import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import LandingModel from 'bo2/models/shop/landing';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'landings',
        idAttribute: '_id',

        map(landing) {
            return LandingModel.create(landing);
        },

        dataUrl(params) {
            return this._super(params, '/landings');
        },

        actions: {
            add() {
                this.currentModel.unshiftObject(LandingModel.create({publish: false, header: false, footer: false}));
            },

            del(landingPage) {
                if (confirm('This will permanently delete this landingspage, are you sure?')) {
                    this.deleteModel(landingPage);
                }
            },

            save(landingPage) {
                this.storeModel(landingPage, this.dataUrl(), landingPage.get('_id'));
            }
        }
    }
);
