import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import KeywordsaleModel from 'bo2/models/shop/keywordsale';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'keywords',
        idAttribute: 'keyword',

        map(data) {
            return KeywordsaleModel.create(data);
        },

        dataUrl(params) {
            return this._super(params, '/keywordsale');
        },

        actions: {
            addKeywordSale() {
                this.currentModel.unshiftObject(KeywordsaleModel.create({}));
            },

            deleteKeyword(keyword) {
                if (confirm('This will permanently delete this keyword, are you sure?')) {
                    this.deleteModel(keyword);
                }
            },

            save(keywordSale) {
                keywordSale.validate().then(()=> {
                    this.storeModel(keywordSale, this.dataUrl(), keywordSale.get('keyword')).then(()=> {
                        this.transitionTo('shop.keywordsales');
                    });
                }).catch(e => {
                    console.error('The model is invalid', e);
                });
            }
        }
    }
);
