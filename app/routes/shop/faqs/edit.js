import FutEmberRoute from 'bo2/routes/fut-ember-route';
import FaqModel from 'bo2/models/shop/faq';

export default FutEmberRoute.extend({
    idAttribute: '_id',
    entityName: 'faq',

    setupController(controller, model) {
        this._super(controller, model);
        controller.set('categories', this.controllerFor('shop.faqs').get('categories'));
    },

    parse(resp) {
        return FaqModel.create(resp);
    },

    dataUrl(params) {
        var url = '/faq';

        if (params) {
            url += '/' + params['_id'];
        }

        return this._super(params, url);
    },

    actions: {
        save(faq) {
            this.storeModel(faq).then(() => {
                this.transitionTo('shop.faqs.index');
            });
        },
        deleteFaq(model) {
            if (confirm('This will permanently delete this FAQ, are you sure?')) {
                this.deleteModel(model, undefined, true).then(() => {
                    this.modelFor('shop.faqs.index').removeObject(model);
                    this.transitionTo('shop.faqs.index');
                });
            }
        },
        cancel() {
            this.transitionTo('shop.faqs.index');
        }
    }
});