import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import HolidayModel from 'bo2/models/shop/holiday';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'holidays',
        idAttribute: '_id',

        map(holiday) {
            return HolidayModel.create(holiday);
        },

        dataUrl(params) {
            return this._super(params, '/holidays');
        },

        actions: {
            addHoliday() {
                var m = HolidayModel.create();
                this.currentModel.unshiftObject(m.setDefaults());
            },
            deleteHoliday(holiday) {
                if (confirm('This will permanently delete this holiday, are you sure?')) {
                    this.deleteModel(holiday);
                }
            },
            save(holiday) {
                this.storeModel(holiday, this.dataUrl(), holiday.get('_id'));
            }
        }
    }
);
