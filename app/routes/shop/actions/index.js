import Ember from 'ember';

import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import GroupPriceModel from 'bo2/models/shop/groupprice';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'groupprice',
        cartProductsUrl: '/shop/cartspecialproducts',
        model(params) {
            return Ember.RSVP.hash({
                groupprices: this._super(params),
                cartspecialproducts: this.getCartSpecialProducts()
            });
        },

        dataUrl(params) {
            return this._super(params, '/products/groupprices');
        },

        getCartSpecialProducts() {
            return this.futRest
                .loadData(this.get('cartProductsUrl'), undefined, {limit: 100})
                .then(response => {
                    let data = response.cartspecialproduct;
                    if (!Ember.isArray(data)) {
                        data = [data];
                    }
                    data = _.map(data, (prodlevid, index) => {
                        return Ember.Object.create({id: index, prodlevid: prodlevid});
                    });

                    return Ember.A(data);
                });
        },

        map(data) {
            return GroupPriceModel.create(data);
        },

        actions: {
            addCartProduct(newCartProduct) {
                return this.futRest
                    .store(this.get('cartProductsUrl'), {'prodlevid': newCartProduct}, 'POST')
                    .then(response => {
                        this.refresh();
                        return response;
                    });
            },

            deleteCartProduct(product) {
                let url = this.get('cartProductsUrl') + '/' + product.get('prodlevid');
                this.futRest
                    .deleteRequest(url)
                    .then(response => {
                        this.refresh();
                        return response;
                    });
            }
        }
    }
);
