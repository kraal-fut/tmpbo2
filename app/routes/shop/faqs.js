import Ember from 'ember';
import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import FaqModel from 'bo2/models/shop/faq';

export default PageableRoute.extend(
    FilterableMixin,
    {
        categories: [],
        idAttribute: '_id',
        entityName: 'faq',

        setupController(controller, model) {
            this._super(controller, model);
            controller.set('categories', this.categories);
        },

        renderTemplate() {
            this._super();
            this.render('shop/faqs/sidebar', {
                into: 'application',
                outlet: 'sidebar',
                model: this.get('categories')
            });
        },

        parse(resp) {
            this.categories = _.map(resp.categories, cat => {
                return Ember.Object.create(cat);
            });
            return this._super(resp);
        },

        map(faq) {
            return FaqModel.create(faq);
        },

        dataUrl(params) {
            return this._super(params, '/faq');
        },

        actions: {
            add() {
                this.currentModel.unshiftObject(FaqModel.create({category: {"title":"", "queue":"", "image":""}}));
            },
            addCategory() {
                this.controller.get('categories').unshiftObject(Ember.Object.create({}));
            },
            saveCat(cat, component) {
                var method = 'post';
                var id = '';
                if (typeof cat.queue !== 'undefined') {
                    method = 'put';
                    id = '/' + cat.queue;
                }
                var data = cat.getProperties.apply(cat, _.without(Object.keys(cat), 'errors', 'toString'));

                this.futRest.store('/faq/category' + id, data, method).then(response => {
                    if (id === '') {
                        this.currentModel.unshiftObject(FaqModel.create(response));
                    }
                    component.set('editing', false);
                }, response => {
                    this.get('notifier').notify({text: response.jqXHR.responseText, options: {status: 'error'}});
                });
            },

            deleteCat(cat) {
                if (!confirm('This will permanently delete this category, are you sure?')) {
                    return;
                }
                var id = cat.queue;
                if (!id) {
                    return this.controller.get('categories').removeObject(cat);
                }

                this.futRest.deleteRequest('/faq/category/' + id).then(response => {
                    this.refresh();
                    return response;
                }, response => {
                    this.get('notifier').notify({text: response.jqXHR.responseText, options: {status: 'error'}});
                });
            }
        }
    }
);
