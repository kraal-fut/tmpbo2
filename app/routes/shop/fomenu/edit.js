import FutEmberRoute from 'bo2/routes/fut-ember-route';
import FoMenuModel from 'bo2/models/shop/fomenu';

export default FutEmberRoute.extend({
    entityName: 'frontoffice_menu_pseudo',

    parse(data) {
        return FoMenuModel.create(data);
    },

    dataUrl(params) {
        let url = '/frontofficemenus';

        if (params) {
            url += '/' + params['id'];
        }

        return this._super(params, url);
    },

    persist (model, shouldPublish = false) {
        let url = this.dataUrl();
        if (shouldPublish) {
            url += '?publish=true';
        }

        this.storeModel(model, url).then(() => {
            this.transitionTo('shop.fomenus');
        });
    },

    actions: {
        save(model) {
            this.persist(model);
        },

        saveAndPublish(model) {
            this.persist(model, true);
        },

        cancel() {
            this.transitionTo('shop.fomenus');
        }
    }
});
