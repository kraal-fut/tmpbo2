import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import BannerModel from 'bo2/models/shop/banner';

export default PageableRoute.extend(
  FilterableMixin,
  {
    entityName: 'banners',

    map(banner) {
      return BannerModel.create(banner);
    },

    dataUrl(params) {
      return this._super(params, '/banners');
    },

    actions: {
      deleteBanner(banner) {
        if (confirm('This will permanently delete this banner, are you sure?')) {
          this.deleteModel(banner);
        }
      },

      save(banner) {
        this.storeModel(banner);
      },

      addBanner() {
        let m = BannerModel.create();
        this.currentModel.unshiftObject(m.setDefaults());
      },

      queryCats(query, deferred) {
        this.futRest.loadData('/frontofficemenus/list?q=' + query.term)
          .then(response => {
            return response.fomenumap;
          }).then(deferred.resolve, deferred.reject);
      }
    }
  }
);
