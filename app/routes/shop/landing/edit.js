import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';
import LandingModel from 'bo2/models/shop/landing';

export default FutEmberRoute.extend({
    entityName: 'landings',
    idAttribute: '_id',

    parse(data) {
        return LandingModel.create(data);
    },

    dataUrl(params) {
        let url = '/landings';

        if (params) {
            url += '/' + params['id'];
        }

        return this._super(params, url);
    },

    removeImage(image) {
        if (image.url) {
            this.get('currentModel.images').forEach(item => {
                if (image.url === item.url) {
                    item.remove = true;
                }
            });
            return;
        }

        let uploadImage = [];

        this.get('currentModel.uploadImages').forEach(item => {
            if (image.ImageFilename !== item.ImageFilename) {
                uploadImage.push(item);
            }
        });

        this.set('currentModel.uploadImages', uploadImage);
    },

    addImageAddedNotification() {
        this.notifications.addNotification({
            message: "Image added, don't forget to save the landingpage",
            type: 'warning',
            autoClear: true,
            clearDuration: 2400
        });
    },

    actions: {
        save(model) {
            this.storeModel(model).then(()=>{
                model.set('uploadImages', []);
                model.set('uploadImage', undefined);
            });
        },

        close() {
            this.transitionTo('shop.landings');
        },

        addUploadImage(theFile, model) {
            let uploadImage = model.get('uploadImages') || [];
            uploadImage.push( theFile );
            model.set('uploadImages', uploadImage);
            this.addImageAddedNotification();
        },

        refreshImages(id) {
            let url = '/api/v3' + this.dataUrl({id: id}) + '/images';
            Ember.$.ajax({
                url: url,
                contentType: 'application/json'
            }).then(images => {
                this.set('currentModel.images', images);
            });
        },

        deleteImage(image) {
            if (!confirm('Are you sure you want to remove this picture?')) {
                return false;
            }

            this.removeImage(image);
        }
    }
});
