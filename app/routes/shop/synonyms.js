import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import SynonymModel from 'bo2/models/shop/synonym';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'lucenesynonyms',

        map(order) {
            return SynonymModel.create(order);
        },

        dataUrl(params) {
            return this._super(params, '/lucenesynonyms');
        },

        actions: {
            save(synonym) {
                synonym.validate().then(() => {
                    this.storeModel(synonym);
                }).catch(e => {
                    console.error('model is invalid', e);
                });
            },
            remove(synonym) {
                if (confirm('This will permanently delete this synonym, are you sure?')) {
                    this.deleteModel(synonym);
                }
            },
            addSynonym() {
                this.currentModel.unshiftObject(SynonymModel.create({}));
            }
        }
    }
);
