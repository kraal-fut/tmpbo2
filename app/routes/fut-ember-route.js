import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(
    AuthenticatedRouteMixin,
    {
        futRest: Ember.inject.service(),

        idAttribute: 'id',
        serverQueryParams: {},

        parse(data) {

            const checkForGeneralData = function() {
                if (Ember.isPresent(data.data)) {
                    return data.data;
                }
                return data;
            };

            const hasNoDataMatchingEntity = () => {
                return Ember.isNone(this.entityName) || Ember.isNone(data[this.entityName]);
            };
            let d;

            if (hasNoDataMatchingEntity()) {
                d = checkForGeneralData(data);
            } else {
                d = data[this.entityName];
                if (!Ember.isNone(this.entityName) && !Ember.isArray(d)) {
                    d = [d];
                }
            }

            if (_.isFunction(this.map)) {
                if (!Ember.isArray(d)) {
                    d = [d];
                }
                d = _.map(d, this.map.bind(this));
            }

            return Ember.A(d);
        },

        model(params) {
            let parse = _.isFunction(this.parse) ? this.parse.bind(this) : data => data;

            if (this.dataUrl) {
                let url = this.dataUrl(params);
                if (Ember.isNone(url)) {
                    return Ember.Object.create({});
                }

                return this.futRest.loadData(url, null, this.serverQueryParams).then(parse);
            }
        },

        storeData(params) {
            let defaultParams = {};
            defaultParams[this.idAttribute] = this.currentModel[this.idAttribute];

            let url = this.dataUrl(_.extend({}, defaultParams, params));
            let data = this.currentModel.getJSON();

            if (_.isFunction(this.serializeModel)) {
                data = this.serializeModel(this.currentModel);
            }

            return this.futRest.storeData(url, data, null, 'PUT');
        },

        deleteModel(model, url, norefresh = false) {
            let id = model.get(this.idAttribute);

            url = url || this.dataUrl();

            if (Ember.isNone(id)) {
                return this.currentModel.removeObject(model);
            }

            url = url + '/' + model.get(this.idAttribute);

            return this.futRest.deleteRequest(url).then(response => {
                this.futRest.notifySuccessAjax('Delete');
                if (!norefresh) {
                    this.refresh();
                }
                return response;
            }).catch(e => {
                console.error(e);
                this.futRest.notifyFailedAjax('Delete');
            });
        },

        storeModel(model, url, id, norefresh = false, urlParams = undefined) {
            url = url || this.dataUrl();
            id = id || model.get(this.idAttribute);

            let objKeys = _.without(Object.keys(model), 'errors');
            let data = model.getProperties.apply(model, objKeys);

            if (_.isFunction(this.serializeModel)) {
                data = this.serializeModel(data);
            }

            let method = 'PUT';
            if (Ember.isNone(id)) {
                method = 'POST';
            } else {
                url += '/' + id;
            }
            return this.futRest.storeData(url, data, urlParams, method).then(response => {
                if (!norefresh) {
                    this.refresh();
                }
                return response;
            }, response => {
                let e = this.showResponseError(response, model, this.get('notifier'));
                if (e) {
                    throw e;
                }
            }).then(response => {
                this.controller.toggleProperty('editing');
                this.futRest.notifySuccessAjax();
                return response;
            });
        },

        dataUrl(params, url) {
            return url;
        },

        showResponseError(response, model, notifier) {
            return this.futRest.parseError(response, model, notifier);
        }
    }
);
