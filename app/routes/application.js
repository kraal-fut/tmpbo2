import Ember from 'ember';
import ENV from 'bo2/config/environment';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Ember.Route.extend(ApplicationRouteMixin, {
    session: Ember.inject.service(),

    beforeModel(transition) {
        this._super(transition);

        if (!Ember.isEmpty(this.get('session.appSettings.BASE_URL'))) {
            ENV.APP.fo2url = this.get('session.appSettings.BASE_URL');
        }
        if (!Ember.isEmpty(this.get('session.appSettings.BO1_URL'))) {
            ENV.APP.bo1url = this.get('session.appSettings.BO1_URL');
        }
    },

    actions: {
        showModal(name, model, fieldName) {
            var c;
            if (!Ember.isNone(fieldName)) {
                c = this.controllerFor(name);
                c.set('fieldName', fieldName);
            }

            this.render(name, {
                into: 'application',
                outlet: 'modal',
                model: model
            });
        },
        search(query) {
            if (this.get('searchSvc').hasHandler()) {
                return this.get('searchSvc').callhandler(query);
            }
            this.transitionTo('orders.orders', {queryParams: {q: query}});
        },

        removeModal() {
            this.disconnectOutlet({
                outlet: 'modal',
                parentView: 'application'
            });
        },

        transitionToLoginRoute() {
            this.transitionTo('login');
        }
    }
});
