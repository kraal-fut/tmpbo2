import FutEmberRoute from 'bo2/routes/fut-ember-route';
import Ember from 'ember';

export default FutEmberRoute.extend({
    session: Ember.inject.service('futsession'),

    model() {
        return this.get('session.settings');
    },

    actions: {
        save(model) {
            this.set('session.userSettings.emailSignature', model.emailSignature);
            this.futRest.store('/bo2settings/' + model._id, model, 'PUT');
        }
    }
});
