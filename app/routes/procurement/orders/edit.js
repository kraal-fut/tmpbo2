import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';
import OrderModel from 'bo2/models/procurement/order';
import OrderLineModel from 'bo2/models/procurement/line';
import SortableMixin from 'bo2/mixins/sortable';

export default FutEmberRoute.extend(
    SortableMixin,
    {
        setupController(controller, model) {
            this._super(controller, model);
            this.futRest
                .load('/stockproducts/list', undefined, 'stock')
                .then(allowedStocks => {
                    controller.set('allowedStocks', allowedStocks);
            }   );
        },

        dataUrl(params) {
            let url = '/procurement/orders/' + params[this.idAttribute];
            delete params[this.idAttribute];
            params = _.extend({}, this.queryParams, params);

            let urlParams = Ember.$.param(params);
            if (urlParams) {
                url += '?' + urlParams;
            }

            return url;
        },

        parse(data) {
            data.lines = Ember.A(data.lines.map(line => OrderLineModel.create(line)));

            return OrderModel.create(data);
        },

        serializeModel(model) {
            let data = model.getJSON();
            data.lines = data.lines.toArray();
            return data;
        },

        actions: {
            removeLine(line) {
                this.currentModel.lines.removeObject(line);
            },

            saveOrder() {
                this.doRequest();
            },

            saveAndPreview() {
                this.doRequest('Order saved and sent to preview', {preview: true});
            },

            saveAndFinal() {
                return this.currentModel.validate({task: 'sendFinal'}).then(model => {
                    model.setAsNew();
                    this.doRequest('Order saved and sent to supplier', {send: true});
                }).catch( e => {
                    console.error('The model is invalid', e);
                });
            },

            addToOrder() {
                this.currentModel.lines.unshiftObject({
                    qty: 0,
                    scanned_qty: 0,
                    isNew: true
                });
            }
        },

        doRequest(notifyText, params) {
            return this.storeData(params).then(() => {
                this.notifySuccess(notifyText || 'Order saved');
                return true;
            });
        },

        notifySuccess(text) {
            this.notifications.addNotification({
                message: text,
                type: 'success',
                autoClear: true,
                clearDuration: 1200
            });
        }
    }
);
