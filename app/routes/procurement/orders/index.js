import FutEmberRoute from 'bo2/routes/fut-ember-route';
import FilterableMixin from 'bo2/mixins/filterable';
import CanSearchMixin from 'bo2/mixins/cansearch';
import OrderModel from 'bo2/models/procurement/order';

export default FutEmberRoute.extend(
    FilterableMixin,
    CanSearchMixin,
    {
        searchPlaceholder: 'Find Procurement order',

        queryParams: {
            status: {refreshModel: true, scope: 'controller'},
            q: {refreshModel: true, scope: 'controller'}
        },

        entityName: 'ProcurementOrder',

        map(order) {
            return OrderModel.create(order);
        },

        dataUrl(params) {
            var url = '/procurement/orders';
            if (params && params.status !== 'allopen') {
                url += '?status=' + params.status;
            }
            return this._super(params, url);
        },

        renderTemplate: function () {
            this._super(this, arguments);

            this.render('procurement/orders/filterview', {
                outlet: 'filter',
                into: 'application',
                controller: this.get('controller')
            });
        },

        makeCopy(original) {
            return this.futRest.store('/procurement/orders', {'copyfromexisting': original.get('id')}, 'POST');
        },

        actions: {
            makeCopy(original) {
                this.makeCopy(original).then(() => {
                    this.refresh();
                });
            },
            closeOrder: function (order) {
                this.storeModel(order);
            }
        }
    }
);
