import FutEmberRoute from 'bo2/routes/fut-ember-route';
import Ember from 'ember';

export default FutEmberRoute.extend({
    entityName: 'stock',

    dataUrl: function () {
        return '/stockproducts/list';
    },

    setupController: function (controller, model) {
        controller.set('allowedstocks', model);
    },

    parse: function (stocks) {
        return Ember.A(stocks.stock).mapBy('stock');
    }

});
