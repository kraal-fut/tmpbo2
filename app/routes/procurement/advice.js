import FutEmberRoute from 'bo2/routes/fut-ember-route';
import SortableMixin from 'bo2/mixins/sortable';
import FilterableMixin from 'bo2/mixins/filterable';
import AdviceModel from 'bo2/models/procurement/advice';

export default FutEmberRoute.extend(
    FilterableMixin,
    SortableMixin,
    {
        entityName: 'advice',

        map(advice) {
            return AdviceModel.create(advice);
        },

        dataUrl(params) {
            var url = '/procurement/advices';
            if (params && params.regenerate) {
                url += '?regenerate=true';
            }
            return this._super(params, url);
        },

        actions: {
            regenerateAdvice() {
                let c = this.controllerFor('procurement.advice.index');
                if (confirm('This will delete all current advices.\nRegenerating takes about 30 seconds.\nAre you sure?')) {
                    c.set('isGenerating', true);

                    this.model({regenerate: true}).then(results => {
                        this.set('controller.model', results);
                    }).finally(()=>{
                        c.set('isGenerating', false);
                        this.futRest.notifySuccessAjax('Regenerate');
                        this.refresh();
                    });
                }
            }
        }

    }
);
