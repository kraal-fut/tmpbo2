import FutEmberRoute from 'bo2/routes/fut-ember-route';
import FilterableMixin from 'bo2/mixins/filterable';

export default FutEmberRoute.extend(
    FilterableMixin,
    {
        model() {
            return this.modelFor('procurement.advice');
        },

        actions: {
            deleteAdvices() {
                let selecteds = this.get('controller.selecteds').mapBy('id');
                if (!selecteds.get('length')) {
                    return;
                }
                this.futRest.deleteRequest('/procurement/advices/' + selecteds.join(',')).then(()=> {
                    this.refresh();
                });
            }
        }
    }
);
