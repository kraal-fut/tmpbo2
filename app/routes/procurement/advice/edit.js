import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';
import SortableMixin from 'bo2/mixins/sortable';
import FilterableMixin from 'bo2/mixins/filterable';

export default FutEmberRoute.extend(
    FilterableMixin,
    SortableMixin,
    {
        entityName: 'advice',

        map(advice) {
            return Ember.Object.create(advice);
        },

        dataUrl(params) {
            return this._super(params, '/suppliers/' + params['id'] + '/advices?limit=500');
        },

        navigateTo(current, directionOffset) {
            let models = this.modelFor('procurement.advice');
            let c = models.indexOf(models.findBy('id', current));
            let next = models[c + directionOffset];

            if (!next && directionOffset < 0) {
                next = models[models.length - 1];
            } else if (!next && directionOffset > 0) {
                next = models[0];
            }

            this.transitionTo('procurement.advice.edit', next.id);
        },

        actions: {
            createOrder(supplierId) {
                let c = this.get('controller');

                return c.createOrder(supplierId, c.get('selecteds')).then(model => {
                    return this.futRest.store('/procurement/orders', model.getJSON(), 'POST')
                        .then(()=> {
                            c.get('model').removeObjects(c.get('selecteds'));
                        });
                });
            },
            gotoList() {
                this.transitionTo('procurement.advice.index');
            },
            gotoPrev(current) {
                this.navigateTo(current, -1);
            },
            gotoNext(current) {
                this.navigateTo(current, +1);
            }
        }
    }
);
