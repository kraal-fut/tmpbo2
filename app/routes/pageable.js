import BaseRoute from 'bo2/routes/fut-ember-route';
import Ember from 'ember';

export default BaseRoute.extend({
    queryParams: {
        page: {refreshModel: true, scope: 'controller'},
        limit: {refreshModel: true, scope: 'controller'}
    },

    state: Ember.Object.create({
        page: 1,
        total: 0,
        itemsPerPage: 10
    }),

    setupController(controller, model) {
        this._super(controller, model);
        controller.get('pagination').setProperties({
            state: this.get('state')
        });
    },

    model(params) {
        let page;

        if (params.page) {
            page = params.page;
            page = isNaN(page) ? 1 : Math.floor(Math.abs(page));
            this.set('state.page', page);
            this.serverQueryParams.page = page;
        }

        if (params.limit) {
            this.set('state.itemsPerPage', params.limit);
            this.serverQueryParams.limit = params.limit;
        }

        return this._super(params);
    },

    afterModel(model, transition) {
        if (model && model.meta) {
            this.parseState(model.meta);
        }
        this._super(model, transition);
    },

    actions: {
        pageChanged(newPage) {
            this.get('controller.pagination').doTransition(newPage);
        },

        willTransition: function (transition) {
            if (transition.targetName !== this.routeName) {
                delete this.serverQueryParams.page;
            }
            return true;
        }
    },

    parse(data) {
        if (data.meta) {
            this.parseState(data.meta);
        }
        return this._super(data);
    },

    parseState(meta) {
        var state = {};

        if (meta.total) {
            state.totalRecords = parseInt(meta.total, 10);
        }
        if (meta.pageCount) {
            state.totalPages = parseInt(meta.pageCount, 10);
        }
        if (meta.page) {
            state.page = parseInt(meta.page, 10);
        }

        this.state.setProperties(state);
    }
});
