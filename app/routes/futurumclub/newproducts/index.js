import Ember from 'ember';

export default Ember.Route.extend({
    model() {
        return this.store.findAll('futurumclub/newproducts');
    },

    actions:{
        findProduct(prodlevid, model) {
            model.get('multiImages').forEach((image) => {
                if (image) {
                    image.destroyRecord();
                }
            });

            this.futRest.load('/products/' + prodlevid).then((data)=>{

                model.setProperties({
                    name: data.Name,
                    vendor: data.Vendor,
                    euprice: data.euprice,
                    description: data.description.Koms
                } );

                if (Ember.isArray(data.description.Image)) {
                    data.description.Image.forEach((image)=>{
                        let imgRec = this.store.createRecord('product/image', image);
                        model.get('multiImages').addObject(imgRec);
                    });
                }
            });
        },

        addProduct() {
            this.store.createRecord('futurumclub/newproducts', {});
        },

        deleteProduct(model) {
            if (confirm('This will remove this product from the mobile app, sure?')) {
                model.destroyRecord();
            }
        },

        save(model, component) {
            model.save().then(()=>{
                component.set('editing', false);
            }).catch((e)=>{
                console.error('Something happened while saving', e.errors);
            });
        }
    }
});
