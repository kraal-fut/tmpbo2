import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';

export default FutEmberRoute.extend({
        model() {
            return Ember.ArrayProxy.create({
                content: Ember.A([])
            });
        },

        getSelected() {
            return this.currentModel.filterBy('selected', true);
        },

        hasSelected() {
            return this.currentModel.isAny('selected', true);
        },

        actions: {
            selectAll() {
                this.currentModel.filter((item) => {
                    return item.get('action') !== 'none';
                }).setEach('selected', !this.hasSelected());
            },

            refresh() {
                this.currentModel.clear();
            },

            doUpload(collection) {
                if (!this.hasSelected()) {
                    this.get('notifier').notify({options: {status: 'warning'}, text: 'Select images to upload'}, true);
                    return;
                }

                let selected = this.getSelected();

                let promises = [];
                let futRest = this.futRest;

                selected.forEach((m) => {
                    let url = '/productdescription/' + m.get('prodlevid') + '/images/' + m.get('filename');
                    promises.push(
                        futRest.store(url, m.getJSON(['action'], ['images', 'file', 'selected']), 'PUT')
                            .then((response) => {
                                collection.removeObject(m);
                                return response;
                            }).fail((response) => {
                                m.set('error', response.jqXHR.responseText);
                                return response;
                            }));
                });

                Ember.RSVP.all(promises).then(() => {
                    this.get('notifier').notify('All images uploaded');
                });
            }
        }
    }
);
