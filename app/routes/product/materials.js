import Ember from 'ember';
import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import MaterialModel from 'bo2/models/product/material';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'materials',

        map(model) {
            return MaterialModel.create(model);
        },

        dataUrl(params) {
            return this._super(params, '/productmaterials');
        },

        uploadFail() {
            if (confirm('The import contains errors do you wish to download these?')) {
                window.location = '/api/v3/' + this.dataUrl() + '/download';
            }
        },

        actions: {
            imported(importeds) {
                Ember.$.ajax({
                    url: '/api/v3/' + this.dataUrl() + '/import',
                    method: 'POST',
                    data: {products: JSON.stringify(importeds)}
                }).then(() => {
                    this.refresh();
                }).fail(() => {
                    this.uploadFail();
                });
            },
            save(model) {
                this.storeModel(model);
            },
            addMaterial() {
                this.currentModel.unshiftObject(MaterialModel.create({part: 1}));
            },
            deleteMaterial(model) {
                if (confirm('This will permanently delete this item, are you sure?')) {
                    this.deleteModel(model);
                }
            }
        }
    }
);
