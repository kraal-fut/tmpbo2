import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import QuestionModel from 'bo2/models/product/question';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'product_questions',

        map(model) {
            return QuestionModel.create(model);
        },

        dataUrl(params) {
            return this._super(params, '/productquestions');
        },

        makeDate: (date) => {
            return moment(date).format('YYYY-MM-DD HH:mm:ss');
        },

        actions: {
            save(model) {
                this.storeModel(model);
            },
            deleteQuestion(model) {
                if (confirm('This will permanently delete this question, are you sure?')) {
                    this.deleteModel(model);
                }
            },
            convert(model) {
                if (!confirm('Convert this question to a review?')) {
                    return;
                }

                var commentModel = {
                    Name: model.get('user_name'),
                    Score: 1,
                    Active: 0,
                    Email: model.get('user_email'),
                    Comments: model.get('question'),
                    ProdLevId: model.get('prodlevid'),
                    sign_question: 0,
                    created: this.makeDate(new Date())
                };

                this.futRest.store('/productcomments', commentModel, 'POST').then(() => {
                    this.deleteModel(model);
                });
            }
        }
    }
);
