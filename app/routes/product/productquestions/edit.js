import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';
import ProductQuestionModel from 'bo2/models/product/question';

export default FutEmberRoute.extend({
    entityName: 'product_questions',
    idAttribute: 'id',

    parse(data) {
        return ProductQuestionModel.create(data);
    },

    dataUrl(params) {
        var url = '/productquestions';
        if (params) {
            url += '/' + params[this.idAttribute];
        }
        return this._super(params, url);
    },

    actions: {
        save(model) {
            model.setData();
            model.checkEmailInput();
            model.validate().then(()=> {
                this.storeModel(model).then(()=> {
                    this.transitionTo('product.productquestions.index');
                });
            }, (e) => {
                console.error('The model is invalid', e);
            });
        },
        cancel() {
            this.transitionTo('product.productquestions.index');
        }
    }
});
