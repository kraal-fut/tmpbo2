import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import CanSearchMixin from 'bo2/mixins/cansearch';
import BundleModel from 'bo2/models/product/bundle';

export default PageableRoute.extend(
    FilterableMixin,
    CanSearchMixin,
    {
        entityName: 'bundles',
        idAttribute: '_id',
        searchPlaceholder: 'find bundles',

        map: function (model) {
            return BundleModel.create(model);
        },

        dataUrl: function (params) {
            return this._super(params, '/bundles');
        },

        actions: {
            add: function () {
                var m = BundleModel.create({
                    type: 'DiscountBundle',
                    discountType: 'euro',
                    enabled: true,
                    visibleInSearch: false,
                    rules: [{prodlevid: '', vendor: '', category: ''}],
                    bundledproducts: [{prodlevid: '', qty: 1}]
                });
                this.currentModel.unshiftObject(m);
                this.transitionTo('product.bundles.edit', m);
            },
            remove: function (model) {
                if (confirm('This will permanently delete this bundle, are you sure?')) {
                    this.deleteModel(model);
                }
            }
        }
    }
);
