import FutEmberRoute from 'bo2/routes/fut-ember-route';
import BundleModel from 'bo2/models/product/bundle';
import Ember from 'ember';

export default FutEmberRoute.extend({
    entityName: 'bundles',
    idAttribute: '_id',

    setupController: function (controller, model) {
        this._super(controller, model);
        controller.getCategories(controller);
        controller.getVendors(controller);
    },
    parse: function (data) {
        let d = this._super(data);
        return BundleModel.create(d);
    },

    dataUrl: function (params) {
        var url = '/bundles';

        if (params) {
            url += '/' + params['id'];
        }

        return this._super(params, url);
    },

    validateBundle(model) {
        var errors = 0;
        var messages = [];

        return new Ember.RSVP.Promise(function (resolve, reject) {
            model.get('rules').forEach((rule, key) => {
                var empty = 0;
                if (typeof rule.prodlevid === 'undefined' || rule.prodlevid === '') {
                    empty++;
                }
                if (typeof rule.vendor === 'undefined' || rule.vendor === '') {
                    empty++;
                }
                if (typeof rule.frontoffice_menu_id === 'undefined' || rule.frontoffice_menu_id === '') {
                    empty++;
                }
                if (empty === 3) {
                    errors++;
                    var number = key + 1;
                    messages.push(' Rule ' + number + ' is empty.');
                }
            });

            model.get('bundledproducts').forEach((product, key) => {
                var number = key + 1;
                if (typeof product.prodlevid === 'undefined' || product.prodlevid === '') {
                    errors++;
                    messages.push('Product ' + number + ' is incorrect. Prodlevid is missing.');
                }
                if (typeof product.qty === 'undefined' || product.qty === '') {
                    errors++;
                    messages.push('Product ' + number + ' is incorrect. Qty is missing.');
                } else if (product.qty < 1) {
                    errors++;
                    messages.push('Product ' + number + ' is incorrect. Amount has to be at least 1.');
                }
            });

            if (errors === 0) {
                resolve(model);
            } else {
                reject(messages);
            }
        });

    },

    actions: {
        addRule: function (model) {
            model.get('rules').pushObject({});
        },
        deleteRule: function (model, rule) {
            model.get('rules').removeObject(rule);
        },
        addProduct: function (model) {
            model.get('bundledproducts').pushObject({qty: 1});
        },
        deleteProduct: function (model, product) {
            model.get('bundledproducts').removeObject(product);
        },
        save(model) {
            return model.validate({}).then((model) => {
                return this.validateBundle(model).then(() => {
                    this.storeModel(model).then(() => {
                        this.transitionTo('product.bundles.index');
                    });
                }).catch((e) => {
                    if (Ember.isArray(e)) {
                        e.forEach((message) => {
                            this.notify(message);
                        });
                    } else {
                        throw e;
                    }
                });
            }).catch((e)=> {
                console.error('The model is invalid', e);
            });
        },
        cancel: function () {
            this.transitionTo('product.bundles.index');
        }
    }
});
