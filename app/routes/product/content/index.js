import Ember from 'ember';
import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import ContentModel from 'bo2/models/product/content';

export default PageableRoute.extend(FilterableMixin, {
    entityName: 'productdescription',

    map(model) {
        if (Ember.typeOf(model.Image) === 'string' && !Ember.isEmpty(model.Image)) {
            try {
                model.Image = JSON.parse(model.Image);
            } catch (ignored) {
                model.Image = {
                    1: {
                        filename: model.Image,
                        prodlevid: model.ProdLevId,
                        oldSituation: true
                    }
                };
            }
        }

        if (_.isObject(model.Image)) {
            model.Image = _.map(_.toArray(model.Image), item => {
                return Ember.Object.create(item);
            });
        }

        return ContentModel.create(model);
    },

    dataUrl(params) {
        return this._super(params, '/productdescription');
    }
});
