import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';
import ContentModel from 'bo2/models/product/content';

export default FutEmberRoute.extend({
    entityName: 'productdescription',

    parse(model) {
        if (Ember.typeOf(model.Image) === 'string' && !Ember.isEmpty(model.Image)) {
            try {
                model.Image = JSON.parse(model.Image);
            } catch (ignored) {
                model.Image = {
                    1: {
                        filename: model.Image,
                        prodlevid: model.ProdLevId,
                        oldSituation: true
                    }
                };
            }
        }

        if (_.isObject(model.Image)) {
            model.Image = _.map(_.toArray(model.Image), (item) => {
                return Ember.Object.create(item);
            });
        }
        return ContentModel.create(model);
    },

    dataUrl(params) {
        let url = '/productdescription';

        if (params) {
            url += '/' + params['id'];
        }

        return this._super(params, url);
    },

    actions: {
        save(model) {
            model = model.updateImages();

            this.storeModel(model).then(() => {
                this.transitionTo('product.content.index');
            });
        },
        cancel() {
            this.transitionTo('product.content.index');
        },
        copy(model) {
            let id = model.get('id');
            if (Ember.isEmpty(model.get('relatedProducts.variations'))) {
                this.get('notifier').notify({text: 'Product does not have variations'});
                return;
            }
            let variations = model.get('relatedProducts.variations').removeObject(id);
            let data = {
                Koms: model.get('Koms'),
                Loms: model.get('Loms'),
                variations: variations,
                copyToVariations: true
            };

            this.futRest.store('/productdescription/' + id, data, 'PUT').then(() => {
                this.get('notifier').notify({text: 'Description is copied to variations'});
            }, (response) => {
                this.showResponseError(response);
            });
        },

        deleteImage(image) {
            if (!confirm('Are you sure you want to remove this picture?')) {
                return false;
            }
            let id = image.prodlevid;
            let url = '/productdescription/' + id + '/images/' + image.filename;

            let removeFromDisk = false;
            if (!this.currentModel.get('oldSituation')) {
                removeFromDisk = confirm('Do you want to remove it from the disk?');
            }
            this.futRest.deleteRequest(url, {removeFromDisk: removeFromDisk}).then(() => {
                this.get('notifier').notify({text: 'Image is removed'});
                this.refresh();
            }, (response) => {
                this.showResponseError(response);
            });
        }
    }
});
