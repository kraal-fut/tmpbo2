import PageableRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import SupplierModel from 'bo2/models/product/supplier';

export default PageableRoute.extend(
    FilterableMixin,
    {
        entityName: 'supplier',

        map(model) {
            return SupplierModel.create(model);
        },

        dataUrl(params) {
            return this._super(params, '/suppliers');
        },

        actions: {
            save(model) {
                this.storeModel(model);
            },
            deleteSupplier(model) {
                if (confirm('This will permanently delete this supplier, are you sure?')) {
                    this.deleteModel(model);
                }
            }
        }
    }
);
