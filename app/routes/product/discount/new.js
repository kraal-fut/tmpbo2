import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/fut-ember-route';
import CouponModel from 'bo2/models/product/discount';
import CouponRule from 'bo2/models/product/couponrule';

export default FutEmberRoute.extend({
    entityName: 'coupon',

    dataUrl(params) {
        let url = '/coupons';
        return this._super(params, url);
    },

    model() {
        return CouponModel.create({coupon_rule: []});
    },

    actions: {
        addRule(model) {
            let rule = model.get('coupon_rule') || Ember.A();
            rule.unshiftObject(CouponRule.create({coupon_rule_type: 1}));
            model.set('coupon_rule', rule);
        },

        deleteRule(model, rule) {
            model.get('coupon_rule').removeObject(rule);
        },

        cancel() {
            this.transitionTo('product.discount.index');
        },

        save(model) {
            model.validate().then(model => {
                this.storeModel(model, undefined, undefined, true).then(() => {
                    this.transitionTo('product.discount.index');
                });
            }).catch((e)=> {
                console.error('The model is invalid', e);
            });
        }
    }
});
