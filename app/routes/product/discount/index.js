import Ember from 'ember';
import FutEmberRoute from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import CouponModel from 'bo2/models/product/discount';
import CouponRule from 'bo2/models/product/couponrule';

export default FutEmberRoute.extend(
    FilterableMixin,
    {
        entityName: 'coupon',

        map(model) {
            model.coupon_rule = Ember.A(_.map(model.coupon_rule, rule => CouponRule.create(rule)));
            return CouponModel.create(model);
        },
        dataUrl(params) {
            return this._super(params, '/coupons');
        },

        serializeModel(model) {
            if (model.optional_arguments && model.optional_arguments.characters) {
                model.optional_arguments.length = model.optional_arguments.characters;
            }
            return model;
        },

        actions: {
            add() {
                this.transitionTo('product.discount.new');
                //this.currentModel.unshiftObject(CouponModel.create());
            },

            check(code) {
                this.get('controller').loadCode(code);
            },

            save(model) {
                this.storeModel(model);
            },

            downloadCodes(model) {
                window.location = this.dataUrl() + '/' + model.get('id') + '/codes';
            }
        }
    }
);
