import FutEmberRoute from 'bo2/routes/fut-ember-route';
import VendorModel from 'bo2/models/product/vendor';

export default FutEmberRoute.extend({
    entityName: 'vendor',

    parse(data) {
        return VendorModel.create(data);
    },

    dataUrl(params) {
        var url = '/vendors';

        if (params) {
            url += '/' + params['id'];
        }

        return this._super(params, url);
    },

    actions: {
        save(model) {
            let kw = model.get('keyword');

            if (typeof kw === 'string') {
                model.set('keyword', kw.split(','));
            }

            model.validate().then(()=> {
                this.storeModel(model).then(()=> {
                    this.transitionTo('product.vendors.index');
                });
            }, (e) => {
                console.error('The model is invalid', e);
            });
        },
        cancel() {
            this.transitionTo('product.vendors.index');
        }
    }
});
