import Pageable from 'bo2/routes/pageable';
import FilterableMixin from 'bo2/mixins/filterable';
import VendorModel from 'bo2/models/product/vendor';

export default Pageable.extend(FilterableMixin,
    {
        entityName: 'vendor',
        hasEditbaleRows: true,

        dataUrl(params) {
            return this._super(params, '/vendors');
        },

        map(data) {
            return VendorModel.create(data);
        },

        actions: {
            save(model) {
                this.storeModel(model);
            }
        }
    });
