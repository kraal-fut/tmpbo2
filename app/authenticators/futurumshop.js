/* global unescape: false */

import Ember from 'ember';
import Base from 'ember-simple-auth/authenticators/base';

export default Base.extend({
    getHash(data) {
        return 'Basic ' + window.btoa(unescape(encodeURIComponent(data.identification + ':' + data.password)));
    },

    restore(data) {
        return new Ember.RSVP.Promise((resolve, reject) => {
            if (Ember.isEmpty(data.token)) {
                reject();
            } else {
                resolve(data);
            }
        });
    },

    authenticate(options) {
        let hash = this.getHash(options);

        return Ember.$.ajax({
            type: 'GET',
            url: '/api/v3/bo2settings',
            data: {},
            crossDomain: false,
            beforeSend: xhr => {
                xhr.setRequestHeader('Authorization', hash);
            }
        }).then(data => {
            return {
                token: hash,
                data: data
            };
        });
    },
    invalidate() {
        return new Ember.RSVP.Promise(resolve => {
            resolve();
        });
    }
});
