import Ember from 'ember';
import {stripTags} from './strip-tags';

export function strTruncate(params) {

    let str = params[0] || '';
    let limit = params[1];
    let omission = params[2];
    if (params[3]) {
        str = stripTags(str);
    }
    if (typeof omission === 'undefined') {
        omission = '';
    }
    if (str.length > limit) {
        return str.substring(0, limit - omission.length) + omission;
    } else {
        return str;
    }
}

export default Ember.Helper.helper(strTruncate);
