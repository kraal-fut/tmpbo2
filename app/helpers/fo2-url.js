import Ember from 'ember';
import ENV from 'bo2/config/environment';

export function fo2url(value, context) {
    if (_.isUndefined(value)) {
        return '';
    }

    var options = context || {};
    var type;
    if (options.type) {
        type = options.type;
    }
    var host = ENV.APP.fo2url;
    if (options.host) {
        host = options.host;
    }
    var url = '';

    if (type === 'product') {
        url += host + '/?action=product&prodlevid=' + value;
    } else if (type === 'category') {
        url += host + '?action=search&category=' + value;
    } else if (type === 'vendor') {
        url += host + '?action=vendor&name=' + value;
    } else {
        if (_.isObject(value) && !_.isUndefined(value.Image)) {
            value = value.Image;
        }

        if (value.indexOf('base64,') > -1) {
            return value;
        }
        url += host + value;
    }

    if (options.anchor) {
        url += '#' + options.anchor;
    }

    return url;
}
export default Ember.Helper.helper(fo2url);
