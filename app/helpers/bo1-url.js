import Ember from 'ember';
import ENV from 'bo2/config/environment';

export function bo1Url(value, hash) {
    var action, identifier, type = 'order';

    if (hash && hash.type) {
        type = hash.type;
    }

    var host = ENV.APP.bo1url;
    if (hash && hash.host) {
        host = hash.host;
    }

    if (type === 'order') {
        action = 'to_order';
        identifier = 'ordnu=';
    } else if (type === 'customer') {
        action = 'client2';
        identifier = 'regid=';
    } else if (type === 'product') {
        action = 'to_product';
        identifier = 'prodlevid=';
    }

    return host + '/backoffice/bo40.cgi?action=' + action + '&' + identifier + value;
}
export default Ember.Helper.helper(bo1Url);
