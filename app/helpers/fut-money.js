import Ember from 'ember';

/**
 * @param value
 * @returns {string}
 */
export function futMoney(value) {
    var c = 2, d = ',', t = '.';

    var s = value < 0 ? '-' : '',
        i = parseInt(value = Math.abs(+value || 0).toFixed(c)) + '',
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : '') +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
        (c ? d + Math.abs(value - i).toFixed(c).slice(2) : '');
}
export default Ember.Helper.helper(futMoney);
