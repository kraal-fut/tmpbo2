import Ember from 'ember';

export function stripTags(value) {
    if (_.isUndefined(value) || _.isNull(value)) {
        return '';
    }
    var v = Ember.isArray(value) ? value[0] : value;

    if (Ember.isNone(v)) {
        return '';
    }

    return v
        .replace(/(&nbsp;)/gi, ' ')
        .replace(/(<wbr>)/gi, '')
        .replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>?/gi, '');
}

export default Ember.Helper.helper(stripTags);
