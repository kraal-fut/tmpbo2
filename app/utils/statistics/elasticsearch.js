import Ember from 'ember';

class Elasticsearch {
    constructor(options) {
        this.options = options;
        this.host = options.host || 'http://localhost:9200';
    }

    search(params) {
        let url = this.host + '/';

        if (params.index) {
            url += params.index + '/';
        }

        if (params.type) {
            url += params.type + '/';
        }

        url += '_search';

        if (this.options.onlyCounts) {
            url += '?search_type=count';
        }

        return Ember.$.ajax({
            url: url,
            data: JSON.stringify(params.body || {}),
            dataType: 'json',
            type: 'POST'
        });
    }
}

export default Elasticsearch;
