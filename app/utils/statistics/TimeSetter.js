const ranges = {
    'Today': [moment().startOf('day'), moment().endOf('day')],
    'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
    'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
    'Last week': [moment().subtract(6, 'days').startOf('week'), moment().subtract(6, 'days').endOf('week')],
    'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
    'Last 365 Days': [moment().subtract(364, 'days').startOf('day'), moment().endOf('day')],
    'This Year': [moment().startOf('year').startOf('day'), moment().endOf('year').endOf('day')]
};

class TimeSetter {
    constructor(model) {
        this.ranges = ranges;
        this.model = model;
        this.serverFormat = 'YYYY-MM-DD HH:mm:ss';
    }

    setTimes(from, to = undefined) {
        if (from.length) {
            to = from[1];
            from = from[0];
        }

        this.model.beginPropertyChanges();
        this.model.setProperties({
            from: from,
            to: to
        });
        this.model.notifyPropertyChange('from');
        this.model.notifyPropertyChange('to');
        this.model.endPropertyChanges();
    }

    currentFrom() {
        return this.model.get('from');
    }

    currentTo() {
        return this.model.get('to');
    }

    getRange(from, to) {
        return {
            gte: moment(from || this.currentFrom() || {hour: 0}).format(this.serverFormat),
            lte: moment(to || this.currentTo()).format(this.serverFormat)
        };
    }

    getDefaultTime() {
        this.getLast30Days();
    }

    getToday() {
        this.setTimes(ranges.Today);
    }

    getDayBefore() {
        let from = this.currentFrom().subtract(1, 'days');
        let to = this.currentTo().subtract(1, 'days');

        if (from.isSame(to)) {
            to = to.add(1, 'days');
        }

        this.setTimes(from, to);
    }

    getWeekBefore() {
        let from = this.currentFrom().subtract(1, 'weeks');
        let to = this.currentTo().subtract(1, 'weeks');

        if (from.isSame(to)) {
            to = to.add(1, 'days');
        }

        this.setTimes(from, to);
    }

    getDayAfter() {
        let from = this.currentFrom().add(1, 'days');
        let to = this.currentTo().add(1, 'days');
        if (from.isSame(to)) {
            to = to.add(1, 'days');
        }
        this.setTimes(from, to);
    }

    getWeekAfter() {
        let from = this.currentFrom().add(1, 'weeks');
        let to = this.currentTo().add(1, 'weeks');
        if (from.isSame(to)) {
            to = to.add(1, 'days');
        }
        this.setTimes(from, to);
    }

    getThisMonth() {
        this.setTimes(ranges['This Month']);
    }

    getLast30Days() {
        this.setTimes(ranges['Last 30 Days']);
    }
}

export default TimeSetter;
