import Ember from 'ember';
import Elastic from 'bo2/utils/statistics/elasticsearch';
import TimeSetter from './TimeSetter';

let Loader = Ember.Object.extend({
    init(values) {
        if (values) {
            this.setProperties(values);
        }
        this._super();
    },

    loadData() {},

    client: undefined,
    index: 'futurum2_nl',
    dateFormat: 'YYYY-MM-DD HH:mm:ss',
    timeSetter: undefined,
    status: ['sameasbo1'],
    vendorFilter: [],
    readyForLoading: false,

    connect(url) {
        this.timeSetter = new TimeSetter(this);
        this.timeSetter.getDefaultTime();

        this.client = new Elastic({
            host: url,
            onlyCounts: this.get('onlyCounts')
        });

        return this;
    },

    variousChanged: Ember.observer(
        'readyForLoading', 'status', 'from', 'to', 'includeNazend', 'includeDE', 'onlyBundleOrders', 'searchQuery',
        function () {
            this.loadData();
        }
    ),

    vendorChanged: Ember.observer('vendorFilter', function () {
        if (this.get('vendorFilter').contains('All vendors')) {
            this.set('vendorFilter', []);
        }
        this.loadData();
    }),

    countriesChanged: Ember.observer('countries.[]', function () {
        this.loadData();
    }),

    paymentMethodChanged: Ember.observer('paymentmethods.[]', function () {
        this.loadData();
    }),

    getMainQuery(range, statuses) {
        let bool = {
            must: [
                {range: {OrderDate: range}}
            ],
            must_not: []
        };

        if (Ember.isPresent(statuses)) {
            bool.must.push({terms: {Status: statuses}});
        }

        if (this.get('countries.length')) {
            let countries = this.get('countries');

            if (Ember.isArray(countries)) {
                countries.forEach(country => {
                    bool.must.push({term: {country: country}});
                });
            }
        }

        if (this.get('paymentmethods.length')) {
            let pm = this.get('paymentmethods');

            if (Ember.isArray(pm)) {
                pm.forEach(method => {
                    bool.must.push({term: {PaymentMethod: method}});
                });
            }
        }

        if (!this.get('includeDE')) {
            bool.must_not.push({term: {PaymentMethod: 'duitsland'}});
            bool.must_not.push({term: {PaymentMethod: 'de_rmbrs'}});
        }

        if (!this.get('includeNazend')) {
            bool.must_not.push({term: {PaymentMethod: 'nazend'}});
        }

        if (Ember.isPresent(this.get('vendorFilter'))) {
            bool.must.push(
                {
                    nested: {
                        path: 'orderlines',
                        query: {terms: {Vendor: _.values(this.get('vendorFilter')).compact()}}
                    }
                }
            );
        }

        return bool;
    },

    ordersearch(range, statuses, aggregations) {
        let filters = {
            bool: this.getMainQuery(range, statuses)
        };

        if (this.get('onlyBundleOrders')) {
            filters.bool.must.push(
                {
                    nested: {
                        path: 'orderlines',
                        filter: {exists: {'field': 'orderlines.discount_source.bundle'}}
                    }
                }
            );
        }

        let q = this.get('searchQuery') ? {query_string: {query: this.get('searchQuery')}} : {match_all: {}};

        return this.client.search({
            index: this.index,
            type: 'orders',
            body: {
                query: {
                    filtered: {
                        filter: filters,
                        query: {
                            bool: {
                                should: [
                                    q,
                                    {
                                        nested: {
                                            path: 'orderlines',
                                            query: q
                                        }
                                    }
                                ]
                            }
                        }
                    }
                },
                aggs: aggregations
            }
        });
    },

    search(range, statuses) {
        return this.client.search({
            index: this.index,
            body: {
                query: {
                    filtered: {
                        filter: {
                            bool: this.getMainQuery(range, statuses)
                        }
                    }
                },
                aggs: this.getAggregations()
            }
        });
    },

    orderlinesSearch(range, statuses, aggregations) {
        let filters = {
            bool: this.getMainQuery(range, statuses)
        };

        if (this.get('onlyBundleOrders')) {
            filters.bool.must.push(
                {
                    nested: {
                        path: 'orderlines',
                        filter: {exists: {'field': 'discount_source.bundle'}}
                    }
                }
            );
        }

        let q = this.get('searchQuery') ? {query_string: {query: this.get('searchQuery')}} : {match_all: {}};

        return this.client.search({
            index: this.index,
            type: 'orders',
            body: {
                query: {
                    filtered: {
                        filter: filters,
                        query: {
                            bool: {
                                should: [
                                    q,
                                    {
                                        nested: {
                                            path: 'orderlines',
                                            query: q
                                        }
                                    }
                                ]
                            }
                        }
                    }
                },
                aggs: aggregations
            }
        });
    },

    getPopupularBundles() {
        let bool = this.getMainQuery(this.timeSetter.getRange(
            moment().startOf('month'),
            moment().endOf('month')
        ), _.range(10, 70));

        bool.must.push({
            nested: {
                path: 'orderlines',
                query: {
                    term: {
                        'orderlines.discount_source.type': 'main'
                    }
                }
            }
        });

        let query = {
            bool: bool
        };

        let aggregations = {
            orderlines: {
                nested: {
                    path: 'orderlines'
                },
                aggs: {
                    bundles: {
                        terms: {field: 'orderlines.discount_source.bundle', size: 20}
                    }

                }
            }
        };

        return this.client.search({
            index: this.index,
            type: 'orders',
            body: {
                query: {
                    filtered: {
                        filter: query
                    }
                },
                aggs: aggregations
            }
        }).then(results => {
            return results.aggregations.orderlines.bundles.buckets.map(result => {
                return {
                    _id: result.key,
                    count: result.doc_count
                };
            });
        });

    },

    getAggregations() {
        return {};
    },

    getStatuses() {
        let status = this.get('status');

        if (status && status.contains('sameasbo1')) {
            status = _.union(_.without(status, 'sameasbo1'), _.range(10, 70));
        }

        if (Ember.isNone(status)) {
            status = ['60'];
        }
        if (!Ember.isArray(status)) {
            status = [status];
        }

        return status.map(item => parseInt(item, 10));
    },

    addVendorFilter(vendor) {
        this.get('vendorFilter').pushObject(vendor);
    }
});

export default Loader;
