import Loader from 'bo2/utils/statistics/loader';
import Ember from 'ember';

export default Loader.extend({
    revHist: [],
    avgRevHist: [],
    averageOrderMargeHist: [],
    margeHist: [],
    countries: Ember.A([]),
    paymentmethods: Ember.A([]),
    onlyCounts: true,

    interval: Ember.computed('from', 'to', function () {
        let diffDays = Math.abs(moment(this.get('from')).diff(this.get('to'), 'days'));

        if (diffDays < 2) {  // 2 days
            return 'hour';
        }

        if (diffDays < 60) {  // about 2 months
            return 'day';
        }

        return 'week';
    }),

    loadData() {
        if (!this.get('readyForLoading')) {
            return this;
        }

        let updateOrderData = (resp) => {
            this.setOrderValuesStatistics(resp);
            this.setDeliverMethodStatistics(resp);
            this.setCouponCodeStatistics(resp);
            this.setOrderLineStatistics(resp);
        };

        Ember.run.debounce(this, () => {
            this.ordersearch(
                this.timeSetter.getRange(),
                this.getStatuses(),
                this.getOrderAggregations()
            ).then(updateOrderData);
        }, 250);

        return this;
    },

    getOrderAggregations() {
        return {
            shipmentmethods: {terms: {field: 'DeliveryMethod'}},
            paymentmethods: {terms: {field: 'PaymentMethod'}},
            averageOrderAmount: {avg: {field: 'totalExclTax'}},
            averageDiscount: {avg: {field: 'allDiscount'}},
            totalOrderAmount: {sum: {field: 'totalExclTax'}},
            totalDiscount: {sum: {field: 'allDiscount'}},
            averageOrderMarge: {avg: {field: 'marge'}},

            history: {
                date_histogram: {
                    field: 'OrderDate',
                    interval: this.get('interval')
                },
                aggs: {
                    revenue: {sum: {field: 'totalExclTax'}},
                    avgOrderAmount: {avg: {field: 'totalExclTax'}},
                    averageOrderMarge: {avg: {field: 'marge'}}
                }
            },
            couponCampaigns: {terms: {field: 'coupon_source'}},
            lines: {
                nested: {
                    path: 'orderlines'
                },
                aggs: {
                    vendor: {terms: {field: 'Vendor'}},
                    couponsUsed: {terms: {field: 'extra_data.couponcode'}},
                    products: {
                        terms: {
                            field: 'ProdLevId'
                        }
                    },
                    averagePurchasePrice: {
                        avg: {field: 'orderlines.Pprice'}
                    },
                    bundlesOrdered: {
                        filter: {
                            term: {
                                'orderlines.discount_source.type': 'main'
                            }
                        }
                    },
                    totalBundleDiscount: {sum: {field: 'orderlines.discount'}}
                }
            }
        };
    },

    setOrderValuesStatistics(resp) {
        let aggs = resp.aggregations;
        let buckets = aggs.history.buckets;

        let parse = (dest, source = undefined) => {
            this.set(
                dest,
                buckets.map(item => {
                    return {x: item.key, y: source ? item[source].value : item.doc_count};
                })
            );
        };

        this.set('totalOrders', resp.hits.total);

        parse('revHist', 'revenue' );
        parse('avgRevHist', 'avgOrderAmount');
        parse('averageOrderMargeHist', 'averageOrderMarge');
        parse('nrOfOrdersHist');

        this.set('totalOrderAmount', aggs.totalOrderAmount.value);
        this.set('averageOrderAmount', aggs.averageOrderAmount.value);
        this.set('averageOrderMarge', aggs.averageOrderMarge.value);
        this.set('averageDiscount', aggs.averageDiscount.value);
        this.set('totalDiscount', aggs.totalDiscount.value);
    },

    setOrderLineStatistics(resp) {
        let aggs = resp.aggregations.lines;

        this.setVendorStatistics(aggs);
        this.setProductStatistics(aggs);
        this.setDiscountStatistics(aggs);
        this.set('averagePurchasePrice', aggs.averagePurchasePrice.value);
    },

    setCouponCodeStatistics(resp) {
        let couponCodes = resp.aggregations.couponCampaigns.buckets;

        this.set('couponCampaigns', [
            {
                key: 'couponCampaigns',
                values: couponCodes.map(item => {
                    let key = parseInt(item.key, 10);

                    if (key === 0) {
                        return;
                    }

                    if (key === '') {
                        key = '[empty]';
                    }
                    return {label: key, value: item.doc_count};
                }).compact()
            }
        ]);
    },

    setDeliverMethodStatistics(resp) {
        let deliveryMethods = resp.aggregations.shipmentmethods.buckets;

        this.set('deliverymethods', [
            {
                key: 'deliveryMethods',
                values: deliveryMethods.map((item) => {
                    let key = item.key;
                    if (key === '') {
                        key = '[empty]';
                    }
                    return {label: key, value: item.doc_count};
                })
            }
        ]);
    },

    setVendorStatistics(aggs) {
        let vendors = aggs.vendor.buckets;

        this.set('vendors', [
            {
                key: 'vendors',
                values: vendors.map((item) => {
                    let key = item.key;
                    if (key === '') {
                        key = '[empty]';
                    }
                    return {label: key, value: item.doc_count};
                })
            }
        ]);
    },

    setDiscountStatistics(aggs) {
        let coupons = aggs.couponsUsed.buckets;

        this.set('couponsUsed', coupons.reduce((memo, item) => {
            return memo + item.doc_count;
        }, 0));

        this.set('bundlesOrdered', aggs.bundlesOrdered.doc_count);
        this.set('totalBundleDiscount', aggs.totalBundleDiscount.value / 1.21);
    },

    setProductStatistics(aggs) {
        let products = aggs.products.buckets;

        if (Ember.isEmpty(products)) {
            this.set('products', []);
            return;
        }

        this.set('products', [
            {
                key: 'products',
                values: products.map((item) => {
                    let key = item.key;
                    if (key === '') {
                        key = '[empty]';
                    }
                    return {label: key, value: item.doc_count};
                })
            }
        ]);
    }
});
