import Loader from 'bo2/utils/statistics/loader';
import Ember from 'ember';

let Product = Ember.Object.extend({
    percentMarge: Ember.computed('marge.avg', 'euprice.value', function () {
        return ( ( this.get('marge.avg') / this.get('euprice.value') ) * 100).toPrecision(2);
    }),

    avgQuantity: Ember.computed('quant.avg', function () {
        return ( this.get('quant.avg') || 0.00 ).toPrecision(2);
    })
});

let ProductLoader = Loader.extend({
    products: [],
    total: {},
    onlyCounts: true,

    doLoad() {
        this.orderlinesSearch(
            this.timeSetter.getRange(),
            this.getStatuses(),
            this.getAggregations()
        ).then(this.updateData.bind(this));
    },

    loadData() {
        if (!this.get('readyForLoading')) {
            return this;
        }
        Ember.run.debounce(this, this.doLoad, 250);

        return this;
    },

    updateData(resp) {
        this.setProductStatistics(resp);
    },

    getAggregations() {
        return {
            lines: {
                nested: {
                    path: 'orderlines'
                },
                aggs: {
                    products: {
                        terms: {field: 'ProdLevId', size: 50},
                        aggs: {
                            vendor: {terms: {field: 'Vendor'}},
                            revenue: {stats: {field: 'revenue'}},
                            marge: {stats: {field: 'orderlines.marge'}},
                            euprice: {avg: {field: 'EUprice'}},
                            pprice: {avg: {field: 'Pprice'}},
                            quant: {stats: {field: 'Quant'}},
                            names: {terms: {field: 'Name'}}
                        }
                    },
                    revenue: {stats: {field: 'revenue'}},
                    marge: {stats: {field: 'orderlines.marge'}},
                    euprice: {avg: {field: 'EUprice'}},
                    pprice: {avg: {field: 'Pprice'}},
                    quant: {stats: {field: 'Quant'}}
                }
            }
        };
    },

    setProductStatistics(resp) {
        let total = resp.aggregations.lines;
        total.doc_count = resp.aggregations.lines.doc_count;
        this.set('total', Product.create(total));

        if (typeof resp.aggregations.lines.products === 'undefined') {
            return;
        }

        let buckets = resp.aggregations.lines.products.buckets;

        this.set(
            'products',
            buckets.map((item) => {
                return Product.create(item);
            })
        );
    }
});

export default ProductLoader;
