import Ember from 'ember';

var defaultColumnMap = {
    '0': 'prodlevid',
    '1': 'part',
    '2': 'description',
    '3': 'material',
    '4': 'percentage'
};

class Importer {
    constructor(columnMap, lineDelim, fieldDelim, onlySplitLines) {
        this.lineDelim = lineDelim || /\r\n|\r|\n/g;
        this.fieldDelim = fieldDelim || /\;|\,/g;
        this.importProducts = [];
        this.columnMap = columnMap || defaultColumnMap;
        this.onlySplitLines = onlySplitLines;
    }

    parseData(data) {
        return new Ember.RSVP.Promise((resolve) => {
            this.importProducts = [];

            _.each(data.split(this.lineDelim), (line) => {
                if (this.onlySplitLines) {
                    this.importProducts.push(line);
                    return;
                }

                if (line !== '' && !line.match(/prodlevid/i) && !line.match(/product/i)) {
                    this.handleLine(line);
                }
            });

            resolve(this.importProducts);
        });
    }

    handleLine(line) {
        var data = line.split(this.fieldDelim);

        var obj = {};
        data.forEach((value, idx)  => {
            if (this.columnMap[idx]) {
                obj[this.columnMap[idx]] = value;
            }
        });

        this.importProducts.push(Ember.Object.create(obj));
    }
}

export default Importer;
