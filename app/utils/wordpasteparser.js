import Ember from 'ember';

export default class WordPasteParser {

    constructor(input) {
        this.text = input;
    }

    parse() {
        if (/<!--StartFragment-->([^]*)<!--EndFragment-->/.test(this.text)) {
            this.text = this.text.match(/<!--StartFragment-->([^]*)<!--EndFragment-->/)[1];
        }
        this.text = this.text.replace(/<p class=(?:'|")?MsoNormal(?:'|")?>([\s\S]*)<\/p>/g, '<div>$1</div>\n');
        this.text = this.text.replace(/<p class=(?:'|")?MsoNoSpacing(?:'|")?(?:[\s\S]*?)>([\s\S]*?)<\/p>/g, '$1');
        this.text = this.text.replace(/<p class=(?:'|")?MsoTitle(?:'|")?>([\s\S]*?)<\/p>/g, '## $1');

        this.removeEmptyTags();
        this.removeComments();
        this.text = this.text.replace(/^\s*\n/gm, '');

        var $content = Ember.$('<div>' + this.text + '</div>');

        $content = this.removeInlineStyle($content);
        $content = this.removeAttributes($content);

        return $content.html();

    }

    removeComments() {
        this.text = this.text.replace(/<![^>]*>/g, '');
    }

    removeInlineStyle($content) {
        $content.find('*').removeAttr('style');
        return $content;
    }


    removeAttributes($content) {
        $content.find('*')
            .removeAttr('class')
            .removeAttr('lang');

        return $content;
    }

    removeEmptyTags() {
        this.text = this.text.replace(/<o:p>(&nbsp;)?<\/o:p>/g, '');
        this.text = this.text.replace(/&nbsp;/g, ' ');
    }
}
