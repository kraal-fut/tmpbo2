import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
    location: config.locationType
});

export default Router.map(function () {
  this.route('orders', function () {
      this.route('admin');
      this.route('itpakket');
      this.route('orders', function () {
          this.route('edit', {path: '/:id'}, function() {
              this.route('mail');
          });
      });
      this.route('processpayments');
      this.route('retours', function () {
          this.route('find', {path: '/:id'});
      });
      this.route('tradetrackers');

      this.route('customers', function () {
          this.route('edit', {path: '/:id'});
      });
  });

  this.route('product', function () {
      this.route('content', function () {
          this.route('edit', {path: '/:id'});
      });
      this.route('discount', function () {
        this.route('edit', {path: '/:id'});
        this.route('new', {});
      });
      this.route('imageupload');
      this.route('materials');
      this.route('productcomments');
      this.route('productquestions', function () {
          this.route('edit', {path: '/:id'});
      });
      this.route('suppliers');
      this.route('vendors', function () {
          this.route('edit', {path: '/:id'});
      });
      this.route('bundles', function () {
          this.route('edit', {path: '/:id'});
      });
  });

  this.route('shop', function () {
      this.route('articles', function () {
          this.route('edit', {path: '/:id'});
      });
      this.route('automails', function () {
          this.route('edit', {path: '/:id'});
      });
      this.route('banners');
      this.route('faqs', function () {
          this.route('edit', {path: '/:_id'});
      });
      this.route('fomenus');
      this.route('highlightcategories');
      this.route('holidays');
      this.route('keywordsales');
      this.route('landings');
      this.route('synonyms');

      this.route('fomenu', function () {
          this.route('edit', {path: '/:id'});
      });

      this.route('landing', function () {
          this.route('edit', {path: '/:id'});
      });
      this.route('actions', function () {
          this.route('edit', {path: '/:id'});
      });
      this.route('shippingmethods');
  });

  this.route('statistics', function () {
      this.route('productsales');
      this.route('order');
      this.route('bundles');
  });

  this.route('procurement', function () {
      this.route('advice', function () {
          this.route('edit', {path: '/:id'});
      });
      this.route('orders', function () {
          this.route('edit', {path: '/:id'});

          this.route('retours', function () {
              this.route('find');
          });

          this.route('customers', function () {
              this.route('edit');
          });
      });
      this.route('importstock');
  });

  this.route('login');
  this.route('settings');

  this.route('futurumclub', function() {
    this.route('newproducts', function() {});
  });
});
