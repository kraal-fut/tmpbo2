import Ember from 'ember';

export default Ember.Mixin.create({
    editing: false,
    notediting: Ember.computed.not('editing'),
    actions: {
        toggleEdit() {
            this.toggleProperty('editing');
        }
    }
});
