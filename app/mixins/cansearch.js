import Ember from 'ember';

export default Ember.Mixin.create({
    queryParams: {
        q: {refreshModel: true, scope: 'controller'}
    },
    originalPlaceHolder: undefined,

    setupController(controller, model) {
        this.set('originalPlaceHolder', this.get('searchSvc.searchPlaceholder'));
        this.set('searchSvc.searchPlaceholder', this.get('searchPlaceholder'));

        this._super(controller, model);
    },

    resetController(controller, isExiting) {
        if (isExiting && controller.unsetSearchHandler) {
            this.set('searchSvc.searchPlaceholder', this.get('originalPlaceHolder'));
            controller.unsetSearchHandler();
        }
    },

    dataUrl(params, url) {
        if (params && params.q) {
            url += '?find=' + params.q;
        }
        return this._super(params, url);
    }
});
