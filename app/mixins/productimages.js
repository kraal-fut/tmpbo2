import Ember from 'ember';
import ENV from 'bo2/config/environment';

export default Ember.Mixin.create({
    getBaseUrl() {
        return ENV.APP.fo2url;
    },

    getImages(images) {
        if (!Ember.isArray(images) && !_.isObject(images)) {
            return [];
        }

        var baseUrl = this.getBaseUrl();
        var result = Ember.A();

        images.map((image, key) => {
            var url = this.getUrl(baseUrl, image);
            result.pushObject(Ember.Object.create({
                id: key + 1,
                url: url,
                filename: image.get('filename') || '',
                prodlevid: image.get('prodlevid')
            }));
        });
        return result;
    },

    getUrl(baseUrl, image) {
        var url;
        if (image.get('oldSituation')) {
            url = baseUrl + '/img/thumbs/' + image.get('filename');
        } else if (image.get('RawData')) {
            url = image.get('RawData');
        } else {
            url = baseUrl + '/img/products/' + image.get('prodlevid') + '/thumb/' + image.get('filename');
        }
        return url;
    }
});
