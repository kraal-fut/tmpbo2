import Ember from 'ember';

export default Ember.Mixin.create({
    sorts: {},

    setSortsToQuery() {
        if (_.isEmpty(this.sorts)) {
            delete this.serverQueryParams.sort;
        }

        this.serverQueryParams.sort = JSON.stringify(this.sorts);
        this.refresh();
    },

    actions: {
        sort(filterColumn, sortDirection) {
            if (_.isUndefined(sortDirection)) {
                delete this.sorts[filterColumn];
            } else {
                this.sorts[filterColumn] = sortDirection;
            }

            this.setSortsToQuery();
        },
        removeFilter(filterColumn) {
            delete this.sorts[filterColumn];
            this.setSortsToQuery();
        },
        removeAllFilters() {
            this.sorts = {};
            this.setSortsToQuery();
        }
    }
});
