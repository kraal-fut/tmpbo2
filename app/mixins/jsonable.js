import Ember from 'ember';

export default Ember.Mixin.create({
    getJSON(includeComputeds, without) {
        var keys = _.difference(_.extend(Object.keys(this), includeComputeds), without || []);
        return this.getProperties.apply(this, keys);
    }
});
