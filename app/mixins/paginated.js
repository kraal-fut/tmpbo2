import Ember from 'ember';

export default Ember.Mixin.create({
    pagination: Ember.inject.controller('pagination'),
    queryParams: ['page', 'limit'],
    limit: 10
});
