import Ember from 'ember';
/**
 * Validation rules, beware: the methods should return __true__ when the field is NOT valid
 */
let rules = {
    required(field, criteria) {
        return criteria && Ember.isEmpty(field);
    },
    min(field, criteria) {
        return Ember.isEmpty(field) || field.length < criteria;
    },
    max(field, criteria) {
        if (Ember.isEmpty(field)) {
            return false;
        }
        return field.length >= criteria;
    },
    oneOf(field, criteria) {
        if (!Ember.isArray(criteria)) {
            return true;
        }
        return !criteria.contains(field);
    },
    email(field) {
        let re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
        return !re.test(field);
    },
    alphanum(field) {
        let expression = /^[a-z0-9]+$/i;
        return !expression.test(field);
    },
    pattern(field, criteria) {
        if (Ember.isEmpty(field)) {
            return false;
        }
        let re = new RegExp(criteria);
        return !re.test(field);
    },
    hasRelateds(field) {
        return !field.reduce((memo, item)=> {
            var itemInvalid = item.validateSync(item, item.validations);
            if (memo) {
                return itemInvalid;
            }
            return memo;
        }, true);
    }
};

let messages = {
    required(field, value) {
        let rtext = value ? 'required' : 'not required';
        return `The field ${field} is ${rtext}.`;
    },
    min(field, value) {
        return `The field ${field} should be at least ${value} long.`;
    },
    max(field, value) {
        return `The field ${field} should be at most ${value} long.`;
    },
    oneOf(field) {
        return `The field ${field} does not have a correct value.`;
    },
    email(field) {
        return `The field ${field} should be a valid email format.`;
    },
    alphanum(field) {
        return `The field ${field} should only contain alphanumeric charaters (A-Z and 0-9)`;
    },
    hasRelateds(field) {
        return `One of the items in ${field} is not valid`;
    },

    pattern(field) {
        return `The field ${field} should match the valid format`;
    }
};

export function containsAny(tasks, rules) {
    return rules.filter(n => tasks.indexOf(n) !== -1).length > 0;
}

export function checkWhen(rule, task) {
    if (typeof rule === 'string') {
        return true;
    }

    let isAllways = function () {
        return rule.when === 'all' || rule.when === 'allways';
    };

    let hasNoTaskOrWhen = function () {
        return Ember.isNone(task) || Ember.isNone(rule.when);
    };

    if (hasNoTaskOrWhen() || isAllways()) {
        return true;
    }

    let tasks = typeof task === 'string' ? [task] : task;
    let whens = typeof rule.when === 'string' ? [rule.when] : rule.when;
    return containsAny(whens, tasks);
}

export default Ember.Mixin.create({
    init() {
        this._super();
        this.errors = Ember.A([]);
    },

    validateSync(model, validations, options) {
        options = options || {};

        let errors = model.get('errors');
        errors.clear();

        for (let field in validations) {
            if (validations.hasOwnProperty(field)) {
                let f = validations[field];

                for (let ruleName in f) {
                    if (f.hasOwnProperty(ruleName)) {
                        let rule = f[ruleName];
                        let value = Ember.typeOf(rule) === 'object' ? rule.value : rule;
                        if (checkWhen(rule, options.task) && rules[ruleName]) {
                            let invalid = rules[ruleName].call(model, model.get(field), value);

                            let niceName = f.niceName ? `<i>${f.niceName}</i>` : field;

                            if (invalid) {
                                errors.pushObject({
                                    field: field,
                                    reason: ruleName,
                                    value: value,
                                    message: messages[ruleName](niceName, value)
                                });
                            }
                        }
                    }
                }
            }
        }

        return !errors.get('length');
    },

    validate(options) {
        options = options || {};
        let model = this;
        let validations = model.validations;

        return new Ember.RSVP.Promise((resolve, reject) => {
            let result = this.validateSync(model, validations, options);

            if (result) {
                resolve(model);
            } else {
                reject(model.get('errors'));
            }
        });
    },

    setServerValidationErrors(errors) {
        let model = this;

        for (let key in errors) {
            if (errors.hasOwnProperty(key)) {
                errors[key].forEach(message => {
                    let errorObj = {
                        field: key,
                        reason: 'servervalidation',
                        value: 'nogtebepalen',
                        message: message
                    };

                    model.get('errors').pushObject(errorObj);
                });
            }
        }
        return true;
    }
});
