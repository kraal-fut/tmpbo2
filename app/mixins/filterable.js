import Ember from 'ember';

export default Ember.Mixin.create({
    filters: {},

    setFiltersToQuery() {
        if (_.isEmpty(this.filters)) {
            delete this.serverQueryParams.find;
        }
        this.serverQueryParams.find = JSON.stringify(this.filters);
        this.refresh();
    },

    actions: {
        willTransition(transition) {
            if (transition.targetName !== this.routeName) {
                delete this.serverQueryParams.find;
                delete this.serverQueryParams.page;
            }
            return true;
        },

        filter(filterColumn, filterValue) {
            if (_.isUndefined(filterValue)) {
                delete this.filters[filterColumn];
            } else {
                this.filters[filterColumn] = filterValue;
            }
            this.serverQueryParams.page = 1;
            this.setFiltersToQuery();
        },

        removeFilter(filterColumn) {
            delete this.filters[filterColumn];
            if (this.state) {
                this.state.set('itemsPerPage', 10);
            }
            this.setFiltersToQuery();
        },

        removeAllFilters() {
            this.filters = {};
            if (this.state) {
                this.state.set('itemsPerPage', 10);
            }
            this.setFiltersToQuery();
        }
    }
});
