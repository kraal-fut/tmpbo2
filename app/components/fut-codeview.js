import Ember from 'ember';
import codemirror from 'codemirror';

let cmOptions = {
    theme: 'monokai',
    mode: 'mustache',
    lineNumbers: true,
    lineWrapping: false,
    matchBrackets: true,
    autoCloseBrackets: true,
    autoCloseTags: true,
    matchTags: true,
    indentUnit: 4,
    viewportMargin: Infinity
};

export default Ember.Component.extend({
    tagName: 'textarea',

    didInsertElement() {
        this.addMustacheMode();

        let cm = codemirror.fromTextArea(this.$().get(0), cmOptions);

        cm.setValue(this.get('value'));

        cm.on('change', (instance) => {
            Ember.run(() => {
                this.set('value', instance.getValue());
            });
        });

        this.set('editor', cm);
    },

    addMustacheMode() {
        codemirror.defineMode('mustache', (config, parserConfig) => {
            let mustacheOverlay = {
                token: stream => {
                    var ch;
                    if (stream.match('{{')) {
                        while (( ch = stream.next() )) {
                            if (ch === '}' && stream.next() === '}') {
                                stream.eat('}');
                                return 'mustache';
                            }
                        }
                    }
                    while (stream.next() && !stream.match('{{', false)) {
                    }
                    return null;
                }
            };

            return codemirror.overlayMode(
                codemirror.getMode(config, parserConfig.backdrop || 'text/html'),
                mustacheOverlay
            );
        });
    }
});
