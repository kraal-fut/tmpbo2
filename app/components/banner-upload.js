import Ember from 'ember';
import ENV from 'bo2/config/environment';

export default Ember.Component.extend({
    hovering: false,
    hoverTimer: undefined,
    hasImage: Ember.computed.or('uploadImage', 'banner'),
    hasLabel: Ember.computed.bool('label'),

    maxHeight: '2em',
    minHeight: null,
    showoutline: Ember.computed.or('hovering', 'alwaysOutline'),

    getImageContent: Ember.computed('uploadImage', 'banner', 'path', function () {
        if (this.get('uploadImage') && this.get('uploadImage.Image')) {
            return this.get('uploadImage.Image');
        }

        let path = this.get('path');
        let banner = this.get('banner');

        if (banner && banner.indexOf('http') > -1) {
            return banner;
        }

        let base = this.get('session.appSettings.BASE_URL') || ENV.APP.fo2url;
        let url = base + '/';
        if (path) {
            url += '/' + path + '/';
        }
        url += this.get('banner');

        return url;
    }),

    safeStyle: Ember.computed('style', 'width', function () {
        let style = this.get('style') || '';
        if (this.get('width')) {
            style += ';max-width: ' + this.get('width') + ';overflow-x:hidden;';
        }
        style += ';min-width: 90%;min-height: 2em';
        return new Ember.Handlebars.SafeString(style.htmlSafe());
    }),

    safeMaxStyle: Ember.computed('maxHeight', 'minHeight', function() {
        let style = '';
        if (this.get('minHeight')) {
            style += 'min-height: ' + this.get('minHeight') + '; ';
        }
        style += 'max-height: ' + this.get('maxHeight') + ';';
        return new Ember.Handlebars.SafeString(style.htmlSafe());
    }),

    mouseEnter(event) {
        if (!this.get('width')) {
            return false;
        }

        this.hoverTimer = setTimeout(() => {
            this.$(event.currentTarget).find('.imageDropArea').animate({'max-width': 400});
        }, 300);

        return false;
    },

    mouseLeave(event) {
        clearTimeout(this.hoverTimer);
        if (!this.get('width')) {
            return false;
        }
        this.$(event.currentTarget).find('.imageDropArea').animate({'max-width': this.get('width')});
        return false;
    },

    dragEnter(event) {
        if (this.get('editing')) {
            this.set('hovering', true);
        }
        event.preventDefault();
    },

    dragOver(event) {
        event.preventDefault();
    },

    dragLeave(event) {
        this.set('hovering', false);
        event.preventDefault();
    },

    drop(event) {
        event.preventDefault();
        if (this.get('editing')) {
            this.setFiles(event.originalEvent.dataTransfer.files);
        }
    },

    setFiles(files) {
        _.forEach(files, file => {
            this.setFile(file).then( uploadImage => {
                this.sendAction('imageDropped', uploadImage);
            });
        });
    },

    setFile(f) {
        return new Ember.RSVP.Promise(resolve => {
            let reader = new FileReader();

            reader.onload = ((file) => {
                return e => {
                    let obj = {ImageFilename: file.name, Image: e.target.result}
                    this.set('uploadImage', obj);
                    resolve(obj);
                };
            })(f);

            reader.readAsDataURL(f);
        });
    }
});
