import Ember from 'ember';

export default Ember.Component.extend({
    actions: {
        collapse(id) {
            this.$('#' + id).collapse('toggle');
            var $collapser = Ember.$('#' + id).prev('.collapser');
            $collapser.toggleClass('open');
            $collapser.children('i').toggleClass('fa-plus-square-o fa-minus-square-o');
        },
        filter(id, model) {
            this.sendAction('getChildren', id, model);
        },
        itemClicked(id, model) {
            this.sendAction('itemClicked', id, model);
        }

    }
});
