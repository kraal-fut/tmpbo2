import Ember from 'ember';

export default Ember.Component.extend({
    sortProperties: ['id:asc'],
    sortedImages: Ember.computed.sort('images', 'sortProperties'),

    dragLeave(event) {
        event.preventDefault();
        return false;
    },

    actions: {
        deleteImage(image, images) {
            this.sendAction('deleteImage', image, images);
        }
    }
});
