import Ember from 'ember';

export default Ember.Component.extend({
    tagName: '',

    getSearched: Ember.computed('query', 'lines', function () {
        let q = this.get('query');
        let withProdLevId = this.get('lines').filterBy('prodlevid', q);

        let withProdId = this.get('lines').filterBy('prodid', q);
        return withProdLevId.concat(withProdId);
    }),

    isSearched: Ember.computed('getSearched', function () {
        return !Ember.isEmpty(this.get('getSearched'));
    })
});
