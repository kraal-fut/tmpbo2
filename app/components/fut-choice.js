import Ember from 'ember';
import FutInput from 'bo2/components/fut-input';
const {computed} = Ember;

export default FutInput.extend({
    content: [],

    componentContent: computed('content', 'valuePath', 'labelPath', function () {
        return this.get('content').map((item) => {
            return {
                value: this.get('valuePath') === 'content' ? item : item[this.get('valuePath')],
                label: this.get('labelPath') === 'content'? item : item[this.get('labelPath')]
            };
        });
    }),

    valuePath: computed('optionValuePath', function () {
        return this.get('optionValuePath') || 'content';
    }),

    labelPath: computed('optionLabelPath', function () {
        return this.get('optionLabelPath') || 'content';
    }),

    showPrompt: computed('prompt', function () {
        return this.get('prompt') || undefined;
    }),

    selectChanged: function () {
        if (typeof this.get('changedAction') === 'undefined') {
            return;
        }
        this.$('select').on('change', () => {
            this.sendAction('changedAction', parseInt(this.$('select').val(), 10));
        });
    }.on('didInsertElement'),

    cleanup: function () {
        this.$('select').off('change');
    }.on('willDestroyElement')
});
