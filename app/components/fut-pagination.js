import Ember from 'ember';
const {alias, bool} = Ember.computed;
let throttled;

export default Ember.Component.extend({
    perPageOptions: [5, 10, 20, 30],

    totalRecords: alias('data.state.totalRecords'),
    perPage: alias('data.perPage'),
    totalPages: alias('data.totalPages'),

    currentPage: alias('data.currentPage'),
    hasNumberFound: bool('totalRecords'),

    pageSet: Ember.computed('currentPage', 'totalPages', function () {
        let currentPage = this.get('currentPage');
        let totalPages = this.get('totalPages') || Infinity;

        return _.map(_.first(_.reject(_.range(currentPage - 2, currentPage + 5), num => {
            return num < 1 || num > totalPages;
        }), 5), num => {
            return {num: num, active: num === currentPage};
        });
    }),

    doTransition() {
        throttled = typeof throttled === 'undefined' ? function() {
            this.sendAction('pageChanged', this.get('currentPage'));
        } : throttled;

        Ember.run.debounce(this, throttled, 200);
    },

    actions: {
        gotoPage(page) {
            this.set('currentPage', parseInt(page, 10));
            this.doTransition();
        },
        firstPage() {
            this.set('currentPage', 1);
            this.doTransition();
        },
        prevPage() {
            if (this.get('currentPage') <= 1) {
                return;
            }

            if (this.decrementProperty('currentPage') > this.get('totalPages')) {
                this.set('page', this.get('totalPages'));
            }

            this.doTransition();
        },
        nextPage() {
            if (this.get('currentPage') < this.get('totalPages')) {
                this.incrementProperty('currentPage');
                this.doTransition();
            }
        },
        lastPage() {
            this.set('currentPage', this.get('totalPages'));
            this.doTransition();
        },
        perPageChanged(count) {
            this.set('perPage', count);
            this.doTransition();
        }
    }
});
