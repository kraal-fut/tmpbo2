import Ember from 'ember';

export default Ember.Component.extend({
    title: 'BarChart',
    fieldName: '',
    data: [],
    classNameBindings: 'class',
    multi: false,
    labels: undefined,

    dataChanged: Ember.observer('data', function () {
        d3.select(this.$('svg')[0]).datum(this.get('data')).call(this.chart);
    }),

    didInsertElement() {
        nv.addGraph(() => {
                let chartBuilder;

                if (this.get('multi')) {
                    chartBuilder = nv.models.multiBarChart;
                } else {
                    chartBuilder = nv.models.discreteBarChart;
                }

                this.chart = chartBuilder()
                    .x(d => {
                        let label = d.label;
                        if (this.get('labels') && this.get('labels')[label]) {
                            label = this.get('labels')[label];
                        }
                        return label;
                    }).y(d => {
                        return d.value;
                    })
                    .staggerLabels(true)
                    .duration(250);

                if (!this.get('multi')) {
                    this.chart.showValues(true);
                }

                return this.chart;
            }
        );
    }
});
