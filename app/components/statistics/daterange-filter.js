import Ember from 'ember';

export default Ember.Component.extend({
    ts: Ember.computed.alias('model.timeSetter'),

    actions: {
        lastmonth() {
            this.get('ts').getLastMonth();
        },
        thismonth() {
            this.get('ts').getThisMonth();
        },
        lastweek() {
            this.get('ts').getLastWeek();
        },
        thisweek() {
            this.get('ts').getThisWeek();
        },
        yesterday() {
            this.get('ts').getYesterdayTime();
        },
        daybefore() {
            this.get('ts').getDayBefore();
        },
        dayafter() {
            this.get('ts').getDayAfter();
        },
        weekbefore() {
            this.get('ts').getWeekBefore();
        },
        weekafter() {
            this.get('ts').getWeekAfter();
        },
        today() {
            this.get('ts').getToday();
        }
    }
});
