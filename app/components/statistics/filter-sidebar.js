import Ember from 'ember';

export default Ember.Component.extend({
    model: undefined,

    futRest: Ember.inject.service('futrest'),

    statuses: [{
        id: 'sameasbo1',
        description: 'Same as bo1'
    }],

    availableCountries: ['nederland', 'belgie', 'deutschland'],
    availablePaymentmethods: [
        'ideal',
        'overboeking',
        'duitsland',
        'ogonestd',
        'nazend',
        'pin',
        'paypal',
        'mastercard',
        'rmbrs',
        'visa',
        'mistercash',
        'bibit',
        'rabo',
        'aexpress',
        'de_rmbrs',
        'rembours',
        'creditcard',
        'onbekend',
        'überweisung'
    ],

    loadData: function () {
        let promises = [];
        promises.push(this.getStatuses());
        promises.push(this.getVendors());
        Ember.RSVP.all(promises).then(()=> {
            this.get('model').set('readyForLoading', true);
        });
    },

    getStatuses() {
        return this.futRest.loadData('/status/list').then(statuses => {
            this.get('statuses').pushObjects(statuses);
            return this.get('statuses');
        });
    },

    getVendors() {
        return this.futRest.loadData('/vendors/list').then(vendors => {
            let v = vendors.vendor.map(vendor => {
                if (!Ember.isNone(vendor.displayname)) {
                    vendor.name = vendor.displayname;
                }
                return vendor;
            });
            v.unshiftObject({id: null, name: 'All vendors'});
            return v;
        }).then(vendors => {
            this.set('vendors', vendors);
            return this.get('vendors');
        });
    },

    didInsertElement() {
        this.loadData();
    },

    actions: {
        sameasbo1() {
            this.set('model.status', ['sameasbo1']);
        },
        statuscancelled() {
            this.set('model.status', [90, 91, 93, 94, 99]);
        },
        statuscomplete() {
            this.set('model.status', [60]);
        },
        allstatuses() {
            this.set('model.status', []);
        },
        clearvendor() {
            this.set('model.vendorFilter', []);
        },
        clearcats() {

        }
    }
});
