import Ember from 'ember';

export default Ember.Component.extend({
    isHovering: false,
    attributeBindings: ['classes'],
    classes: Ember.computed.alias('class'),

    dragOver(event) {
        event.preventDefault();
    },

    dragEnter(event) {
        event.preventDefault();
        this.set('isHovering', true);
    },

    dragLeave(event) {
        this.set('isHovering', false);
        event.preventDefault();
    },

    drop(event) {
        event.preventDefault();
        this.set('isHovering', false);

        this.handleDrop(event).then(processeddata => {
            this.sendAction('dropped', processeddata);
        }).catch(error => {
            console.error('dropping failed', error);
        });
    },

    handleDrop(event) {
        return new Ember.RSVP.Promise(resolve => resolve(event));
    }
});
