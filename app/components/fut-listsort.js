import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'span',
    sortDirection: undefined,
    field: '',

    actions: {
        toggleSort() {
            let sd = this.get('sortDirection');

            if (Ember.isNone(sd)) {
                this.set('sortDirection', 'asc');
            } else if (sd === 'asc') {
                this.set('sortDirection', 'desc');
            } else {
                this.set('sortDirection', undefined);
            }

            this.sendAction('setSort', this.get('field'), this.get('sortDirection'));
        }
    },

    isAsc: Ember.computed.equal('sortDirection', 'asc'),
    isDesc: Ember.computed.equal('sortDirection', 'desc'),
    hasSort: Ember.computed.or('isAsc', 'isDesc'),
    isNatural: Ember.computed.not('hasSort')
});
