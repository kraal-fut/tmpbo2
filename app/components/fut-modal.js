import Ember from 'ember';

export default Ember.Component.extend({
    actions: {
        ok() {
            this.$('.modal').modal('hide');
            this.sendAction('close');
        }
    },

    didInsertElement() {
        this.$('.modal').modal();
        this.$('.modal').modal().on('hidden.bs.modal', () => {
            this.sendAction('close');
        });
    }
});
