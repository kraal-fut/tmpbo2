import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'div',
    classNames: '',
    placement: 'bottom',
    title: 'bo2',
    filterOpen: false,

    didInsertElement() {
        let contents = this.$('.popovercontent');

        this.$().popover({
            animation: true,
            placement: this.get('placement'),
            html: true,
            trigger: 'manual',
            content: contents,
            title: this.get('title')
        });

    },

    filterOpenChanged: Ember.observer('filterOpen', function() {
        if (this.get('filterOpen')) {
            this.$('.popovercontent').removeClass('hide');
            this.$().popover('show');

            return;
        }

        this.$().popover('hide');
    }),

    actions: {
        close() {
            this.set('filterOpen', false);
        }
    }
});
