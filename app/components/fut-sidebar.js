import Ember from 'ember';

export default Ember.Component.extend( {
    isOpen: false,

    actions: {
        toggleSidebar() {
            this.toggleProperty('isOpen');
        }
    }
} );