import Ember from 'ember';
import {fo2url} from 'bo2/helpers/fo2-url';

export default Ember.Component.extend({
    tagName: 'span',
    type: 'product',

    defaultTitle: Ember.computed('type', function () {
        return 'Preview ' + this.get('type');
    }),

    url: Ember.computed('href', 'value', 'type', function () {
        if (this.get('href')) {
            return this.get('href');
        }

        let o = {
            type: this.get('type'),
            anchor: this.get('anchor')
        };

        return fo2url(this.get('value'), o);

    }),

    outputTitle: Ember.computed.or('title', 'defaultTitle')
});
