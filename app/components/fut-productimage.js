import Ember from 'ember';

export default Ember.Component.extend({
    classNames: ['imageContainer'],
    classNameBindings: ['isDragging:drag'],

    attributeBindings: ['draggable'],
    draggable: 'true',
    isDragging: false,

    dragStart(event) {
        this.set('isDragging', true);
        event.dataTransfer.setData('text/data', this.get('image.id'));
        return true;
    },

    dragOver(event) {
        this.stopEvent(event);
    },

    dragEnd(event) {
        this.set('isDragging', false);
        this.stopEvent(event);
    },

    drop(event) {
        let id = parseInt(event.dataTransfer.getData('text/data'), 10);

        let droppedOnImage = this.get('image');

        if (id === droppedOnImage.get('id')) {
            return false;
        }

        let draggingImage = this.get('images').findBy('id', id);
        draggingImage.set('id', droppedOnImage.get('id'));
        droppedOnImage.set('id', id);

        return false;
    },

    stopEvent(event) {
        event.preventDefault();
        event.stopPropagation();
        return false;
    },

    actions: {
        deleteImage() {
            this.sendAction('deleteImage', this.get('image'), this.get('images'));
        }
    }
});
