import Ember from 'ember';
const {computed} = Ember;

export default Ember.Component.extend({
    editing: false,
    nostyle: false,
    deleteable: computed('delete', function () {
        return !Ember.isEmpty(this.get('delete'));
    }),

    notediting: computed.not('editing'),
    showDelete: computed.and('editing', 'deleteable'),

    tagName: 'tr',

    actions: {
        toggleEdit() {
            this.toggleProperty('editing');
        },
        save(model) {
            this.sendAction('save', model, this);
        },
        deleteModel(model) {
            this.sendAction('delete', model);
        }
    }
});
