import Ember from 'ember';
const {computed} = Ember;

export default Ember.Component.extend({
    classNameBindings: ['hasError', 'hasFeedback:has-feedback', 'formGroup'],
    formGroup: true,
    hasLabel: computed.bool('label'),
    model: undefined,
    field: undefined,
    feedback: undefined,

    omission: '...',

    actions: {
        onEnter(value) {
            this.sendAction('enter', value, this.get('model'));
        }
    },
    componentValue: computed('model.[]', 'field', 'value', {
        get() {
            if (this.get('value')) {
                return this.get('value');
            }
            return this.get('model.' + this.get('field'));
        },
        set(key, value) {
            if (this.get('field')) {
                this.set('model.' + this.get('field'), value);
            } else {
                this.set('value', value);
            }
            return value;
        }
    }),

    componentDisabled: computed('editing', 'notediting', 'disabled', function () {
        let e = this.get('editing');
        let n = this.get('notediting');
        let d = this.get('disabled');
        let en = this.get('enabled');

        return e === false || en === false || n === true || d === true;
    }),

    allErrors: computed.alias('model.errors'),

    errors: computed('allErrors.[]', 'field', function () {
        if (!this.get('allErrors')) {
            return [];
        }
        return this.get('allErrors').filterBy('field', this.get('field'));
    }),

    hasError: computed.bool('errors.length'),
    hasFeedback: computed('feedback', 'hasError', function () {
        return !!this.get('feedback') || this.get('hasError');
    }),

    componentClasses: computed('class', function () {
        return this.get('class') + " form-control";
    }),

    errorsChanged: function () {
        if (this.get('hasError')) {
            this.$('input').focus();
        }
    }.observes('hasError')
});
