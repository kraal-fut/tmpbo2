import Ember from 'ember';
import ImportCsv from 'bo2/utils/importCsv';
import DropArea from 'bo2/components/fut-droparea';

export default DropArea.extend({
    layoutName: 'components/fut-droparea',

    columnMap: undefined,
    lineDelim: undefined,
    fieldDelim: undefined,
    onlySplitLines: false,

    handleDrop(event) {
        return new Ember.RSVP.Promise((resolve, reject) => {
            const file = event.dataTransfer.files[0];
            const reader = new FileReader();

            const importer = new ImportCsv(
                this.get('columnMap'),
                this.get('lineDelim'),
                this.get('fieldDelim'),
                this.get('onlySplitLines')
            );
            reader.readAsText(file);
            reader.onload = (evt) => {
                importer.parseData(evt.target.result)
                    .then(importeds => resolve(importeds));
            };

            reader.onerror = () => {
                reject(new Error('Something went wrong with reading the file'));
            };
        });
    },

    uploadFail() {
        if (confirm('The import contains errors do you wish to download these?')) {
            window.location = this.baseUrl + '/download';
        }
    }
});
