import Ember from 'ember';

export default Ember.Component.extend({
    classNames:['navbar-user','pull-right'],
    actions:{
        search(value) {
            this.sendAction('search', value);
        },
        login() {
            this.sendAction('login');
        },

        logout() {
            this.get('session.session').invalidate();
        }
    }
});
