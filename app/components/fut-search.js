import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'span',
    searchSvc: Ember.inject.service('search'),

    searchQuery: '',
    searchPlaceholder: Ember.computed.reads('searchSvc.searchPlaceholder'),

    actions: {
        search() {
            return this.sendAction('search', this.get('searchQuery'));
        }
    }
});
