import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'span',
    dataType: 'string',
    filterOpen: false,
    isStringType: Ember.computed.equal('dataType', 'string'),

    hasValue: Ember.computed.notEmpty('filtervalue'),

    clear() {
        this.set('filtervalue', undefined);
        this.sendAction('clearFilter', this.get('field'));
        this.set('filterOpen', false);
    },

    actions: {
        toggleFilter() {
            this.toggleProperty('filterOpen');
        },

        close() {
            this.set('filterOpen', false);
        },

        filter() {
            if (!this.get('hasValue')) {
                this.clear();
            }

            let value = this.get('filtervalue');
            if (this.get('dataType') === 'number') {
                value = parseInt(value, 10);
            }

            this.sendAction('setFilter', this.get('field'), value);
            this.set('filterOpen', false);
        },

        clear()  {
           this.clear();
        }
    }
});
