import Ember from 'ember';

export default Ember.Component.extend({
    picker: undefined,
    showWeekNumbers: true,
    timePicker24Hour: true,
    timePicker: false,
    showDropdowns: true,
    format: 'DD-MM-YYYY HH:mm:ss',
    singleDatePicker: true,

    didInsertElement() {
        let component = this;
        Ember.run.scheduleOnce('afterRender',() => {
            component.picker = this.$('input').daterangepicker({
                    format: this.get('format'),
                    startDate: this.get('from'),
                    endDate: this.get('to'),
                    showWeekNumbers: this.get('showWeekNumbers'),
                    timePicker: this.get('timePicker'),
                    timePicker24Hour: this.get('timePicker24Hour'),
                    showDropdowns: this.get('showDropdowns'),
                    ranges: this.get('ranges'),
                    minDate: this.get('minDate'),
                    maxDate: this.get('maxDate'),
                    dateLimit: this.get('dateLimit'),
                    cancelClass: 'btn-danger',
                    singleDatePicker: this.get('singleDatePicker')
                },
                (start, end) => {
                    Ember.run.next(() => {
                        if (component.get('singleDatePicker')) {
                            component.set('value', end);
                        } else {
                            component.set('from', start);
                            component.set('to', end);
                        }
                    });

                }).data('daterangepicker');
        });
    },

    value: Ember.computed('from', 'to', function() {
        if (this.picker) {
            this.picker.setStartDate(this.get('from'));
            this.picker.setEndDate(this.get('to'));
        }

        let format = 'MM/DD/YYYY HH:mm:ss';
        let from = this.get('from').format(format);
        let to = this.get('to').format(format);
        return `${from} - ${to}`;
    } )
});
