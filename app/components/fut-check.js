import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'span',
    hasLabel: Ember.computed.bool('label'),
    enabled: false,

    isEnabled: Ember.computed.or('enabled', 'editing'),

    actions: {
        check(){
            if (!this.get('isEnabled')) {
                return;
            }
            this.toggleProperty('checked');
        }
    }
});
