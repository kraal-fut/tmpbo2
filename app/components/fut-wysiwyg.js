import FutInput from 'bo2/components/fut-input';
import WordPasteParser from 'bo2/utils/wordpasteparser';

var getClipboardText = function (event, type) {
    type = type || 'text/html';

    var pastedText;

    if (window.clipboardData && window.clipboardData.getData) { // IE
        pastedText = window.clipboardData.getData(type);
    } else if (event.clipboardData && event.clipboardData.getData) {
        pastedText = event.clipboardData.getData(type);
    }
    if (pastedText === '' && type === 'text/html') {
        return getClipboardText(event, 'text/plain');
    }

    return pastedText;
};

var replaceSelectionWithHtml = function (html) {
    var range;
    if (window.getSelection && window.getSelection().getRangeAt) {
        range = window.getSelection().getRangeAt(0);
        range.deleteContents();
        var div = document.createElement('div');
        div.innerHTML = html;
        var frag = document.createDocumentFragment(), child;
        while ((child = div.firstChild)) {
            frag.appendChild(child);
        }
        range.insertNode(frag);
    } else if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        range.pasteHTML(html);
    }
};

export default FutInput.extend({
    height: 300,
    disableDragAndDrop: true,

    show: function () {

        var component = this;

        this.$('.wysi').summernote({
            onChange: function (value) {
                component.set('componentValue', value);
            },

            height: component.get('height'),
            focus: component.get('focus'),
            insertTableMaxSize: {
                col: 20,
                row: 20
            },
            airMode: false,
            disableDragAndDrop: component.get('disableDragAndDrop'),
            toolbar: [
                ['style', ['style']], // no style button
                ['style', ['bold', 'italic', 'underline', 'clear']],
//                    ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
//                    ['height', ['height']],
//                    ['insert', ['picture', 'link']], // no insert buttons
                ['insert', ['link']],
                ['table', ['table']], // no table button
                ['view', ['codeview']]
                //['help', ['help']] //no help button
            ],

            codemirror: {
                theme: 'monokai',
                mode: 'htmlmixed',
                lineNumbers: true,
                lineWrapping: false
            },

            onPaste: function (event) {
                event = event.originalEvent;
                event.preventDefault();

                var pastedText = getClipboardText(event);
                var parser = new WordPasteParser(pastedText);
                replaceSelectionWithHtml(parser.parse());
            }
        });
    }.on('didInsertElement')
});
