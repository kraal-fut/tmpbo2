import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'span',
    classNames: 'pull-right',
    showingTemplates: false,
    mailTemplate: undefined,

    actions: {
        showTemplates() {
            this.toggleProperty('showingTemplates');
        },
        makeMail() {
            if (!this.get('mailTemplate')) {
                return;
            }
            this.sendAction('makeMail', this.get('mailTemplate'));
        }
    }
});
