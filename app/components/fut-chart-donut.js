import Ember from 'ember';

export default Ember.Component.extend({
    title: 'DonutChart',
    fieldName: '',
    data: [],
    classNameBindings: 'class',

    dataChanged: Ember.observer('data', function () {
        d3.select(this.$('svg')[0]).datum(this.get('data')).call(this.chart);
    }),

    didInsertElement() {
        nv.addGraph(() => {
            this.chart = nv.models.pieChart()
                .x(d => {
                    return d.label;
                }).y(d => {
                    return d.value;
                })
                .donut(true)
                .showLegend(false)
                .padAngle(0.05)
                .cornerRadius(5)
                .duration(250);


            this.chart.pie.labelsOutside(false).donut(true);

            return this.chart;
        });
    }
});
