import Ember from 'ember';

export default Ember.Component.extend({
    title: 'LineChart',
    fieldName: '',
    data: [],
    domains: [],
    interval: 'week',
    scale: 1,
    classNameBindings: 'class',

    dataChanged: Ember.observer('data.avgRevHist', 'data.averageOrderMargeHist', 'data.nrOfOrdersHist', function () {
        d3.select(this.$('svg')[0]).datum(this.getData()).call(this.chart);
    }),

    getData() {
        return this.get('domains').map(item => {
            item.values = this.get('data.' + item.fieldname) || [];
            return item;
        });
    },

    didInsertElement() {
        this.makeChart();
        this.dataChanged();
    },

    makeChart() {
        var chart = nv.models.lineChart()
            .showYAxis(true)
            .showXAxis(true)
            .useInteractiveGuideline(true);

        chart.xAxis
            .axisLabel('Time')
            .tickFormat(d => {
                let format = '%d %b %Y';

                if (this.get('interval') === 'hour') {
                    format = '%X';
                }
                return d3.time.format(format)(moment(d).subtract(2, 'hours').toDate());
            });

        chart.yAxis
            .axisLabel('Euro')
            .tickFormat(d3.format('.02f'));

        nv.addGraph(() => {
            nv.utils.windowResize(chart.update);
            return chart;
        });

        this.chart = chart;
    }
});
