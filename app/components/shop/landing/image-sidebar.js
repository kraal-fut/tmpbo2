import Ember from 'ember';

export default Ember.Component.extend({

    imageHost: Ember.computed.alias('session.appSettings.BASE_URL'),

    actions: {
        refreshImages(id) {
            this.sendAction('refreshImages', id);
        },

        deleteImage(image) {
            this.sendAction('deleteImage', image);
        },
        addUploadImage(file) {
            this.sendAction('addUploadImage', file, this.get('model'));
        }
    }
});
