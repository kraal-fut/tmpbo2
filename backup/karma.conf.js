// Karma configuration
// Generated on Mon Jul 15 2013 09:08:56 GMT+0200 (W. Europe Daylight Time)

module.exports = function ( karma ) {
    'use strict';

    karma.set({
        basePath: process.cwd(),

        singleRun: true,
        captureTimeout: 7000,
        autoWatch: true,

        reporters: ['dots','coverage'],
        browsers: ['PhantomJS'],

        // Change this to the framework you want to use.
        frameworks: ['mocha','sinon-chai'],

        preprocessors: {
            'app/**/*.js': 'coverage'
        },

        coverageReporter: {
            type: 'lcov',
            dir: 'test/coverage'
        },

        files: [
            'vendor/bower/requirejs/require.js',
            'test/runner.js',

            { pattern: 'app/**/*.*', included: false, served:true, watched:true },
            {
                pattern: 'test/mocha/**/*.spec.js',
                included: false,
                watched:true
            },
            { pattern: 'test/fixtures/**/*', included: false, served:true, watched:true},
            { pattern: 'vendor/**/*.js', included: false, served:true, watched:false }
        ]
    });
};