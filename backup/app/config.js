require.config( {
    hbs: {
        templateExtension: 'hbs',
        i18n: false,
        partialsUrl: ''
    },
    paths: {
        // module aliases
        'vendor': '../vendor',
        'test': '../test',
        'order': 'modules/order',
        'product': 'modules/product',
        'shop': 'modules/shopmanagement',
        'statistics': 'modules/statistics',

        //libraries
        'almond': '../vendor/bower/almond/almond',
        'underscore': '../vendor/bower/underscore/underscore',
        'jquery': '../vendor/bower/jquery/dist/jquery',
        'backbone': '../vendor/bower/backbone/backbone',
        'text': '../vendor/bower/requirejs-text/text',
        'i18nprecompile': '../vendor/i18nprecompile',
        'json2': '../vendor/bower/json3/lib/json3',
        'Modernizr':'../vendor/bower/modernizr/modernizr',
        'backbone-validator': '../vendor/bower/backbone-validator/backbone-validator',
        'bootstrap': '../vendor/bower/bootstrap/dist/js/bootstrap',
        'backbone.babysitter': '../vendor/bower/backbone.babysitter/lib/backbone.babysitter',
        'backbone.wreqr': '../vendor/bower/backbone.wreqr/lib/backbone.wreqr',
        'backbone.stickit': '../vendor/bower/backbone.stickit/backbone.stickit',
        'hbs': '../vendor/bower/require-handlebars-plugin/hbs',
        'requirejs': '../vendor/bower/requirejs/require',
        'cocktail': '../vendor/bower/cocktail/Cocktail',
        'backbone-associations': '../vendor/bower/backbone-associations/backbone-associations',
        'require-handlebars-plugin': '../vendor/bower/require-handlebars-plugin/hbs',
        'bootstrap-datepicker': '../vendor/bower/bootstrap-datepicker/js/bootstrap-datepicker',
        'moment': '../vendor/bower/moment/moment',
        'bloodhound': '../vendor/bower/typeahead.js/dist/bloodhound',
        'typeahead.jquery': '../vendor/bower/typeahead.js/dist/typeahead.jquery',
        'typeahead.bundle': '../vendor/bower/typeahead.js/dist/typeahead.bundle.min',
        'backbone.marionette': '../vendor/bower/marionette/lib/core/backbone.marionette',
        'mousetrap':'../vendor/bower/mousetrap/mousetrap',
        'summernote':'../vendor/bower/summernote/dist/summernote',
        'cm':'../vendor/bower/codemirror',
        'when':'../vendor/bower/when',
        'backbone.epoxy': '../vendor/bower/backbone.epoxy/backbone.epoxy',
        'elasticsearch': '../vendor/bower/elasticsearch/elasticsearch',
        'd3': '../vendor/bower/d3/d3',
        'nv': '../vendor/bower/nvd3/nv.d3',
        'select2': '../vendor/bower/select2/select2',
        'backbone.marionette.export': '../vendor/bower/backbone.marionette.export/dist/amd/backbone.marionette.export',
        'ember':'../vendor/bower/ember/ember.debug',
        'template-compiler':'../vendor/bower/ember/ember-template-compiler'
    },

    shim: {
        'bootstrap': {
            deps: ['jquery']
        },
        'ember': {
            deps:['template-compiler','hbs/handlebars']
        },
        'bootstrap-datepicker': {
            deps: ['bootstrap']
        },
        'bloodhound': {
            exports: 'Bloodhound'
        },
        'backbone.memento': {
            deps: ['backbone']
        },
        'backbone.marionette.export': {
            deps: ['backbone.marionette']
        },
        'backbone-associations': {
            deps:['backbone']
        },
        'backbone.stickit': {
            deps: ['backbone']
        },
        'typeahead.jquery': {
            deps: ['jquery']
        },
        'd3': {
            exports: 'd3'
        },
        'nv': {
            exports: 'nv',
            deps: ['d3']
        }
    },
    map: {
        '*': {
            'codemirror': 'cm/lib/codemirror',
            'CodeMirror': 'cm/lib/codemirror',
            'marionette': 'backbone.marionette'
        }
    }
} );
