/**
 * @module router
 */
define ( function ( require ) {
    'use strict';

    var Backbone = require( 'backbone' ),
        Marionette = require( 'marionette' ),
        _ = require( 'underscore' ),
        OrderRouter = require( 'order/router' ),
        ProductRouter = require( 'product/router' ),
        ShopManagementRouter = require( 'shop/router' ),
        StatisticsRouter = require( 'statistics/router' ),
        ProcurementRouter = require( 'modules/procurement/router'),
        UserSettingsView = require( 'views/userSettings');

    return Marionette.AppRouter.extend( {
        navView: undefined,

        /**
         * Initializes the main router
         */
        initialize: function() {
            if( _.isObject( App.navRegion ) && _.isObject( App.navRegion.currentView ) ) {
                this.navView = App.navRegion.currentView;
            } else {
                this.navView = new Backbone.View();
            }
            window.orderrouter = new OrderRouter({App: App, AppRouter: this});
            window.productrouter = new ProductRouter( {App: App, AppRouter: this} );
            window.shopManagementRouter = new ShopManagementRouter( {App: App, AppRouter: this} );
            window.StatisticsRouter = new StatisticsRouter( {App: App, AppRouter: this} );
            window.ProcurementRouter = new ProcurementRouter( {App: App, AppRouter: this });

            App.routerStarted();
        },


        /**
         * Register a route to the main appRouter
         * @param route
         * @param handler
         * @param controller
         */
        register: function( route, handler, controller ) {
            var handlerMethod;
            var name;

            if ( _.isObject( handler ) ) {
                handlerMethod = handler.method;
                name = handler.name;
            } else {
                handlerMethod = handler;
                name = handler;
            }

            if ( _.isUndefined( handler.hideInMenu ) && _.isFunction( this.navView.addMenu ) ) {
                this.navView.addMenu( route, name );
            }

            this.route( route, handlerMethod, function() {
                var args = arguments;

                var options = null;
                var parameters = route.match( /[:\*]\w+/g );

                // Map the route parameters to options for the View.
                if ( parameters ) {
                    options = {};
                    _.each( parameters, function( name, index ) {
                        options[ name.substring(1) ] = args[index];
                    });
                }

                var loadParams = {};
                if ( !_.isUndefined( handler.preLoadById ) && handler.preLoadById !== false ) {
                    var idAttribute = 'id';
                    if ( _.isString( handler.preLoadById ) ) {
                        idAttribute = handler.preLoadById;
                    }
                    loadParams = {preLoadById: idAttribute, id: options.id};
                }
                controller.showModule( handlerMethod, options, loadParams);
            });
        },

        registerEmberRoute: function( routeUrl, routeObj ) {

            window.EApp.Router.map( function() {
                this.route( routeObj.routeName, {path: routeUrl} );
            } );

            window.EApp[routeObj.method + 'Route'] = routeObj.emberRoute;

            if ( _.isUndefined( routeObj.hideInMenu ) && _.isFunction( this.navView.addMenu ) ) {
                this.navView.addMenu( routeUrl, '<span class="emberroute">' + routeObj.name + "<span>" );
            }
        },

        /**
         * Register a modules route to the main router
         *
         * @param module Reference to the module
         * @param controller Optional Reference to the controller
         */
        registerModule: function( module, controller ) {
            _.each( module.routes, function( handler, route ) {
                if ( _.isObject( handler ) && !_.isUndefined( handler.emberRoute ) ) {
                    this.registerEmberRoute( route, handler );
                } else {
                    this.register( route, handler, controller || module.controller || {} );
                }
            }, this );
        },

        /**
         * @var Main controller actions
         */
        controller: {
            defaultAction: function() {
                var contentContainer = $( '#contentcontainer' );
                contentContainer.append( '<div class="proTip alert alert-danger">Behaviour change: editing products in the CMS now ONLY works in \'block-edit\' mode (using the <i class="glyphicon glyphicon-pencil"></i> icon)</div>' );
                contentContainer.append( '<div class="proTip alert alert-success">You can CTRL-Click on a products selectbox to select all it\'s variations.</div>' );
                contentContainer.append( '<div class="proTip alert alert-danger">When you\'re testing BO2 or FO2 on single, you\'re working on the <strong>live</strong> database.</div>' );
                contentContainer.append( '<div class="proTip alert alert-warning">Since BO2 is not ready yet, be carefull with what you do, and think twice about your actions.</div>' );
            },
            product: function() {
            },
            shopmanagement: function() {
            },

            userSettings: function() {
                App.mainRegion.show( new UserSettingsView( {model:App.settingsmodel} ) );
            }
        },

        /**
         * @var
         * main routes to the various modules.
         * These are not yet actually used for routing, only defined
         */
        appRoutes: {
            '': 'defaultAction',
            'settings': 'userSettings'
        }
    } );
} );