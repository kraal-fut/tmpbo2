define ( function ( require ) {
    'use strict';
    var Backbone = require( 'backbone-associations' );

    return Backbone.AssociatedModel.extend( {
            relations: [
                {
                    key: 'routes',
                    type: Backbone.Many,
                    relatedModel: Backbone.Self
                }
            ]
        });
});
