define ( function ( require ) {
    'use strict';
    var settingsModel = require( 'models/settings' );

    /**
     * @class settings
     */
    return settingsModel.extend( {
        constructor: function( data, options ) {
            this.url = options.url;
            settingsModel.apply( this, arguments );
            this.set( data );
        }
    } );
} );