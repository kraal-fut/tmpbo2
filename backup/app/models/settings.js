define( function( require ) {
    'use strict';

    var Fut = require( 'fut/entities' ),
        _ = require( 'underscore' );

    var UserSettingsModel = Fut.Model.extend({
        idAttribute: '_id',

        url: function() {
            var url = 'api/v3/bo2settings';
            if (this.id) {
                url+= '/' + this.id;
            }
            return url;
        }
    });

    /**
     * @class settings
     */
    return Fut.Model.extend( {
        user: undefined,
        userSettings: undefined,

        defaults:{
            BASEURL: ''
        },

        url: 'api/v3/bo2settings',

        initialize: function() {
            this.init();
        },

        init: function() {
            this.user = new Fut.Model();
            this.userSettings =new UserSettingsModel();
        },

        getSetting: function( setting, defaultValue ) {
            var getIt = function( memo, settingName ) {
                if ( this.userSettings.has( settingName ) ) {
                    return this.userSettings.get( settingName );
                }

                if ( this.user.has( settingName ) ) {
                    return this.user.get( settingName );
                }

                return this.get( settingName ) || memo;
            };

            return _.reduce( _.isArray( setting ) ? setting : [setting], getIt, defaultValue, this );
        },

        setSetting: function( setting, value ) {
            this.userSettings.set( setting, value );
            this.throttledStore();
        },

        getCombined: function() {
            return _.extend({ user: this.user.toJSON()}, this.toJSON(), this.userSettings.toJSON() );
        },

        throttledStore: _.debounce(function() {
            this.store( true );
        }, 10000),

        /**
         * Store the settings for this user
         * @param {bool} hideNotify  when true, no notification is shown
         * @returns {Promise}
         */
        store: function( hideNotify ) {
            var settings = this;

            return this.userSettings.save().then(function() {
                console.info('settings saved');
                if ( !hideNotify ) {
                    App.notify( 'Settings saved', {delay: 2000} );
                }
                return settings;
            }).catch(function( error ){
                App.notify( 'Could not save settings (' + error.message + ')' );
            });
        },

        resetToDefaults: function() {
            var settingsmodel = this;
            var init = this.init.bind( this );

            return this.userSettings.deleteOnServer().then(function() {
                localStorage.clear();
                init();
                return settingsmodel.load();
            });
        },

        parse: function( data ) {
            var stored = JSON.parse( localStorage.getItem( 'userSetting' ) );

            this.user.set(  data.user );

            this.userSettings.set( _.extend( {}, stored, data.userSettings ) );

            return data.appSettings;
        }
    } );
} );