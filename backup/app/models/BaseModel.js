define ( function ( require, exports ) {
    'use strict';
    var Backbone = require( 'backbone-associations' ),
        _ = require( 'underscore' ),
        when = require( 'when/when' );

    exports.BaseModel = Backbone.AssociatedModel.extend( {
        load: function( options ) {
            var that = this;

            return when.promise(function( resolve, reject ) {
                options = options || {};
                var _options = _.extend( {}, options );

                _options.success = function ( model ) {
                    if ( _.isFunction( options.success ) ) {
                        options.success.apply( that, arguments );
                    }
                    resolve( model );
                };

                _options.error = function( model ) {
                    if ( _.isFunction( options.error ) ) {
                        options.error.apply( that, arguments );
                    }
                    reject( new Error( 'Could not save to server' ) );
                };

                that.fetch( _options );
            });
        },

        /**
         * Overrides the original save method to show notifications on success.
         * @param attributes
         * @param options
         * @return {Promise}
         */
        save: function( attributes, options ) {
            var that = this;

            return when.promise(function(resolve, reject){
                options = options || {};

                var _options = _.extend( {}, options );

                _options.success = function ( model ) {
                    if ( _.isFunction( options.success ) ) {
                        options.success.apply( that, arguments );
                    }
                    resolve( model );
                };

                _options.error = function ( model, response ) {
                    if ( _.isFunction( options.error ) ) {
                        options.error.apply( that, arguments );
                    }
                    var error = new Error('Server said ' + response.status + ': ' + response.statusText);
                    error.model = model;
                    error.response = response;
                    reject( error );
                };

                if ( !Backbone.AssociatedModel.prototype.save.call( that, attributes, _options ) ) {
                    reject( new Error( 'Validation failed' ) );
                }
            });
        },

        deleteOnServer: function( options ) {
            var that = this;

            return when.promise( function ( resolve, reject ) {
                options = options || {};

                var _options = _.extend( {}, options );

                _options.success = function ( model ) {
                    if ( _.isFunction( options.success ) ) {
                        options.success.apply( that, arguments );
                    }
                    resolve( model );
                };

                _options.error = function ( model, response ) {
                    if ( _.isFunction( options.error ) ) {
                        options.error.apply( that, arguments );
                    }
                    var error = new Error( 'Server said ' + response.status + ': ' + response.statusText );
                    error.model = model;
                    error.response = response;
                    reject( error );
                };

                Backbone.AssociatedModel.prototype.destroy.call( that, _options );
            });
        },
        //sync: function() {
        //    console.log('sync', arguments);
        //
        //    return Backbone.AssociatedModel.prototype.sync.call( this, arguments );
        //},

        parse: function( response ) {
            if (!_.isUndefined(this.entityName) && !_.isUndefined(response[this.entityName])) {
                response = response[this.entityName];
            }

            if ( _.isArray( response ) && response.length === 1 ) {
                return _.first( response );
            }
            return response;
        },

        toggle: function( attribute ) {
            if ( this.get( attribute ) ) {
                this.set( attribute, false );
            } else {
                this.set( attribute, true );
            }
        },
        /**
         * Fill this model with promised or not promised data.
         *
         * @param data
         * @returns {Promise}
         */
        fill: function( data ) {
            var that = this;
            return when( data ).then(function( pd ){
                var d = pd;
                if ( _.isUndefined( pd) ) {
                    return that;
                }
                if ( _.isFunction( pd.toJSON ) ) {
                    d = pd.toJSON();
                }

                that.set( d );
                return that;
            });

        }
    });

    return exports.BaseModel;
});