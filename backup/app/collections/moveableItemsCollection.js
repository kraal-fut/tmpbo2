define ( function () {
    'use strict';
    var _ = require( 'underscore' );

    return {
        moveModelUp: function( m, fieldName, fieldIsString ) {
            fieldIsString = fieldIsString || false;

            var currentPos = m.get( fieldName );
            if ( currentPos <= 0 ) {
                return;
            }

            var abovePos = currentPos;
            var modelAbove;

            do {
                abovePos--;
                var criteria = {};
                criteria[fieldName] = fieldIsString ? abovePos.toString() : abovePos;
                modelAbove = this.findWhere( criteria );
            } while ( _.isUndefined( modelAbove ) && abovePos > 0 );

            if ( modelAbove ) {
                modelAbove.set( fieldName, abovePos + 1 );
            }

            m.set( fieldName, abovePos );
            this.sort();

        },

        moveModelDown: function( m, fieldName, fieldIsString ) {
            fieldIsString = fieldIsString || false;

            var largest = this.max(function( item ){
                return item.get( fieldName );
            } ).get( fieldName );

            var modelBelow;

            var belowPos = m.get( fieldName );
            if ( belowPos >= largest ) {
                return;
            }
            do {
                belowPos++;
                var criteria = {};
                criteria[fieldName] = fieldIsString ? belowPos.toString() : belowPos;
                modelBelow = this.findWhere( criteria );
            } while ( _.isUndefined( modelBelow ) && belowPos <= largest );

            if ( modelBelow ) {
                modelBelow.set( fieldName, belowPos - 1 );
            }

            m.set( fieldName, belowPos );
            this.sort();
        },

        moveModelToBottom: function( model, value, fieldName ) {
            if ( value === 1 ) {
                return false;
            }
            var modelBelow;

            var largest = this.max(function( item ){
                return item.get( fieldName );
            } ).get( fieldName );

            var belowPos = model.get( fieldName );
            if ( belowPos > largest ) {
                return;
            }

            do {
                belowPos++;
                var criteria = {};
                criteria[fieldName] = belowPos;
                modelBelow = this.findWhere( criteria );
                if ( modelBelow ) {
                    modelBelow.set( fieldName, belowPos - 1 );
                }
            } while ( belowPos < largest );

            model.set( fieldName, belowPos );
            this.sort();
        }
    };
} );