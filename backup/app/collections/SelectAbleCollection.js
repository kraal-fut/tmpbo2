define( function( require, exports ) {
    'use strict';

    var _ = require( 'underscore' );

    /**
     * This mixin is ripped from Derick Bailey 'Backbone.Picky' but we should modify it to our needs.
     *
     * @constructor
     */
    var SelectAbleCollection = {
        selected: {},
        selectedLength: 0,

        doSelect: function( model ) {
            if ( this.selected[model.cid] ) {
                return;
            }

            this.selected[model.cid] = model;
            model.select();

            calculateSelectedLength( this );
        },

        deselect: function( model ) {
            if ( !this.selected[model.cid] ) {
                return;
            }

            delete this.selected[model.cid];
            model.deselect();
            calculateSelectedLength( this );
        },

        selectAll: function() {
            this.each( function( model ) {
                model.select();
            } );
            calculateSelectedLength( this );
        },

        selectNone: function() {
            if ( this.selectedLength === 0 ) {
                return;
            }
            this.each( function( model ) {
                model.deselect();
            } );
            calculateSelectedLength( this );
        },

        toggleSelectAll: function() {
            if ( this.selectedLength === this.length ) {
                this.selectNone();
            } else {
                this.selectAll();
            }
        },

        getSelected: function( options ) {
            return this.where( _.extend( {selected: true}, options ) );
        },

        hasSelecteds: function() {
            return this.selectedLength > 0;
        }
    };

    var calculateSelectedLength = function( collection ) {
        collection.selectedLength = _.size( collection.selected );

        var selectedLength = collection.selectedLength;
        var length = collection.length;

        if ( selectedLength === length ) {
            collection.trigger( 'select:all', collection );
            return;
        }

        if ( selectedLength === 0 ) {
            collection.trigger( 'select:none', collection );
            return;
        }

        if ( selectedLength > 0 && selectedLength < length ) {
            collection.trigger( 'select:some', collection );
        }
    };

    exports.SelectAbleCollection = SelectAbleCollection;

    return SelectAbleCollection ;
} );