define( function ( require ) {
    'use strict';
    var _ = require( 'underscore' );

    return {
        state: {
            totalRecords: 0,
            totalPages: 0,
            currentPage: 1,
            itemsPerPage: 0
        },

        parseQuery: function () {
            return _.reduce( this.queryParams, function ( memo, item, key ) {
                var value = _.isFunction( item ) ? item.apply( this ) : item;

                if ( !_.isUndefined( value ) ) {
                    memo[key] = value;
                }
                return memo;

            }, {}, this );
        },

        queryParams: {
            limit: function () {
                return this.limit;
            },

            page: function () {
                return this.state.currentPage;
            },

            find: function () {
                if ( _.isEmpty( this.dataSendSearch ) ) {
                    return undefined;
                }

                return _.isString( this.dataSendSearch ) ? this.dataSendSearch : JSON.stringify( this.dataSendSearch );
            },

            sort: function () {
                if ( _.isEmpty( this.dataSort ) ) {
                    return undefined;
                }
                return JSON.stringify( this.dataSort );
            }
        },

        setPageSize: function ( value ) {
            this.limit = value;
            return this.load();
        },

        getPage: function ( value ) {
            this.state.currentPage = parseInt( value, 10 );
            return this.load();
        },

        getNextPage: function () {
            if ( this.state.currentPage < this.state.totalPages ) {
                this.state.currentPage++;
                this.load();
            }
        },

        getLastPage: function () {
            this.state.currentPage = this.state.totalPages;
            return this.load();
        },

        getFirstPage: function () {
            this.state.currentPage = 1;
            return this.load();
        },

        getPreviousPage: function () {
            if ( this.state.currentPage > 1 ) {
                this.state.currentPage--;
                this.load();
            }
        },

        parseState: function ( response ) {
            var state = {
                totalRecords: 0,
                totalPages: 0,
                currentPage: 1,
                itemsPerPage: 0
            };

            if ( response.meta ) {
                if ( response.meta.total ) {
                    state.totalRecords = parseInt( response.meta.total, 10 );
                }
                if ( response.meta.pageCount ) {
                    state.totalPages = parseInt( response.meta.pageCount, 10 );
                }
                if ( response.meta.page ) {
                    state.currentPage = parseInt( response.meta.page, 10 );
                }
                if ( response.meta.pageRecords ) {
                    state.itemsPerPage = parseInt( response.meta.pageRecords, 10 );
                }
            }

            return state;
        },

        parse: function ( response ) {
            this.state = this.parseState( response );
            return this.parseRecords( response );
        }
    };

} );