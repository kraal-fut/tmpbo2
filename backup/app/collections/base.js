define ( function ( require, exports ) {
    'use strict';

    var Backbone = require( 'backbone' ),
        Cocktail = require( 'cocktail' ),

        PageableCollection = require( 'collections/PageAbleCollection' ),
        _ = require( 'underscore' ),
        when = require( 'when/when' ),
        Events = require( 'components/AppEvents' );

    exports.Collection = Backbone.Collection.extend( {
        dataSendSearch: undefined,
        dataSort: undefined,

        settings: undefined,
        LocalStorageSortKey: undefined,
        LocalStorageItemsPerPageKey: undefined,
        LocalStorageCriteriaKey: undefined,
        noSearch: undefined,
        limit: undefined,
        state: {},
        options: undefined,

        url: function() {
            return this.baseUrl( '/' + this.entityName );
        },

        baseUrl: function( suffix ) {
            var base = '';
            if ( !_.isUndefined( App.apiroot ) ) {
                base = App.apiroot || '';
            }
            return base + suffix;
        },

        initialize: function( models, options ) {
            this.options = _.extend( {}, options );
            this.settings = this.options.settings || App.setttingsmodel || new Backbone.Model();

            this.noSearch = this.options.noSearch;
            this.limit = this.options.limit || this.limit || 10;

            this.loadFromPersist();
        },

        load: function( options ) {
            options = options || {};

            this.state.pageSize = this.limit || this.settings.get( this.LocalStorageItemsPerPageKey ) || 10;

            var collection = this;
            return when.promise(function( resolve, reject ) {

                var _options = _.extend( {
                    success: function( collection ) {
                        resolve( collection );
                    },
                    error: function() {
                        reject( new Error( 'Failed load data' ) );
                    }
                }, options );

                _options.data = _.extend( {}, options.data, collection.parseQuery() );
                collection.fetch( _options );
            });
        },

        parseRecords: function( response ) {
            var sourceData;
            if ( !_.isUndefined( this.entityName ) ) {
                sourceData = response[this.entityName];
            } else if ( !_.isUndefined( response.data ) ) {
                sourceData = response.data;
            } else {
                sourceData = response;
            }
            return sourceData;
        },

        /**
         *
         * @param {object} params nested key/value object, or key if data is given too
         * @param [field] optional parameter when first one is used as key
         * @param {boolean} [chain] return without persisting the criteria
         */
        addCriteria: function( params, field, chain ) {

            var apply = function( collection ) {
                if ( chain ) {
                    return this;
                }

                return collection.persistCriteria();
            };

            if ( _.isString( params ) ) {
                this.dataSendSearch = params;
                return apply( this );
            }

            var isNotFilterable = function( p ) {
                return !_.isNumber( p ) && _.isEmpty( p ) && !_.isBoolean( p );
            };

            if ( isNotFilterable( params ) ) {
                return this.removeFieldCriteria( field, chain );
            }

            var newParams = {};

            var addParam = function( item, key ) {
                newParams[key] = item;
            };

            if ( _.isUndefined( field ) ) {
                if ( params instanceof Backbone.Model ) {
                    _.each ( params.toJSON(), addParam );
                } else {
                    _.each( params, addParam );
                }
            } else {
                addParam( params, field );
            }

            this.dataSendSearch = _.extend( this.dataSendSearch, newParams );

            return apply( this );
        },

        addSort: function( params, order, chain ) {
            if ( _.isUndefined( order ) ) {
                this.dataSort = params;
            } else {
                this.dataSort[params] = order;
            }

            if (chain) {
                return this;
            }
            return this.persistCriteria();
        },

        removeSort: function( field, chain ) {
            this.dataSort = _.omit( this.dataSort, field );
            if ( chain ) {
                return this;
            }
            return this.persistCriteria();
        },

        removeFieldCriteria: function( field, chain ) {
            delete this.dataSendSearch[field];
            if ( chain ) {
                return this;
            }
            return this.persistCriteria();
        },

        resetCriteria: function( chain ) {
            this.dataSendSearch = {};
            this.dataSort = {};
            if ( chain ) {
                return this;
            }
            return this.persistCriteria();
        },

        getCriteriaAll: function() {
            return this.dataSendSearch;
        },

        getCriteria: function( name ) {
            if ( name in this.dataSendSearch ) {
                return this.dataSendSearch[name];
            }
            return '';
        },

        getSortAll: function() {
            return this.dataSort;
        },

        getSort: function( field ) {
            if ( !_.isUndefined( this.dataSort[ field ] ) ) {
                return this.dataSort[ field ];
            }
            return '';
        },

        setLimit: function( limit ) {
            this.limit = limit;
        },

        loadFromPersist: function() {
            try {
                this.dataSendSearch = JSON.parse( this.settings.get( this.LocalStorageCriteriaKey ) ) || {};
                this.dataSort = JSON.parse( this.settings.get( this.LocalStorageSortKey ) ) || {};
            } catch ( ignore ) {
                this.dataSendSearch = this.dataSendSearch || {};
                this.dataSort = this.dataSort || {};
            }
        },

        persistCriteria: function() {
            this.trigger( Events.RESET_BREADCRUMBS );
            return this.load( {reset: true, remove: true} );
        }
    } );

    Cocktail.mixin( exports.Collection, PageableCollection );

    return exports.Collection;
} );
