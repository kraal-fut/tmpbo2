define( function( require, exports ) {
    'use strict';
    var BaseCollection = require( 'collections/base' ),
        Backbone = require( 'backbone' ),
        FrontofficeMenuPseudo = require( 'product/models/frontofficemenupseudo' );

    exports.Collection = BaseCollection.extend( {
        idAttribute: 'frontoffice_menu_id',
        entityName: 'frontoffice_menu_pseudo',

        url: function() {
            return this.baseUrl( '/v3/frontofficemenus' );
        },

        model: FrontofficeMenuPseudo,
        LocalStorageSortKey: 'fomenu_sort',
        LocalStorageItemsPerPageKey: 'fomenu_itemsPerPage',
        LocalStorageCriteriaKey: 'fomenu_criteria',

        getItemsByParentId: function( parentId ) {
            return new Backbone.Collection( this.where ( {'parent_id': parentId} ) );
        }
    } );

    return exports.Collection;
});
