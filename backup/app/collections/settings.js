define ( function ( require ) {
    'use strict';
    var Backbone = require( 'backbone' ),
        _ = require( 'underscore' ),
        fut = require( 'fut/entities' );

    return Backbone.Collection.extend({

        initialize: function() {
            App.reqres.setHandler( 'settings', _.bind( this.onSettingsRequest, this ) );
            localStorage.clear();
        },

        onSettingsRequest: function( module, setting ) {
            return fut.promise(function( resolve ){

                if ( _.isUndefined( setting ) ) {
                    resolve( App.settingsmodel );
                    return;
                }

                resolve( App.settingsmodel.get( setting ) );
            });
        }
    });
});