define(function( require, exports ) {
    'use strict';
    var _ = require( 'underscore' ),
        Epoxy = require( 'backbone.epoxy' ),
        Fut = require( 'fut/entities' );

    exports.Collection = Fut.Collection.extend( {
        idAttribute: 'frontoffice_menu_id',
        entityName: 'frontoffice_menu_pseudo',

        url: function() {
            return this.baseUrl( '/v3/frontofficemenus/list' );
        },

        loadVisibles: function() {

            return this.load().then(function( collection ) {

                var isVisible = function( item ) { return item.get( 'visible' ) === true; };

                return collection.reset( collection.filter( isVisible ) );
            });
        },

        model: Epoxy.Model.extend({
            computeds: {
                label: function() {
                    var name = '';
                    if (!_.isUndefined( this.get( 'isParent' ) ) ) {
                        name += ' • ';
                    }
                    name += this.get( 'name' );

                    return name;
                },
                value: function() {
                    return this.get( 'frontoffice_menu_id' );
                }
            }
        }),

        parse: function( response ) {
            return response.fomenumap;
        },

        baseUrl: function( suffix ) {
            var base = '';
            if ( !_.isUndefined( App.apiroot ) ) {
                base = App.apiroot;
            }
            return base + suffix;
        },

        getIdsByParent: function( parentId ) {
            return new Fut.Collection( this.where( {parent_id: parentId} ) ).pluck( 'frontoffice_menu_id' );
        },

        getChildren: function( parentId ) {
            var children = new Fut.Collection( this.where( {parent_id: parentId} ) );
            var parent = this.findWhere( {frontoffice_menu_id: parentId} );

            if ( !_.isUndefined( parent ) ) {
                parent = parent.clone();
                parent.set( 'isParent', true );
                children.unshift( parent );
            }

            return children;
        }
    } );

    return exports.Collection;
});
