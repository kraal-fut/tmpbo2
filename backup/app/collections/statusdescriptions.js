define ( function ( require ) {
    'use strict';
    var Backbone = require( 'backbone' );

    return new Backbone.Collection(
        [{
            "status_id": "1",
            "description": "Nieuwe bestelling (zodra uw betaling is goedgekeurd wordt de bestelling verwerkt)"
        }, {
            "status_id": "2",
            "description": "Uw Ideal betaling is gelukt ('s ochtends of 's middags wordt uw bestelling verwerkt)"
        }, {
            "status_id": "3",
            "description": "Wij zijn in afwachting van uw bijbetaling (daarna wordt uw bestelling verder verwerkt)"
        }, {
            "status_id": "4",
            "description": "Uw creditcard betaling is gelukt ('s ochtends of 's middags wordt uw bestelling verwerkt)"
        }, {"status_id": "5", "description": "Duitse order"}, {
            "status_id": "6",
            "description": "Uw betaling is goedgekeurd (bestelling wordt verwerkt)"
        }, {
            "status_id": "7",
            "description": "Uw bestelling is aangepast (bestelling wordt verwerkt)"
        }, {
            "status_id": "9",
            "description": "Uw betaling is goedgekeurd (bestelling wordt verwerkt)"
        }, {
            "status_id": "10",
            "description": "Uw betaling is goedgekeurd (bestelling wordt verwerkt)"
        }, {"status_id": "11", "description": "Verstuurd met TNT Post"}, {
            "status_id": "12",
            "description": "Verstuurd met TNT Post"
        }, {"status_id": "18", "description": "Verzenden DE akkoord"}, {
            "status_id": "19",
            "description": "Vrije status 2"
        }, {"status_id": "20", "description": "Vrije status 3"}, {
            "status_id": "21",
            "description": "Uw bestelling is in behandeling"
        }, {"status_id": "22", "description": "Adres Aanpassen"}, {
            "status_id": "23",
            "description": "DE verzending aanpassen"
        }, {
            "status_id": "24",
            "description": "Uw bestelling is aangepast (bestelling wordt verwerkt)"
        }, {"status_id": "25", "description": "Vrije status 4"}, {
            "status_id": "26",
            "description": "Duitsland"
        }, {"status_id": "27", "description": "Opnieuw verzenden Duitse bestelling"}, {
            "status_id": "28",
            "description": "Duitsland aanpassen"
        }, {"status_id": "29", "description": "Uw bestelling is per TNT Post verstuurd"}, {
            "status_id": "30",
            "description": "Uw bestelling is verstuurd met TNT Post"
        }, {"status_id": "31", "description": "Uw bestelling is verstuurd met TNT Post"}, {
            "status_id": "32",
            "description": "Uw bestelling is verstuurd met TNT Post"
        }, {
            "status_id": "33",
            "description": "Uw betaling is goedgekeurd (bestelling wordt verwerkt)"
        }, {"status_id": "34", "description": "Duitsland Backorders"}, {
            "status_id": "35",
            "description": "Uw betaling is goedgekeurd (bestelling wordt verwerkt)"
        }, {"status_id": "36", "description": "Uw bestelling is in verwerking"}, {
            "status_id": "37",
            "description": "Uw bestelling wordt verstuurd"
        }, {"status_id": "38", "description": "Uw bestelling is verstuurd met TNT Post"}, {
            "status_id": "39",
            "description": "Uw bestelling is in verwerking"
        }, {"status_id": "40", "description": "Uw bestelling is verstuurd met TNT Post"}, {
            "status_id": "41",
            "description": "Uw bestelling is verstuurd met TNT Post"
        }, {"status_id": "42", "description": "Uw bestelling is verstuurd met TNT Post"}, {
            "status_id": "43",
            "description": "Uw bestelling is gereed om af te halen (u wordt persoonlijk gebeld zodra het klaarligt)"
        }, {"status_id": "44", "description": "Uw bestelling is verstuurd met TNT Post"}, {
            "status_id": "45",
            "description": "Afhaalbestelling in behandeling (wij berichten u zodra het voor u klaarligt)"
        }, {"status_id": "46", "description": ""}, {
            "status_id": "48",
            "description": "Uw Afhaalbestelling wordt verwerkt"
        }, {
            "status_id": "49",
            "description": "Afhaalbestelling in behandeling (wij berichten u zodra het voor u klaarligt)"
        }, {
            "status_id": "50",
            "description": "Uw bestelling ligt klaar voor afhaling (mits per e-mail of telefoon door ons bevestigd)"
        }, {"status_id": "51", "description": "Uw bestelling is afgehaald"}, {
            "status_id": "55",
            "description": "Rembours verstuurd"
        }, {"status_id": "56", "description": "Uw bestelling is verstuurd met TNT Post"}, {
            "status_id": "57",
            "description": "Uw bestelling is per TNT Post verstuurd"
        }, {"status_id": "59", "description": "Uw bestelling is per TNT Post verstuurd"}, {
            "status_id": "60",
            "description": "Uw bestelling is verstuurd en afgerond"
        }, {"status_id": "61", "description": "Uw bestelling is gedeelleverd"}, {
            "status_id": "62",
            "description": "Uw bestelling is in behandeling ivm een garantie aanspraak"
        }, {"status_id": "63", "description": "Uw bestelling is in behandeling onder garantie"}, {
            "status_id": "64",
            "description": "Uw bestelling is in behandeling onder garantie"
        }, {"status_id": "65", "description": "Uw bestelling is in behandeling onder garantie"}, {
            "status_id": "66",
            "description": "Uw bestelling is in behandeling onder garantie"
        }, {"status_id": "67", "description": "Uw bestelling is in behandeling onder garantie"}, {
            "status_id": "68",
            "description": "Uw zending is retour ontvangen en wordt verwerkt"
        }, {"status_id": "70", "description": "Frankrijk Afgesloten"}, {
            "status_id": "71",
            "description": "Test irders"
        }, {"status_id": "74", "description": "Niet aankomen ivm incidententemplates"}, {
            "status_id": "75",
            "description": "Tijdelijkstevanofferte"
        }, {"status_id": "77", "description": "Nazend afgerond refund"}, {
            "status_id": "78",
            "description": "Bij IDeal storing"
        }, {"status_id": "79", "description": "Uw bestelling staat in backorder"}, {
            "status_id": "80",
            "description": "Uw bestelling wordt speciaal verwerkt ivm uw wensen en in afwachting van uw betaling"
        }, {"status_id": "81", "description": "Interne Order"}, {
            "status_id": "82",
            "description": "Retourorder aanpassen"
        }, {"status_id": "83", "description": "Order aanpassen"}, {
            "status_id": "84",
            "description": "Bijbetalen"
        }, {"status_id": "85", "description": "Nazend bijbetalen"}, {
            "status_id": "86",
            "description": "Verzenden"
        }, {"status_id": "87", "description": "Nazend verzenden"}, {
            "status_id": "88",
            "description": "Afgerond"
        }, {"status_id": "89", "description": "Nazend afgerond bijbetalen"}, {
            "status_id": "90",
            "description": "Uw bestelling is door ons geannuleerd"
        }, {"status_id": "91", "description": "Annuleren"}, {
            "status_id": "92",
            "description": "Pin en Rembours"
        }, {"status_id": "93", "description": "Uw bestelling is geannuleerd"}, {
            "status_id": "94",
            "description": "Geannuleerd (Duitsland)"
        }, {"status_id": "95", "description": "Uw bestelling is in verwerking"}, {
            "status_id": "96",
            "description": "Creditatie (deel\/geheel) (etiket)"
        }, {"status_id": "97", "description": "Creditatie (deel\/geheel)"}, {
            "status_id": "98",
            "description": "Betaalverzoek verstuurd"
        }, {"status_id": "99", "description": "Betalingsherinnering verstuurd"}]
    );
} );