define( function ( require ) {
    'use strict';
    var $ = require( 'jquery' ),
        _ = require( 'underscore' ),
        Backbone = require( 'backbone' ),
        paginationTemplate = require( 'hbs!/templates/search');


    return Backbone.View.extend( {

        initialize: function() {
            this.collection.bind( 'reset', this.render, this );
            this.collection.bind( 'change', this.render, this );
            this.clearSearch( null, true );
        },

        el: '#uicontainer .search',

        events: {
            'submit .form-search': 'performSearch',
            'click button.clearBtn': 'clearSearch'
        },

        template: paginationTemplate,

        clearSearch: function( event, noloadData ) {
            if ( event ) {
                event.preventDefault();
            }

            this.collection.querydata = _.difference( this.collection.querydata, this.collection.searchQuery );
            this.collection.searchQuery = {};
            this.collection.searchTerm = '';

            if ( !noloadData ) {
                this.collection.fetch( {data: 'q=[' + JSON.stringify( this.collection.querydata ) + ']'} );
            }
        },

        performSearch: function( event ) {
            this.clearSearch( event, true );
            var searchField = $( '#mainSearchField' );
            var inputValue = searchField.val();
            if ( inputValue !== '' ) {
                this.collection.searchTerm = inputValue;
                if ( $.isArray( this.collection.searchField ) ) {
                    this.collection.searchQuery.$or = [];

                    for ( var index in this.collection.searchField ) {
                        if ( this.collection.searchField.hasOwnProperty( index ) ) {
                            var searchObj = {};
                            searchObj[this.collection.searchField[index]] = inputValue;
                            this.collection.searchQuery.$or.push( searchObj );
                        }
                    }
                }
                else {
                    this.collection.searchQuery[this.collection.searchField] = inputValue;
                }
                this.collection.querydata = _.union( this.collection.querydata, this.collection.searchQuery );
            }
            var query = 'q=[' + JSON.stringify( this.collection.querydata ) + ']';

            this.collection.fetch( {data: query} );
            return false;
        }
    } );
} );