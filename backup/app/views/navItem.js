define( function ( require ) {
    'use strict';
    var fut = require( 'fut/entities' ),
        $ = require( 'jquery' ),
        _ = require( 'underscore' ),
        Backbone = require( 'backbone' ),
        Template = require( 'hbs!templates/navItem' );

    return fut.ItemView.extend( {
        template: Template,
        tagName: 'li',
        className: 'dropdown',

        modelEvents: {
            'change':'render'
        },

        //events: {
        //    'click .dropdown-menu a':'navClick'
        //},

        //navClick: function( event ){
        //    event.preventDefault();
        //    Backbone.history.navigate( $( event.currentTarget ).attr( 'href' ),  {trigger: true} );
        //}
    });
});
