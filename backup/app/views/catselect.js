define( function ( require ) {
    'use strict';

    var fut = require( 'fut/entities' ),
        Components = require( 'components/components' ),
        _ = require( 'underscore' );

    require( 'select2' );

    var ChildView = fut.CompositeView.extend( {
        childViewContainer: 'optgroup',

        initialize: function( options ) {
            _.bindAll( this, 'template' );
            var c = options.model.get( 'children' );
            if ( c ) {
                this.collection = c;
            }
        },

        template: function ( data ) {
            var result = '<option id="' + data.frontoffice_menu_id + '">' + data.name + '</option>';

            if (this.collection && this.collection.length) {
                result += '<optgroup id="' + data.frontoffice_menu_id + '" label="' + data.name + '"></optgroup>';
            }
            return result;
        }
    } );

    var CatSelect = fut.CompositeView.extend({
        collection: null,
        childViewContainer: 'select',
        childView: ChildView,

        bindings: {
            '.select2': 'value:category_id'
        },

        initialize: function( options ) {
            this.className = options.class;
            this.collection = App.request( 'collection', 'fomenu' );
            this.listenTo( this.collection, 'change', this.render );

            console.log('this model', this.model);
        },

        template: function() {
            return '<select class="select2"></select>';
        }
    });

    return fut.mixin( CatSelect, Components.EditableView );
});
