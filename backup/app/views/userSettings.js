define( function ( require, exports ) {
    'use strict';

    var fut = require( 'fut/entities' ),
        _ = require( 'underscore' ),
        Template = require( 'hbs!templates/userSettings' );


    exports.UserSettingsView = fut.ItemView.extend( {
        template: Template,

        templateHelpers: function() {
            return {
                settings: this.model.getCombined()
            };
        },
        triggers: {
            'click #reset': 'resetClicked',
            'click #save': 'saveClicked'
        },

        ui: {
            emailSignature: '#emailSignature',
            elasticSearchUrl: '#ELASTICSEARCHURL'
        },

        onSaveClicked: function() {
            App.settingsmodel.userSettings.set( 'emailSignature', this.ui.emailSignature.val() );
            App.settingsmodel.userSettings.set( 'ELASTICSEARCHURL', this.ui.elasticSearchUrl.val() );
            App.settingsmodel.store();
        },

        onResetClicked: function() {
            var r = this.render;
            App.settingsmodel.resetToDefaults().then(r);
        }
    });

    return exports.UserSettingsView;
} );