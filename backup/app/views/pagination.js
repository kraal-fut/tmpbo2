define ( function ( require ) {
    'use strict';
    var Fut = require( 'fut/entities' ),
        _ = require ( 'underscore' ),
        $ = require( 'jquery' ),
        paginationTemplate = require( 'hbs!templates/pagination' );

    return Fut.Layout.extend( {
        destroyImmediate: true,

        model: undefined,
        className: 'col-xs-12 row',
        template: paginationTemplate,
        filterView: undefined,

        initialize: function( options ) {
            this.model = App.settingsmodel;

            this.hidePerPage = options.hidePerPage;

            this.setCollection( options.collection );
        },

        setCollection: function( collection, render ) {
            this.stopListening( this.collection );
            this.collection = collection;
            this.listenTo( this.collection, 'sync', this.render );

            if ( render ) {
                this.render();
            }
        },

        regions: {
            additionalfiltersview: '#additionalfiltersview'
        },

        templateHelpers: function() {
            var view = this;
            return {
                currentPage: view.collection.state.currentPage,
                perPage: view.collection.state.itemsPerPage,
                pageSet: view.getPageSet.bind( view ),
                perPageOptions: [5, 10, 20, 30],

                showPerPage: function() {
                    return !view.hidePerPage;
                },

                paginationVisible: function() {
                    var totalR = view.collection.state.totalRecords;
                    return _.isNull( totalR ) || totalR > view.collection.state.itemsPerPage;
                }
            };
        },

        addFilterView: function( View, options ) {
            var view = new View( _.extend( {collection: this.collection}, options ) );
            App.filterRegion.show( view );
        },

        onDestroy: function() {
            App.filterRegion.reset();
        },

        ui: {
            perPage: '.perPage'
        },

        events: {
            'click .gotoPage': 'onGotoPageClicked'
        },

        triggers: {
            'click .firstPage': 'firstPageClicked',
            'click .prevPage': 'prevPageClicked',
            'click .nextPage': 'nextPageClicked',
            'click .lastPage': 'lastPageClicked',
            'change @ui.perPage': 'changePerPage'
        },

        onChangePerPage: function() {
            this.collection.setPageSize( this.ui.perPage.val() );
        },

        onFirstPageClicked: function() {
            this.collection.getFirstPage();
        },

        onPrevPageClicked: function() {
            this.collection.getPreviousPage();
        },

        onNextPageClicked: function() {
            this.collection.getNextPage();
        },

        onLastPageClicked: function() {
            this.collection.getLastPage();
        },

        onGotoPageClicked: function( event ) {
            event.preventDefault();
            this.collection.getPage( parseInt( $( event.target ).text(), 10 ) );
        },

        getPageSet: function() {
            var state = this.collection.state;

            return _.first( _.reject( _.range( state.currentPage - 2, state.currentPage + 5 ), function( num ) {
                return num < 1 || num > state.totalPages;
            }, this ), 5 );
        }

    } );
} );
