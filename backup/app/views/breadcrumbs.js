define ( function ( require ) {
    'use strict';

    var Backbone = require( 'backbone' ),
        _ = require( 'underscore' ),
        fut = require( 'fut/entities' ),
        Template = require( 'hbs!templates/breadcrumbs/breadcrumbs' ),
        BreadCrumbTemplate = require( 'hbs!templates/breadcrumbs/breadcrumb' ),
        Events = require( 'components/AppEvents' ),
        EditableView = require( 'components/components' ).EditableView;

    var breadCrumbView = fut.ItemView.extend({
        tagName: 'li',
        template: BreadCrumbTemplate,

        triggers: {
            'click .reset-criteria a': 'resetCriteriaClick',
            'click .remove-criteria a': 'removeCriteriaClick'
        },

        viewComputeds: {
            breadcrumbValue: function() {
                var value = this.getBinding( 'value' );
                if ( _.isObject( value ) ) {
                    if ( _.isObject( value.in ) ) {
                        return value.in.join();
                    }
                    if ( _.isObject( value.$in ) ) {
                        return value.$in.join();
                    }
                    if ( !_.isUndefined( value.gte ) || !_.isUndefined( value.lte ) ) {
                        var result = '';
                        if ( value.gte ) {
                            result += ' >= ' + value.gte;
                        }
                        if ( value.lte ) {
                            result += ' =< ' + value.lte;
                        }
                        return result;
                    }
                }
                return value;
            }
        },
        bindings:{
            '#value': 'text:breadcrumbValue'
        }
    });

    fut.mixin( breadCrumbView, EditableView );

    return fut.CompositeView.extend( {
        tagName: 'ul',
        id: 'breadcrumbsList',
        className: 'breadcrumb',
        template: Template,
        otherCollection: undefined,
        childView: breadCrumbView,

        initialize: function( options ) {
            this.collection = new fut.Backbone.Collection();
            this.otherCollection = options.collection;
            this.listenTo( this.otherCollection, Events.RESET_BREADCRUMBS, this.resetBreadcrumbs );
            this.resetBreadcrumbs();
        },

        childEvents: {
            'removeCriteriaClick': function( child ) {
                this.otherCollection.removeFieldCriteria( child.model.get( 'id' ) );
            }
        },

        onBeforeRender: function() {
            this.$el.find( 'li.remove-criteria' ).remove();
        },

        onRender: function() {
            if ( this.collection.length === 0 ) {
                this.$el.hide();
            } else {
                this.$el.show();
            }
        },

        triggers: {
            'click .reset-criteria a': 'resetCriteriaClick'
        },

        onResetCriteriaClick: function() {
            this.otherCollection.resetCriteria();
        },

        resetBreadcrumbs: function() {
            this.makeBreadcrumbObject();
            this.addSortsToBreadcrumb();
            this.render();
        },

        makeBreadcrumbObject: function() {
            this.collection.reset();

            _.each( this.otherCollection.dataSendSearch, function( Parameters, Field ) {

                if ( Parameters instanceof Backbone.Model ) {
                    Parameters = Parameters.toJSON();
                }

                this.collection.add(
                    new Backbone.Model({
                        sort: undefined,
                        name: Field.split( '.' ).pop(),
                        value: Parameters,
                        id: Field
                    })
                );

            }, this );

            return this.collection.toJSON();
        },

        addSortsToBreadcrumb: function() {
            _.each( this.otherCollection.dataSort, function( direction, field ) {
                var m = this.collection.get( field );
                if ( _.isUndefined( m ) ) {
                    m = new Backbone.Model( { 'name': field } );
                    this.collection.add( m );
                }
                m.set( 'sort', direction );
            }, this );
        }
    } );
} );
