define(function ( require ) {
    'use strict';
    var MultiSelect = require( 'views/filterPopupViews/multiSelect' ),
        Backbone = require( 'backbone' ),
        _ = require( 'underscore' );

    return MultiSelect.extend( {
        multiSelect: false,

        afterInitialize: function() {
            MultiSelect.prototype.afterInitialize.call(this);

            this.model.set(
                'collection',
                new Backbone.Collection([{id: null, label:'all'}, {id:1, label:this.yeslabel||'yes'}, {id:0, label:this.nolabel||'no'}])
            );
        },

        onSendFilter: function () {
            var criterium = this.ui.input.val();
            if ( criterium === 'null') {
                criterium = null;
            } else {
                criterium = !!parseInt( criterium, 10 );
            }

            this.collection.addCriteria( criterium, this.nameColumn );
            this.destroy();
        }

    } );
});
