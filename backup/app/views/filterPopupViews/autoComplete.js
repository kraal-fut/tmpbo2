define ( function ( require ) {
    'use strict';
    var fut = require( 'fut/entities' ),
        _ = require( 'underscore' ),
        TemplateFilterText = require( 'hbs!templates/filterPopupViews/filter_text' ),
        Bloodhound = require( 'bloodhound' );

    require( 'typeahead.jquery' );

    return fut.ItemView.extend( {

        template: TemplateFilterText,
        suggestions: undefined,
        $popoverEl: undefined,
        sourceCollection: undefined,
        fieldName: undefined,

        initialize: function( options ) {
            this.fieldName = options.fieldName;

            this.nameColumn = options.nameColumn || this.fieldName;
            this.$popoverEl = options.$el;
            this.sourceCollection = options.sourceCollection || this.collection;

            this.initializeSuggestions();

            this.render();
        },

        initializeSuggestions: function() {
            this.suggestions = new Bloodhound({
                datumTokenizer: function( d ) {
                    return Bloodhound.tokenizers.whitespace( d );
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:{
                    url: this.sourceCollection.url() + '/ac/' + this.nameColumn + '/%QUERY?limit=10&startonly=true',
                    filter: function( parsedResponse ){
                        return parsedResponse.results;
                    }
                }
            });

            this.suggestions.initialize();
        },

        render: function() {
            this.$popoverEl.popover( {
                html: true,
                title: 'Filter',
                placement: 'bottom',
                trigger: 'manual',
                container: 'body',
                content: this.getContent()
            } );
        },

        getContent: function() {
            return fut.ItemView.prototype.render.apply( this ).el;
        },

        triggers: {
            'click .cancel': 'hide',
            'click .filter': 'sendFilterClick',
            'click .clear': 'clearFilter'
        },

        ui: {
            'input':'input'
        },

        onSendFilter: function( value ) {
            this.collection.addCriteria( value, this.fieldName || this.nameColumn );
            this.$popoverEl.popover( 'destroy' );
        },

        onShow: function() {
            this.$popoverEl.popover( 'show' );

            this.ui.input.typeahead(
                null,
                {
                    source: this.suggestions.ttAdapter(),
                    displayKey: function( key ){
                        return key;
                    }
                }
            ).on( 'typeahead:selected typeahead:autocompleted', _.bind( this.itemSelected, this ) );

            this.ui.input.focus();
        },

        onSendFilterClick: function() {
            this.onSendFilter( this.ui.input.val() );
        },

        itemSelected: function( $e, datum ) {
            this.onSendFilter( datum || this.ui.input.val() );
        },

        onHide: function() {
            this.collection.addCriteria(null, this.fieldName || this.nameColumn);
            this.$popoverEl.popover( 'destroy' );
        }
    } );
} );
