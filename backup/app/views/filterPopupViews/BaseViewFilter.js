define(function ( require ) {
    'use strict';

    var Backbone = require( 'backbone' ),
        _ = require( 'underscore' );

    return {
        currentCellFilter: null,
        yeslabel: '',
        nolabel: '',
        nameColumn: '',
        dataHtml: {},
        tagName: 'div',
        className: 'dropdown open',
        App: undefined,

        initialize: function( options ) {
            this.model = new Backbone.Model();

            this.currentCellFilter = options.$el;
            this.nameColumn = this.currentCellFilter.attr( 'data-name' ) || options.fieldName;
            this.yeslabel = options.yeslabel || '';
            this.nolabel = options.nolabel || '';

            this.collection = options.collection;
            var that = this;

            this.currentCellFilter.popover({
                html : true,
                title: 'Filter',
                placement: 'bottom',
                trigger: 'manual',
                container: 'body',
                content: function() {
                    return that.render().el;
                }
            });

            this.currentCellFilter.popover( 'show' );

            if (_.isFunction(this.afterInitialize)) {
                this.afterInitialize();
            }
        },

        onShow: function() {
            this.$el.find( 'input' ).first().focus();
        },

        keys: {
            'return': 'sendFilterEnter',
            'esc':'destroy'
        },

        sendFilterEnter: function() {
            this.trigger( 'sendFilter' );
        },

        triggers: {
            'click .cancel': 'destroy',
            'click .filter': 'sendFilter',
            'click .clear': 'clearFilter'
        },

        onDestroy: function() {
            this.currentCellFilter.popover( 'destroy' );
        },

        onClearFilter: function() {
            this.collection.removeFieldCriteria( this.nameColumn );
            this.destroy();
            return false;
        }
    };
});
