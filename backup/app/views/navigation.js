define( function ( require ) {
    'use strict';

    var fut = require( 'fut/entities' ),
        _ = require( 'underscore' ),
        Backbone = require( 'backbone' ),
        navigationTemplate = require( 'hbs!templates/navigation' ),
        NavItem = require( 'views/navItem' ),
        NavModel = require( 'models/navigation' );


    return fut.CompositeView.extend( {
        template: navigationTemplate,
        model: undefined,
        childView: NavItem,
        childViewContainer: 'ul.nav',
        className: 'navbar navbar-inverse navbar-fixed-top',
        childViewFollowsViewModel: true,

        templateHelpers: function() {
            return {
                userName: App.settingsmodel.user.get( 'name' )
            };
        },

        initialize: function() {
            this.collection = new Backbone.Collection();
        },

        collectionEvents:{
            'change':'render'
        },

        events: {
            'click #settings': 'settings',
            'click #search': 'search',
            'keyup #generalsearch': 'search'
        },

        settings: function ( event ) {
            event.preventDefault();
            Backbone.history.navigate( $( event.currentTarget ).attr( 'href' ), {trigger: true} );
        },

        search: function( event ) {
            if ( event.type === 'keyup' && event.keyCode !== 13 ) {
                return;
            }
            event.preventDefault();
            App.search.find( $( '#generalsearch' ).val() );
        },

        addItem: function( parent, id, route, name) {
            parent.add( new NavModel( {'id': id, 'route': route, 'name': name, routes: new Backbone.Collection() } ) );
        },

        addMenu: function( route, name ) {
            var segments = route.split('/');

            if ( segments.length === 1 ){
                this.addItem( this.collection, segments[0], route, name);
                return;
            }

            var mainMenu = this.collection.get( segments[0] );

            if ( _.isUndefined( mainMenu ) ) {
                mainMenu = this.collection.add( new NavModel( {
                    'id': segments[0],
                    'route': route,
                    'name': segments[0],
                    routes: new Backbone.Collection()
                } ) );
            }

            mainMenu.get( 'routes' ).add( new NavModel( {'id': segments[1], 'route': route, 'name': name } ) );

            this.render();
        },

        onShow: function() {
            $( document ).ajaxStart( function() {
                $( '#loading' ).toggleClass( 'fa-spin' ).show();
                $( '.blockwhileajax' ).prop( 'disabled', true );
            } ).ajaxComplete( function() {
                $( '.blockwhileajax' ).prop( 'disabled', false );
                $( '#loading' ).toggleClass( 'fa-spin' ).hide();
            } );
        }

    } );
} );
