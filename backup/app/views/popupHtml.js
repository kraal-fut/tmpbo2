define( function( require ) {
    'use strict';
    var Em = require( 'ember-load' ),
        template = require( 'text!templates/popupHtml.hbs' ),
        summernote = require( 'summernote' ),
        cm = require( 'codemirror' ),
        WordPasteParser = require( 'components/wordPasteParser' );

    var getClipboardText = function( event, type ) {
        type = type || 'text/html';

        var pastedText;

        if ( window.clipboardData && window.clipboardData.getData ) { // IE
            pastedText = window.clipboardData.getData( type );
        } else if ( event.clipboardData && event.clipboardData.getData ) {
            pastedText = event.clipboardData.getData( type );
        }
        if ( pastedText === '' && type === 'text/html' ) {
            return getClipboardText( event, 'text/plain' );
        }

        return pastedText;
    };

    var replaceSelectionWithHtml = function( html ) {
        var range;
        if ( window.getSelection && window.getSelection().getRangeAt ) {
            range = window.getSelection().getRangeAt( 0 );
            range.deleteContents();
            var div = document.createElement( 'div' );
            div.innerHTML = html;
            var frag = document.createDocumentFragment(), child;
            while ( (child = div.firstChild) ) {
                frag.appendChild( child );
            }
            range.insertNode( frag );
        } else if ( document.selection && document.selection.createRange ) {
            range = document.selection.createRange();
            range.pasteHTML( html );
        }
    };

    window.EApp.PopupHtmlController = Em.Controller.extend({
        modalValue: Em.computed( 'fieldName', function( key, value ) {
            if ( value ) {
                this.setValue( value );
            }
            return this.model.get( this.get( 'fieldName' ) );
        } ).property( 'fieldName', 'model' ),

        setValue: function( value ) {
            this.model.set( this.get( 'fieldName' ), value );
        }
    });


    window.EApp.PopupHtmlView = Em.View.extend({
        template: Em.Handlebars.compile( template ),
        show: function() {
            var view = this;

            this.$( 'textarea' ).summernote( {
                onChange: function( $editable, contents ) {
                    view.get( 'context' ).setValue( contents );
                },
                height: 300,
                focus: true,
                insertTableMaxSize: {
                    col: 20,
                    row: 20
                },
                toolbar: [
                    ['style', ['style']], // no style button
                    ['style', ['bold', 'italic', 'underline', 'clear']],
//                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
//                    ['height', ['height']],
//                    ['insert', ['picture', 'link']], // no insert buttons
                    ['insert', ['link']],
                    ['table', ['table']], // no table button
                    ['view', ['codeview']]
                    //['help', ['help']] //no help button
                ],
//
                codemirror: {
                    theme: 'monokai',
                    mode: 'htmlmixed',
                    lineNumbers: true,
                    lineWrapping: false
                },
//
                onpaste: function( event ) {
                    event = event.originalEvent;
                    event.preventDefault();

                    var pastedText = getClipboardText( event );
                    var parser = new WordPasteParser( pastedText );
                    var result = parser.parse();

                    replaceSelectionWithHtml( result );
                }
            } );
        }.on( 'didInsertElement' ),

        close: function() {
            this.$( 'textarea' ).destroy();
        }.on( 'willDestroyElement' )
    });
} );