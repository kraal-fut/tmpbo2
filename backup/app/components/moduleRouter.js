define( function( require, exports) {
    'use strict';

    var Em = require( 'ember-load' );

    exports.moduleRouter = {
        controller: undefined,
        AppRouter: undefined,

        initialize: function( options ) {
            this.AppRouter = options.AppRouter || window.AppRouter ;
            this.AppRouter.registerModule( this, this.controller );
        }
    };

    return exports.moduleRouter;
} );
