define( function( require, exports ) {
    'use strict';

    var _ = require( 'underscore' ),
        TextFilter = require( 'views/filterPopupViews/Text' ),
        NumberFilter = require( 'views/filterPopupViews/Number' ),
        BoolFilter = require( 'views/filterPopupViews/dropDownYesNo' ),
        Handlebars = require( 'hbs/handlebars' ),
        AutocompleteFilter = require( 'views/filterPopupViews/autoComplete' ),
        template;

    var filterTypes = {
        'text': TextFilter,
        'number': NumberFilter,
        'autocomplete': AutocompleteFilter,
        'bool': BoolFilter
    };

    exports.FilterableView = {
        events: {},

        initialize: function() {
            template = Handlebars.compile( '<span class="fa fa-search" title="Filter on: {{name}}">{{name}}</span>' );

            if ( _.isUndefined( this.filters ) ) {
                return;
            }

            _.each( this.filters, this.makeFilter, this );
        },

        makeFilter: function ( item ) {
            this.events[('click ' + item.el + ' .fa-search')] = function () {
                this.openFilter( item );
            };
        },

        showFilter: function( item ) {
            item.$el = this.$( item.el ).append( template( {name: item.title || item.field} ) );
        },

        openFilter: function( filterConf ) {
            var that = this;

            var type = function() {
                return filterConf.type ? filterConf.type.toLowerCase() : 'text';
            };

            var collection = function() {
                return filterConf.collection || that.collection;
            };

            var params = {
                $el: filterConf.$el,
                collection: collection(),
                fieldName: filterConf.field,
                nameColumn: filterConf.columnName,
                sourceCollection: filterConf.sourceCollection ? new filterConf.sourceCollection() : collection(),
                yeslabel: filterConf.yeslabel,
                nolabel: filterConf.nolabel
            };

            (new filterTypes[ type() ]( params )).onShow();
        },

        onRender: function() {
            _.each( this.filters, this.showFilter, this );
        }

    };
    
    return exports.FilterableView;
} );