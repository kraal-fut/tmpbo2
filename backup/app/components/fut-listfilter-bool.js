define( function( require ) {
    'use strict';
    var Em = require( 'ember-load' ),
    template = require( 'text!templates/components/fut-listfilter-bool.hbs' );
    require('components/fut-listfilter-text');

    Em.TEMPLATES['components/fut-listfilter-bool'] = Em.Handlebars.compile( template );

    window.EApp.FutListfilterBoolComponent = window.EApp.FutListfilterTextComponent.extend( {
        selectvalues: [
            {label: 'All', value: undefined},
            {label: 'Yes', value: true},
            {label: 'No', value: false}
        ]
    });
} );