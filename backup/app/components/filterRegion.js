define( function( require, exports ) {
    'use strict';
    var Marionette = require( 'marionette' ),
        $ = require( 'jquery' );

    exports.modalRegion = Marionette.Region.extend( {
        el: '#filtercontainer',

        constructor: function() {
            Marionette.Region.prototype.constructor.apply( this, arguments );
            this.on( 'show', this.showRegion, this );
        },

        getEl: function( selector ) {
            return $( selector );
        },

        showRegion: function( view ) {
            view.on( 'destroy', this.hideRegion, this );
            this.$el.show();
            $( '#uicontainer' ).toggleClass( 'col-md-12 col-md-5' );
        },

        hideRegion: function() {
            this.$el.hide();
            $( '#uicontainer' ).toggleClass( 'col-md-12 col-md-5' );
        }
    } );

    return exports.modalRegion;
} );