/*global define:false*/
define(function ( require ) {
    'use strict';
    var _ = require ( 'underscore' ),
        $ = require( 'jquery' ),
        Fut = require ( 'fut/entities' ),
        Epoxy = require ( 'backbone.epoxy' );

    var settings = window.App ? window.App.settingsmodel : undefined;

    var helpers = {

        getBaseUrl: function() {
            var s = settings ? settings : App.settingsmodel;
            return s.get( 'BASE_URL' );
        },

        setSettings: function( settingsmodel ) {
            settings = settingsmodel;
        },

        getBannerImageUrl: function( value ) {
            if ( _.isEmpty( value ) ) {
                return '';
            }

            var url;
            if ( _.isObject( value ) ) {
                url = value.Image;
            } else {
                url = helpers.getBaseUrl() + value;
            }
            return url;
        },

        stripTags: function( value ) {
            if ( _.isUndefined( value ) || _.isNull( value ) ) {
                return '';
            }
            return value.replace( /<\/?([a-z][a-z0-9]*)\b[^>]*>?/gi, '' ).replace( /(&nbsp;)/gi, '' );
        },

        list: function( values ) {
            if ( _.isEmpty( values ) ) {
                return 'empty';
            }
            return _.reduce( values, function( memo, value ) {
                return memo + value + ',';
            }, '' ).slice( 0, -1 );
        },

        pluckList: function( values, field ) {
            if ( _.isEmpty( values ) || _.isUndefined( values ) || _.isUndefined( field ) ) {
                return values;
            }

            if ( _.isFunction( values.pluck ) ) {
                return values.pluck( field );
            }

            return helpers.list( _.pluck( values, field ) );
        },

        yesno: function( value ) {
            return helpers.bool( value ) ? 'Yes' : 'No';
        },

        zero_one: function( value ) {
            return helpers.bool( value ) ? '1' : '0';
        },

        bool: function( value ) {
            if (value === true || value === 'true') { return true; }
            if (value === false || value === 'false') { return false; }

            var intval = parseInt( value, 10 );
            return !(isNaN ( intval ) || intval === 0);
        },

        boolToInt: function( value ) {
            if ( value ) {
                return 1;
            }
            return 0;
        },

        YesNoCollection: {
            collection: [
                {name: 'Yes', value: '1'},
                {name: 'No', value: '0'}
            ],
            labelPath: 'name',
            valuePath: 'value'
        },

        OpenCloseCollection: {
            collection: [
                {name: 'Open', value: '1'},
                {name: 'Close', value: '0'}
            ],
            labelPath: 'name',
            valuePath: 'value'
        },

        striptags: function( val ) {
            return helpers.stripTags ( val );
        },

        round: function( value ) {
            var rounded = parseFloat( value ).toFixed ( 2 );
            return isNaN( rounded ) ? '' : rounded;
        },

        getBannerImage: function( value ) {
            return '<img style="max-height: 100px" src="' + helpers.getBannerImageUrl( value ) + '" >';
        },

        getProductImages: function( images, returnOne ) {
            var baseUrl = helpers.getBaseUrl();
            if ( _.isUndefined( images ) ) {
                return baseUrl + 'img/thumbs/unavail.jpg';
            }

            var result = [];
            var i = _.isFunction( images.toJSON ) ? images.toJSON() : images;

            _.each( i, function( item ) {
                var filename = item.filename;
                var image = baseUrl + 'img/';
                if ( _.isUndefined( item.situation ) ) {
                    var prodlevid = item.prodlevid;
                    image += 'products/' + prodlevid + '/thumb/' + filename;
                } else {
                    image += 'thumbs/' + filename;
                }
                result.push ( image );
            } );

            if ( !_.isUndefined( returnOne ) ) {
                return _.first( result );
            }
            return result;
        },

        /**
         * get Image elements
         * @returns {jQuery}
         */
        getImages: function( images ) {
            if ( _.isUndefined( images ) ) {
                images = this.model.get( 'product_description.Image' );
            }

            var baseUrl = helpers.getBaseUrl();
            var $result = $( '<div/>' );

            if ( _.isArray( images ) || _.isObject( images ) ) {
                _.each( images, function( item ) {
                    var $image = $( '<img/>' );
                    if ( item.get( 'oldSituation' ) ) {
                        $image.attr( 'src', baseUrl + 'img/thumbs/' + item.get( 'filename' ) );
                    } else if ( item.get( 'RawData' ) ) {
                        $image.attr( 'src', item.get( 'RawData' ) );
                    } else {
                        $image.attr( 'src', baseUrl + 'img/products/' + item.get( 'prodlevid' ) + '/thumb/' + item.get( 'filename' ) );
                    }
                    $result.append( $image );
                }, this );
            }

            return $result.html();
        },

        boLink: function( value, type ) {
            var action;
            var identifier;
            if ( type === 'order' ) {
                action = 'to_order';
                identifier = 'ordnu=';
            } else if ( type === 'customer' ) {
                action = 'client2';
                identifier = 'regid=';
            }

            return 'https://' + helpers.getBaseUrl() + 'backoffice/bo40.cgi?action=' + action + '&' + identifier + value;
        },

        fit: function ( value, length, replacement, trimWhere ) {
            value = value.trim();

            if ( length && ('' + value).length > length ) {
                replacement = '' + (replacement || '...');
                length -= replacement.length;
                value = '' + value;
                switch ( trimWhere ) {
                    case 'left':
                        return replacement + value.slice( -length );
                    case 'middle':
                        var leftLen = Math.ceil( length / 2 );
                        return value.substr( 0, leftLen ) + replacement + value.slice( leftLen - length );
                    default:
                        return value.substr( 0, length ) + replacement;
                }
            } else {
                return value;
            }
        }
    };

    Epoxy.binding.addHandler( 'bannerImage', function( $element, value ) {
        $element.attr( 'src', helpers.getBannerImageUrl ( value ) );
    } );

    Epoxy.binding.addFilter( 'striptags', helpers.stripTags );
    Epoxy.binding.addFilter( 'list', helpers.list );
    Epoxy.binding.addFilter( 'plucklist', helpers.pluckList );
    Epoxy.binding.addFilter( 'fit', helpers.fit );

    Epoxy.binding.addFilter( 'checked', {
        get: helpers.bool,
        set: helpers.boolToInt
    } );

    return helpers;
});