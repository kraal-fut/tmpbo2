define( function( require ) {
    'use strict';
    var Em = require( 'ember-load' );
    var d3 = require( 'd3' ),
        nv = require( 'nv' );

    var template = require( 'text!templates/components/fut-chart-bar.hbs' );

    Em.TEMPLATES['components/fut-chart-bar'] = Em.Handlebars.compile( template );

    window.EApp.FutChartBarComponent = Em.Component.extend( {
        title: 'BarChart',
        fieldName: '',
        data: [],
        classNameBindings: 'class',

        dataChanged: function() {
            d3.select( this.$( 'svg' )[0] ).datum( this.get( 'data' ) ).call( this.chart );
        }.observes( 'data' ),

        makeChart: function() {
            var chart = nv.models.discreteBarChart()
                .x( function( d ) {
                return d.label;
            } ).y( function( d ) {
                return d.value;
            } ).tooltips( false )
                .staggerLabels( true )
                .showValues( true )
                .transitionDuration( 250 );

            nv.addGraph( function() {
                nv.utils.windowResize( chart.update );
                return chart;
            } );

            this.chart = chart;

        }.on( 'didInsertElement' )
    });

} );