define( function( require ) {
    'use strict';
    var Em = require( 'ember-load' ),
        template = require( 'text!templates/components/fut-modal.hbs' );

    Em.TEMPLATES['components/fut-modal'] = Em.Handlebars.compile( template );

    window.EApp.FutModalComponent = Em.Component.extend( {
        actions: {
            ok: function() {
                this.$( '.modal' ).modal( 'hide' );
                //this.sendAction( 'ok' );
            }
        },
        show: function() {
            this.$( '.modal' ).modal().on( 'hidden.bs.modal', function() {
                this.sendAction( 'close' );
            }.bind( this ) );
        }.on( 'didInsertElement' )
    } );
} );