/**
 * @module components/AppEvents
 */
define( function( require, exports ) {
    'use strict';
        /**
         * @enum {string}
         */
    exports.AppEvents = {
        /**
         * Authorized event, create new product
         */
        AUTHORIZED_PRODUCT_NEWPRODUCT: 'authorized:product:newProduct',
        /**
         * Authorized event, delete image
         */
        AUTHORIZED_PRODUCT_DELETEIMAGE: 'authorized:product:deleteImage',
        /**
         * Request authorisation for creating a new product
         */
        PRODUCT_NEWPRODUCT: 'product:newProduct',
        /**
         * Request authorisation for deleting an image
         */
        PRODUCT_DELETEIMAGE: 'product:deleteImage',
        /**
         * Show a notification
         */
        UI_NOTIFY: 'ui:notify',
        /**
         * Show a desktop notification (requires special permissions from browser)
         */
        UI_DESKTOP_NOTIFY: 'ui:desktopnotify',
        /**
         * Add a undoable task
         */
        UI_ADDUNDO: 'ui:addUndo',
        /**
         * Undo a task
         */
        UI_DOUNDO: 'ui:doUndo',
        /**
         * Load criteria
         */
        UI_SEARCH_FETCHCRITERIA: 'ui:search:fetchCriteria',

        /**
         * Open a popup for filtering
         */
        UI_CMS_OPENFILTERPOPUP: 'ui:cms:openFilterPopup',
        /**
         * A product is selected
         */
        UI_PRODUCT_SELECT: 'ui:product:select',
        /**
         * A product is deselected
         */
        UI_PRODUCT_DESELECT: 'ui:product:deselect',
        /**
         * Scrolling is happing in cms
         */
        UI_SCROLL_CMS: 'ui:scroll:cms',
        /**
         * The cms should scroll to the bottom
         */
        UI_CMS_SCROLLTOBOTTOM: 'ui:cms:scrollToBottom',
         /**
         * Go to next cell
         */
        UI_CELL_NEXT: 'ui:cell:next',
        /**
         * Go to previous cell
         */
        UI_CELL_PREV: 'ui:cell:prev',
        /**
         * Don't know
         */
        UI_FOCUS_CMS_BACK: 'ui:focus:cms:back',
        /**
         * Also don't know
         */
        UI_CLICK_FOMENU_COLLAPSER: 'ui:click:fomenu:collapser',

        RESET_BREADCRUMBS: 'resetBreadcrumbs'
    };

    return exports.AppEvents;
} );
