define( function( require, exports ) {
    'use strict';
    var Marionette = require( 'marionette' );

    exports.SideBarRegion = Marionette.Region.extend({
        el: '#sidebar',
        open: false,

        constructor: function() {
            Marionette.Region.prototype.constructor.apply( this, arguments );
            this.on( 'show', this.showRegion, this );
        },

        getEl: function( selector ) {
            return $( selector );
        },

        showRegion: function( view ) {
            view.on( 'destroy', this.hideRegion, this );
            var that = this;

            this.$el.show();
            this.$el.append(
                $('<div id="sidebarCollapser"><i class="fa fa-bars fa-2x"></i></div>').on(
                    'click',
                    function() {
                        that.toggleSidebar();
                    }
                )
            );

        },
        toggleSidebar: function() {
            this.$el.css( 'left', this.open ? '-25%' : '0%' );
            this.open = !this.open;
        },

        hideRegion: function() {
            this.$el.hide();
        }
    });

    return exports.SideBarRegion;
});