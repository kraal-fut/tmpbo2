define( function( require ) {
    'use strict';

    var Em = require( 'ember-load' ),
        codemirror = require( 'codemirror' );

    require( 'cm/addon/edit/matchbrackets' );
    require( 'cm/addon/mode/overlay' );
    require( 'cm/addon/edit/closebrackets' );
    require( 'cm/addon/edit/matchtags' );
    require( 'cm/addon/edit/closetag' );

    var cmOptions = {
        theme: 'monokai',
        mode: 'mustache',
        lineNumbers: true,
        lineWrapping: false,
        matchBrackets: true,
        autoCloseBrackets: true,
        autoCloseTags: true,
        matchTags: true,
        indentUnit: 4,
        viewportMargin: Infinity
    };

    window.EApp.FutCodeviewComponent = Em.Component.extend( {
        tagName: 'textarea',

        initializeEditor: function() {
            var self = this;

            this.addMustacheMode();

            var cm = codemirror.fromTextArea( self.$().get( 0 ), cmOptions );

            cm.setValue( self.get( 'value' ) );

            cm.on( 'change', function( instance ) {
                Em.run( function() {
                    self.set( 'value', instance.getValue() );
                } );
            } );

            this.set( 'editor', cm );

        }.on( 'didInsertElement' ),

        addMustacheMode: function() {
            codemirror.defineMode( 'mustache', function ( config, parserConfig ) {
                var mustacheOverlay = {
                    token: function ( stream ) {
                        var ch;
                        if ( stream.match( '{{' ) ) {
                            while ( ( ch = stream.next() ) ) {
                                if ( ch === '}' && stream.next() === '}' ) {
                                    stream.eat( '}' );
                                    return 'mustache';
                                }
                            }
                        }
                        while ( stream.next() && !stream.match( '{{', false ) ) {
                        }
                        return null;
                    }
                };

                return codemirror.overlayMode(
                    codemirror.getMode( config, parserConfig.backdrop || 'text/html' ),
                    mustacheOverlay
                );
            } );
        }
    });
} );