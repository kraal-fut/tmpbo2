define( function( require, exports ) {
    'use strict';

    var $ = require( 'jquery' ),
        _ = require( 'underscore' ),
        Events = require( 'components/AppEvents' );

    exports.notificationCenter = function() {
        this.permission = 'default';

        this.addResponders();

        App.notifyQuick = function( message, options ) {
            App.vent.trigger( Events.UI_NOTIFY, { text: message, options: options || {} } );
        };

        App.notify = function( message, options ) {
            App.vent.trigger( Events.UI_DESKTOP_NOTIFY, { text: message, options: options || {} } );
        };
    };

    exports.notificationCenter.prototype = {
        addResponders: function() {
            App.vent.on( Events.UI_NOTIFY, function ( notifyEvent ) {
                var notifycontainer = $( '#notificationcontainer' );
                var notification = $( '<div/>' )
                    .addClass( 'alert alert-' + ( notifyEvent.options.status || 'info' ) )
                    .append( notifyEvent.text );

                if ( typeof( notifyEvent.options.onClick ) === 'function' ) {
                    notification.append(
                        $( '<button/>' )
                            .addClass( 'btn' )
                            .addClass( 'btn-mini' )
                            .addClass( 'btn-success' )
                            .attr( 'data-dismiss', 'alert' )
                            .css('margin-left', '20px')
                            .html( 'Ok!' )
                            .on( 'click', notifyEvent.options.onClick )
                    );

                    notification.append(
                        $( '<button/>' )
                            .addClass( 'btn' )
                            .addClass( 'btn-xs' )
                            .addClass( 'btn-danger' )
                            .attr( 'data-dismiss', 'alert' )
                            .html( 'Close' )
                            .css('margin-left', '20px')
                    );
                } else {
                    notification.append(
                        $( '<button/>' )
                            .addClass( 'close' )
                            .attr( 'data-dismiss', 'alert' )
                            .html( '&times;' )
                    );

                    var delay = 5000;
                    if ( !_.isUndefined( notifyEvent.options.delay ) ) {
                        delay = notifyEvent.options.delay;
                    }

                    if ( delay !== 0 ) {
                        notification.delay( delay ).fadeOut( 1000, function () {
                            $( this ).remove();
                        } );
                    }
                }

                notifycontainer.append( notification ).show();
            } );

            App.vent.on( Events.UI_DESKTOP_NOTIFY, function( notifyEvent ) {
                var sendDesktopNotify = function() {
                    var notification = new window.Notification( 'Futurumshop BO2', {
                        body: notifyEvent.text,
                        icon: '/apple-touch-icon.png'
                    } );

                    setTimeout( function() {
                        notification.close();
                    }, notifyEvent.options.delay || 5000 );
                };

                if ( window.Notification.permission === 'granted' ) {
                    sendDesktopNotify();
                    return;
                }
                if ( window.Notification.permission !== 'denied' ) {
                    window.Notification.requestPermission( function ( permission ) {
                        if ( permission === 'granted' ) {
                            sendDesktopNotify();
                        } else {
                            App.vent.trigger( Events.UI_NOTIFY, notifyEvent );
                        }
                    } );
                    return;
                }
                App.vent.trigger( Events.UI_NOTIFY, notifyEvent );
            } );
        }
    };

    return exports.notificationCenter;
} );