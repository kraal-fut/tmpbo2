define( function( require, exports ) {
    'use strict';
    var _ = require( 'underscore' );

    exports.base64ImageUpload = {

        initialize: function() {
            this.toggleHover( undefined, false );
        },

        events: {
            'dragstart .imageDropArea': 'sinkEvent',
            'dragover .imageDropArea[data-editing="false"]': 'sinkEvent',
            'dragover .imageDropArea[data-editing="true"]': 'handleDragOver',
            'dragenter .imageDropArea': 'startDrag',
            'dragend .imageDropArea': 'stopDrag',
            'dragleave .imageDropArea': 'stopDrag',
            'drop .imageDropArea[data-editing="false"]': 'sinkEvent',
            'drop .imageDropArea[data-editing="true"]': 'handleDrop'
        },

        /**
         * Default event handler for events that should just be prevented
         * @param event
         * @returns {boolean}
         */
        sinkEvent: function ( event ) {
            event.preventDefault();
            return false;
        },

        /**
         * Handle a drop operation
         * @param event
         */
        handleDrop: function ( event ) {
            event.preventDefault();

            var $target = this.toggleHover( event, false );
            $target.html( 'Drop images here' );

            var files = event.originalEvent.dataTransfer.files;
            var fieldName = $( event.currentTarget ).data( 'field' ) || $( event.currentTarget ).attr( 'id' );
            this.setFiles( files, fieldName, event );
        },

        setFiles: function( files, fieldName ) {
            var self = this;

            _.each( files, function ( f ) {

                var reader = new FileReader();

                reader.onload = ( function ( theFile, that ) {
                    return function ( e ) {
                        if ( _.isFunction( that.imageSetter ) ) {
                            that.imageSetter( theFile, e, fieldName, e );
                        } else {
                            self.fileSetter( theFile, e, that, fieldName );
                        }
                    };
                } )( f, this );

                reader.readAsDataURL( f );
            }, this );
        },

        fileSetter: function( file, event, context, fieldName ) {
            context.model.set( fieldName, {
                ImageFilename: file.name,
                Image: event.target.result
            } );
        },

        /**
         * Handle a dragover and set the indicator explicitly to copy
         * @param event
         */
        handleDragOver: function( event ) {
            event.stopPropagation();
            event.preventDefault();
            event.originalEvent.dataTransfer.dropEffect = 'copy';
        },

        startDrag: function( event ) {
            var $target = this.toggleHover( event, true );

            $target.removeClass( 'dropError' );
            $target.html( 'Drop image' );
            this.sinkEvent( event );
        },

        stopDrag: function( event ) {
            var $target = this.toggleHover( event, false );

            $target.html( 'Drop images here' );
            this.sinkEvent( event );
        },

        toggleHover: function( event, value ) {
            if ( this.viewModel ) {
                this.viewModel.set( 'hovering', value );
            }

            var hasUiDropArea = function( view ) {
                return !_.isUndefined( view.ui ) && !_.isUndefined( view.ui.dropArea ) && !_.isString( view.ui.dropArea );
            };

            if ( hasUiDropArea( this ) ) {
                this.ui.dropArea.toggleClass( 'dropHover', value );
                return this.ui.dropArea;
            }

            if ( !_.isUndefined( event ) ) {
                return $( event.target );
            }
        }
    };

    return exports.base64ImageUpload;
});