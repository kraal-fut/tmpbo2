define( function( require ) {
    'use strict';

    var Em = require( 'ember-load' ),
        template = require( 'text!templates/components/banner-upload.hbs' );

    Em.TEMPLATES['components/banner-upload'] = Em.Handlebars.compile( template );

    window.EApp.BannerUploadComponent = Em.Component.extend( {
        hovering: false,
        hoverTimer: undefined,
        hasImage: Em.computed.or( 'uploadImage', 'banner' ),

        getImageContent: function() {
            if ( this.get( 'uploadImage' ) && this.get( 'uploadImage.Image' ) ) {
                return this.get( 'uploadImage.Image' );
            }

            var url = App.settingsmodel.get( 'BASE_URL' );
            if ( this.get( 'path' ) ) {
                url+= '/' + this.get( 'path' ) + '/';
            }
            url += this.get( 'banner' );

            return url;
        }.property( 'uploadImage', 'banner', 'path' ),

        safeStyle: function() {
            var style = this.get( 'style' ) || '';
            if ( this.get( 'width' ) ) {
                style+=';max-width: ' + this.get( 'width' ) + ';overflow-x:hidden;';
            }
            style+=';min-width: 90%;min-height: 2em';
            return style.htmlSafe();
        }.property( 'style', 'width' ),

        mouseEnter: function( event ) {
            if (!this.get('width')) {
                return false;
            }
            var that = this;
            this.hoverTimer = setTimeout( function() {
                that.$( event.currentTarget ).find( '.imageDropArea' ).animate( {'max-width': 400} );
            }, 300 );
            return false;
        },

        mouseLeave: function( event ) {
            clearTimeout( this.hoverTimer );
            if ( !this.get( 'width' ) ) {
                return false;
            }
            this.$( event.currentTarget ).find( '.imageDropArea' ).animate( {'max-width': this.get('width')} );
            return false;
        },
        dragEnter: function( event ) {
            if ( this.get( 'editing' ) ) {
                this.set( 'hovering', true );
            }
            event.preventDefault();
        },

        dragOver: function( event ) {
            event.preventDefault();
        },

        dragLeave: function( event ) {
            this.set( 'hovering', false );
            event.preventDefault();
        },

        drop: function( event ) {
            event.preventDefault();
            if ( this.get( 'editing' ) ) {
                this.setFiles( event.originalEvent.dataTransfer.files );
            }
        },

        setFiles: function( files ) {
            for ( var i = 0; i < files.length; i++ ) {
                this.setFile( files[i] );
            }
        },

        setFile: function( f ) {
            var reader = new FileReader();

            reader.onload = (function( file, that ) {
                return function( e ) {
                    that.set( 'uploadImage', {ImageFilename: file.name, Image: e.target.result} );
                };
            })( f, this );

            reader.readAsDataURL( f );
        }
    } );
} );