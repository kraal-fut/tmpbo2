define ( function ( require, exports ) {
    'use strict';

    var PaginationView = require( 'views/pagination' );

    exports.PaginatedView = {
        paginator: undefined,

        initialize: function() {
            var c = this.sourceCollection || this.collection;
            this.paginator = new PaginationView( {collection: c, settings: this.settings} );
        },

        setCollection: function( collection, render ) {
            this.stopListening( this.collection );

            this.collection = collection;

            this.listenTo( this.collection, 'sync', this.render );

            this.paginator.setCollection( collection, render );

            if ( render ) {
                this.render();
            }
        },

        onShow: function() {
            App.uiRegion.show( this.paginator );
        },

        onDestroy: function() {
            App.uiRegion.reset();
        }
    };

    return exports.PaginatedView;
});