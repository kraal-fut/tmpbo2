define( function( require ) {
    'use strict';

    var Em = require( 'ember-load' ),
    template = require( 'text!templates/components/fut-listfilter-text.hbs' );

    Em.TEMPLATES['components/fut-listfilter-text'] = Em.Handlebars.compile( template );

    window.EApp.FutListfilterTextComponent = Em.Component.extend( {
        tagName: 'span',
        filterOpen: false,

        hasValue: Em.computed.notEmpty( 'filtervalue' ),

        popoverClose: function( popover ) {
            if ( popover && popover.close ) {
                popover.close();
            }
        },

        actions: {
            toggleFilter: function() {
                this.toggleProperty( 'filterOpen' );
            },

            filter: function( popover ) {
                this.sendAction( 'setFilter', this.get( 'field' ), this.get( 'filtervalue' ) );
                this.popoverClose( popover );
            },

            clear: function( popover ) {
                this.set( 'filtervalue', undefined );
                this.sendAction( 'clearFilter', this.get( 'field' ) );
                this.popoverClose( popover );
            }
        }
    });
} );