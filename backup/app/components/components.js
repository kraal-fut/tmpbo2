define ( function ( require, exports ) {
    'use strict';
    exports.components = {
        Events: require( 'components/AppEvents' ),
        DefaultListView: require( 'fut/defaultListView' ),
        EditableView: require( 'components/editableEpoxyView' ),
        SortableView: require( 'components/sortableView' ),
        PaginatedView: require( 'components/paginatedView' ),
        StickitHelpers: require( 'components/stickitHelpers' ),
        ImageUploader: require( 'components/base64ImageUpload' ),
        Notification: require( 'components/notifications' ),
        ModalRegion: require( 'components/modalRegion' ),
        FilterRegion: require( 'components/filterRegion' ),
        SideBarRegion: require( 'components/sideBarRegion' ),
        FutKey: require( 'components/futKey' ),
        Search: require( 'components/search' ),

        SelectAbleCollection: require( 'collections/SelectAbleCollection' )
    };

    return exports.components;
});
