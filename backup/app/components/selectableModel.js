define( function ( require, exports ) {
    'use strict';

    exports.SelectableModel = {

        select: function() {
            if ( this.selected ) {
                return;
            }

            this.selected = true;
            this.set( 'selected', true );

            this.trigger( 'selected', this );

            if ( this.collection ) {
                this.collection.doSelect( this );
            }
        },

        deselect: function() {
            if ( !this.selected ) {
                return;
            }

            this.selected = false;
            this.set( 'selected', false );

            this.trigger( 'deselected', this );

            if ( this.collection ) {
                this.collection.deselect( this );
            }
        },

        toggleSelected: function() {
            if ( this.selected ) {
                this.deselect();
            } else {
                this.select();
            }
        }
    };

    return exports.SelectableModel;
} );