define(function( require, exports ) {
    'use strict';

    var _ = require( 'underscore' ),
        $ = require( 'jquery' ),
        Events= require( 'components/AppEvents' ),
        Epoxy = require( 'backbone.epoxy' ),
        FutBinding = require( 'components/FutBinding' );

    var EditableView = {
        App: undefined,
        vent: undefined,
        viewModel: undefined,
        handlesErrors: false,
        binder: undefined,
        $currentCell: undefined,

        defaultTemplateHelpers: function() {
            var view = this;
            return {
                editing: function() {
                    return view.editing;
                },
                editClass: function() {
                    return view.editing ? 'glyphicon-ban-circle' : 'glyphicon-pencil';
                }
            };
        },

        initialize: function( options ) {
            if ( options.model ) {
                this.model = options.model;
                this.bindValidation();
            }

            this.setTemplateHelpers();

            if ( _.isFunction( this.AfterInitialize ) ) {
                this.AfterInitialize( options );
            }

            this.binder = new FutBinding( this );
        },

        onRender: function() {
            if ( _.isUndefined( this.editing ) ) {
                return this.toggleEdit( false );
            }
            this.toggleEdit( this.editing );
        },

        setTemplateHelpers: function() {
            if ( _.isUndefined( this.templateHelpers ) ) {
                this.templateHelpers = this.defaultTemplateHelpers;
            } else {
                var h = _.isFunction( this.templateHelpers ) ? this.templateHelpers() : this.templateHelpers;
                this.templateHelpers = _.extend( {}, h, this.defaultTemplateHelpers() );
            }
        },

        ui: {
            selected: '#selected',
            editable: '.editable',
            saveButton: '#save',
            showEdit: '.show-edit',
            toggleEdit: '#toggleEdit',
            deleteButton: '#delete'
        },

        triggers: {
            'click @ui.toggleEdit': 'toggleEditClicked',
            'click @ui.saveButton': 'saveClicked',
            'click @ui.deleteButton': 'deleteClicked',
            'click @ui.selected': {
                event: 'selected',
                preventDefault: false,
                stopPropagation: false
            }
        },

        events: {
            'dblclick td.popup.editing': 'openPopup',
            'click td': 'select'
        },

        bindingFilters: {
            defined: {
                get: function( value ) {
                    return value;
                },
                set: function( value ) {
                    if ( value === '' ) {
                        return null;
                    }
                    return value;
                }
            }
        },

        onSelected: function( options ) {
            if ( _.isFunction( options.model.select ) ) {
                options.model.select();
            }
        },

        onToggleEditClicked: function() {
            if ( !this.followViewModel ) {
                this.toggleEdit();
            }
        },

        toggleEdit: function( value, afterSave ) {
            var newValue = _.isUndefined( value ) ? !this.editing : value;
            this.editing = newValue;

            if ( newValue ) {
                this.ui.editable.attr( 'data-editing', 'true' );
                this.ui.editable.toggleClass( 'editing', true );
                this.ui.editable.prop( 'disabled', false );
                this.ui.saveButton.show();
                this.ui.showEdit.show();
                this.ui.toggleEdit.toggleClass( 'glyphicon-ban-circle', true );
                this.ui.toggleEdit.toggleClass( 'glyphicon-pencil', false );
            } else {
                this.ui.editable.attr( 'data-editing', 'false' );
                this.ui.editable.toggleClass( 'editing', false );
                this.ui.editable.prop( 'disabled', true );
                this.ui.saveButton.hide();
                this.ui.showEdit.hide();
                this.ui.toggleEdit.toggleClass( 'glyphicon-ban-circle', false );
                this.ui.toggleEdit.toggleClass( 'glyphicon-pencil', true );
            }

            if ( newValue === false && afterSave === true ) {
                this.model.fetch();
            }
        },

        onDeleteClicked: function() {
            var result = confirm( 'This will permanently delete this item, are you sure?' );
            if ( !result ) {
                return;
            }

            this.model.destroy( {
                error: function( model, response ) {
                    console.log( 'error', model, response );
                }
            } );
        },

        modelEvents: {
            'validated:invalid': 'invalid',
            'validated:valid': 'valid',
            'change:selected': 'changeSelected'
        },

        onSaveClicked: function() {
            this.binder.updateModel();

            if ( _.isFunction( this.beforeSave ) ) {
                var proceed = this.beforeSave( this.model );
                if ( proceed === false ) {
                    return;
                }
            }

            if ( !this.model.isValid() ) {
                return;
            }

            this.model.save( {}, {
                success: _.bind( this.saveSuccess, this ),
                error: _.bind( this.saveFailed, this )
            } );
        },

        saveSuccess: function() {
            this.toggleEdit( false, true );

            var render = true;
            if ( _.isFunction( this.onAfterSave ) ) {
                render = this.onAfterSave( this.model );
            }

            if ( render ) {
                this.render();
            }
        },

        saveFailed: function( model, error, response ) {
            if ( _.isFunction( this.onSaveError ) ) {
                this.onSaveError( model, error, response );
            }
        },


        getDataFieldType: function() {
            if ( this.$currentCell ) {
                return this.$currentCell.data( 'field-type' );
            }
            return '';
        },


        select: function( event ) {
            var $target = $( event.target );

            if ( $target.is( 'img' ) ) {
                $target = $target.parents( 'td' );
            }

            this.$currentCell = $target;
        },


        valid: function() {
            this.$el.find( '.error-cell-red' ).removeClass( 'error-cell-red' );
        },

        onValidField: function( attrName ) {
            var $field = this.$el.find( '#' + attrName );

            $field.removeClass( 'error-cell-red' );
            $field.parent().removeClass( '.has-error' );
            $field.closest( '.form-control-feedback' ).removeClass( 'glyphicon-remove' );
        },

        onInvalidField: function( name ) {
            if ( this.handlesErrors ) {
                return;
            }

            var $field = this.$el.find( '#' + name );
            $field.parent().addClass( 'has-error' ).find( '.form-control-feedback' ).addClass( 'glyphicon-remove' );
        },

        invalid: function( model, data, errors ) {
            if ( this.handlesErrors ) {
                return;
            }

            var msgErrors = _.reduce( errors, function( memo, error, fieldName ) {
                return memo + fieldName + ' ' + error + ', ';
            }, '', this );

            if ( !_.isEmpty( msgErrors ) ) {
                var message = msgErrors.slice( 0, -2 ).toLowerCase();
                App.vent.trigger( Events.UI_NOTIFY, {text: message, status: 'danger'} );
            }
        },

        fo2Url: function( url ) {
            return App.settingsmodel.get( 'BASE_URL' ) + url;
        },

        changeSelected: function( model, selected ) {
            this.ui.selected.prop( 'checked', selected );
        }
    };

    exports.EditableView = Epoxy.View.mixin( EditableView );

    return exports.EditableView;
} );
