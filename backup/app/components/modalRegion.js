define ( function ( require, exports ) {
    'use strict';
    var Marionette = require( 'marionette' ),
        $ = require( 'jquery' );

    exports.modalRegion = Marionette.Region.extend({
        el: '#popupcontainer',

        constructor: function() {
            Marionette.Region.prototype.constructor.apply( this, arguments );
            this.on( 'show', this.showPopover, this );
        },

        getEl: function( selector ) {
            var $el = $( selector );
            $el.on( 'hidden', this.destroy );
            return $el;
        },

        showPopover: function( view ) {
            view.on( 'destroy', this.hidePopover, this );
            this.$el.modal( 'show' );
        },

        hidePopover: function() {
            this.$el.modal( 'hide' );
        }
    });

    return exports.modalRegion;
});