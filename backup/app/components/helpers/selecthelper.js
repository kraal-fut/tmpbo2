define( function ( require, exports ) {
    'use strict';

    var Handlebars = require( 'hbs/handlebars' ),
        _ = require( 'underscore' ),
        objToParams = require( 'components/helperObjectToParam' ),
        Backbone = require( 'backbone' );

    var worker = {
        htmloptions: function ( original ) {
            return _.omit( original, ['idfield', 'label', 'collection', 'default'] );
        },

        makeOption: function ( value, id, label ) {
            if ( _.isUndefined( label ) ) {
                label = id;
            }

            var option = $( '<option/>' )
                .prop( 'value', id )
                .text( label );

            if ( id == value ) {
                option.attr( 'selected', 'selected' );
            }

            return option;
        },

        getCollection: function( collection ) {
            var c;

            if ( _.isString( collection ) ) {
                c = App.request( 'collection', collection );
            } else {
                c = collection;
            }

            c = c || [];

            if ( _.isArray( c ) ) {
                var getItem = function ( memo, item ) {
                    if ( _.isObject( item ) ) {
                        memo.push( item );
                    } else {
                        memo.push( {id: item} );
                    }
                    return memo;
                };

                c = new Backbone.Collection( _.reduce( c, getItem, [] ) );
            }

            return c;
        },

        build: function( value, options, modelData ) {
            var collection = this.getCollection( options.collection );

            var idField = options.idfield || 'id';
            var textFieldName = options.label || 'label';

            var result = $( '<span/>' );
            var select = $( '<select ' + objToParams( this.htmloptions( options ), modelData.editing ) + '/>' );

            if ( _.has( options, 'default' ) ) {
                select.append( this.makeOption( value, null, options.default ) );
            }

            collection.each( function ( item ) {
                select.append( this.makeOption( value, item.get( idField ), item.get( textFieldName ) ) );
            }, this );

            result.append( select );

            return new Handlebars.SafeString( result.html() );
        }
    };

    exports.SelectHelper = {
        helper: function ( value, context ) {
            return worker.build.call( worker, value, context.hash || {}, this );
        },
        worker: worker
    };

    return exports.SelectHelper;
} );