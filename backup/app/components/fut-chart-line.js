define( function( require ) {
    'use strict';
    var Em = require( 'ember-load' );
    var d3 = require( 'd3' ),
        nv = require( 'nv' ),
        _ = require( 'underscore' );

    var template = require( 'text!templates/components/fut-chart-line.hbs' );

    Em.TEMPLATES['components/fut-chart-line'] = Em.Handlebars.compile( template );

    window.EApp.FutChartLineComponent = Em.Component.extend( {
        title: 'LineChart',
        fieldName: '',
        data: [],
        domains: [],
        classNameBindings: 'class',

        dataChanged: function() {
            var data = this.getData();
            d3.select( this.$( 'svg' )[0] ).datum( data ).call( this.chart );
        }.observes( 'data.avgRevHist' ).on('didInsertElement'),

        getData: function() {
            return _.map( this.get( 'domains' ), function( item ) {
                item.values = this.get( 'data.' + item.fieldname ) || [];
                return item;
            }, this );
        },

        makeChart: function() {
            var chart = nv.models.lineChart()
                .showYAxis( true )
                .showXAxis( true )
                .useInteractiveGuideline( true );

            chart.xAxis
                .axisLabel( 'Time' )
                .tickFormat( function( d ) {
                    return d3.time.format( '%d %b %Y' )( new Date( d ) );
                } );

            chart.yAxis
                .axisLabel( 'Euro' )
                .tickFormat( d3.format( '.02f' ) );

            nv.addGraph( function() {
                nv.utils.windowResize( chart.update );
                return chart;
            } );

            this.chart = chart;
        }.on( 'didInsertElement' )
    });

} );