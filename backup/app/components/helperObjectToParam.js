define( function( require, exports ) {
    'use strict';

    var _ = require( 'underscore' );

    var isEditable = function( options ) {
        return !_.isUndefined( options.class ) && options.class.indexOf( 'editable' ) > -1;
    };

    exports.helperObjectToParam = function( obj, editing ) {

        var truthyFalseyParams = ['checked', 'disabled', 'selected'];

        if ( isEditable( obj ) ) {
            obj.disabled = obj.disabled || editing;
        }

        var makeParam = function ( memo, value, key ) {
            if ( _.contains( truthyFalseyParams, key ) ) {
                if ( value ) {
                    value = key;
                } else {
                    return memo;
                }
            }
            if ( _.isUndefined( value ) || _.isNull( value ) ) {
                value = '';
            }
            return memo + key + '="' + value + '" ';
        };

        return _.reduce( obj, makeParam, '' );
    };

    return exports.helperObjectToParam;
} );