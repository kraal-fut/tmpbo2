define( function( require, exports ) {
    'use strict';
    var _ = require( 'underscore' ),
        Mousetrap = require( 'mousetrap' );

    exports.futKey = {
        initialize: function() {
            this.bindKeys();

            this.on('focus', function(){
                this.bindKeys();
            });
        },

        onDestroy: function( ) {
            Mousetrap.unbind( _.keys( this.keys ) );
        },

        bindKeys: function() {
            _.each( this.keys, function( handler, key ){
                Mousetrap.bind( key, _.bind(function( event ){
                    event.preventDefault();
                    this.triggerMethod( handler );

                    var fn = this[handler];

                    if ( _.isFunction( fn ) ) {
                        fn.call(this, event);
                    }
                }, this));
            }, this);
        }
    };

    return exports.futKey;
});