define( function( require, exports ) {
    'use strict';

    var _ = require( 'underscore' ),
        Backbone = require( 'backbone-associations' ),
        Fut = require( 'fut/entities' );
        //Orders = require( 'order/Order/collections/orders' );
        //OrdersListView = require( 'order/Order/views/OrderListView' ),
        //OrderDetailView = require( 'order/Order/views/OrderDetailView' );

    exports.search = function() {
        this.collection = new Fut.Collection( [] );
        this.collection.url = '/api/v3/orders/search';
    };

    exports.search.prototype = {

        find: function( query ) {

            var view = this;

            var generalSeach = function() {
                if ( _.isEmpty( query ) ) {
                    query = '*';
                }
                view.collection.load( { data: { q: query }, success: view.handleFind, error: view.handleError } );
            };

            var cv = App.mainRegion.currentView;

            if ( cv && _.isFunction( cv.search ) ) {

                return cv.search( query ).then(function( collection ) {
                    if ( _.isFunction( cv.setCollection ) ) {
                        cv.setCollection( collection, true );
                    }
                }).catch(function() {
                    generalSeach();
                });
            }

            generalSeach();
        },

        handleFind: function( collection ) {
            if ( collection.length === 1 ) {
                var model = collection.first();
                Backbone.history.navigate( 'orders/orders/' + model.id );
                //App.mainRegion.show( new OrderDetailView( { model: model } ) );
            } else {
                Backbone.history.navigate( 'orders/orders' );
                //App.mainRegion.show( new OrdersListView( { collection: collection } ) );
            }
        },

        handleError: function( collection, error ) {
            var response = JSON.parse( error.responseText );
            App.notify( response.error.message );
        }
    };

    return exports.search;
} );