define( function( require ) {
    'use strict';
    var Em = require( 'ember-load' ),
        template = require( 'text!templates/components/bootstrap-popover.hbs' );

    Em.TEMPLATES['components/bootstrap-popover'] = Em.Handlebars.compile( template );

    window.EApp.BootstrapPopoverComponent = Em.Component.extend( {
        tagName: 'div',
        classNames: '',
        placement: 'bottom',
        title: 'bo2',

        didInsertElement: function() {
            var component = this,
                contents = this.$( '.popovercontent' );

            component.$().popover( {
                animation: true,
                placement: component.get( 'placement' ),
                html: true,
                trigger: 'manual',
                content: contents,
                title: component.get( 'title' )
            } ).on( 'show.bs.popover', function() {
                contents.removeClass( 'hide' );
            } );

            component.$().popover( 'show' );
        },

        close: function() {
            this.$().popover( 'destroy' );
        },

        willDestroyElement: function() {
            this.close();
        },

        actions: {
            close: function() {
                this.close();
            }
        }
    } );

} );