define ( function ( require, exports ) {
    'use strict';
    var _ = require( 'underscore' ),
        $ = require( 'jquery' );

    exports.SortableView = {
        events: {
            'click th .sort': 'dosort'
        },

        initialize: function() {
            this.listenTo( this.collection, 'sync', this.disableSort );
        },

        dosort: function( event ) {
            console.log('going to sort');
            event.stopPropagation();

            var target = $( event.currentTarget );
            var fieldName = target.closest( 'th' ).data( 'name' );
            var sortOrder = this.getSortOrder( fieldName );

            if ( sortOrder === 'removeSort' ) {
                this.collection.removeSort( fieldName );
            } else {
                this.collection.addSort( fieldName, sortOrder );
            }
        },

        getSortOrder: function( fieldName ) {
            switch ( this.collection.getSort( fieldName ) ) {
                case 'ASC':
                    return 'DESC';
                case 'DESC':
                    return 'removeSort';
                default:
                    return 'ASC';
            }
        },

        disableSort: function() {
            $( '.sort' ).find( 'i' ).removeClass( 'removeSort' );

            _.each( this.collection.getSortAll(), function ( value, name ) {
                var $th = $( 'thead' ).find( 'th[data-name=\'' + name + '\']' );
                $th.find( 'i.fa-sort-' + value.toLowerCase() ).addClass( 'removeSort' );
            }, this );
        }
    };

    return exports.SortableView;
});

