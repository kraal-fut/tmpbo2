define( function( require, exports ) {
    'use strict';
    var _ = require( 'underscore' );

    var filters = {
        parseint: function( value ) {
            return parseInt( value, 10 );
        },
        bool: function( value ) {
            return !!value;
        },
        empty: function( value ) {
            if ( _.isEmpty( value ) ) {
                return undefined;
            }
            return value;
        }
    };

    exports.FutBinding = function( view, model, bindings ) {
        this.view = view;
        this.model = model || view.model;
        this.bindings = bindings || view.futSaveBindings;
    };

    exports.FutBinding.prototype = {
        updateModel: function() {
            if ( _.isUndefined( this.bindings ) || _.isUndefined( this.model ) ) {
                return;
            }

            _.each( this.bindings, this.parseBindings, this );
        },

        parseBindings: function ( modelAttr, elemSelector ) {
            var $elem = this.$( elemSelector );

            if ( !$elem.length ) {
                return;
            }
            this.setModel( modelAttr, this.getValue( $elem ) );
        },

        setModel: function( attr, value ) {
            var attrName = attr;

            if ( _.isObject( attr ) ) {
                attrName = attr.observe;

                var applyFilter = function( filter, view ) {
                    var f;

                    if ( _.isFunction( filter ) ) {
                        f = filter;
                    } else if ( _.isString( filter ) ) {
                        var filterName = filter.toLowerCase();

                        if ( _.isFunction( filters[filterName] ) ) {
                            f = filters[filterName];
                        } else if ( _.isFunction( view[ filterName])) {
                            f = view[ filterName ];
                        }
                    }

                    if ( _.isUndefined( f ) ) {
                        f = _.identity;
                    }

                    value = f( value );
                };

                applyFilter( attr.filter, this.view );
            }

            this.model.set( attrName, value );
        },

        $: function ( selector ) {
            return this.view.$el.find( selector );
        },

        getValue: function ( $elem ) {
            var tag = $elem.prop( 'tagName' ).toLowerCase();
            var type = $elem.prop( 'type' ).toLowerCase();

            if ( tag === 'input' && (type === 'checkbox' || type === 'radio') ) {
                return $elem.is( ':checked' );
            }

            return $elem.val();
        }
    };

    return exports.FutBinding;
} );