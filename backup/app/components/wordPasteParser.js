define( function( require, exports ) {
    'use strict';

    exports.WordPasteParser = function( input ) {
        this.text = input;
    };

    exports.WordPasteParser.prototype = {
        parse: function() {
            if ( /<!--StartFragment-->([^]*)<!--EndFragment-->/.test ( this.text ) ) {
                this.text = this.text.match ( /<!--StartFragment-->([^]*)<!--EndFragment-->/ )[1];
            }
            this.text = this.text.replace(/<p class=(?:'|")?MsoNormal(?:'|")?>([\s\S]*)<\/p>/g, '<div>$1</div>\n');
            this.text = this.text.replace(/<p class=(?:'|")?MsoNoSpacing(?:'|")?(?:[\s\S]*?)>([\s\S]*?)<\/p>/g, '$1');
            this.text = this.text.replace(/<p class=(?:'|")?MsoTitle(?:'|")?>([\s\S]*?)<\/p>/g, '## $1');

            this.removeEmptyTags();
            this.removeComments();
            this.text = this.text.replace(/^\s*\n/gm, '');

            var $content = $( '<div>' + this.text + '</div>' );

            $content = this.removeInlineStyle( $content );
            $content = this.removeAttributes( $content );

            return $content.html();

        },

        removeComments: function() {
            this.text = this.text.replace(/<![^>]*>/g, '');
        },

        removeInlineStyle: function( $content ) {
            $content.find('*').removeAttr('style');
            return $content;
        },

        removeAttributes: function( $content ) {
            $content.find('*')
                .removeAttr('class')
                .removeAttr ( 'lang' );

            return $content;
        },

        removeEmptyTags: function() {
            this.text = this.text.replace ( /<o:p>(&nbsp;)?<\/o:p>/g, '' );
            this.text = this.text.replace ( /&nbsp;/g, ' ' );
        }
    };

    return exports.WordPasteParser;
} );