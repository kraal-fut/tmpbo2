/*global require:false*/

var EApp = window.EApp = {};

require(['config'], function() {
    'use strict';

    require( [
        'app',
        'ember-load',
        'underscore',
        'jquery',
        'backbone',
        'marionette',
        'backbone.marionette.export',
        'bootstrap',
        'hbs',
        'typeahead.jquery',
        'backbone-validator',
        'moment',
        'fut/EmberMixins'
    ], function ( App, Em, _ ) {

        EApp.Router = Em.Router.extend( {
            location: 'history'
        } );

        App.routerStarted = function() {
            var beforeEapp = EApp;

            EApp = Em.Application.create( {
                rootElement: '#ember_content',
                LOG_TRANSITIONS: true,
                LOG_TRANSITIONS_INTERNAL: true,
                LOG_ACTIVE_GENERATION: true,
                LOG_RESOLVER: true,
                LOG_VIEW_LOOKUPS: true
            } );

            EApp = _.extend( EApp, beforeEapp );
        };

        App.start();

        return App;
    });
});
