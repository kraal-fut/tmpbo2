define ( function ( require ) {
    'use strict';

    var Backbone = require( 'backbone-associations' ),
        Marionette = require ('marionette' ),
        Router = require( 'router' ),
        NavigationView = require( 'views/navigation' ),
        AppSettings = require( 'models/settings' ),
        Settings = require( 'collections/settings' ),
        Components = require( 'components/components' ),
        Epoxy = require( 'backbone.epoxy' );

    require( 'views/catselect' );

    var App = new Marionette.Application();

    window.App = App;

    Epoxy.Model.mixin( Backbone.AssociatedModel );

    App.apiroot = '/api';

    App.addRegions( {
        navRegion: '#navigationcontainer',
        mainRegion: '#contentcontainer',
        uiRegion: '#uicontainer',
        filterRegion: Components.FilterRegion,
        sideBarRegion: Components.SideBarRegion,
        popupRegion: Components.ModalRegion
    } );


    App.addInitializer( function () {
        App.settingsmodel = new AppSettings();

        App.settingsmodel.load().then(function(){
            App.notificationcenter = new Components.Notification( App, App.vent );
            App.search = new Components.Search();

            App.root = '/';

            App.settings = new Settings( {}, {App: App} );

            App.navRegion.show( new NavigationView( {App: App, settings: App.settingsmodel} ) );

            window.AppRouter = new Router( {App: App} );

            Backbone.history.start( {pushState: true} );
        }, function( rejection ){
            console.error( 'Could not load settings', rejection );
        });
    });

    window.App = App;


    return App;
});
