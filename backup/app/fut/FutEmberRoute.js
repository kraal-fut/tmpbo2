define( function( require, exports ) {
    'use strict';

    var Em = require( 'ember-load' ),
        _ = require( 'underscore' );

    exports.FutEmberRoute = Em.Route.extend( {
        Ember: Em,
        idAttribute: 'id',
        queryParams: {},

        init: function() {
            if ( this.rawTemplate && this.moduleName ) {
                Em.TEMPLATES[this.moduleName] = Em.Handlebars.compile( this.rawTemplate );
            }
            this._super();
        },

        parse: function( data ) {
            var d = _.isUndefined( this.entityName ) || _.isUndefined(data[this.entityName]) ? data : data[this.entityName];

            if ( !_.isUndefined( this.entityName ) && !_.isArray( d ) ) {
                d = [d];
            }

            if ( _.isFunction( this.map ) ) {
                d = _.map( d, this.map );
            }

            return Em.ArrayProxy.create( {content: Em.A( d )} );
        },

        model: function( params ) {
            if ( _.isFunction( this.futModel ) ) {
                return this.futModel( params );
            }

            var parse = _.isFunction( this.parse ) ? this.parse.bind( this ) : function( data ) { return data; };

            if ( this.dataUrl  ) {
                var url = _.isFunction( this.dataUrl ) ? this.dataUrl( params ) : this.dataUrl;
                if ( _.isUndefined( url ) ) {
                    return Em.Object.create( {} );
                }
                return Em.$.getJSON( url, this.queryParams  ).then( parse );
            }
        },

        store: function( params ) {
            var defaultParams = {};
            defaultParams[this.idAttribute] = this.currentModel[this.idAttribute];

            var url = this.dataUrl( _.extend( {}, defaultParams, params ) );

            var data = this.currentModel.getJSON();
            if ( _.isFunction( this.serializeModel ) ) {
                data = this.serializeModel( this.currentModel );
            }

            return Em.$.ajax({
                type: 'PUT',
                url: url,
                data: JSON.stringify( data ),
                contentType: 'application/json'
            });
        },

        deleteModel: function( model ) {
            var id = model.get( this.idAttribute );

            if ( _.isUndefined( id ) ) {
                return this.currentModel.removeObject( model );
            }

            var url = this.dataUrl() + '/' + model.get( this.idAttribute );
            var route = this;

            return Em.$.ajax( {
                type: 'DELETE',
                url: url,
                contentType: 'application/json'
            } ).then(function( response ) {
                route.refresh();
                return response;
            });
        },

        storeModel: function( model, url, id ) {
            var route = this;

            url = url || this.dataUrl();
            id = id || model.get( this.idAttribute );

            var data = model.getProperties.apply( model, Object.keys( model ) );

            if ( _.isFunction( this.serializeModel ) ) {
                data = this.serializeModel( this.currentModel );
            }

            var method = 'PUT';
            if ( _.isUndefined( id ) ) {
                method = 'POST';
            } else {
                url += '/' + id;
            }

            return Em.$.ajax( {
                type: method,
                url: url,
                data: JSON.stringify( data ),
                contentType: 'application/json'
            } ).then(function( response ) {
                route.refresh();
                return response;
            }).then(function( response ) {
                route.controller.toggleProperty( 'editing' );
                return response;
            });
        },

        dataUrl: function( params, url ) {
            return url;
        }
    } );

    return exports.FutEmberRoute;
});