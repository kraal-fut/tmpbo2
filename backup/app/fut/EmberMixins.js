define( function( require ) {
    'use strict';

    var Em = require( 'ember-load' ),
        _ = require( 'underscore' ),
        PaginationTemplate = require( 'text!templates/pagination2.hbs' ),
        EditingPartial = require( 'text!templates/editing.hbs' );

    require( 'components/banner-upload' );
    require( 'components/fut-codeview' );
    require( 'components/fut-listfilter-text' );
    require( 'components/fut-listfilter-bool' );

    require( 'components/fut-chart-bar' );
    require( 'components/fut-chart-line' );

    require( 'components/bootstrap-popover' );
    require( 'components/fut-modal' );
    require( 'modules/statistics/FilterView' );
    require( 'views/popupHtml' );
    require( 'templates/helpers/money' );
    require( 'templates/helpers/striptags' );
    require( 'templates/helpers/bo1url' );
    require( 'templates/helpers/fo2url' );

    Em.TEMPLATES.editing = Em.Handlebars.compile( EditingPartial );
    Em.TEMPLATES.pagination = Em.Handlebars.compile( PaginationTemplate );

    window.EApp.Jsonable = Em.Mixin.create( {
        getJSON: function( includeComputeds, without ) {
            var keys = _.difference( _.extend( Object.keys( this ), includeComputeds ), without || [] );
            return this.getProperties.apply( this, keys);
        }
    } );

    window.EApp.PaginationController = Em.Controller.extend({
        perPage: Em.computed.alias( 'model.itemsPerPage' ),
        totalPages: Em.computed.alias( 'model.totalPages' ),
        currentPage: Em.computed.alias( 'model.currentPage' ),

        perPageOptions: [5, 10, 20, 30],

        pageSet: function() {
            var currentPage = this.get( 'currentPage' );
            var totalPages = this.get( 'totalPages' );

            return _.map( _.first( _.reject( _.range( currentPage - 2, currentPage + 5 ), function( num ) {
                return num < 1 || num > totalPages;
            }, this ), 5 ), function( num ){
                return {num: num, active: num === currentPage};
            });

        }.property( 'currentPage', 'totalPages' ),

        actions: {
            gotoPage: function( page ) {
                this.set( 'currentPage', parseInt( page, 10 ) );
            },
            firstPage: function() {
                this.set( 'currentPage', 1 );
            },
            prevPage: function() {
                this.decrementProperty( 'currentPage' );
            },
            nextPage: function() {
                this.incrementProperty( 'currentPage' );
            },
            lastPage: function() {
                this.set( 'currentPage', this.get( 'totalPages' ) );
            }
        }
    });

    window.EApp.Pageable = Em.Mixin.create( {
        state: Em.Object.create({
            totalRecords: 0,
            totalPages: 0,
            currentPage: 1,
            itemsPerPage: 10
        }),

        renderTemplate: function() {
            this._super();

            if (!_.isUndefined( this.FilterView )) {
                //this.paginator.addFilterView( this.FilterView );
            }

            this.render( 'pagination', {
                into: 'application',
                outlet: 'pagination',
                model: this.state
            } );

            this.state.addObserver( 'itemsPerPage', this, 'changePerPage' );
            this.state.addObserver( 'currentPage',  this, 'changeCurrentPage' );
        },

        changePerPage: function() {
            this.queryParams.limit = this.state.get( 'itemsPerPage' );
            this.refresh();
        },

        changeCurrentPage: function() {
            this.queryParams.page = this.state.get( 'currentPage' );
            this.refresh();
        },

        parse: function( data ) {
            if ( !_.isUndefined( data.meta ) ) {
                this.parseState( data.meta );
            }
            return this._super( data );
        },

        parseState: function( meta ) {
            var state = {};

            if ( meta.total ) {
                state.totalRecords = parseInt( meta.total, 10 );
            }
            if ( meta.pageCount ) {
                state.totalPages = parseInt( meta.pageCount, 10 );
            }
            if ( meta.page ) {
                state.currentPage = parseInt( meta.page, 10 );
            }
            if ( meta.pageRecords ) {
                state.itemsPerPage = parseInt( meta.pageRecords, 10 );
            }

            this.state.setProperties( state );
        },

        willDestroy: function() {
            this.state.removeObserver( 'itemsPerPage', this, 'changePerPage' );
            this.state.removeObserver( 'currentPage', this, 'changeCurrentPage' );
        }

    });

    window.EApp.Editable = Em.Mixin.create( {
        editing: false,
        notediting: Em.computed.not( 'editing' ),
        actions: {
            toggleEdit: function() {
                this.toggleProperty( 'editing' );
            }
        }
    } );

    window.EApp.EditableController = Em.Controller.extend( window.EApp.Editable, {
        actions: {
            popup: function() {
                return this.get( 'editing' );
            }
        }
    } );

    window.EApp.Filterable = Em.Mixin.create({
        filters: {},

        setFiltersToQuery: function() {
            if ( _.isEmpty( this.filters ) ) {
                delete this.queryParams.find;
            }
            this.queryParams.find = JSON.stringify( this.filters );
            this.refresh();
        },

        actions: {
            filter: function( filterColumn, filterValue ) {
                if ( _.isUndefined( filterValue ) ) {
                    delete this.filters[filterColumn];
                } else {
                    this.filters[filterColumn] = filterValue;
                }

                this.setFiltersToQuery();
            },
            removeFilter: function( filterColumn ) {
                delete this.filters[filterColumn];
                if ( this.state ) {
                    this.state.set( 'itemsPerPage', 10 );
                }
                this.setFiltersToQuery();
            },
            removeAllFilters: function() {
                this.filters = {};
                if ( this.state ) {
                    this.state.set( 'itemsPerPage', 10 );
                }
                this.setFiltersToQuery();
            }
        }
    });

    window.EApp.ApplicationRoute = Em.Route.extend( {
        actions: {
            showModal: function( name, model, fieldName ) {
                var c;
                if ( !_.isUndefined( fieldName ) ) {
                    c = this.controllerFor( name );
                    c.set( 'fieldName', fieldName );
                }

                this.render( name, {
                    into: 'application',
                    outlet: 'modal',
                    model: model
                    //controller: c
                } );
            },
            removeModal: function() {
                this.disconnectOutlet( {
                    outlet: 'modal',
                    parentView: 'application'
                } );
            }
        }
    } );

} );