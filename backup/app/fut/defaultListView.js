define ( function ( require, exports ) {
    'use strict';

    var fut = require( 'fut/entities' ),
        _ = require( 'underscore' );

    exports.defaultView = {
        tagName: 'div',
        childViewContainer: 'tbody',

        ui: {
            selectAll: '#selectAll'
        },

        getOptions: function() {
            var optionsFromView = function( view ) {
                if ( _.isUndefined( view.collectionOptions ) ) {
                    return {};
                }

                if ( _.isFunction( view.collectionOptions ) ) {
                    return view.collectionOptions();
                }

                return view.collectionOptions;
            };

            return _.extend(
                {noSearch: 0},
                optionsFromView( this )
            );
        },

        initialize: function() {
            if ( this.Collection ) {
                this.collection = new this.Collection([], this.getOptions());
                this.collection.load();
            }
        },

        triggers: {
            'click #AddButton': 'addItem',
            'click @ui.selectAll': {
                event: 'selectAll',
                preventDefault: false,
                stopPropagation: false
            }
        },

        onAddItem: function() {
            this.collection.unshift( new this.collection.model({}) );
        },

        onAddChild: function( childView ) {
            if ( childView.model.isNew() ) {
                childView.viewModel.set( 'editing', true );
            }
        },

        onSelectAll: function() {
            this.collection.toggleSelectAll();
        }
    };

    return exports.defaultView;
});
