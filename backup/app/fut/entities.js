define ( function( require, exports ) {
    'use strict';

    var Backbone = require( 'backbone' ),
        Marionette = require( 'marionette' ),
        Cocktail = require( 'cocktail' ),
        $ = require( 'jquery' ),
        _ = require( 'underscore' ),
        when = require( 'when/when' ),

        BaseCollection = require( 'collections/base' ),
        BaseModel = require( 'models/BaseModel' ),
        DefaultListView = require( 'fut/defaultListView' ),
        MoveableItemsCollection = require( 'collections/moveableItemsCollection' );

    var baseComputeds = {
        fo2url: function() {
            return this.getBinding( 'BASE_URL' );
        }
    };

    var setViewModel = function( view, model ) {
        view.viewModel.set( model.toJSON() );

        view.listenTo( model, 'change', function () {
            view.viewModel.set( model.toJSON() );
        } );
    };

    var setOptions = function( options, view ) {
        options = options || {};
        view.setViewModel = _.partial( setViewModel, view );

        view.viewModel = options.viewModel || new BaseModel( App.settingsmodel.toJSON() );

        if( _.isUndefined( options.viewModel ) ) {
            view.listenTo( window.App.settingsmodel, 'change', function () {
                view.viewModel.set( window.App.settingsmodel.toJSON() );
            } );
        }

        view.computeds = function() {
            return _.extend( {}, view.viewComputeds, baseComputeds );
        };

        view.listenTo( view, 'before:render', function() {
            view.listenTo( view.viewModel, 'change:editing', view.render );
        });
    };

    var ItemView = Marionette.ItemView.extend( {
        model: undefined,

        serializeData: function() {
            var viewModelData;
            var originalData = Marionette.ItemView.prototype.serializeData.apply( this );
            if ( !_.isUndefined( this.viewModel ) ) {
                viewModelData = this.viewModel.toJSON();
            }
            return _.extend( {}, viewModelData, originalData );
        },

        constructor: function( options ) {
            setOptions( options, this );
            Marionette.ItemView.prototype.constructor.apply( this, arguments );
        }

    } );

    var CollectionView = Marionette.CollectionView.extend( {
        collection: undefined,

        constructor: function( options ) {
            setOptions( options, this );
            Marionette.CollectionView.prototype.constructor.apply( this, arguments );
        },

        childViewOptions: function( model, index ) {
            return {
                itemIndex: index,
                viewModel: this.childViewFollowsViewModel ? this.viewModel : undefined,
                followViewModel: this.childViewFollowsViewModel
            };
        }

    } );

    var CompositeView = Marionette.CompositeView.extend( {
        model: undefined,
        collection: undefined,
        computeds: baseComputeds,

        constructor: function( options ) {
            setOptions( options, this );
            Marionette.CompositeView.prototype.constructor.apply( this, arguments );
        },
        serializeData: function () {
            var viewModelData;
            var originalData = Marionette.CompositeView.prototype.serializeData.apply( this );
            if ( !_.isUndefined( this.viewModel ) ) {
                viewModelData = this.viewModel.toJSON();
            }
            return _.extend( {}, viewModelData, originalData );
        },

        childViewOptions: function( model, index ) {
            return {
                itemIndex: index,
                viewModel: this.childViewFollowsViewModel ? this.viewModel : undefined,
                followViewModel: this.childViewFollowsViewModel
            };
        }
    } );

    var Layout = Marionette.LayoutView.extend( {
        settings: undefined,
        model: undefined,

        constructor: function( options ) {
            this.settings = App.settingsmodel;

            setOptions( options, this );
            Marionette.LayoutView.prototype.constructor.apply( this, arguments );
        }
    } );

    exports = {
        Model: BaseModel,
        Collection: BaseCollection,
        ItemView: ItemView,
        CollectionView: CollectionView,
        CompositeView: CompositeView,
        DefaultListView: DefaultListView,
        Layout: Layout,
        Backbone: Backbone,
        _: _,
        $: $,
        Cocktail: Cocktail,
        mixin: function( object ) {
            Cocktail.mixin.apply( object, arguments );
            return object;
        },
        MoveableItemsCollection: MoveableItemsCollection,

        Many: Backbone.Many,
        One: Backbone.One,
        Self: Backbone.Self,

        Promise: when.Promise,
        promise: when.promise
    };

    return exports;
} );
