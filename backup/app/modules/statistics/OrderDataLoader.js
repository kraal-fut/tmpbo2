define( function( require, exports ) {
    'use strict';

    var Loader = require('modules/statistics/loader'),
        _ = require( 'underscore' );

    exports.Loader = Loader.extend({
        revHist: [],
        avgRevHist: [],
        margeHist: [],

        domain: function() {
            return [{fieldname: 'revHist', key: 'Revenue', color: '#ff7f0e', area: true},
            {fieldname: 'margeHist', key: 'Average Marge', color: '#2ca02c', area: false},
            {fieldname: 'avgRevHist', key: 'Average Revenue', color: '#ff0000', area: false}];
        }.property(),

        loadData: _.debounce( function() {
            var updateOrderData = _.bind( function( resp ) {
                this.setOrderValuesStatistics( resp );
                this.setDeliverMethodStatistics( resp );
            }, this );

            var updateOrderLinesData = _.bind( function( resp ) {
                if ( resp.hits.total === 0 ) {
                    return;
                }
                this.setOrderLineStatistics( resp );
            }, this );

            this.ordersearch(
                this.getRange(),
                this.getStatuses(),
                this.getOrderAggregations()
            ).then( updateOrderData );

            this.orderlinesSearch(
                this.getRange(),
                this.getStatuses(),
                this.getOrderLineAggregations()
            ).then( updateOrderLinesData );

            return this;
        }, 250 ),


        getOrderAggregations: function() {
            return {
                shipmentmethods: {terms: {field: 'DeliveryMethod'}},
                paymentmethods: {terms: {field: 'PaymentMethod'}},
                averageOrderAmount: {avg: {field: 'totalExclTax'}},
                averageDiscount: {avg: {field: 'allDiscount'}},
                totalOrderAmount: {sum: {field: 'totalExclTax'}},
                totalDiscount: {sum: {field: 'allDiscount'}},
                averageOrderMarge: {avg: {field: 'marge'}},

                history: {
                    date_histogram: {
                        field: 'OrderDate',
                        interval: 'day'
                    },
                    aggs: {
                        revenue: {sum: {field: 'totalExclTax'}},
                        avgOrderAmount: {avg: {field: 'totalExclTax'}}
                    }
                }
            };
        },

        getOrderLineAggregations: function() {
            return {
                vendor: {terms: {field: 'Vendor'}},
                products: {terms: {field: 'ProdLevId'}},
                couponsUsed: {terms: {field: 'extra_data.couponcode'}},
                averagePurchasePrice: {avg: {field: 'Pprice'}},
                history: {
                    date_histogram: {
                        field: 'OrderDate',
                        interval: 'day'
                    },
                    aggs: {
                        avgMarge: {avg: {field: 'marge'}}
                    }
                }
            };
        },

        setOrderValuesStatistics: function( resp ) {
            this.set( 'totalOrders', resp.hits.total );
            this.set(
                'revHist',
                _.map( resp.aggregations.history.buckets, function( item ) {
                    return {x: item.key, y: item.revenue.value};
                } )
            );
            this.set(
                'avgRevHist',
                _.map( resp.aggregations.history.buckets, function( item ) {
                    return {x: item.key, y: item.avgOrderAmount.value};
                } )
            );

            this.set( 'totalOrderAmount', resp.aggregations.totalOrderAmount.value );
            this.set( 'averageOrderAmount', resp.aggregations.averageOrderAmount.value );
            this.set( 'averageOrderMarge', resp.aggregations.averageOrderMarge.value );
            this.set( 'averageDiscount', resp.aggregations.averageDiscount.value );
            this.set( 'totalDiscount', resp.aggregations.totalDiscount.value );
        },

        setOrderLineStatistics: function( resp ) {
            this.setVendorStatistics( resp );
            this.setProductStatistics( resp );
            this.setDiscountStatistics( resp );
            this.set(
                'margeHist',
                _.map( resp.aggregations.history.buckets, function( item ) {
                    return {x: item.key, y: item.avgMarge.value};
                } )
            );
            this.set( 'averagePurchasePrice', resp.aggregations.averagePurchasePrice.value );
        },

        setDeliverMethodStatistics: function( resp ) {
            var deliveryMethods = resp.aggregations.shipmentmethods.buckets;

            this.set( 'deliverymethods', [
                {
                    key: 'deliveryMethods',
                    values: _.map( deliveryMethods, function( item ) {
                        var key = item.key;
                        if ( key === '' ) {
                            key = '[empty]';
                        }
                        return {label: key, value: item.doc_count};
                    } )
                }
            ] );
        },

        setVendorStatistics: function( resp ) {
            var vendors = resp.aggregations.vendor.buckets;

            this.set( 'vendors', [
                {
                    key: 'vendors',
                    values: _.map( vendors, function( item ) {
                        var key = item.key;
                        if ( key === '' ) {
                            key = '[empty]';
                        }
                        return {label: key, value: item.doc_count};
                    } )
                }
            ] );
        },

        setDiscountStatistics: function( resp ) {
            var coupons = resp.aggregations.couponsUsed.buckets;

            this.set( 'couponsUsed', _.reduce( coupons, function( memo, item ) {
                return memo + item.doc_count;
            }, 0 ) );
        },

        setProductStatistics: function( resp ) {
            var products = resp.aggregations.products.buckets;

            this.set( 'products', [
                {
                    key: 'products',
                    values: _.map( products, function( item ) {
                        var key = item.key;
                        if ( key === '' ) {
                            key = '[empty]';
                        }
                        return {label: key, value: item.doc_count};
                    } )
                }
            ] );
        }
    });

    return exports.Loader;

} );