define( function( require ) {
    'use strict';

    var Em = require( 'ember-load' ),
        template = require( 'text!modules/statistics/filterview.hbs' );

    window.EApp.StatisticsFilterView = Em.View.extend({
        template: Em.Handlebars.compile( template )
    });
} );