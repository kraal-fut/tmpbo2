define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Loader = require( 'modules/statistics/ProductDataLoader' ),
        Template = require( 'text!modules/statistics/productview.hbs' ),
        ProductItemPartial = require( 'text!modules/statistics/productitempartial.hbs' );

    Em.TEMPLATES['statistics/productitem'] = Em.Handlebars.compile( ProductItemPartial );

    return FutEmberRoute.extend( {
        moduleName: 'statistics/product',
        rawTemplate: Template,
        ts: undefined,

        actions: {
            lastmonth: function() {
                this.ts.getLastMonth();
            },
            thismonth: function() {
                this.ts.getThisMonth();
            },
            lastweek: function() {
                this.ts.getLastWeek();
            },
            thisweek: function() {
                this.ts.getThisWeek();
            },
            yesterday: function() {
                this.ts.getYesterdayTime();
            },
            daybefore: function() {
                this.ts.getDayBefore();
            },
            dayafter: function() {
                this.ts.getDayAfter();
            },
            today: function() {
                this.ts.getToday();
            }
        },
        model: function() {
            var loader = Loader.create({}).connect();
            loader.loadData();
            this.ts = loader.timeSetter;
            return loader;
        }
    } );

} );