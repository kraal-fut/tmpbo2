define( function( require, exports ) {
    'use strict';

    var Controller = require( 'modules/controller' );

    exports.Controller = Controller.extend( {
        views: {
        }
    } );

    return exports.Controller;
} );
