define( function( require, exports ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        _ = require( 'underscore' ),
        elasticsearch = require( 'elasticsearch' ),
        moment = require( 'moment' );

    var TimeSetter = function( model ) {
        this.model = model;
    };

    TimeSetter.prototype = {
        dateFormat: 'YYYY-MM-DD HH:mm:ss',

        setTimes: function( from, to ) {
            var _from = from.format( this.dateFormat );
            var _to = to.format( this.dateFormat );

            this.model.set( 'from', _from);
            this.model.set( 'to', _to);
        },

        formatDates: function( from, to ) {
            return {
                gte: moment( from || {hour: 0} ).format( this.dateFormat ),
                lte: moment( to ).format( this.dateFormat )
            };
        },

        currentFrom: function() {
            return this.model.get( 'from' );
        },

        currentTo: function() {
            return this.model.get( 'to' );
        },

        getDefaultTime: function( settings ) {
            if ( !_.isEmpty( settings ) ) {
                return this.setTimes( moment( settings.from ), moment( settings.to ) );
            }
            this.getThisMonth();
        },

        getToday: function() {
            this.setTimes( moment().startOf( 'day' ), moment().endOf( 'day' ) );
        },

        getYesterdayTime: function() {
            var from = moment().startOf( 'day' ).subtract( 1, 'days' );
            var to = moment().endOf( 'day' ).subtract( 1, 'days' );
            this.setTimes( from, to );
        },

        getDayBefore: function() {
            var from = moment( this.currentFrom() ).startOf( 'day' ).subtract( 1, 'days' );
            var to = moment( this.currentTo() ).endOf( 'day' ).subtract( 1, 'days' );
            if ( from.isSame( to ) ) {
                from = to.endOf( 'day' );
            }
            this.setTimes( from, to );
        },

        getDayAfter: function() {
            var from = moment( this.currentFrom() ).startOf( 'day' ).add( 1, 'days' );
            var to = moment( this.currentTo() ).endOf( 'day' ).add( 1, 'days' );
            if ( from.isSame( to ) ) {
                to = to.endOf( 'day' );
            }
            this.setTimes( from, to );
        },

        getLastWeek: function() {
            var from = moment().startOf( 'isoWeek' ).subtract( 7, 'days' );
            var to = moment().endOf( 'isoWeek' ).subtract( 7, 'days' );
            this.setTimes( from, to );
        },

        getThisWeek: function() {
            var from = moment().startOf( 'isoWeek' );
            var to = moment().endOf( 'isoWeek' );
            this.setTimes( from, to );
        },

        getThisMonth: function() {
            var from = moment().startOf( 'month' );
            var to = moment().endOf( 'month' );
            this.setTimes( from, to );
        },

        getLastMonth: function() {
            var from = moment().startOf( 'month' ).subtract( 1, 'month' );
            var to = moment().endOf( 'month' ).subtract( 1, 'month' );
            this.setTimes( from, to );
        }
    };

    exports.Loader = Em.Object.extend({
        client: undefined,
        index: 'futurum2_nl',
        dateFormat: 'YYYY-MM-DD HH:mm:ss',
        timeSetter: undefined,

        connect: function() {
            this.timeSetter = new TimeSetter( this );
            this.timeSetter.getDefaultTime();

            this.client = new elasticsearch.Client( {
                host: App.settingsmodel.getSetting( 'ELASTICSEARCHURL' )
            } );

            return this;
        },

        fromTime: function() {
            return moment( this.get( 'from' ) ).format( 'YYYY-MM-DDTHH:mm:ss' );
        }.property( 'from' ),

        toTime: function() {
            return moment( this.get( 'to' ) ).format( 'YYYY-MM-DDTHH:mm:ss' );
        }.property( 'to' ),

        rangeChanged: function() {
            this.loadData();
        }.observes( 'from', 'to' ),

        getMainQuery: function( range, statuses ) {
            var bool = {
                must: [
                    {range: {OrderDate: range}},
                    {terms: {Status: statuses}}
                ],
                must_not: []
            };

            if ( !this.get( 'includeDE' ) ) {
                bool.must_not.push( {match: {PaymentMethod: 'duitsland'}} );
                bool.must_not.push( {match: {PaymentMethod: 'de_rmbrs'}} );
            }

            if ( !this.get( 'nazend' ) ) {
                bool.must_not.push( {match: {PaymentMethod: 'nazend'}} );
            }

            if ( !_.isEmpty( this.get( 'vendorFilter' ) ) ) {
                bool.must.push(
                    {
                        has_child: {
                            type: 'orderlines',
                            query: {terms: {Vendor: _.values( this.get( 'vendorFilter' ) )}}
                        }
                    }
                );
            }

            return bool;
        },

        ordersearch: function( range, statuses, aggregations ) {
            var query = {
                bool: this.getMainQuery( range, statuses )
            };

            return this.client.search( {
                index: this.index,
                type: 'orders',
                body: {
                    query: query,
                    aggs: aggregations
                }
            } );
        },

        search: function( range, statuses ) {
            return this.client.search( {
                index: this.index,
                body: {
                    query: {
                        bool: this.getMainQuery( range, statuses )
                    },
                    aggs: this.getAggregations()
                }
            } );
        },
        orderlinesSearch: function( range, statuses, aggregations ) {
            var query = {
                bool: {
                    must: [
                        {
                            has_parent: {
                                parent_type: 'orders',
                                query: {
                                    bool: this.getMainQuery( range, statuses )
                                }
                            }
                        }
                    ]
                }
            };

            if ( !_.isEmpty( this.get( 'vendorFilter' ) ) ) {
                query.bool.must.push( {terms: {Vendor: _.values( this.get( 'vendorFilter' ) )}} );
            }

            return this.client.search( {
                index: this.index,
                type: 'orderlines',
                body: {
                    query: query,
                    aggs: aggregations
                }
            } );
        },

        getAggregations: function() {
            return {};
        },

        getRange: function() {
            return this.timeSetter.formatDates( this.get( 'from' ), this.get( 'to' ) );
        },

        getStatuses: function() {
            var status = this.get( 'status' );

            if ( _.isUndefined( status ) || _.isNull( status ) ) {
                status = ['60'];
            }
            if ( !_.isArray( status ) ) {
                status = [status];
            }

            return _.map( status, function( item ) {
                return parseInt( item, 10 );
            } );
        },

        addVendorFilter: function( vendor ) {
            var vf = _.clone( this.get( 'vendorFilter' ) ) || [];
            vf.push( vendor );
            this.set( 'vendorFilter', vf );
        }
    });

    return exports.Loader;
} );