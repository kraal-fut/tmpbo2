define( function ( require ) {
    'use strict';
    var Fut = require( 'fut/entities' ),
        Epoxy = require( 'backbone.epoxy' ),
        _ = require( 'underscore'),
        Template = require( 'hbs!../templates/SideFilterView' );

    var View = Fut.ItemView.extend({
        template: Template,

        bindings: {
            '#includeDE': 'checked:includeDE',
            '#includeNazend': 'checked:nazend',
            '#statuses': 'value:status, options:statuses',
            '#vendors': 'value:vendorFilter, options:vendors',
            '#cats': 'value:catFilter, options:fomenu'
        },

        triggers: {
            'click #allStatuses': 'allStatuses',
            'click #sameasbo1': 'sameasbo1',
            'click #statuscancelled': 'statuscancelled',
            'click #statuscomplete': 'statuscomplete',
            'click #clearvendors': 'clearvendors',
            'click #clearcats': 'clearcats'
        },

        onRender: function() {
            this.applyBindings();
        },

        onSameasbo1: function() {
            this.model.set( 'status', _.range( 10, 69 ) );
        },

        onAllStatuses: function() {
            this.model.set( 'status', this.viewModel.get( 'statuses' ).pluck( 'id' ) );
        },

        onStatuscancelled: function() {
            this.model.set( 'status', [90, 91, 93, 94, 99] );
        },

        onStatuscomplete: function() {
            this.model.set( 'status', [60] );
        },

        onClearvendors: function() {
            this.model.set( 'vendorFilter', [] );
        },

        onClearcats: function() {
            this.model.set( 'catFilter', [] );
        }
    });
    Epoxy.View.mixin( View.prototype );

    return View;
} );