define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Loader = require( 'modules/statistics/OrderDataLoader' ),
        Template = require( 'text!modules/statistics/orderview.hbs' );

    return FutEmberRoute.extend( {
        moduleName: 'statistics/order',
        rawTemplate: Template,
        ts: undefined,

        actions: {
            lastmonth: function() {
                this.ts.getLastMonth();
            },
            thismonth: function() {
                this.ts.getThisMonth();
            },
            lastweek: function() {
                this.ts.getLastWeek();
            },
            thisweek: function() {
                this.ts.getThisWeek();
            },
            yesterday: function() {
                this.ts.getYesterdayTime();
            },
            daybefore: function() {
                this.ts.getDayBefore();
            },
            dayafter: function() {
                this.ts.getDayAfter();
            },
            today: function() {
                this.ts.getToday();
            }
        },
        model: function() {
            var loader = Loader.create({}).connect();
            loader.loadData();
            this.ts = loader.timeSetter;
            return loader;
        }
    } );

} );