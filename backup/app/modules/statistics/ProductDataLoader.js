define( function( require, exports ) {
    'use strict';

    var Loader = require('modules/statistics/loader'),
        Em = require('ember-load'),
        _ = require( 'underscore' );


    var Product = Em.Object.extend({
        productAvgSelling: function() {
            return this.get( 'revenue.value' ) / this.get( 'quant.value' );
        }.property( 'revenue.value', 'quant.value' ),
        percentMarge: function() {
            return ( ( this.get( 'marge.value' ) / this.get( 'euprice.value' ) ) * 100).toPrecision( 2 );
        }.property( 'marge.value', 'euprice.value' ),
        avgQuantity: function() {
            return this.get( 'avgQuant.value' ).toPrecision( 2 );
        }.property( 'avgQuant.value' )
    });

    exports.Loader = Loader.extend({
        products: [],
        total: {},
        loadData: _.debounce( function() {
            var model = this;

            var updateData = function( resp ) {
                model.updateData( resp );
            };

            this.orderlinesSearch(
                this.getRange(),
                this.getStatuses(),
                this.getAggregations()
            ).then( updateData );

            return model;
        }, 250 ),

        updateData: function( resp ) {
            this.setProductStatistics( resp );
        },

        getAggregations: function() {
            return {
                products: {
                    terms: {field: 'ProdLevId', size: 50},
                    aggs: {
                        vendor: {terms: {field: 'Vendor'}},
                        revenue: {avg: {field: 'revenue'}},
                        totalrevenue: {sum: {field: 'revenue'}},
                        marge: {avg: {field: 'marge'}},
                        euprice: {avg: {field: 'EUprice'}},
                        pprice: {avg: {field: 'Pprice'}},
                        quant: {sum: {field: 'Quant'}},
                        avgQuant: {avg: {field: 'Quant'}},
                        names: {terms: {field: 'Name'}}
                    }
                },
                revenue: {avg: {field: 'revenue'}},
                totalrevenue: {sum: {field: 'revenue'}},
                marge: {avg: {field: 'marge'}},
                euprice: {avg: {field: 'EUprice'}},
                pprice: {avg: {field: 'Pprice'}},
                quant: {sum: {field: 'Quant'}},
                avgQuant: {avg: {field: 'Quant'}}
            };
        },

        setProductStatistics: function( resp ) {
            this.set(
                'products',
                _.map( resp.aggregations.products.buckets, function(item) {
                    return Product.create( item );
                } )
            );

            var total = _.omit( resp.aggregations );
            total.doc_count = resp.hits.total;
            this.set( 'total', Product.create( total ) );
        }
    });

    return exports.Loader;

} );