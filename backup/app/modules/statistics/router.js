define( function( require ) {
    'use strict';

    var Marionette = require( 'marionette' ),
        Fut = require( 'fut/entities' ),
        ModuleRouter = require( 'components/moduleRouter' ),
        OrderView = require( 'modules/statistics/OrderView' ),
        ProductView = require( 'modules/statistics/ProductView' );

    var router = Marionette.AppRouter.extend( {

        routes: {
            'statistics/order': {
                method: 'StatisticsOrder',
                name: 'Order',
                routeName: 'statistics.order',
                emberRoute: OrderView
            },
            'statistics/productsales': {
                method: 'StatisticsProduct',
                name: 'Product Sales',
                routeName: 'statistics.product',
                emberRoute: ProductView
            }
        }
    } );

    return Fut.mixin( router, ModuleRouter );
} );

