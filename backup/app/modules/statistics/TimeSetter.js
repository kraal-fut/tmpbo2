define( function( require, exports ) {
    'use strict';

    var moment = require( 'moment' ),
        _ = require( 'underscore' );

    exports.TimeSetter = function( model ) {
        this.model = model;
    };

    exports.TimeSetter.prototype = {
        dateFormat: 'YYYY-MM-DD HH:mm:ss',

        setTimes: function( from, to ) {
            this.model.set( {
                from: from.format( this.dateFormat ),
                to: to.format( this.dateFormat )
            } );
        },

        formatDates: function( from, to ) {
            return {
                gte: moment( from || {hour: 0} ).format( this.dateFormat ),
                lte: moment( to ).format( this.dateFormat )
            };
        },

        currentFrom: function() {
            return this.model.get( 'from' );
        },

        currentTo: function() {
            return this.model.get( 'to' );
        },

        getDefaultTime: function( settings ) {
            if ( !_.isEmpty( settings ) ) {
                return this.setTimes( moment( settings.from ), moment( settings.to ) );
            }
            this.getThisMonth();
        },

        getToday:function() {
            this.setTimes( moment().startOf( 'day' ), moment().endOf( 'day' ) );
        },

        getYesterdayTime: function() {
            var from = moment().startOf( 'day' ).subtract( 1, 'days' );
            var to = moment().endOf( 'day').subtract( 1, 'days' );
            this.setTimes( from, to );
        },

        getDayBefore: function() {
            var from = moment( this.currentFrom() ).startOf( 'day' ).subtract( 1, 'days' );
            var to = moment( this.currentTo() ).endOf( 'day' ).subtract( 1, 'days' );
            if ( from.isSame( to ) ) {
                from = to.endOf( 'day' );
            }
            this.setTimes( from, to );
        },

        getDayAfter: function() {
            var from = moment( this.currentFrom() ).startOf( 'day' ).add( 1, 'days' );
            var to = moment( this.currentTo() ).endOf( 'day' ).add( 1, 'days' );
            if ( from.isSame( to ) ) {
                to = to.endOf( 'day' );
            }
            this.setTimes( from, to );
        },

        getLastWeek: function() {
            var from = moment().startOf( 'isoWeek' ).subtract( 7, 'days' );
            var to = moment().endOf( 'isoWeek' ).subtract( 7, 'days' );
            this.setTimes( from, to );
        },

        getThisWeek: function() {
            var from = moment().startOf( 'isoWeek' );
            var to = moment().endOf( 'isoWeek' );
            this.setTimes( from, to );
        },

        getThisMonth: function() {
            var from = moment().startOf( 'month' );
            var to = moment().endOf( 'month' );
            this.setTimes( from, to );
        },

        getLastMonth: function () {
            var from = moment().startOf( 'month' ).subtract( 1, 'month' );
            var to = moment().endOf( 'month' ).subtract( 1, 'month' );
            this.setTimes( from, to );
        }
    };

    return exports.TimeSetter;
} );