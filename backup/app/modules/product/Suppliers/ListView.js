define ( function ( require ) {
    'use strict';
    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!modules/product/Suppliers/ListView.hbs' );

    var SupplierModel = Em.Object.extend( {} );

    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            moduleName: 'product/suppliers',
            rawTemplate: Template,
            entityName: 'supplier',

            map: function( model ) {
                return SupplierModel.create( model );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/suppliers' );
            },

            actions: {
                save: function( model ) {
                    this.storeModel( model );
                },
                deleteSupplier: function( model ) {
                    var result = confirm( 'This will permanently delete this supplier, are you sure?' );
                    if ( !result ) {
                        return;
                    }
                    this.deleteModel( model );
                }
            }
        }
    );
//
//    var Fut = require( 'fut/entities' ),
//        ListItemView = require( 'product/Suppliers/views/ListItemView' ),
//        ListTemplate = require( 'hbs!product/Suppliers/templates/ListView' ),
//        Collection = require( 'product/Suppliers/collections/suppliers' ),
//        VendorCollection = require( 'product/Vendor/collections/vendors'),
//        //PopupFilter = require( 'product/views/viewsfilterpopup/autoComplete' ),
//        Components = require( 'components/components' );
//
//    var view = Fut.CompositeView.extend( {
//        collection: undefined,
//
//        tagName: 'div',
//        childView: ListItemView,
//        childViewContainer: 'tbody',
//        template: ListTemplate,
//
//        emptyView: Fut.ItemView.extend( {
//            template: function() {
//                return 'No suppliers found';
//            }
//        } ),
//
//        ui: {
//            'filter': '.fa-filter'
//        },
//
//        triggers: {
//            'click @ui.filter': 'openFilter'
//        },
//
//        initialize: function() {
//            this.collection = new Collection( [], {noSearch:0} );
//            this.collection.load();
//            this.listenTo( this.collection, 'remove', this.update );
//        },
//
//        filters: [
//            {
//                field: 'id',
//                el: '#idfilter',
//                type: 'autocomplete',
//                title: ''
//            }
//        ]
//    } );
//
//    return Fut.mixin( view, Components.PaginatedView, Components.SortableView, Components.FilterableView );
} );