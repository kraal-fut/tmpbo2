define ( function ( require ) {
    'use strict';
    
    var BaseCollection = require( 'collections/base' ),
        VendorModel = require( 'product/Vendor/vendor'),
        _ = require( 'underscore' );

    return BaseCollection.extend({
        entityName: 'vendor',
        model: VendorModel,
        idAttribute: 'name',
        LocalStorageSortKey: 'vendor_sort',
        LocalStorageItemsPerPageKey: 'vendor_itemsPerPage',
        LocalStorageCriteriaKey: 'vendor_criteria',

        list: undefined,

        url: function() {
            return this.baseUrl( '/v3/vendors' );
        },
        comparator: 'name',

        getlist: function() {
            if ( !_.isUndefined( this.list ) ) {
                return this.list;
            }

            var Col = BaseCollection.extend({
                model: VendorModel,
                entityName: 'vendor',
                url: this.url() + '/list'
            });

            this.list = new Col();

            return this.list;
        }
    });
});
