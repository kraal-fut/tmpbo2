define ( function ( require ) {
    'use strict';
    var Fut = require( 'fut/entities' ),
        Epoxy = require( 'backbone.epoxy' );

    var Model = Fut.Model.extend({

        initialize: function() {
            this.initComputeds();
        },

        validation: {
            'name': { required: true },
            'displayname': { required: false, minLength: 3 },
            'info': { required: false, minLength: 5, maxLength: 4096 },
            'title': {required: false, minLength: 5, maxLength: 2048 },
            'description': {required: false, minLength: 5, maxLength: 155 },
            'keyword': {
                required: false,
                minLength: 5,
                maxLength: 255
            }
        },

        computeds: {
            label: function() {
                return this.get('displayname') || this.get( 'name' );
            },
            value: function() {
                return this.get('displayname') || this.get( 'name' );
            }
        }
    } );

    Epoxy.Model.mixin( Model.prototype );

    return Model;
} );
