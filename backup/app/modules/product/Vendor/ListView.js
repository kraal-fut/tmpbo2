define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!modules/product/Vendor/listview.hbs' );

    var VendorModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            moduleName: 'product/vendors',
            rawTemplate: Template,
            entityName: 'vendor',

            map: function( model ) {
                return VendorModel.create( model );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/vendors' );
            },

            actions: {
                save: function( model ) {
                    this.storeModel( model, this.dataUrl(), model.get( 'id' ) );
                }
            }
        }
    );

} );