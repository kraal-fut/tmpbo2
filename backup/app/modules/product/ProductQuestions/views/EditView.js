define( function ( require ) {
    'use strict';
    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        _ = require( 'underscore' ),
        Moment = require( 'moment' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!modules/product/ProductQuestions/templates/EditView.hbs' );

    var ProductQuestionModel = Em.Object.extend( {
        hasEmptyMailBody: Em.computed.empty( 'email_answer' ),

        mailBody: function( key, value ) {
            if ( !_.isUndefined( value ) ) {
                this.set( 'email_answer', value );
            }

            if ( this.get( 'hasEmptyMailBody' ) ) {
                return App.settingsmodel.getSetting( 'emailSignature', '' );
            }
            return this.get( 'email_answer' );
        }.property( 'email_answer' )

    } );

    return FutEmberRoute.extend( {
        moduleName: 'product/questions/edit',
        rawTemplate: Template,
        entityName: 'product_questions',
        idAttribute: 'id',

        parse: function( data ) {
            return ProductQuestionModel.create( data );
        },

        dataUrl: function( params ) {
            var url = '/api/v3/productquestions';
            if (params){
                url += '/' + params[this.idAttribute];
            }
            return this._super( params, url );
        },

        actions: {
            save: function( model ) {
                model.set( 'AnswerDateTime', new Moment().format( 'YYYY-MM-DD HH:mm:ss' ) );

                if ( model.get( 'email_input' ) && _.isEmpty( model.get( 'user_email' ) ) ) {
                    alert( 'Please enter an e-mail addres' );
                    return false;
                }

                this.storeModel( model );
            }
        }
    } );

//    var fut = require( 'fut/entities' ),
//        Template = require( 'hbs!product/ProductQuestions/templates/PopupView' ),
//        datepicker = require( 'bootstrap-datepicker' ),
//        Components = require( 'components/components' ),
//        Moment = require( 'moment' );
//
//    var ItemView = fut.ItemView.extend( {
//        template: Template,
//
//        initialize: function() {
//            this.setViewModel( new fut.Model( { 'for_email': !!parseInt( this.model.get( 'answer_type' ), 10 ) } ) );
//            this.listenTo( this.model, 'change', this.render );
//            this.model.set( 'answerer', App.settingsmodel.getSetting( ['contactName', 'name'] ), 'Futurumshop' );
//        },
//
//        modelEvents: {
//            'change:answer_type': 'answerTypeChanged'
//        },
//
//        bindings: {
//            '#answer': 'value:answer',
//            '#is_answered': 'checked:checked(is_answered)',
//            '#question_input': 'value:defined(question)',
//            '#answer_input': 'value:defined(answer)',
//            '#answerer_input': 'value:defined(answerer)',
//            '#is_answered_input': 'checked:checked(is_answered)',
//            '#email_input': 'checked:checked(answer_type)',
//            '#email_to': 'value:user_email',
//            '#fo_input': 'checked:checked(fo_answer)',
//            '#fo_remove_input': 'checked:checked(fo_remove_question)',
//            '#email_text': 'value:email_answer'
//        },
//
//        triggers: {
//            'click #send': 'send',
//            'click #edit': 'edit',
//            'click #save_model': 'save',
//            'click .destroy': 'close'
//        },
//
//        onClose: function() {
//            this.model.fetch();
//            this.destroy();
//        },
//
//        answerTypeChanged: function( model, value ) {
//            this.viewModel.set( 'for_email', value );
//
//            if ( value ) {
//                var header = 'Dear ' + model.get( 'user_name' ) + '.';
//                var footer = '\n\n' + App.settingsmodel.getSetting( 'emailSignature', '' );
//
//                var emailValue = model.get( 'email_answer' ) || '';
//
//                emailValue = emailValue.replace( header, '' );
//                emailValue = emailValue.replace( footer, '' );
//                model.set( 'email_answer', header + emailValue + footer );
//            }
//        },
//
//        onSave: function() {
//            this.model.set( 'is_answered', 1 );
//            this.model.set( 'answer', this.model.escape( 'answer' ) );
//            this.model.set( 'answerer', this.model.escape( 'answerer' ) );
//            this.model.set( 'email_answer', $( '#email_text' ).val() );
//
//            this.model.set( 'AnswerDateTime', new Moment().format( 'YYYY-MM-DD HH:mm:ss' ) );
//            if($('#email_input').prop('checked') && !$('#email_to').val()) {
//                alert('Please enter an e-mail address');
//                return false;
//            }
//
//            if ( this.model.isValid() ) {
//                this.model.save();
//                this.destroy();
//            }
//        }
//    });
//
//    return fut.mixin( ItemView, Components.EditableView );
} );