define ( function ( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!modules/product/ProductQuestions/templates/ListView.hbs' );

    var ProductQuestionModel = Em.Object.extend( {} );

    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            moduleName: 'product/questions',
            rawTemplate: Template,
            entityName: 'product_questions',

            map: function( model ) {
                return ProductQuestionModel.create( model );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/productquestions' );
            },

            actions: {
                save: function( model ) {
                    this.storeModel( model, this.dataUrl(), model.get( 'id' ) );
                },
                convert: function( model ) {
                    var comments = model.get( 'question' );
                    var prodlevid = model.get( 'prodlevid' );
                    var name = model.get( 'user_name' );
                    var email = model.get( 'user_email' );
                    var date = new Date();
                    var year = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice( -2 ) + '-' + ('0' + date.getDate()).slice( -2 );
                    var hours = date.getHours();
                    var minutes = date.getMinutes();
                    var seconds = date.getSeconds();
                    date = year + ' ' + hours + ':' + minutes + ':' + seconds;
                    var commentModel = {
                        Name: name,
                        Score: 1,
                        Active: 0,
                        Email: email,
                        Comments: comments,
                        ProdLevId: prodlevid,
                        sign_question: 0,
                        created: date
                    };

                    var route = this;
                    Em.$.post( '/api/v3/productcomments', commentModel ).then(function(){
                        route.deleteModel( model );
                    });
                }
            }
        }
    );
    //
    //var Fut = require( 'fut/entities' ),
    //    ListItemView = require( 'product/ProductQuestions/views/ListItemView' ),
    //    ListTemplate = require( 'hbs!product/ProductQuestions/templates/ListView' ),
    //    Collection = require( 'product/ProductQuestions/collections/productquestions' ),
    //    VendorCollection = require( 'product/Vendor/vendors'),
    //    Components = require( 'components/components' );
    //
    //var view = Fut.CompositeView.extend( {
    //    collection: undefined,
    //
    //    tagName: 'div',
    //    childView: ListItemView,
    //    childViewContainer: 'tbody',
    //    template: ListTemplate,
    //
    //    emptyView: Fut.ItemView.extend( {
    //        template: function() {
    //            return 'No questions found';
    //        }
    //    } ),
    //
    //    filters: [
    //        {
    //            field: 'product.Vendor',
    //            el: '#vendorfilter',
    //            type: 'autocomplete',
    //            title: 'Vendor',
    //            sourceCollection: VendorCollection,
    //            columnName: 'name'
    //        },
    //        {
    //            el: '#isansweredfilter',
    //            type: 'bool',
    //            title: '',
    //            sourceCollection: Collection,
    //            nameColumn: 'is_answered',
    //            yeslabel: 'answered',
    //            nolabel: 'unanswered'
    //        }
    //    ],
    //
    //    initialize: function() {
    //        this.collection = new Collection( [], {noSearch:0} );
    //        this.collection.load();
    //    }
    //
    //} );
    //
    //return Fut.mixin( view, Components.PaginatedView, Components.SortableView );
} );