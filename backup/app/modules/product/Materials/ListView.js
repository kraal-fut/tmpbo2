define( function( require ) {
    'use strict';
    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        ImportCsv = require( 'modules/product/Materials/importCsv' ),
        Template = require( 'text!modules/product/Materials/listview.hbs' );

    var MaterialsModel = Em.Object.extend({});

    window.EApp.ProductMaterialsView = Em.View.extend({
        dragOver: function( event ) {
            event.preventDefault();
        },

        dragEnter: function( event ) {
            event.preventDefault();
        },

        dragLeave: function( event ) {
            event.preventDefault();
        },

        drop: function( event ) {
            event.preventDefault();
            var file = event.dataTransfer.files[0];
            var reader = new FileReader();

            var importer = new ImportCsv( '/api/v3/productmaterials' );
            reader.readAsText( file );
            reader.onload = function( evt ) {
                importer.parseData( evt.target.result );
            };
            reader.onerror = function() {
                App.notify( 'Something went wrong with reading the file' );
            };
        }
    });

    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            moduleName: 'product/materials',
            rawTemplate: Template,
            entityName: 'materials',

            map: function( model ) {
                return MaterialsModel.create( model );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/productmaterials' );
            },

            actions: {
                save: function( model ) {
                    this.storeModel( model, this.dataUrl(), model.get( 'id' ) );
                },
                addMaterial: function() {
                    this.currentModel.unshiftObject( new MaterialsModel( {} ) );
                },
                deleteMaterial: function( model ) {
                    this.deleteModel( model );
                }
            }
        }
    );
} );