define( function( require ) {
    'use strict';

    var _ = require( 'underscore' );

    var importer = function( baseUrl ) {
        this.baseUrl = baseUrl;
        this.importProducts = [];
    };

    importer.prototype = {
        parseData: function( data ) {
            _.each( data.split( /\r\n|\r|\n/g ), function( line ) {
                if ( line !== '' && !line.match( 'prodlevid' ) && !line.match( 'Product' ) ) {
                    this.handleLine( line );
                }
            }, this );

            var that = this;
            var url = this.baseUrl + '/import';
            var products = JSON.stringify( this.importProducts );

            $.ajax( {
                url: url,
                method: 'POST',
                data: {products: products}
            } ).done( function( data ) {
                data = JSON.parse( data );
                App.notify( data.error.message );
            } ).fail( function() {
                that.uploadFail();
            } ).always( function() {
                //that.collection.fetch();
            } );

            this.importProducts = [];
        },

        handleLine: function( line ) {
            var data = line.split( ';' );
            if ( !_.isArray( data ) || data.length === 1 ) {
                data = line.split( ',' );
            }

            this.importProducts.push( {
                prodlevid: data[0],
                part: data[1],
                description: data[2],
                material: data[3],
                percentage: data[4]
            } );
        },

        uploadFail: function() {
            var confirmed = confirm(
                'The import contains errors do you wish to download these?'
            );

            if ( confirmed ) {
                window.location = this.baseUrl + '/download';
            }
        }
    };

    return importer;
} );