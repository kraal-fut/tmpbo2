define( function( require, exports ) {
    'use strict';

    var Em = require( 'ember-load' ),
        _ = require( 'underscore' ),
        EmberMixins = require( 'fut/EmberMixins' );

    var CouponRuleModel = Em.Object.extend( window.EApp.Jsonable, {
        getType: function() {
            var couponRuleType = this.get( 'coupon_rule_type' );
            if ( _.isUndefined( couponRuleType ) ) {
                return '';
            }

            var type = this.get( 'couponRuleTypes' ).filter( function( obj ) {
                return obj.value === parseInt( couponRuleType );
            }, this );

            if ( _.isEmpty( type ) ) {
                return '';
            }
            return _.first( type ).label;
        }.property( 'coupon_rule_type' ),

        getCouponRuleArgument: function() {
            return this.couponRuleArguments( this.get( 'coupon_rule_type' ) );
        }.property( 'coupon_rule_type' ),

        hasArguments: function() {
            return !_.isUndefined( this.get( 'getCouponRuleArgument' ) );
        }.property( 'getCouponRuleArgument' ),

        isSelectableArgument: function() {
            var arg = this.get( 'getCouponRuleArgument' );
            return _.isObject( arg ) && arg.options;

        }.property( 'getCouponRuleArgument' ),

        selectOptions: function() {
            if ( !this.get('isSelectableArgument') ) {
                return [];
            }
            return this.get('getCouponRuleArgument.options');
        }.property( 'isSelectableArgument' ),

        isInputArgument: function() {
            return _.isUndefined( this.get( 'getCouponRuleArgument.options' ) );
        }.property( 'getCouponRuleArgument' ),

        inputLabel: function() {
            if ( !this.get('isInputArgument') ) {
                return '';
            }
            return this.get('getCouponRuleArgument.label');

        }.property( 'isInputArgument' ),

        hasButtons: function() {
            var optional = this.get( 'getCouponRuleArgument' );
            return _.isObject( optional ) && !_.isUndefined( optional.buttons ) && !_.isUndefined( this.get( 'id' ) );
        }.property( 'getCouponRuleArgument', 'id' ),

        isMultipleCodes: function() {
            return this.get( 'coupon_rule_type' ) === 128;
        }.property( 'coupon_rule_type' ),

        couponRuleTypes: function() {
            return [
                {label: 'Code', value: 1},
                {label: 'Discount', value: 2},
                {label: 'Minimum order', value: 4},
                {label: 'Maximum order', value: 8},
                {label: 'Multiple Codes', value: 128},
                {label: 'Max code usage', value: 256}
            ];
        }.property(),

        couponRuleArguments: function( type ) {
            var Arguments = {
                0: undefined,
                1: undefined,
                2: {options:[
                    {label: 'Procent', value: 'procent'},
                    {label: 'Euro', value: 'euro'}
                ]},
                4: {label:'Euro'},
                8: {label:'Euro'},
                128: {buttons: true},
                256: undefined
            };

            return Arguments[type];
        }
    } );

    var CouponModel = Em.Object.extend( window.EApp.Jsonable, {
        totalCodes: Em.computed.sum( 'ActiveCodeCount', 'UsedCodeCount' ),

        hasMultipleCodes: function() {
            return this.get( 'coupon_rule' ).isAny( 'isMultipleCodes', true );
        }.property( 'coupon_rule.@each.isMultipleCodes' )
    } );

    exports.Models = {
        CouponModel: CouponModel,
        CouponRuleModel: CouponRuleModel
    };

    return exports.Models;
} );