define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        _ = require( 'underscore' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        PopupGenerateTemplate = require( 'text!modules/product/Discount/generate_codes.hbs' ),
        PopupEditRuleTemplate = require( 'text!modules/product/Discount/edit_rules.hbs' ),
        Models = require( 'modules/product/Discount/discount' ),
        Template = require( 'text!modules/product/Discount/listview.hbs' ),
        DisountRulePartial = require( 'text!modules/product/Discount/rule.hbs' );

    Em.TEMPLATES['discount/generate-popup'] = Em.Handlebars.compile( PopupGenerateTemplate );
    Em.TEMPLATES['discount/edit-rules-popup'] = Em.Handlebars.compile( PopupEditRuleTemplate );
    Em.TEMPLATES['discount/rule'] = Em.Handlebars.compile( DisountRulePartial );

    window.EApp.DiscountEditRulesPopupController = Em.Controller.extend({
        actions: {
            addRule: function( coupon ) {
                coupon.coupon_rule.pushObject( Models.CouponRuleModel.create( {} ) );
            },
            deleteRule: function( coupon, rule ) {
                coupon.coupon_rule.removeObject( rule );
            }
        }
    });

    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            moduleName: 'product/discount',
            rawTemplate: Template,
            entityName: 'coupon',

            map: function( model ) {
                model.coupon_rule = Em.A( _.map( model.coupon_rule, function( rule ) {
                    return Models.CouponRuleModel.create( rule );
                }));

                return Models.CouponModel.create( model );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/coupons' );
            },

            actions: {
                save: function( model ) {
                    this.storeModel( model, this.dataUrl(), model.get( 'id' ) );
                },
                downloadCodes: function( model ) {
                    window.location = this.dataUrl() + '/' + model.get( 'id' ) + '/codes';
                }
            }
        }
    );
} );