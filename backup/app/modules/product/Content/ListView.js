define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        _ = require( 'underscore' ),
        StickitHelpers = require( 'components/stickitHelpers' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!modules/product/Content/listview.hbs' );

    var ContentModel = Em.Object.extend( window.EApp.Jsonable, {
        productImagesHtml: function() {
            return StickitHelpers.getImages( this.get( 'Image' ) ).htmlSafe();
        }.property( 'Image' )
    } );

    /**
     * @todo refactor the popup used for editing images to Ember
     */
    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            moduleName: 'product/contents',
            rawTemplate: Template,
            entityName: 'productdescription',

            map: function( model ) {
                if ( _.isString( model.Image ) && !_.isEmpty( model.Image ) ) {
                    try {
                        model.Image = JSON.parse( model.Image );
                    } catch ( ignored ) {
                        model.Image = {
                            1: {
                                filename: model.Image,
                                prodlevid: model.ProdLevId,
                                oldSituation: true
                            }
                        };
                    }
                }

                if ( _.isObject( model.Image ) ) {
                    model.Image = _.map( _.toArray( model.Image ), function( item ) {
                        return Em.Object.create(item);
                    });
                }

                return ContentModel.create( model );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/productdescription' );
            },

            actions: {
                save: function( model ) {
                    this.storeModel( model, this.dataUrl(), model.get( 'id' ) );
                },
                openImages: function( model, fieldName, editing ) {
                    if ( !editing ) {
                        return;
                    }
                }
            }
        }
    );

} );