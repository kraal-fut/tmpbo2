define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        _ = require( 'underscore' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        UploadModel = require( 'modules/product/ImageUpload/uploadModel' ),
        Template = require( 'text!modules/product/ImageUpload/imageUploadView.hbs' );

    window.EApp.ProductImageuploadView = Em.View.extend({
        dragOver: function( event ) {
            event.preventDefault();
        },
        dragEnter: function( event ) {
            event.preventDefault();
        },
        dragLeave: function( event ) {
            event.preventDefault();
        },
        drop: function( event ) {
            event.preventDefault();
            this.checkFiles( event.originalEvent.dataTransfer.files );
        },

        checkFiles: function( files ) {
            var models = this.get( 'controller.model' );
            models.pushObjects( _.map( files, this.extractDataFromFilename ) );

            var ids = models.getEach( 'prodlevid' ).uniq().join( ',' );
            var that = this;

            Em.$.getJSON( '/api/v3/productdescription/' + ids + '/images?limit=' + (models.get( 'length' ) + 1) )
                .then(function makeArray( response ) {
                    if ( _.isObject( response ) ) {
                        if (!_.isUndefined( response.ProductImage )) {
                            return response.ProductImage;
                        }
                        response = [response];
                    }
                    return response;
                }).then( function processFounds( response ) {
                    _.each( response, that.findMatchingUploadModel.bind( that ));
                    return response;
                }).then( function processNotFounds( response ) {
                    var notFounds = _.difference( models.getEach( 'prodlevid' ), response.getEach( 'id' ) );

                    models.filter(function( model ){
                        return notFounds.contains( model.get( 'prodlevid' ) );
                    }).setEach( 'invalid', true );

                    return models;
                });
        },

        extractDataFromFilename: function( file ) {
            var fullFileName = file.name;
            var fileExt = fullFileName.split( '.' ).pop().toLowerCase();
            var fileName = fullFileName.substr( 0, ( fullFileName.length - fileExt.length ) - 1 );

            var extraPos = fileName.indexOf( '_extra' );
            var isExtra = extraPos !== -1;
            var prodlevid = fileName.substr( 0, isExtra ? extraPos : undefined );

            return UploadModel.create({
                invalid: false,
                prodlevid: prodlevid,
                filename: fullFileName,
                fileExt: fileExt,
                isExtra: isExtra,
                file: file
            }).readFile();
        },

        findMatchingUploadModel: function( foundAtServer ) {
            var newFile = _.reduce( foundAtServer.images, function( memo, image ) {
                return parseInt( image.filename, 10 ) > memo ? parseInt( image.filename, 10 ) : memo;
            }, 0 );

            this.get( 'controller.model' )
                .filterBy( 'prodlevid', foundAtServer.id )
                .forEach(function( imageToUploadModel ) {
                    imageToUploadModel.set( 'images', foundAtServer.images );
                    imageToUploadModel.setVariationInfo( foundAtServer.relatedProducts );
                    newFile = imageToUploadModel.setFileName( newFile );
                } );
        }
    });

    return FutEmberRoute.extend( {
            moduleName: 'product/imageupload',
            rawTemplate: Template,
            model: function() {
                return Em.ArrayProxy.create({
                    content: Em.A([])
                });
            },
            getSelected: function() {
                return this.currentModel.filterBy( 'selected', true );
            },
            hasSelected: function() {
                return this.currentModel.isAny( 'selected', true );
            },

            actions: {
                selectAll: function() {
                    this.currentModel.filter(function( item ) {
                        return item.get( 'action' ) !== 'none';
                    }).setEach( 'selected', !this.hasSelected() );
                },

                refresh: function() {
                    this.currentModel.clear();
                },

                doUpload: function( collection ) {
                    if ( !this.hasSelected() ) {
                        window.App.notifyQuick( 'Select images to upload', {status: 'warning'} );
                        return;
                    }

                    var selected = this.getSelected();

                    var promises = [];

                    selected.forEach(function( m ) {
                        var url = '/api/v3/productdescription/' + m.get( 'prodlevid' ) + '/images/' + m.get( 'filename' );
                        promises.push( Em.$.ajax( {
                            type: 'PUT',
                            url: url,
                            data: JSON.stringify( m.getJSON( ['action'], ['images', 'file', 'selected'] ) ),
                            contentType: 'application/json'
                        } ).then( function( response ) {
                            collection.removeObject( m );
                            return response;
                        } ).fail(function( response ){
                            m.set( 'error', response.responseText );
                            return response;
                        }) );

                    });

                    Em.RSVP.all( promises ).then(function(){
                        window.App.notifyQuick( 'All images uploaded' );
                    });
                }
            }
        }
    );

} );