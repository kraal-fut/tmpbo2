define( function( require, exports ) {
    'use strict';
    var Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        _ = require( 'underscore' );

    exports.UploadModel = Em.Object.extend( window.EApp.Jsonable, {
        images: [],
        isExtra: false,
        hasVariations: false,
        isMain: false,
        allVariations: false,

        readFile: function() {
            var reader = new FileReader();

            var f = this.get( 'file' );

            reader.onload = _.bind( function( e ) {
                this.fileSetter( f, e, this, 'RawData' );
            }, this );

            reader.readAsDataURL( f );

            return this;
        },

        setFileName: function( oldFile ) {
            if ( this.get( 'isExtra' ) ) {
                oldFile++;
                if ( oldFile === 1 ) {
                    oldFile++;
                }
                this.set( 'filename', oldFile + '.' + this.get( 'fileExt' ) );
            } else {
                this.set( 'filename', '1.' + this.get( 'fileExt' ) );
            }

            return oldFile;
        },

        fileSetter: function( file, event, context, fieldName ) {
            this.set( fieldName, {
                ImageFilename: file.name,
                Image: event.target.result
            } );
        },

        notExtraButHasMain: function() {
            return !this.get( 'isExtra' ) && _.reduce( this.get( 'images' ), function( memo, model ) {
                    if ( parseInt( model.filename, 10 ) === 1 ) {
                        memo = true;
                    }
                    return memo;
                }, false );
        }.property( 'isExtra', 'images' ),

        action: function() {
            if ( this.get( 'invalid' ) ) {
                return 'none';
            }

            var action = 'create';

            if ( this.get( 'isExtra' ) ) {
                action = 'add';
            } else if ( this.get( 'notExtraButHasMain' ) ) {
                action = 'overwrite';
            }

            return action;
        }.property( 'invalid', 'isExtra', 'notExtraButHasMain' ),


        setVariationInfo: function( related ) {
            var hasVariations = false;
            if ( !_.isUndefined( related ) ) {
                hasVariations = !!related.variations;
            }

            var isMain = true;
            if ( hasVariations && related.main !== this.get( 'prodlevid' ) ) {
                isMain = false;
            }

            this.set( 'hasVariations', hasVariations);
            this.set( 'isMain', isMain);
            this.set( 'allVariations', isMain );
        }
    } );

    return exports.UploadModel;
} );