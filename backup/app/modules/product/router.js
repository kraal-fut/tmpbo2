/**
 * @module product/router
 */
define ( function( require ) {
    'use strict';

    var Cocktail = require( 'cocktail' ),
        Marionette = require( 'marionette' ),
        ModuleRouter = require( 'components/moduleRouter' ),
        DiscountListView = require( 'modules/product/Discount/ListView' ),
        ProductQuestionsListView = require( 'modules/product/ProductQuestions/views/ListView' ),
        ProductQuestionsEditView = require( 'modules/product/ProductQuestions/views/EditView' ),
        ContentListView = require( 'modules/product/Content/ListView' ),
        MaterialsListView = require( 'modules/product/Materials/ListView' ),
        VendorListView = require( 'modules/product/Vendor/ListView'),
        SuppliersListView = require( 'modules/product/Suppliers/ListView' ),
        ImageUploadView = require( 'modules/product/ImageUpload/ImageUpload' );

    var router = Marionette.AppRouter.extend( {
        routes: {
            'products/content': {
                method: 'ProductContents',
                name: 'Content',
                routeName: 'product.contents',
                emberRoute: ContentListView
            },
            'products/materials': {
                method: 'ProductMaterials',
                name: 'Materials',
                routeName: 'product.materials',
                emberRoute: MaterialsListView
            },
            'products/productquestions': {
                method: 'ProductQuestions',
                name: 'Product Questions',
                routeName: 'product.questions',
                emberRoute: ProductQuestionsListView
            },
            'products/productquestions/:id': {
                method: 'ProductQuestionsEdit',
                name: 'Product Questions',
                routeName: 'product.questions.edit',
                hideInMenu: true,
                emberRoute: ProductQuestionsEditView
            },
            //'products/productreviews': {
            //    'method':'productreviews',
            //    'name':'Product Reviews'
            //},
            'products/imageupload': {
                method: 'ProductImageupload',
                name: 'Image Upload',
                routeName: 'product.imageupload',
                emberRoute: ImageUploadView
            },
            'products/discount': {
                method: 'ProductDiscount',
                name: 'Discount',
                routeName: 'product.discount',
                emberRoute: DiscountListView
            },
            'products/vendors': {
                method: 'ProductVendors',
                name: 'Vendors',
                routeName: 'product.vendors',
                emberRoute: VendorListView
            },
            'products/suppliers': {
                method: 'ProductSuppliers',
                name: 'Suppliers',
                routeName: 'product.suppliers',
                emberRoute: SuppliersListView
            }
        }
    } );

    Cocktail.mixin( router, ModuleRouter );
    return router;
} );
