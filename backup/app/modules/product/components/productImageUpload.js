define ( function ( require, exports ) {
    'use strict';
    var _ = require( 'underscore' );

    var productImageUpload = function() {
        this.validImagesTypes = ['image/png', 'image/jpeg', 'image/jpg', 'image/gif'];
        this.done = 0;
        this.totalSize = 0;
        this.callbacks = {};
        this.stateModel = undefined;
    };

    productImageUpload.prototype = {

        setCallBacks: function( callbacks, context ){
            var ctx = context || this;
            var dummy = function(){};

            var c = callbacks || {
                success: dummy,
                failure: dummy,
                secondAction: dummy,
                progress: dummy
            };

            this.callbacks = {
                success: _.bind( c.success || dummy, ctx ),
                failure: _.bind( c.failure || dummy, ctx ),
                secondAction: _.bind( c.secondAction || dummy, ctx ),
                progress : _.bind( c.progress || dummy, ctx )
            };
        },

        handleDrop: function( stateModel, callbacks, context ) {
            this.stateModel = stateModel;
            this.setCallBacks( callbacks, context );

            var filteredFiles = this.getTotalSize( this.filterWrongTypes( stateModel.get('files') ) );

            _.each( filteredFiles, function ( file ) {
                this.uploadImage( file );
            }, this );
        },

        filterWrongTypes: function( files ) {
            return _.filter( files, function ( file ) {
                if ( !_.contains( this.validImagesTypes, file.type ) ) {
                    this.callbacks.failure( {message: file.name + ' wrong file type'} );
                    return false;
                }
                return true;
            }, this );
        },

        getTotalSize: function( files ) {
            this.totalSize = 0;
            this.done = 0;
            _.each( files, function ( file ) {
                this.totalSize += file.size;
            }, this );

            return files;
        },

        uploadImage: function( file, overwrite, stateModel ) {
            if( !_.isUndefined(stateModel) ) {
                this.stateModel = stateModel;
            }

            var xhr = new XMLHttpRequest();

            xhr.memo = 0;
            var that = this;

            xhr.upload.addEventListener( 'progress', function( data ) {
                that.progressUpdate.call( that, data, xhr );
            }, false );

            xhr.onreadystatechange = function(){
                that.stateChange.call( that, xhr, file );
            };

            xhr.open( 'POST', this.getUploadUrl(), true );
            xhr.setRequestHeader( 'X-FILE-NAME', file.name );
            xhr.setRequestHeader( 'X-FILE-DATA', JSON.stringify( {mediaType: this.stateModel.get( 'mediaType' ) }) );
            xhr.setRequestHeader( 'X-FILE-OVERWRITE', overwrite || false);
            xhr.send( file );
            return false;
        },

        getUploadUrl: function() {
            return '/api/media/images/' + this.stateModel.get( 'mediaType' );
        },

        stateChange: function( xhr, file ) {
            if ( xhr.readyState !== 4 ) {
                return;
            }

            var status = xhr.status;
            var responseText = xhr.responseText;

            if ( status === 404 ) {
                this.callbacks.failure( responseText );
                return;
            }

            var response = {};
            if ( !_.isEmpty( responseText ) ) {
                response = JSON.parse( responseText );
            }

            if ( status === 200 ) {
                this.callbacks.success( response );
            } else if ( this.hasOverwriteStatus( status ) ) {
                this.callbacks.secondAction( file, JSON.parse( response.message ), this );
            } else {
                this.callbacks.failure( response );
            }
        },

        hasOverwriteStatus: function( status ) {
            return ( status === 409 || status === 412 );
        },

        getProgress: function( serverLoaded, xhrInstance ) {
            this.done += ( serverLoaded - xhrInstance.memo );
            xhrInstance.memo = serverLoaded;
            return ( this.done / this.totalSize ) * 100;
        },

        progressUpdate: function( data, xhrInstance ) {
            if ( data.lengthComputable ) {
                this.callbacks.progress( this.getProgress( data.loaded, xhrInstance ) );
            }
        }
    };

    exports.productImageUpload = productImageUpload;

    return productImageUpload;
} );
