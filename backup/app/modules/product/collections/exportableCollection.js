define ( function ( require, exports ) {
    'use strict';

    var _ = require( 'underscore' );

    exports.ExportableOrder = {
        exportCSV: function() {
            var confirmed = true;

            if ( this.length > 1000 ) {
                confirmed = confirm(
                    'The collection contains more than 1000 records (' +
                     this.length +
                     ')\nAre you sure you want to export?'
                );
            }

            var find = App.settingsmodel.get( this.LocalStorageCriteriaKey );
            if ( _.isUndefined( find ) ) {
                find = '{}';
            }

            var sort = App.settingsmodel.get( this.LocalStorageSortKey );
            if ( _.isUndefined( sort ) ) {
                sort = '{}';
            }

            var url = this.url() + '.csv?' + 'norelations=1' + '&find=' + find + '&sort=' + sort + '&limit=' + this.length;

            if ( confirmed ) {
                window.location = url;
            }
        }
    };

    return exports.ExportableOrder;
});