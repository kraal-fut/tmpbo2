define( function ( require ) {
    'use strict';

    var fut = require( 'fut/entities' ),
        Template = require( 'hbs!product/ProductComments/templates/PopupView' ),
        datepicker = require('bootstrap-datepicker'),
        PlusesCollection = require('product/ProductComments/collections/reviewpluses'),
        MinusesCollection = require('product/ProductComments/collections/reviewminuses'),
        Components = require( 'components/components' );

    var ItemView = fut.ItemView.extend( {
        myOwnSourceCollection: undefined,
        template: Template,
        plusesCollection: undefined,
        minusesCollection: undefined,
        bindings: {
            '#comment_input': 'value:Comments',
            '#active_input': 'checked:checked(Active)'
        },

        initialize: function() {
            this.plusesCollection = new PlusesCollection();
            this.plusesCollection.addCriteria(this.model.get('id'), 'review_id');
            this.minusesCollection = new MinusesCollection();
            this.minusesCollection.addCriteria(this.model.get('id'), 'review_id');
            this.listenTo(this.plusesCollection, 'sync', this.render);
            this.listenTo(this.minusesCollection, 'sync', this.render);
        },

        triggers: {
            'click #save_model': 'save',
            'click .destroy' : 'close',
            'click #popupcontainer' : 'hide'
        },

        onClose: function() {
            this.model.fetch();
            this.destroy();
        },

        setMark: function(array, collection) {
            for(var i = 0; i < array.length; i++) {
                var element = array[i]
                var id = element.getAttribute('id');
                var value = element.value;
                var model = collection.get(id);
                model.set('mark', value);
                model.save();
                model.fetch();
            }
        },

        onSave: function () {
            var pluses = document.getElementsByName('plus');
            var minuses = document.getElementsByName('minus');
            this.setMark(pluses, this.plusesCollection);
            this.setMark(minuses, this.minusesCollection);
            this.model.set('Score',$('#score_input :selected').text());
            if ( this.model.isValid() ) {
                this.model.save();
                this.destroy();
                this.model.fetch();
            }
            else {
                alert('Validation failed');
            }
        }
    });

    return fut.mixin( ItemView, Components.EditableView );
} );