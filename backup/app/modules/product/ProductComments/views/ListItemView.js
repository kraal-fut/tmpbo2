define ( function ( require, exports ) {
    'use strict';
    var fut = require( 'fut/entities' ),
        ListItemTemplate = require( 'hbs!product/ProductComments/templates/listitemview' ),
        PopupView = require( 'product/ProductComments/views/PopupView'),
        QuestionModel = require('product/ProductQuestions/models/productquestion'),
        Components = require ( 'components/components');

    exports.ItemView = fut.CompositeView.extend( {
        tagName: 'tr',
        template: ListItemTemplate,

        triggers: {
            'click #edit': 'edit',
            'click #remove': 'remove',
            'click #convert': 'convert'
        },

        updateView: function() {
            this.render();
        },

        onRemove: function() {
            var result = confirm( 'This will permanently delete this item, are you sure?' );
            if ( !result ) {
                return;
            }
            this.model.destroy();
        },

        onEdit: function() {
            App.popupRegion.show( new PopupView( {model: this.model } ) );
            this.listenTo( this.model, 'change', this.updateView );
        },

        onConvert: function() {
            var question = this.model.get('Comments');
            var prodlevid = this.model.get('ProdLevId');
            var userName = this.model.get('Name');
            var userEmail = this.model.get('Email');
            var questionModel = new QuestionModel();
            questionModel.url = '/api/v2/productquestions';
            questionModel.set('user_name', userName);
            questionModel.set('user_email', userEmail);
            questionModel.set('is_answered', 0);
            questionModel.set('question', question);
            questionModel.set('prodlevid', prodlevid);
            questionModel.save();
            this.model.destroy();
        }

    } );

    return fut.mixin( exports.ItemView, Components.EditableView );
} );