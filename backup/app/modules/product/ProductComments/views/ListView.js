define ( function ( require ) {
    'use strict';

    var Fut = require( 'fut/entities' ),
        ListItemView = require( 'product/ProductComments/views/ListItemView' ),
        ListTemplate = require( 'hbs!product/ProductComments/templates/ListView' ),
        Collection = require( 'product/ProductComments/collections/productcomments' ),
        VendorCollection = require( 'product/Vendor/collections/vendors'),
        Components = require( 'components/components' );

    var view = Fut.CompositeView.extend( {
        collection: undefined,

        tagName: 'div',
        childView: ListItemView,
        childViewContainer: 'tbody',
        template: ListTemplate,

        emptyView: Fut.ItemView.extend( {
            template: function() {
                return 'No comments found';
            }
        } ),

        ui: {
            'filter': '.fa-filter'
        },

        filters: [
            {
                field: 'product.Vendor',
                el: '#vendorfilter',
                type: 'autocomplete',
                title: 'Vendor',
                sourceCollection: VendorCollection,
                columnName: 'name'
            }
        ],

        initialize: function() {
            this.collection = new Collection( [], {noSearch:0} );
            this.collection.load();
        },

        updateView: function() {
            this.render();
        }

    } );

    return Fut.mixin( view, Components.PaginatedView, Components.FilterableView, Components.SortableView );
} );