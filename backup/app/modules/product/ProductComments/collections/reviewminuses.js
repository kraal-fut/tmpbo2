define ( function ( require ) {
    'use strict';
    var BaseCollection = require( 'collections/base' ),
        Model = require( 'product/ProductComments/models/reviewminus' ),
        _ = require( 'underscore' );

    return BaseCollection.extend( {
        entityName: 'review_minuses',
        model: Model,
        limit: undefined,

        dataSendSearch: {},

        url: function() {
            return this.baseUrl( '/v3/reviewminuses' );
        },

        dataSort:{},

        LocalStorageSortKey: 'review_minuses_sort',
        LocalStorageItemsPerPageKey: 'review_minuses_itemsPerPage',
        LocalStorageCriteriaKey: 'review_minuses_criteria'
    } );
} );
