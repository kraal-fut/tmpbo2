define ( function ( require ) {
    'use strict';
    var BaseCollection = require( 'collections/base' ),
        Model = require( 'product/ProductComments/models/productcomment' ),
        _ = require( 'underscore' );

    return BaseCollection.extend( {
        entityName: 'product_comments',
        model: Model,
        limit: undefined,

        dataSendSearch: {},

        url: function() {
            return this.baseUrl( '/v3/productcomments' );
        },

        dataSort:{},

        initialize: function( models,options ) {
            options = _.extend( {}, options );
            this.limit = options.limit || 5;
        },

        LocalStorageSortKey: 'product_comments_sort',
        LocalStorageItemsPerPageKey: 'product_comments_itemsPerPage',
        LocalStorageCriteriaKey: 'product_comments_criteria'
    } );
} );
