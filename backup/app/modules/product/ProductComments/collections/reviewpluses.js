define ( function ( require ) {
    'use strict';
    var BaseCollection = require( 'collections/base' ),
        Model = require( 'product/ProductComments/models/reviewplus' ),
        _ = require( 'underscore' );

    return BaseCollection.extend( {
        entityName: 'review_pluses',
        model: Model,
        limit: undefined,

        dataSendSearch: {},

        url: function() {
            return this.baseUrl( '/v3/reviewpluses' );
        },

        dataSort:{},

        LocalStorageSortKey: 'review_pluses_sort',
        LocalStorageItemsPerPageKey: 'review_pluses_itemsPerPage',
        LocalStorageCriteriaKey: 'review_pluses_criteria'
    } );
} );
