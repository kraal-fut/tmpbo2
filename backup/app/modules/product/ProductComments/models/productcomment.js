define( function ( require ) {
    'use strict';

    var Fut = require( 'fut/entities' );

    return Fut.Model.extend({
        defaults: {
            'Active': false,
            'Comments': undefined,
            'Score': undefined
            //'user_email': undefined
        },

        validation: {
            'Comments': {
                required: true,
                minLength: 3,
                //maxLength: 800,
                blank: false
            }
        }
    });
});
