define ( function ( require ) {
    'use strict';

    var Fut = require( 'fut/entities' ),
        ImageListItemView = require ( 'shop/Landing/ImageListItemView' ),
        Template = require( 'hbs!shop/Landing/imagelistview' ),
        Components = require( 'components/components' );

    var ListView = Fut.CompositeView.extend ( {
        className: 'panel panel-default',
        template: Template,
        childView: ImageListItemView,

        attributes: {'style':'overflow-y:scroll;height:100%'},

        triggers: {
            'click #refresh': 'refresh'
        },

        initialize: function() {
            this.collection = new Fut.Collection(this.model.get( 'images' ));
        },

        onRefresh: function() {
            this.model.scanImages();
        },

        imageSetter: function( theFile, e, fieldName, event ) {
            var model = new this.collection.model();

            model.set( 'uploadImage', {
                ImageFilename: theFile.name,
                Image: event.target.result
            } );

            this.collection.add( model );
        }
    } );

    return Fut.mixin( ListView, Components.ImageUploader );
} );