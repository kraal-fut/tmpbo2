define(function(require){
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/Landing/listview.hbs' );

    var LandingPageModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            moduleName: 'shop/landings',
            rawTemplate: Template,
            entityName: 'landings',
            idAttribute: '_id',

            map: function( order ) {
                return LandingPageModel.create( order );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/landings' );
            },

            actions: {
                add: function() {
                    this.currentModel.unshiftObject( new LandingPageModel( {} ) );
                },

                del: function( landingPage ) {
                    this.deleteModel( landingPage );
                },

                save: function( landingPage ) {
                    this.storeModel( landingPage, this.dataUrl(), landingPage.get( '_id' ) );
                }
            }
        }
    );
});