define( function( require ) {
    'use strict';
    var Fut = require( 'fut/entities' ),
        _ = require( 'underscore' ),
        Epoxy = require( 'backbone.epoxy' ),
        when = require( 'when/when' );

    var ImageModel = Epoxy.Model.extend({
        exportable: ['image'],

        defaults: {
            url: undefined
        },

        image: function() {
            var upload = this.get( 'uploadImage' );
            if ( upload ) {
                return upload.Image;
            }
            var url = this.get( 'url' );
            if ( url ) {
                url = url.replace( 'http:', '');
                url = url.replace( 'https:', '');
                url = url.replace( '//', '');
                url = url.replace( 'www.futurumshop.nl', '' );
                url = url.replace( 'www.futurumshop.de', '' );

                return App.settingsmodel.get( 'BASE_URL' ) + url;
            }

            return '';
        }
    });

    return Fut.Model.extend({
        idAttribute: '_id',
        exportable: ['previewLink'],

        defaults: {
            '_id': undefined,
            'name': '',
            'body': '',
            'products': '',
            'blogitems': undefined,
            'vendors': undefined,
            'header': true,
            'footer': true,
            'publish': false,
            'vendorpage': false,
            'themepage': false,
            'images': []
        },

        relations: [
            { type: Fut.Many, key: 'images', relatedModel: ImageModel }
        ],

        unique: function( list, key ) {
            var added = [];

            return _.reduce( list, function ( memo, item ) {
                if ( _.contains( added, item[key] ) ) {
                    return memo;
                }
                memo.push( item );
                added.push( item[key] );
                return memo;
            }, [] );
        },

        scanImages: function() {
            var $content = $( '<div>' + this.get( 'body' ) + '</div>');

            var $images = $content.find( 'img' );
            var urls = [];

            _.each( $images, function( img ){
                urls.push( {url: img.getAttribute( 'src' )} );
            });

            this.set( 'images', this.unique( urls, 'url' ) );
        },

        hasImages: function() {
            return this.get ( 'images' ).length !== 0;
        },

        copyToDe: function() {
            var that = this;

            return new Fut.Promise(function( resolve, reject ){
                $.ajax( {
                    url: that.url() + '/copy'
                } ).done( function() {
                    resolve( that );
                } ).fail( function ( error ) {
                    error = JSON.parse( error.responseText );
                    reject(new Error( error.error.message ) );
                } );
            });
        },

        previewLink: function () {
            var type;
            var specifier;

            if ( this.get( 'themepage' ) ) {
                type = 'theme';
                specifier = '&element=' + this.get( 'name' );
            } else if ( this.get( 'vendorpage' ) ) {
                type = 'vendor';
                specifier = '&name=' + this.get( 'name' );
            } else {
                type = 'html';
                specifier = '/' + this.get( '_id' );
            }

            return App.settingsmodel.get( 'BASE_URL' ) + '?action=' + type + specifier + '&preview=true';
        }
    });
});
