define ( function ( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Fut = require( 'fut/entities' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        ImageListView = require( 'shop/Landing/ImageListView' ),
        Template = require( 'text!shop/Landing/editview.hbs' );

    var LandingPageModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend( {
        moduleName: 'shop/landing/edit',
        rawTemplate: Template,
        entityName: 'landings',
        idAttribute: '_id',

        parse: function( data ) {
            return LandingPageModel.create( data );
        },

        afterModel: function( landing, transition ) {
            /**
             * @todo Make the SideBar also an Ember thingy
             */
            App.sideBarRegion.show( new ImageListView({model: new Fut.Model( landing.getJSON() )}) );
        },

        deactivate: function() {
            App.sideBarRegion.reset();
        },

        dataUrl: function( params ) {
            var url = '/api/v3/landings';

            if ( params ) {
                url += '/' + params[this.idAttribute];
            }

            return this._super( params, url );
        },

        actions: {
            save: function( model ) {
                this.storeModel( model );
            },
            close: function() {
                this.transitionTo( 'shop.landings' );
            }
        }
    } );
} );