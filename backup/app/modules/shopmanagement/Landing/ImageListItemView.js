define ( function ( require ) {
    'use strict';

    var Fut = require( 'fut/entities' ),
        Template = require( 'hbs!shop/Landing/imagelistitemview'),
        Components = require( 'components/components' );

    var ItemView =Fut.ItemView.extend ( {
        tagName: 'li',
        className: 'list-group-item',
        template: Template,

        ui: {
            dropArea: '.imageDropArea',
            image: '#image'
        },

        initialize: function() {
            this.setViewModel( new Fut.Model( {'hovering': false} ) );
            this.listenTo( this.model, 'change', this.render );
        },

        imageSetter: function( theFile, e, fieldName, event ) {
            this.model.set( 'uploadImage', {
                ImageFilename: theFile.name,
                Image: event.target.result
            } );
        }
    } );

    return Fut.mixin( ItemView, Components.ImageUploader );
} );