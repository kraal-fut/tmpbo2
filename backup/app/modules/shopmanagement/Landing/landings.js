define(function( require ) {
    'use strict';

    var BaseCollection = require( 'collections/base' ),
        Model = require( 'shop/Landing/Landing' ),
        _ = require( 'underscore' );

    return BaseCollection.extend({
        entityName: 'landings',
        model: Model,

        url: function() {
            return this.baseUrl( '/v3/landings' );
        },

        vent: undefined,

        filterByCatId: function( foId ) {

            this.resetCriteria(true);

            var onlyTop = function() {
                return _.all( foId, function( item ){ return item === 0;} );
            };

            if (!onlyTop()) {
                this.addCriteria( {'in':foId}, 'category_id' );
            }

            this.load();
        }
    });
});
