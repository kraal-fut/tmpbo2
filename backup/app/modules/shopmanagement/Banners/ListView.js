define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/Banners/listview.hbs' );

    var BannerModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend( window.EApp.Pageable, {
        moduleName: 'shop/banners',
        rawTemplate: Template,
        entityName: 'banners',

        setupController: function( controller, model ) {
            controller.set( 'bannerTypes', ['top', 'header', 'theme', 'themeSmall'] );
            this._super( controller, model );
        },

        map: function( order ) {
            return BannerModel.create( order );
        },

        dataUrl: function( params ) {
            return this._super( params, '/api/v3/banners' );
        },

        actions: {
            save: function( banner ) {
                this.storeModel( banner, this.dataUrl(), banner.get( 'id' ) );
            },
            addBanner: function() {
                this.currentModel.unshiftObject( new BannerModel( {} ) );
            }
        }
    } );
});