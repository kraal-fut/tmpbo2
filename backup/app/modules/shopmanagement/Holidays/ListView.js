define(function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/Holidays/listview.hbs' );

    var HolidayModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend( window.EApp.Pageable, {
        moduleName: 'shop/holidays',
        rawTemplate: Template,
        entityName: 'holidays',
        idAttribute: '_id',

        map: function( order ) {
            return HolidayModel.create( order );
        },

        dataUrl: function( params ) {
            return this._super( params, '/api/v3/holidays' );
        },

        actions: {
            addHoliday: function() {
                this.currentModel.unshiftObject( new HolidayModel( {} ) );
            },
            deleteHoliday: function( holiday ) {
                this.deleteModel( holiday );
            },
            save: function( holiday ) {
                this.storeModel( holiday, this.dataUrl(), holiday.get( '_id' ) );
            }
        }
    } );
} );