define(function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/KeywordSale/listview.hbs' );

    var KeywordSaleModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend( window.EApp.Pageable, {
        moduleName: 'shop/keywordsale',
        rawTemplate: Template,
        entityName: 'keywords',
        idAttribute: 'keyword',

        map: function( data ) {
            return KeywordSaleModel.create( data );
        },

        dataUrl: function( params ) {
            return this._super( params, '/api/v3/keywordsale' );
        },

        actions: {
            addKeywordSale: function() {
                this.currentModel.unshiftObject( new KeywordSaleModel( {} ) );
            },

            deleteKeyword: function( keyword ) {
                this.deleteModel( keyword );
            },

            save: function( keywordSale ) {
                this.storeModel( keywordSale, this.dataUrl(), keywordSale.get( 'keyword' ) );
            }
        }
    } );
} );