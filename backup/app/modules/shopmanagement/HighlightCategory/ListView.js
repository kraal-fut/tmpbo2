/*global define:false */
define(function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/HighlightCategory/listview.hbs' );

    var HighlightModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend(
        window.EApp.Filterable,
        window.EApp.Pageable,
        {
            moduleName: 'shop/highlights',
            rawTemplate: Template,
            entityName: 'frontofficemenuhighlight',

            map: function( order ) {
                return HighlightModel.create( order );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/frontofficemenuhighlights' );
            },

            actions: {
                save: function( highlight ) {
                    this.storeModel( highlight, this.dataUrl(), highlight.get( 'id' ) );
                },
                addHighlight: function() {
                    this.currentModel.unshiftObject( new HighlightModel( {} ) );
                }
            }
        }
    );
} );