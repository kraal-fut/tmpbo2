/*global define:false */
define(function ( require ) {
    'use strict';

    var Marionette = require( 'marionette' ),
        Fut = require( 'fut/entities' ),
        ModuleRouter = require( 'components/moduleRouter' ),
        LandingPagesListView = require( 'shop/Landing/ListView' ),
        LandingEditView = require( 'shop/Landing/EditView' ),
        BannersListView = require( 'shop/Banners/ListView' ),
        HolidaysListView = require( 'shop/Holidays/ListView' ),
        AutomailsListView = require( 'shop/Automail/ListView'),
        ArticleListView = require( 'shop/Article/ListView'),
        FaqView = require( 'shop/Faq/ListView' ),
        KeywordSaleListView = require( 'shop/KeywordSale/ListView' ),
        SynonymsListView = require( 'shop/synonyms/ListView' ),
        HighlightListView = require( 'shop/HighlightCategory/ListView' ),
        FoMenuListView = require( 'modules/shopmanagement/fomenu/views/fomenu' ),
        FoMenuDetailView = require( 'modules/shopmanagement/fomenu/views/foMenuDetail' );

    var router = Marionette.AppRouter.extend( {
        searchRouter: undefined,
        foMenuRouter: undefined,

        routes: {
            'shopmanagement/automails': {
                method: 'ShopAutomails',
                name: 'Automails',
                routeName: 'shop.automails',
                emberRoute: AutomailsListView
            },
            'shopmanagement/search/synonyms': {
                method: 'ShopSynonyms',
                name: 'Synonyms',
                routeName: 'shop.synonyms',
                emberRoute: SynonymsListView
            },
            'shopmanagement/articles': {
                method: 'ShopArticles',
                name: 'SEO Articles',
                routeName: 'shop.articles',
                emberRoute: ArticleListView
            },
            'shopmanagement/highlighttheme': {
                method: 'ShopHighlights',
                name: 'Highlighted Categories',
                routeName: 'shop.highlights',
                emberRoute: HighlightListView
            },
            'shopmanagement/KeywordSale': {
                method: 'ShopKeywordsale',
                name: 'Keyword Sale',
                routeName: 'shop.keywordsale',
                emberRoute: KeywordSaleListView
            },
            'shopmanagement/Banners': {
                method: 'ShopBanners',
                name: 'Banners',
                routeName: 'shop.banners',
                emberRoute: BannersListView
            },
            'shopmanagement/Landing': {
                method: 'ShopLandings',
                name: 'Landing pages',
                routeName: 'shop.landings',
                emberRoute: LandingPagesListView
            },
            'shopmanagement/Landing/:_id': {
                method: 'ShopLandingEdit',
                name: 'Landing edit',
                routeName: 'shop.landing.edit',
                emberRoute: LandingEditView
            },
            'shopmanagement/LandingEdit/:id': {
                method: 'landingEdit',
                name: 'Landing pages editor',
                hideInMenu: true
            },
            'shopmanagement/holidays': {
                method: 'ShopHolidays',
                name: 'Holidays',
                routeName: 'shop.holidays',
                emberRoute: HolidaysListView
            },
            'shopmanagement/faq': {
                method: 'ShopFaq',
                name: 'Faq',
                routeName: 'shop.faq',
                emberRoute: FaqView
            },
            'shopmanagement/fomenu': {
                method: 'ShopFomenu',
                name: 'Frontoffice Menu',
                routeName: 'shop.fomenu',
                emberRoute: FoMenuListView
            },
            'shopmanagement/fomenu/:id': {
                method: 'ShopFomenuDetail',
                name: 'FO menu edit',
                hideInMenu: true,
                routeName: 'shop.fomenu.detail',
                emberRoute: FoMenuDetailView
            }
        }
    } );

    return Fut.mixin( router, ModuleRouter );
} );

