define ( function ( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/Automail/listview.hbs' );

    var AutomailModel = Em.Object.extend( window.EApp.Jsonable, {
    } );

    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            moduleName: 'shop/automails',
            rawTemplate: Template,
            entityName: 'automails',

            map: function( order ) {
                return AutomailModel.create( order );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/automails' );
            },

            actions: {
                save: function( mail ) {
                    this.storeModel( mail, this.dataUrl(), mail.get( 'id' ) );
                }
            }
        }
    );
});