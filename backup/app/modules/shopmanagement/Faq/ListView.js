define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/Faq/listview.hbs' );

    var FaqModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            categories: [],
            idAttribute: '_id',
            moduleName: 'shop/faq',
            rawTemplate: Template,
            entityName: 'faq',

            setupController: function( controller, model ) {
                this._super( controller, model );
                controller.set( 'categories', this.categories );
            },

            parse: function( resp ){
                this.categories = resp.categories;
                return this._super( resp );
            },

            map: function( order ) {
                return FaqModel.create( order );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/faq' );
            },

            actions: {
                save: function( mail ) {
                    this.storeModel( mail );
                },
                add: function() {
                    this.currentModel.unshiftObject( new FaqModel( {} ) );
                }
            }
        }
    );
} );