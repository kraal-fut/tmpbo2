define( function( require ) {
    'use strict';
    var FoMenuTip = require( 'shop/fomenu/models/fomenutip' ),
        Fut = require( 'fut/entities' );

    var VendorUrlModel = Fut.Model.extend({
        defaults:{
            name:undefined,
            url:undefined
        }
    });

    return Fut.Model.extend({
        idAttribute: 'frontoffice_menu_id',
        defaults: {
            frontoffice_menu_id: undefined,
            parent_id: 0,
            name: '',
            displayname: undefined,
            shorttext: '',
            url: '',
            goodurl: '',
            keyword: '',
            visible: 0,
            showindropdown: 0,
            enabled: 0,
            que_number: 0,
            virtual: 0,
            frontoffice_menu_group_id: 0,
            dropdown: '',
            title: '',
            description: '',
            tips: [],
            vendors: []
        },
        relations: [
            { type: Fut.Many, key: 'vendors', relatedModel: VendorUrlModel },
            { type: Fut.Many, key: 'children', relatedModel: Fut.Self },
            { type: Fut.Many, key: 'tips', relatedModel: FoMenuTip }
        ],

        addTip: function() {
            this.get( 'tips' ).add( new FoMenuTip() );
        },

        getTips: function() {
            return this.get( 'tips' );
        },

        parse: function( response ) {
            response.parent_id = parseInt(response.parent_id, 10) + '';

            return response;
        }
    });
});
