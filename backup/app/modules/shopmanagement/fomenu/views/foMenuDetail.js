define(function ( require ) {
    'use strict';
    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/fomenu/templates/fomenudetails.hbs' );

    var FoMenuModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend( {
        moduleName: 'shop/fomenu/detail',
        rawTemplate: Template,
        entityName: 'frontoffice_menu_pseudo',

        parse: function( data ) {
            return FoMenuModel.create( data );
        },

        dataUrl: function( params ) {
            var url = '/api/v3/frontofficemenus';

            if ( params ) {
                url += '/' + params[this.idAttribute];
            }

            return this._super( params, url );
        },

        actions: {
            save: function( model ) {
                this.storeModel( model );
            }
        }
    } );
});
