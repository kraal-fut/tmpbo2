define(function ( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/fomenu/templates/fomenulist.hbs' );

    var FoMenuModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend( {
        moduleName: 'shop/fomenu',
        rawTemplate: Template,
        entityName: 'frontoffice_menu_pseudo',

        map: function( order ) {
            return FoMenuModel.create( order );
        },
        dataUrl: function( params ) {
            return this._super( params, '/api/v3/frontofficemenus' );
        },

        actions: {
            save: function( model ) {
                this.storeModel( model );
            }
        }
    } );
    //
    //var Fut = require( 'fut/entities' ),
    //    ListItemView = require( 'shop/fomenu/views/FoMenuItemView' ),
    //    Model = require( 'shop/fomenu/models/fomenu' ),
    //    ListTemplate = require( 'hbs!shop/fomenu/templates/fomenulisttableheader' ),
    //    Collection = require( 'shop/fomenu/collections/fomenu' ),
    //    TreeView = require( 'shop/fomenu/views/foMenuTreeView' ),
    //    Components = require( 'components/components' );
    //
    //var FoMenu = Fut.CompositeView.extend({
    //    tagName: 'div',
    //    childView: ListItemView,
    //    childViewContainer: '#tablecontent',
    //    template: ListTemplate,
    //
    //    treeView : undefined,
    //
    //    triggers: {
    //        'click #AddButton': 'addItem'
    //    },
    //
    //    initialize: function () {
    //        this.collection = new Collection( [], {limit: 100} ).filterByParentId( 0 );
    //        this.treeView = new TreeView( {parentView: this} ).build( this.collection, true );
    //
    //        this.listenTo( this.collection, 'reset sort', this.render );
    //
    //        App.vent.on( Components.Events.UI_CLICK_FOMENU_COLLAPSER, this.onFilterClicked, this );
    //    },
    //
    //    onShow: function() {
    //        App.treeViewRegion.show( this.treeView );
    //    },
    //
    //    onDestroy: function() {
    //        App.treeViewRegion.reset();
    //    },
    //
    //    onAddItem: function(){
    //        this.collection.add( new Model( {} ) );
    //    },
    //
    //    onFilterClicked: function( options ) {
    //        this.collection.filterByParentId( options.foId );
    //    }
    //});
    //
    //Fut.Cocktail.mixin( FoMenu, Components.SortableView );
    //
    //return FoMenu;
});