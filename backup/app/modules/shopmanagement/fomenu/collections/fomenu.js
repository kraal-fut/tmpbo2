define( function( require ) {
    'use strict';
    var _ = require( 'underscore' ),
        fut = require( 'fut/entities' ),
        BaseCollection = require( 'collections/base' ),
        FrontofficeMenuPseudo = require( 'shop/fomenu/models/fomenu' );

    var collection = BaseCollection.extend( {
        entityName: 'frontoffice_menu_pseudo',

        url: function() {
            return this.baseUrl( '/v3/frontofficemenus' );
        },

        model: FrontofficeMenuPseudo,
        LocalStorageSortKey: 'fomenu_sort',
        LocalStorageItemsPerPageKey: 'fomenu_itemsPerPage',
        LocalStorageCriteriaKey: 'fomenu_criteria',

        initialize: function( options ) {
            if ( !_.isUndefined( options ) && !_.isUndefined( options.noSearch ) ) {
                this.noSearch = options.noSearch;
            }

            BaseCollection.prototype.initialize.apply( this, arguments );
        },

        filterByParentId: function( foId ) {
            this.resetCriteria( true );
            this.addCriteria( {'eq':_.first( foId )}, 'parent_id', true );
            this.addSort( {'que_number':'ASC'} );

            return this;
        },

        comparator: function( a, b ) {
            var _a = parseInt( a.get( 'que_number' ), 10 );
            var _b = parseInt( b.get( 'que_number' ), 10 );
            if ( _a < _b ) {
                return -1;
            }
            if ( _a > _b ) {
                return 1;
            }
            return 0;
        }
    } );

    return fut.mixin( collection, fut.MoveableItemsCollection );
});
