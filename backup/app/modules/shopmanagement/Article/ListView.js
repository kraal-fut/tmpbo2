define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/Article/listview.hbs' );

    var ArticleModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            moduleName: 'shop/articles',
            rawTemplate: Template,
            entityName: 'news_articles',

            map: function( order ) {
                return ArticleModel.create( order );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/newsarticles' );
            },

            actions: {
                save: function( mail ) {
                    this.storeModel( mail );
                },
                add: function() {
                    this.currentModel.unshiftObject( new ArticleModel( {} ) );
                }
            }
        }
    );
} );