define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!shop/synonyms/listview.hbs' );

    var SynonymModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend( window.EApp.Pageable, {
        moduleName: 'shop/synonyms',
        rawTemplate: Template,
        entityName: 'lucenesynonyms',

        map: function( order ) {
            return SynonymModel.create( order );
        },

        dataUrl: function( params ) {
            return this._super( params, '/api/v3/lucenesynonyms' );
        },

        actions: {
            save: function( mail ) {
                this.storeModel( mail, this.dataUrl(),  mail.get( 'id' ) );
            },
            addSynonym: function() {
                this.currentModel.unshiftObject( new SynonymModel({}) );
            }
        }
    } );
} );