define( function ( require ) {
    'use strict';

    var BaseCollection = require( 'collections/base' ),
        Model = require( 'order/models/status');

    return BaseCollection.extend( {
        model: Model,

        url: function() {
            return this.baseUrl( '/v3/status/list' );
        },

        parse: function( response ) {
            return response;
        }
    } );
} );