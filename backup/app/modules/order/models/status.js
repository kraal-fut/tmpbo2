define( function( require ) {
    'use strict';

    var Fut = require( 'fut/entities' );

    return Fut.Model.extend( {
        parse: function ( response ) {
            response.label = response.description + ' (' + response.id + ')';
            response.value = response.id;
            return response;
        }
    } );
} );