define ( function ( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        _ = require( 'underscore' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!modules/order/ItPakket/ListView.hbs' );

    var OrderModel = Em.Object.extend( window.EApp.Jsonable, {
        parseModelLine: function( lines ) {
            this.getTracknoDateAndName( lines );

            this.getOrderNoAndColli( lines );

            this.getShipcodeAndDetail( lines );

            this.getInsuranceAndAddress( lines );

            this.getWeightRembAndZip( lines );

            this.getSupplierOrCustomerShipment();
        },

        getTracknoDateAndName: function( lines ) {
            var result = lines[0].match( /(3STQOV[0-9]{7,9})\s+([0-9]{1,2}-[0-9]{2}-[0-9]{4})\s+(.*)\s+/ );
            this.setProperties( {
                trackno: result[1].replace( /\s{2,}/g, ' ' ),
                date: result[2].replace( /\s{2,}/g, ' ' ),
                name: result[3].replace( /\s{2,}/g, ' ' )
            } );
        },

        getOrderNoAndColli: function( lines ) {
            var result = lines[1].match( /[\w\s]+\s+([0-9]+?x?)\s+([0-9]+)/i );
            if ( _.isNull( result ) ) {
                result = ['', '', ''];
            }
            this.setProperties( {
                orderno: result[1].replace( /\s{2,}/g, ' ' ).replace( 'x', '' ),
                colli: result[2].replace( /\s{2,}/g, ' ' )
            } );
        },
        getShipcodeAndDetail: function( lines ) {
            var result = lines[2].split( /\s-\s/ );
            this.setProperties( {
                shipcode: result[0].replace( /\s+/m, '' ),
                shipdetail: result[1].replace( /\s{2,}/g, ' ' )
            } );
        },

        getInsuranceAndAddress: function( lines ) {
            var insuranceamount;
            var address;
            if ( lines[3].match( /EUR/ ) ) {
                var result = lines[3].match( /EUR ([0-9\.]+,[0-9]+)\s+(.+)/ );
                insuranceamount = result[1];
                address = result[2];
            } else {
                insuranceamount = '';
                address = lines[3];
            }
            this.setProperties( {
                insuranceamount: insuranceamount.replace( /\s{2,}/g, ' ' ),
                address: address.replace( /\s{2,}/g, ' ' )
            } );
        },

        getWeightRembAndZip: function( lines ) {
            var weight;
            var rembamount;
            var zipetc;
            var result;
            if ( lines[4].match( /EUR [0-9]+,[0-9]+/ ) ) {
                result = lines[4].match( /([0-9]+,[0-9]+)\s+EUR ([0-9]+,[0-9]+)\s+(.+)/ );
                weight = result[1];
                rembamount = result[2];
                zipetc = result[3];
            } else {
                result = lines[4].match( /([0-9]+,[0-9]{3,})\s+(.+)/ );
                weight = result[1];
                rembamount = '';
                if ( lines[5].match( /EUR [0-9]+,[0-9]+/ ) ) {
                    rembamount = lines[5].match( /EUR [0-9]+,[0-9]+/ )[0].replace( /\s*EUR\s*/, '' );
                }
                zipetc = result[2];
            }

            this.setProperties( {
                weight: weight.replace( /\s{2,}/g, ' ' ),
                rembamount: rembamount.replace( /\s{2,}/g, ' ' ),
                zipetc: zipetc.replace( /\s{2,}/g, ' ' )
            } );
        },

        getSupplierOrCustomerShipment: function() {
            var receiver = 'S';
            var order = false;
            if ( this.get( 'orderno' ) ) {
                receiver = 'C';
                order = true;
            }
            this.setProperties( {receiver: receiver, order: order} );
        },

        updateOrder: function( errors ) {
            var ordernumber = this.get( 'orderno' );

            if ( _.isUndefined( ordernumber ) ) {
                return false;
            }

            var url = '/api/v3/itorders/' + ordernumber + 'x/import';
            var data = {
                'barcode': this.get( 'trackno' ),
                'oldStatus': 12,
                'newStatus': 60,
                'action': 'itpakket'
            };

            return this.sendPostData( url, data, errors );
        },

        sendPostData: function( url, data, errors ) {
            var that = this;

            return $.ajax( {
                url: url,
                method: 'POST',
                data: data
            } ).fail( function( response ) {
                var message = response.responseText;

                if ( !_.isUndefined( response.responseJSON.error) && !_.isUndefined( response.responseJSON.error.message ) ) {
                    message = response.responseJSON.error.message;
                }
                errors.pushObject( that.get( 'orderno' ) + 'x: ' + message );
            } );
        }
    } );

    window.EApp.OrdersItpakketView = Em.View.extend({
        dragOver: function( event ) {
            event.preventDefault();
        },
        dragEnter: function( event ) {
            event.preventDefault();
        },
        dragLeave: function( event ) {
            event.preventDefault();
        },
        drop: function( event ) {
            event.preventDefault();
            this.handleDrop( event );
        },
        handleDrop: function( event ) {
            event.preventDefault();
            var files = event.originalEvent.dataTransfer.files;
            var that = this;
            var file = files[0];
            var reader = new FileReader();

            reader.readAsText( file );

            reader.onload = function( evt ) {
                that.parseData( evt.target.result, that.get( 'controller.imports' ) );
            };

            reader.onerror = function() {
                App.notify( 'Something went wrong with reading the file' );
            };
        },
        parseData: function( fileData, lines ) {
            var orders = fileData.match( /(3STQOV[0-9]{7,9}[\S\s]+?)FuturumShop.com bestelling/g );

            _.each( orders, function( order ) {
                try {

                    var m = OrderModel.create({});
                    m.parseModelLine( order.split( /\n/g ) );

                    lines.pushObject( m );

                } catch ( Exception ) {
                    if ( Exception.message === "Cannot read property '1' of null" ) {
                        return false;
                    }
                }
            }, this );
        }
    });

    window.EApp.OrdersItpakketController = Em.Controller.extend({
        noExportable: Em.computed.not( 'hasExportable' ),
        hasExportable: function() {
            return this.get( 'model' ).get( 'length' ) > 0;
        }.property( 'model' ),

        exportableOrders: function() {
            return this.get( 'model' ).get( 'length' );
        }.property( 'model' )
    });

    return FutEmberRoute.extend( {
        moduleName: 'orders/itpakket',
        rawTemplate: Template,
        entityName: 'orders',
        setupController: function( controller, model ) {
            controller.set( 'imports', Em.ArrayProxy.create({content: Em.A([])}) );
            controller.set( 'errors', Em.ArrayProxy.create({content: Em.A([])}) );
            this._super( controller, model );
        },
        map: function( order ) {
            return OrderModel.create( order );
        },

        dataUrl: function( params ) {
            return this._super( params, '/api/v3/itorders?find={"status":11}' );
        },

        actions: {
            importOrders: function( imports ) {
                imports.filterBy( 'order', true ).forEach( function( order ) {
                    order.updateOrder( this.get( 'controller.errors' ) ).then(function(){
                        imports.removeObject( order);
                    });
                }, this );
            },
            exportOrders: function() {
                window.location = '/api/v3/itorders/export.txt';
            },
            downloadLast: function() {
                window.location = '/api/v3/itorders/download';
            }
        }
    } );
} );