define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!modules/order/Retour/listview.hbs' );

    var statuses = {
        0: 'Incomplete',
        10: 'Complete',
        11: 'Waiting for receiving',
        12: 'Received',
        9999: 'Archived'
    };

    var retourType = {
        0: 'No return',
        1: 'Money refund',
        2: 'Change for different product'
    };

    var RetourModel = Em.Object.extend( window.EApp.Jsonable, {
        statusText: function() {
            return statuses[this.get( 'status' )];
        }.property( 'status' ),

        isReceived: Em.computed.gte( 'status', 12 )
    });

    return FutEmberRoute.extend( window.EApp.Pageable, {
        moduleName: 'orders/retours',
        rawTemplate: Template,
        entityName: 'retours',

        map: function( order ) {
            return RetourModel.create( order );
        },

        dataUrl: function( params ) {
            return this._super( params, '/api/v3/retours' );
        },

        actions: {
            toggleItems: function( retour ) {
                retour.toggleProperty( 'itemsVisible' );
            },
            setReceived: function( retour ) {
                if ( retour.get( 'isReceived' ) ) {
                    return;
                }

                retour.set( 'status', 12 );

                return Em.$.ajax( {
                    type: 'PUT',
                    url: this.dataUrl() + '/' + retour.get( '_id' ),
                    data: JSON.stringify( retour.getJSON() ),
                    contentType: 'application/json'
                } );
            }
        }
    });

} );