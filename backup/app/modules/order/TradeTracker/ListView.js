define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Moment = require( 'moment' ),
        Template = require( 'text!modules/order/TradeTracker/listview.hbs' );

    var TradeTrackerModel = Em.Object.extend( {
        regDate: function() {
            return new Moment( this.get( 'received.registrationDate' ) ).format( 'DD-MM-YYYY HH:mm' );
        }.property( 'received.registratrionDate' ),

        orderAmount: Em.computed.alias( 'received.orderAmount' ),
        total: Em.computed.alias( 'ourData.total' ),

        safe: function() {
            return this.get('orderAmount') === this.get( 'total' );
        }.property('orderAmount', 'total'),

        warning: Em.computed.not( 'safe' ),

        danger: function() {
            return this.get( 'ourData.status' ) !== 60;
        }.property( 'ourData.status' ),

        label: function() {
            if ( this.get( 'danger' ) ) {
                return 'label-danger';
            }
            if ( this.get( 'warning' ) ) {
                return 'label-warning';
            }
            if ( this.get( 'safe' ) ) {
                return 'label-success';
            }
        }.property( 'safe', 'warning', 'danger' )
    } );

    return FutEmberRoute.extend(
        window.EApp.Pageable,
        window.EApp.Filterable,
        {
            moduleName: 'order/tradetracker',
            rawTemplate: Template,
            entityName: 'orders',

            map: function( model ) {
                return TradeTrackerModel.create( model );
            },

            dataUrl: function( params ) {
                return this._super( params, '/api/v3/tradetrackers' );
            },

            getChecked: function() {
                return this.currentModel.filterBy( 'selected', true );
            },

            sendApproveChecked: function() {
                var promises = [];

                this.getChecked().each( function( accepteds ) {
                    promises.push( accepteds.sendApprove() );
                } );

                return Em.RSVP.all( promises );
            },

            /**
             *
             * @returns {*|!Promise.<!Array>}
             */
            sendModifyChecked: function() {
                var promises = [];

                this.getChecked().each( function( toModify ) {
                    promises.push( toModify.sendModify() );
                } );

                return Em.RSVP.all( promises );
            },

            sendRejectChecked: function( rejectionreason ) {
                return new Em.RSVP.Promise( function( resolve, reject ) {
                    if ( !rejectionreason ) {
                        reject( new Error( 'Should choose a rejection reason' ) );
                    }

                    var promises = [];

                    this.getChecked().each( function( rejected ) {
                        promises.push( rejected.sendReject( rejectionreason ) );
                    } );

                    resolve( Em.RSVP.all( promises ) );
                } );
            },
            actions: {
                selectAll: function() {
                    var hasSelected = this.currentModel.isAny( 'selected', true );
                    this.currentModel.setEach( 'selected', !hasSelected );
                },

                accept: function( model ) {
                    if ( model ) {
                        model.set( 'newStatus', 'accepted' );
                        return this.storeModel( model, this.dataUrl(), model.get( 'id' ) );
                    }

                    return this.sendApproveChecked();
                },
                modify: function( model ) {
                    if ( model ) {

                        model.set( 'newStatus', 'accepted' );

                        if ( model.get( 'ourData' ) ) {
                            model.set( 'newAmount', model.get( 'ourData.total' ) );
                        }

                        return this.storeModel( model, this.dataUrl(), model.get( 'id' ) );
                    }

                    return this.sendModifyChecked();
                },
                reject: function( model ) {
                    var rejectionReason = prompt( 'Reason for rejection' );

                    if ( model ) {
                        model.set( 'newStatus', 'rejected' );
                        model.set( 'rejectReason', rejectionReason );

                        return this.storeModel( model, this.dataUrl(), model.get( 'id' ) );
                    }

                    return this.sendRejectChecked( rejectionReason );
                }
            }
        }
    );
} );