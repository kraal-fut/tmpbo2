/*global EApp:true*/
define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Template = require( 'text!modules/order/Order/detailview.hbs' ),
        Em = require( 'ember-load' );

    var OrderModel = Em.Object.extend( {} );

    return FutEmberRoute.extend( {
        moduleName: 'order/detail',
        rawTemplate: Template,

        dataUrl: function( params ) {
            return '/api/v3/orders/' + params[this.idAttribute];
        },

        parse: function( data ) {
            //data.lines = Em.A( data.lines );
            return OrderModel.create( data );
        },

        //serializeModel: function( model ) {
        //    var data = model.getJSON();
        //    data.lines = data.lines.toArray();
        //    return data;
        //},

        actions: {
        }
    } );
} );