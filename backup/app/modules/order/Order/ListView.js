define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!order/Order/listview.hbs' );

    var OrderModel = Em.Object.extend( window.EApp.Jsonable, {} );

    return FutEmberRoute.extend( window.EApp.Pageable, {
        moduleName: 'order/orders',
        rawTemplate: Template,
        entityName: 'orders',

        map: function( order ) {
            return OrderModel.create( order );
        },

        dataUrl: function( params ) {
            return this._super( params, '/api/v3/orders' );
        },

        actions: {
        }
    } );
} );