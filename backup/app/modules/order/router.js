define(function( require ) {
    'use strict';

    var Marionette = require( 'marionette' ),
        Fut = require( 'fut/entities' ),
        ModuleRouter = require( 'components/moduleRouter'),
        TradeTrackerListView = require( 'modules/order/TradeTracker/ListView' ),
        OrdersListView = require( 'modules/order/Order/listview' ),
        OrdersDetailView = require( 'modules/order/Order/detailview' ),
        ProcessPaymentsListView = require( 'modules/order/ProcessPayments/ListView' ),
        ItPakketListView = require( 'modules/order/ItPakket/Overview' ),
        RetoursListView = require( 'modules/order/Retour/listview' );

    var router = Marionette.AppRouter.extend( {
        orderRouter: undefined,

        routes: {
            'orders/orders': {
                method: 'OrderOrders',
                name: 'Orders',
                routeName: 'order.orders',
                emberRoute: OrdersListView
            },
            'orders/orders/:id': {
                method: 'OrderDetail',
                routeName: 'order.detail',
                name: 'Order',
                emberRoute: OrdersDetailView,
                hideInMenu: true
            },
            'orders/retours': {
                method: 'OrdersRetours',
                name: 'Retours',
                routeName: 'orders.retours',
                emberRoute: RetoursListView
            },
            //'orders/retours/:ordernumber': 'retours',

            'orders/tradetracker': {
                method: 'OrderTradetracker',
                name: 'TradeTracker',
                routeName: 'order.tradetracker',
                emberRoute: TradeTrackerListView
            },
            'orders/processpayments': {
                method: 'OrdersProcesspayments',
                name: 'Process Payments',
                routeName: 'orders.processpayments',
                emberRoute: ProcessPaymentsListView
            },
            'orders/itpakket': {
                method: 'OrdersItpakket',
                name: 'ItPakket',
                routeName: 'orders.itpakket',
                emberRoute: ItPakketListView
            }
        }

    } );

    return Fut.mixin( router, ModuleRouter );
} );