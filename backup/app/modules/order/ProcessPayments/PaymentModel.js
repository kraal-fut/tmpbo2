define( function( require, exports ) {
    'use strict';

    var Em = require( 'ember-load' ),
        _ = require( 'underscore' );

    exports.Model = Em.Object.extend( {
        orders: undefined,
        order: undefined,
        customerid: undefined,
        date: undefined,
        amount: undefined,
        description: undefined,
        valid: 0,
        process: false,
        bankaccount: '',
        bic: '',
        name: '',
        city: 'unknown',

        checkPayment: function() {
            return this.get( 'paymenttype' ) === 'D';
        }.property( 'paymenttype' ),

        updateOrder: function() {
            var that = this;

            var pm = {
                id: this.get( 'ordernumber' ),
                oldstatus: this.get( 'status' ),
                newstatus: this.get( 'newstatus' ),
                bankaccount: this.get( 'bankaccount' ),
                bic: this.get( 'bic' ),
                City: this.get( 'city' ),
                name: this.get( 'name' )
            };

            var url = '/api/v3/orders/' + this.get( 'ordernumber' ) + '/status?action=processpayment';

            return Em.$.ajax( {
                type: 'PUT',
                url: url,
                data: pm
            } ).then( function() {
                return that.createIncident();
            }, _.bind( that.handleError, that ) );
        },
        createIncident: function() {
            var that = this;
            var m = {
                Ordernumber: this.get( 'ordernumber' ),
                RegId: this.get( 'customerid' ),
                Text: this.get( 'text' )
            };

            var url = '/api/v3/orders/' + this.get( 'ordernumber' ) + '/incidents';

            return Em.$.ajax( {
                type: 'POST',
                url: url,
                data: m
            } ).then( function() {
                that.set( 'failed', false );
                return that;
            }, _.bind( that.handleError, that ) );
        },

        handleError: function( error ) {
            var message = error.responseText;

            if ( !_.isUndefined( error.responseJSON.error.message ) ) {
                message = error.responseJSON.error.message;
            }

            this.set( 'error', message );
            this.set( 'valid', false );
            return this;
        }
    } );

    return exports.Model;
} );