define ( function ( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        _ = require( 'underscore' ),
        PaymentModel = require( 'modules/order/ProcessPayments/PaymentModel' ),
        LineParser = require( 'modules/order/ProcessPayments/LineParser' ),
        Template = require( 'text!modules/order/ProcessPayments/ListView.hbs' );



    window.EApp.OrdersProcesspaymentsView = Em.View.extend({
        dragOver: function( event ) {
            event.preventDefault();
        },
        dragEnter: function( event ) {
            event.preventDefault();
        },
        dragLeave: function( event ) {
            event.preventDefault();
        },
        drop: function( event ) {
            event.preventDefault();
            this.handleDrop( event );
        },
        handleDrop: function( event ) {
            event.preventDefault();
            var files = event.originalEvent.dataTransfer.files;
            var that = this;
            var file = files[0];
            var reader = new FileReader();

            reader.readAsText( file );

            reader.onload = function( evt ) {
                that.parseData( evt.target.result, that.get( 'controller.model' ) );
            };

            reader.onerror = function() {
                App.notify('Something went wrong with reading the file');
            };
        },

        parseData: function( data, model ) {
            _.each( data.split( /\r\n|\r|\n/g ), function( line ) {
                this.addLine( line, model );
            }, this );

            //this.remove( this.where( { paymenttype: 'D' } ) );
        },

        addLine: function( line, lines ) {
            if ( line === '' || line.match( 'Boekdatum' ) ) {
                return false;
            }

            var m = '';
            var error = false;

            try {

                m = new PaymentModel( {line: line} );
                var lineParser = new LineParser( line, m );
                lineParser.parseLine();

            } catch ( e ) {
                m.setProperties( {description: line, valid: 0, process: false} );
                m.set( 'error', e.message );
                error = true;
            }

            lines.pushObject( m );

            //if ( error ) {
            //    m.trigger( 'processFailed' );
            //}
        }

    });

    var Orders = Em.ArrayProxy.extend({
        greenOrders: function() {
            return this.get( 'content' ).filter(function( order ){
                return order.get( 'paymenttype' ) === 'C' && order.get( 'valid' ) === 1;
            }).get( 'length' );
        }.property( 'content.@each.paymenttype', 'content.@each.valid' ),

        redOrders: function() {
            return this.get( 'content' ).filter( function( order ) {
                return order.get( 'paymenttype' ) === 'C' && order.get( 'valid' ) === 0;
            }).get( 'length' );
        }.property( 'content.@each.paymenttype', 'content.@each.valid' ),

        blackOrders: function() {
            return this.get( 'content' ).filterBy( 'paymenttype', 'D' ).get( 'length' );
        }.property( 'content.@each.paymenttype' ),

        ordersToProcess: function() {
            return this.get( 'content' ).filterBy( 'process', true );
        }.property( 'content.@each.process' ),

        hasOrdersToProcess: function() {
            return this.get( 'content' ).isAny( 'process', true );
        }.property( 'content.@each.process' )
    });

    return FutEmberRoute.extend( {
        moduleName: 'orders/processpayments',
        rawTemplate: Template,
        entityName: 'payments',

        model: function() {
            return Orders.create( {content: Em.A([])} );
        },

        processPayments: function( orders ) {
            var promises = [];
            var accepteds = orders.get( 'ordersToProcess' );

            _.each( accepteds, function( accepted ) {
                promises.push( accepted.updateOrder() );
            } );

            return Em.RSVP.allSettled( promises );
        },

        ordersUpdated: function( orders ) {
            orders.removeObjects( orders.filterBy( 'failed', false ) );

            if ( orders.get( 'hasOrdersToProcess' ) ) {
                this.ordersFailed();
            } else {
                App.notify( 'All selected orders are processed' );
            }
        },

        ordersFailed: function() {
            App.notify( 'Not all selected orders are processed' );
        },

        actions: {
            clear: function() {
                this.currentModel.clear();
            },
            processOrders: function( orders ) {
                var that = this;

                if ( !orders.get( 'hasOrdersToProcess' ) ) {
                    App.notify( 'No orders are selected to process' );
                    return false;
                }

                this.processPayments( orders ).then(function(){
                    that.ordersUpdated( orders );
                }, function() {
                    that.ordersFailed();
                });
            }
        }
    } );
});