define( function( require, exports ) {
    'use strict';

    var _ = require( 'underscore' );

    exports.LineParser = function( line, model ) {
        this.line = line;
        this.model = model;
    };

    exports.LineParser.prototype = {
        filters: {
            'bank': /([0-9]{2}\.[0-9]{2}\.[0-9]{2}\.[0-9]{3})/,
            'giro': /GIRO\s+([0-9]+)/,
            'order': /(?:\s|\D|\A)(3[0-9]{6})x?/,
            'orderPayed': /(?:CREF\/)(?:000)(3[0-9]{6})/,
            'orderDE': /(7[0-9]{6})/g,
            'customerid': /KLANT.+?([0-9]{5,7})/i,
            'zipcode': /\s+?([0-9]{4}\s?[A-Z]{2})\s+?/i,
            'rembours': /GEIND\*E([^\/]+).+?\*REMB\*/i,
            'barcode': /(3S[a-z0-9]{11})(?: )/i,
            'ideal': /(IDEAL BETALING)/i,
            'paypal': /PAYPAL/,
            'creditcard': /ATOS WORLDLINE/,
            'pin': /AFREK. BETAALAUTOMAAT/,
            'inter': /intercompany verrekening/,
            'american': /AMERICAN EXPRESS/
        },
        parseLine: function() {
            var data = this.line.split( '\t' );
            if ( !_.isArray( data ) || data.length <= 1 ) {
                data = this.line.replace( '"', '' ).split( '","' );
            }
            if ( !_.isArray( data ) || data.length <= 1 ) {
                data = this.line.split( ';' );
            }

            if ( data[6].match( /NACHNAHME/ ) || ( data[1].match( /DE/ ) && data[6].match( /REMBOURS/ ) ) ) {
                this.parseNachname( data );
            } else if ( data[6].match( /REMBOURS/ ) ) {
                this.parseRemb( data );
            } else if ( data.length > 4 && data.length < 9 && data[1].match( /^BE/ ) ) {
                this.parseIngBE( data );
            } else if ( data.length > 4 && data.length < 9 && data[1].match( /^DE/ ) ) {
                this.parseIngDE( data );
            } else if ( data.length >= 9 ) {
                this.parseRabo( data );
            } else {
                return false;
            }

            this.getAdditionalInfo();
        },
        parseRabo: function( data ) {
            var description = data[10] + data[11];
            var amount = data[4].replace( ',', '.' );
            var match;
            var date = data[2].match( /([0-9]{4})([0-9]{2})([0-9]{2})/ );
            date = date[3] + '-' + date[2] + '-' + date[1];
            var account = data[5];
            var text = 'Klant heeft betaald EUR ' + amount + ' (RABO) Kenmerk: ' + description;
            text += ' Klant: ' + data[6] + ' Rekeningnummer: ' + account;

            var attr = {
                date: date,
                amount: amount,
                description: text,
                paymenttype: data[3],
                bankaccount: account,
                name: data[6],
                text: text
            };

            description = ' ' + description + ' ' + data[6];

            _.each( this.filters, function( filter, name ) {
                match = description.match( filter );
                if ( !_.isNull( match ) && match.length > 1 ) {
                    attr[name] = match[1];
                } else if ( !_.isNull( match ) ) {
                    attr[name] = match[0];
                }
                description = description.replace( filter, '' );
            }, this );

            if ( !_.isEmpty( attr.paypal ) || !_.isEmpty( attr.inter ) || !_.isNull( data[6].match( /Futurumshop/i ) ) ) {
                attr.paymenttype = 'D';
            }
            if ( !_.isEmpty( attr.barcode ) && !_.isNull( data[6].match( /POSTNL PAKKETSERVICE/i ) ) ) {
                this.model.getByBarcode( attr.barcode );
            }

            if ( !_.isEmpty( attr.pin ) || !_.isEmpty( attr.creditcard ) || !_.isEmpty( attr.american ) ) {
                var subject = attr.pin ? 'pin' : attr.creditcard || attr.american;
                subject = subject.charAt( 0 ).toUpperCase() + subject.substring( 1 ).toLowerCase();
                attr.paymenttype = 'D';
                attr.description = 'Bedrag: ' + amount + ' Omschrijving: ' + data[10] + ' ' + data[11] + ' ' + data[12];
                this.sendEmail( subject + ' overzicht NL: ' + attr.date, attr.description );
            }

            this.model.setProperties( attr );
        },

        parseIngBE: function( data ) {
            var date = data[0];
            var description = data[6];
            var amount = data[2].replace( ',', '.' );
            data = data[6].split( '/' );
            var match;
            var attr = {
                paymenttype: 'C',
                date: date,
                amount: amount,
                description: description
            };

            _.each( this.filters, function( filter, name ) {
                match = description.match( filter );
                if ( !_.isNull( match ) && match.length > 1 ) {
                    attr[name] = match[1];
                } else if ( !_.isNull( match ) ) {
                    attr[name] = match[0];
                }
                description = description.replace( filter, '' );
            }, this );

            var temp = ' ' + data[18] + data[19];
            match = temp.match( this.filters.order );

            if ( !_.isNull( match ) && match.length > 1 ) {
                attr.order = match[1];
            } else {
                attr.order = '';
            }

            if ( amount < 0 || !_.isEmpty( attr.orderPayed ) ) {
                attr.paymenttype = 'D';
            }

            if ( _.isEmpty( attr.order ) && !_.isEmpty( attr.orderPayed ) ) {
                attr.order = attr.orderPayed;
                delete attr.orderPayed;
            }

            var text = 'Klant heeft betaald EUR ' + amount + ' (ING): ';

            match = data[14].match( /[0-9]+([a-z]+)?\s+([a-z\W]+)/i );

            if ( !_.isNull( match ) && match.length > 1 ) {
                attr.city = match[2];
            } else if ( !_.isNull( match ) ) {
                attr.city = match[0];
            }

            if ( attr.paymenttype === 'C' ) {
                text += 'Kenmerk: ' + temp.replace( / +(?= )/g, '' ) + ' Klant: ' + data[13];
                text += ' Rekening: ' + data[11] + ' BIC: ' + data[12];
                attr.name = data[13];
                attr.bic = data[12];
                attr.bankaccount = data[11];
            } else {
                text += data.join( '/' );
            }

            attr.text = text;

            this.model.setProperties( attr );
        },

        parseIngDE: function( data ) {
            var date = data[0];
            var description = data[6];
            var amount = data[2].replace( ',', '.' );
            var text = 'Klant heeft betaald EUR ' + amount + ' (ING): ' + description;
            var payment = description;
            var match;
            var attr = {
                paymenttype: 'C',
                date: date,
                amount: amount,
                description: description,
                text: text
            };

            data = description.split( /\/(.+)?/ );

            if ( !_.isEmpty( data[1] ) ) {
                payment = data[0];
                description = data[1];
            }

            match = payment.match( /(DE[0-9]+)/ );
            if ( !_.isEmpty( match ) ) {
                attr.bankaccount = match[0];
            }

            _.each( this.filters, function( filter, name ) {
                match = description.match( filter );
                if ( !_.isNull( match ) && match.length > 1 ) {
                    attr[name] = match[1];
                } else if ( !_.isNull( match ) ) {
                    attr[name] = match[0];
                }
                description = description.replace( filter, '' );
            }, this );

            if ( amount < 0 || !_.isEmpty( attr.orderPayed ) || !_.isEmpty( attr.paypal ) ) {
                attr.paymenttype = 'D';
            }

            if ( _.isEmpty( attr.order ) && !_.isEmpty( attr.orderPayed ) ) {
                attr.order = attr.orderPayed;
                delete attr.orderPayed;
            }

            if ( !_.isEmpty( attr.orderDE ) ) {
                attr.order = attr.orderDE;
                delete attr.orderDE;
            }

            if ( !_.isEmpty( attr.creditcard ) ) {
                attr.paymenttype = 'D';
                attr.description = 'Bedrag: ' + amount + ' Omschrijving: ' + data[1];
                this.sendEmail( 'Atos worldline overzicht DE: ' + attr.date, attr.description );
            }

            this.model.setProperties( attr );
        },

        parseRemb: function( data ) {
            var description = data[10] + data[11];
            var date = data[2].match( /([0-9]{4})([0-9]{2})([0-9]{2})/ ); //data[7]
            date = date[3] + '-' + date[2] + '-' + date[1];
            var amount = description.match( /(?:Geind E)([0-9]+,?.?[0-9]+)/ );
            amount = amount[1].replace( ',', '.' );
            var order = description.match( /(?:KENMERK:)(3[0-9]{6})x?/ )[1];

            var attr = {
                date: date,
                paymenttype: 'C',
                text: this.line,
                description: this.line,
                amount: amount,
                order: order
            };

            this.model.setProperties( attr );
        },

        parseNachname: function( data ) {
            var order;
            var orderDE;
            var orderNL;
            var date = data[0];
            var amount = data[2].replace( ',', '.' );
            amount = parseFloat( amount ) + 2;
            data = data[6].split( '/' );

            if ( data.length > 7 ) {
                orderDE = data[3].match( /(?:NACHNAHMECASH)(7[0-9]{6})x?/i );
                orderNL = data[3].match( /(?:NACHNAHMECASHREMBOURS )(3[0-9]{6})x?/i );
            } else {
                orderDE = data[1].match( /(?:AGREMBOURS )(7[0-9]{6})x?/i );
                orderNL = data[1].match( /(?:AGREMBOURS )(3[0-9]{6})x?/i );
            }

            if ( orderDE !== null && !_.isEmpty( orderDE[1] ) ) {
                order = orderDE[1];
            } else if ( orderNL !== null && !_.isEmpty( orderNL[1] ) ) {
                order = orderNL[1];
                this.newordernumber = true;
            }

            var attr = {
                date: date,
                paymenttype: 'C',
                text: this.line,
                description: this.line,
                amount: amount,
                order: order
            };

            this.model.setProperties( attr );
        },

        getAdditionalInfo: function() {
            var ordernumber = this.model.get( 'order' );
            var that = this;

            if ( this.model.get( 'paymenttype' ) === 'D' ) {
                this.model.set( 'valid', 2 );
            }

            if ( _.isUndefined( ordernumber ) || this.model.get( 'paymenttype' ) === 'D' || this.model.get( 'barcode' ) ) {
                return false;
            }

            var url = '/api/v3/paymentorders/' + ordernumber + 'x';

            if ( this.newordernumber ) {
                url += '?newordernumber=true';
            }

            $.getJSON( url ).done( function( data ) {
                that.setOrderData( data );
            } );
        },

        setOrderData: function( data ) {
            var customerId = data.RegId;
            var amount = parseFloat( this.model.get( 'amount' ) );
            var orderAmount = parseFloat( data.TotalToPay );
            var status = data.Status;
            var newstatus = data.Status;
            var valid = 0;

            if ( amount >= ( orderAmount - 0.05 ) && amount <= ( orderAmount + 0.05 ) ) {
                valid = 1;
            }

            if ( !data.AllowedStatus ) {
                valid = 0;
            } else {
                newstatus = data.AllowedStatus.processpayment;
            }

            this.model.setProperties( {
                customerid: customerId,
                status: status,
                newstatus: newstatus,
                valid: valid,
                process: valid === 1,
                ordernumber: data.Ordernumber,
                lastUpdated: data.Updated,
                order: data.Ordernumber.replace( 'x', '' )
            } );

            //this.collection.sort();
        },
        sendEmail: function( subject, body ) {
            var url = '/api/v3/sendmail';
            var data = {
                emailto: 'administration',
                subject: subject,
                body: body,
                emailfrom: 'administration'
            };

            $.ajax( {
                url: url,
                method: 'POST',
                data: data
            } ).done( function() {
                App.notify( 'Email is send' );
            } );
        }
    };

    return exports.LineParser;
} );