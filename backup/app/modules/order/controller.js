define( function( require ) {
    'use strict';
    var Controller = require( 'modules/controller' ),
        ItPakket = require( 'order/ItPakket/views/OverView' );

    return Controller.extend( {
        views: {
            itpakket: ItPakket
        }
    });
});