define( function( require ) {
    'use strict';
    var Em = require( 'ember-load' );

    return Em.Object.extend( {
        canBeClosed: Em.computed.equal( 'status', 'received' ),
        isClosed: Em.computed.equal( 'status', 'closed' ),
        isNotClosed: Em.computed.not( 'isClosed' ),
        isSent: Em.computed.equal( 'status', 'sent' ),
        isReceived: Em.computed.equal( 'status', 'received' ),
        isSendOrReceived: Em.computed.or( 'isSent', 'isReceived' ),
        isNotSent: Em.computed.not( 'isSent' ),
        hasEmptyMailBody: Em.computed.empty( 'mail_body' ),

        mailBody: function() {
            if ( this.get( 'hasEmptyMailBody' ) ) {
                return App.settingsmodel.getSetting( 'emailSignature', '' );
            }
            return this.get( 'mail_body' );
        }.property( 'mail_body' ),

        totalValue: function() {
            return this.get( 'lines' ).reduce( function( memo, line ) {
                return memo + ( line.pprice * line.qty );
            }, 0 ).toFixed( 2 );
        }.property( 'lines.@each.pprice', 'lines.@each.qty' )
    } );
} );