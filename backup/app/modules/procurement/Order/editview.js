/*global EApp:true*/
define( function( require ) {
    'use strict';

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Template = require( 'text!modules/procurement/Order/editview.hbs' ),
        Em = require( 'ember-load' );

    var EditModel = require( 'modules/procurement/Order/EOrder' );

    return FutEmberRoute.extend( {
        moduleName: 'procurement/order',
        rawTemplate: Template,

        dataUrl: function( params ) {
            return '/api/v3/procurement/orders/' + params[this.idAttribute];
        },

        parse: function( data ) {
            data.lines = Em.A( data.lines );
            return EditModel.createWithMixins( EApp.Jsonable, data );
        },

        serializeModel: function( model ) {
            var data = model.getJSON();
            data.lines = data.lines.toArray();
            return data;
        },

        actions:{
            selectAll: function() {
                var hasSelected = this.currentModel.lines.isAny( 'selected', true );
                this.currentModel.lines.setEach( 'selected', !hasSelected );
            },

            removeLine: function( line ) {
                this.currentModel.lines.removeObject( line );
            },

            saveOrder: function() {
                this.doRequest();
            },

            saveAndPreview: function() {
                this.doRequest( 'Order saved and sent to preview' );
            },

            saveAndFinal: function() {
                this.doRequest( 'Order saved and sent to supplier' );
            },

            deleteSelected: function() {
                var selected = this.currentModel.lines.filterBy( 'selected' );

                if (!confirm( 'Delete all ' + selected.length + ' selected lines?' ) ) {
                    return;
                }

                this.currentModel.lines.removeObjects( selected );
            },

            addToOrder: function() {
                this.currentModel.lines.unshiftObject({});
            }
        },

        doRequest: function( notifyText ) {
            this.store().then(function() {
                App.notify( notifyText || 'Order saved' );
            });
        }
    } );
} );