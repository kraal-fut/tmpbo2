/*global EApp:true*/
define( function( require ) {
    'use strict';

    var Fut = require( 'fut/entities' ),
        Collection = require( 'modules/procurement/Order/Orders' ),
        FiltersTemplate = require( 'hbs!modules/procurement/Order/filters' );

    var FiltersView = Fut.ItemView.extend( {
        className: 'form-inline',
        template: FiltersTemplate,

        ui: {
            statusFilter: '#statusFilter'
        },

        triggers: {
            'change @ui.statusFilter': 'statusFilter'
        },

        onStatusFilter: function( options ) {
            var value = this.ui.statusFilter.val().toLowerCase();
            options.collection.statusFilter = value === 'allopen' ? undefined : value;
            options.collection.load();
        }
    } );

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        OrderModel = require( 'modules/procurement/Order/EOrder' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!modules/procurement/Order/listview.hbs' );

    return FutEmberRoute.extend( window.EApp.Pageable, {
        moduleName: 'procurement/orders',
        rawTemplate: Template,
        Collection: Collection,
        FilterView: FiltersView,
        collection: undefined,
        entityName: 'ProcurementOrder',

        map: function( order ) {
            return OrderModel.create( order );
        },

        dataUrl: function( params ) {
            return this._super( params, '/api/v3/procurement/orders' );
        },

        actions: {
            selectAll: function() {
                var hasSelected = this.currentModel.isAny( 'selected', true );
                this.currentModel.setEach( 'selected', !hasSelected );
            },

            closeOrder: function( order ) {
                order.set( 'status', 'closed' );
            }
        }
    });
} );