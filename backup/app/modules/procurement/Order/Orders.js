define( function( require ) {
    'use strict';

    var Fut = require( 'fut/entities' ),
        _ = require( 'underscore' ),
        Components = require( 'components/components' ),
        Model = require( 'modules/procurement/Order/Order' );

    return Fut.mixin( Fut.Collection.extend( {
        entityName: 'ProcurementOrder',
        model: Model,
        statusFilter: undefined,

        url: function() {
            var orderUrl = this.baseUrl( '/v3/procurement/orders' );

            if ( !_.isUndefined( this.statusFilter ) ) {
                orderUrl += '?status=' + this.statusFilter;
            }

            return orderUrl;
        },

        search: function( query ) {
            var searchCollection = this.clone();

            searchCollection.reset();

            return searchCollection.addCriteria( query ).then(function( collection ) {
                if ( collection.length ) {
                    return collection;
                }

                throw new Error( 'No results found' );
            } );
        }

    } ), Components.SelectAbleCollection );

} );