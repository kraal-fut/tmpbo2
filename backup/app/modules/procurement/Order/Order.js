define( function( require ) {
    'use strict';
    var Fut = require( 'fut/entities' ),
        SelectableModel = require( 'components/selectableModel' );

    var OrderLineModel = Fut.mixin( Fut.Model.extend({
        relations: [
            {type: Fut.One, key: 'product', relatedModel: Fut.Model.extend({})}
        ]
    }), SelectableModel);

    return Fut.mixin( Fut.Model.extend( {
        entityName: 'ProcurementOrder',
        exportable: ['isSent','isNotSent', 'canBeClosed', 'isNotClosed'],

        defaults: {
            status: 'draft',
            created_by: undefined,
            supplier: {},
            supplier_id: undefined,
            ProcurementOrderLine: [],
            user: {}
        },

        relations: [
            {type: Fut.Many, key: 'lines', relatedModel: OrderLineModel}
        ],

        close: function() {
            if ( this.get( 'status' ) !== 'received' ) {
                var override = confirm( 'This order is not yet received, really close?' );
                if ( !override ) {
                    return;
                }
            }

            this.set( 'status', 'closed' );
            return this.save();
        },

        canBeClosed: function() {
            return this.get( 'status' ) === 'received';
        },

        isNotClosed: function() {
            return this.get( 'status' ) !== 'closed';
        },

        isSent: function() {
            return this.get( 'status' ) === 'sent' || this.get( 'status' ) === 'received';
        },

        isNotSent: function() {
            return !this.isSent();
        },

        sendPreview: function() {
            return this.save( undefined, {url: this.url() + '?preview=true'} );
        },

        storeAndSend: function() {
            if ( this.get( 'status' ) === 'draft' ) {
                this.set( 'status', 'new' );
            }

            return this.save( undefined, {url: this.url() + '?send=true'} );
        }

    } ), SelectableModel);
} );
