/*global define:false */
define(function ( require ) {
    'use strict';

    var Marionette = require( 'marionette' ),
        Fut = require( 'fut/entities' ),
        Controller = require( 'modules/procurement/controller' ),
        ModuleRouter = require( 'components/moduleRouter' ),
        OrderListView = require( 'modules/procurement/Order/listview' ),
        AdvicesListView = require( 'modules/procurement/Advice/listview' ),
        AdvicesEditView = require( 'modules/procurement/Advice/editview' ),
        OrderEditView = require( 'modules/procurement/Order/editview' );

    var router = Marionette.AppRouter.extend( {

        initialize: function( options ) {
            this.controller = new Controller( { App: options.App});
        },

        routes: {
            'procurement/orders': {
                method: 'ProcurementOrders',
                name: 'Orders',
                routeName: 'procurement.orders',
                emberRoute: OrderListView
            },

            'procurement/orders/:id': {
                method: 'ProcurementOrder',
                routeName: 'procurement.order',
                name: 'Orders',
                emberRoute: OrderEditView,
                hideInMenu: true
            },

            'procurement/advices': {
                method: 'ProcurementAdvices',
                name: 'Advices',
                routeName: 'procurement.advices',
                emberRoute: AdvicesListView
            },

            'procurement/advices/:id': {
                method: 'ProcurementAdvice',
                name: 'Advice for supplier',
                routeName: 'procurement.advice',
                emberRoute: AdvicesEditView,
                hideInMenu: true
            }
        }
    } );

    return Fut.mixin( router, ModuleRouter );
} );

