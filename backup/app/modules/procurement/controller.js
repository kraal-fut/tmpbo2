define ( function ( require ) {
    'use strict';

    var Controller = require( 'modules/controller' ),
        ProcurementOrdersView = require( 'modules/procurement/Order/listview'),
        OrderCollection = require( 'modules/procurement/Order/Orders' ),
        Em = require( 'ember-load' );

    return Controller.extend( {
        collections: {
            'procurementOrderEdit': OrderCollection
        },

        views: {
            procurementOrders: ProcurementOrdersView
        }
    } );
} );
