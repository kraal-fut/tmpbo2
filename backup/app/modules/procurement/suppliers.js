define( function( require ) {
    'use strict';

    var BaseCollection = require( 'collections/base' ),
        Model = require( 'modules/procurement/models/supplier' );

    return BaseCollection.extend( {
        entityName: 'supplier',
        model: Model,

        url: function() {
            return this.baseUrl( '/v3/suppliers' );
        }
    } );

} );