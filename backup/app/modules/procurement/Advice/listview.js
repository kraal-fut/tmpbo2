define( function( require ) {
    'use strict';

    var Fut = require( 'fut/entities' ),
        FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!modules/procurement/Advice/listview.hbs' );

    var CommandDTO = Fut.Collection.extend( {
        url: '/api/v3/procurement/orders',
        send: function() {
            return this.at( 0 ).save();
        }
    } );

    var AdviceModel = Em.Object.extend( {} );

    return FutEmberRoute.extend( {
        moduleName: 'procurement/advices',
        rawTemplate: Template,
        entityName: 'advice',

        map: function( order ) {
            return AdviceModel.create( order );
        },

        dataUrl: function( params ) {
            return this._super( params, '/api/v3/procurement/advices?limit=500' );
        },

        getSelected: function() {
            return this.currentModel.filterBy( 'selected', true );
        },

        actions: {
            selectAll: function() {
                var hasSelected = this.currentModel.isAny( 'selected', true );
                this.currentModel.setEach( 'selected', !hasSelected );
            },
            createDrafts: function() {
                var advices = this;

                var selecteds = this.getSelected().getEach( 'id' );

                var DTO = new CommandDTO();
                DTO.push( {fromfullsupplieradvices: selecteds} );
                DTO.send().then( function() {
                    advices.refresh();
                } );
            }
        }
    } );
} );