define( function( require ) {
    'use strict';

    var Fut = require( 'fut/entities' ),
        _ = require( 'underscore' ),
        Components = require( 'components/components' ),
        Model = require( 'modules/procurement/Advice/Advice' );

    var CommandDTO = Fut.Collection.extend( {
        url: '/api/v3/procurement/orders',
        send: function() {
            return this.at(0).save();
        }
    } );

    return Fut.mixin( Fut.Collection.extend( {
        entityName: 'advice',
        model: Model,

        url: function() {
            return this.baseUrl( '/v3/procurement/advices' );
        },

        createDrafts: function() {
            var advices = this;

            var selecteds = _.reduce( this.getSelected(), function( memo, selected ) {
                memo.push( selected.get( 'id' ) );
                return memo;
            }, [] );

            var DTO = new CommandDTO();
            DTO.push( {fromfullsupplieradvices: selecteds} );
            DTO.send().then(function(){
                advices.load();
            });
        }

    } ), Components.SelectAbleCollection );

} );