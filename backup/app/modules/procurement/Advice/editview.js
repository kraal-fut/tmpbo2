define( function( require ) {
    'use strict';

    var Collection = require( 'modules/procurement/Advice/AdviceLines' ),
        Components = require( 'components/components' );

    var FutEmberRoute = require( 'fut/FutEmberRoute' ),
        Em = require( 'ember-load' ),
        EmberMixins = require( 'fut/EmberMixins' ),
        Template = require( 'text!modules/procurement/Advice/editview.hbs' );

    var AdviceModel = Em.Object.extend( {
    });

    return FutEmberRoute.extend( {
        moduleName: 'procurement/advice',
        rawTemplate: Template,
        Collection: Collection,
        collection: undefined,
        entityName: 'advice',

        map: function( order ) {
            return AdviceModel.create( order );
        },

        dataUrl: function( params ) {
            return this._super( params, '/api/v3/procurement/advices/' + params[this.idAttribute] );
        },

        actions: {
            selectAll: function() {
                var hasSelected = this.currentModel.isAny( 'selected', true );
                this.currentModel.setEach( 'selected', !hasSelected );
            }
        }
    } );
} );