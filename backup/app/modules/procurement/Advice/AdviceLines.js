define( function( require ) {
    'use strict';

    var Fut = require( 'fut/entities' ),
        Components = require( 'components/components' ),
        Model = require( 'modules/procurement/Advice/Advice' );

    return Fut.mixin( Fut.Collection.extend( {
        entityName: 'advice',
        model: Model,

        url: function() {
            return this.baseUrl( '/v3/suppliers/' + this.options.supplier + '/advices' );
        }

    } ), Components.SelectAbleCollection );

} );