define( function( require ) {
    'use strict';
    var Fut = require( 'fut/entities' ),
        SelectableModel = require( 'components/selectableModel' );

    return Fut.mixin( Fut.Model.extend( {
        defaults: {}
    } ), SelectableModel);
} );
