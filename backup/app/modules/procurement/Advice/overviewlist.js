define( function( require ) {
    'use strict';

    var Fut = require( 'fut/entities' ),
        ListItemView = require( 'modules/procurement/Advice/overviewlistitem' ),
        ListTemplate = require( 'hbs!modules/procurement/Advice/overviewlist' ),
        Collection = require( 'modules/procurement/Advice/Advices' ),
        Components = require( 'components/components' );

    var View = Fut.CompositeView.extend( {
        childView: ListItemView,
        childViewContainer: 'tbody',
        template: ListTemplate,
        Collection: Collection,

        filters: [],
        initialize: function() {
            console.log('overviewlist');
        },
        ui: {
            createDraftsBtn: '#createorder'
        },

        triggers: {
            'click @ui.createDraftsBtn':'createDrafts'
        },

        onCreateDrafts: function() {
            this.collection.createDrafts();
        }
    });

    return Fut.mixin(
        View,
        Components.DefaultListView
    );
} );