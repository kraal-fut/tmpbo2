define(function ( require ) {
    'use strict';

    var _ = require( 'underscore' ),
        fut = require( 'fut/entities' ),
        Marionette = require( 'backbone.marionette' );

    return Marionette.Controller.extend( {
        showModule: function( module, parameters, loadParams ) {
            var controller = this;
            var View = this.views[module];

            if ( !_.isUndefined( View ) ) {
                this.loadModel( module, loadParams, this.collections ).then( function( m ) {
                    var params = _.extend( {model: m, controller: controller}, parameters );
                    App.mainRegion.show( new View( params ) );
                } );
            //.catch( function( error ) {
            //        console.log( error );
            //    } );
            }


        },

        defaultAction: function() {
            console.log( 'default action' );
        },

        loadModel: function( module, loadParams, collections ) {
            return new fut.Promise( function( resolve, reject ) {

                var canNotBePreLoaded = function() {
                    return _.isUndefined( loadParams ) ||
                        _.isUndefined( collections ) ||
                        _.isUndefined( collections[module] );
                };

                if ( canNotBePreLoaded() ) {
                    resolve();
                }

                var c = new collections[module]();
                var data = {};
                data[loadParams.preLoadById] = loadParams.id;

                var m = c.push( new c.model( data ) );

                if ( _.isFunction( m.load ) ) {
                    m.load().then(function( model ){
                        resolve( model );
                    });
                } else {
                    m.fetch({success:function( model ){
                        resolve( model );
                    }, error: function( model, response ){
                        reject( new Error( 'Could not load model: ' + response) );
                    }});
                }
            });
        }
    } );
} );
