/*global define:false*/

define( 'templates/helpers/view', ['hbs/handlebars','underscore'], function ( Handlebars, _ ) {
    'use strict';

    //var view;

    /**
     *
     * @param name
     * @param context
     * @returns {(string|base.HandlebarsEnvironment.SafeString)}
     * @constructor
     */
    var ViewHelper = function( name, context ) {
        //try{
            var View = require( name );
            var view;
            //if ( _.isUndefined( view) ) {
                view = new View( context.hash );
                view.render();
            //}
            return new Handlebars.SafeString( view.$el.html() );
        //} catch( $e ) {
        //    console.error( $e.message );
        //    return '';
        //}
    };

    Handlebars.registerHelper( 'view', ViewHelper );

    return ViewHelper;
} );
