define( function ( require ) {
    'use strict';
    var select = require( 'components/helpers/selecthelper' ),
        Handlebars = require( 'hbs/handlebars' );

    Handlebars.registerHelper( 'select', select.helper );

    return select.helper;
});

