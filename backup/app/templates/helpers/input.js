define( function( require ) {
    'use strict';

    var Handlebars = require( 'hbs/handlebars' ),
        _ = require( 'underscore' ),
        objToParams = require( 'components/helperObjectToParam' );

    var helper = function ( context ) {
        return new Handlebars.SafeString( '<input ' +  objToParams( context.hash, this.editing ) + '/>' );
    };

    Handlebars.registerHelper( 'input', helper );

    return helper;
} );
