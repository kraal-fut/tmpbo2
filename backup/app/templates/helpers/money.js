/*global define:false*/

define( 'templates/helpers/money', ['hbs/handlebars','ember-load','underscore'], function ( Handlebars, Em, _ ) {
    'use strict';

    /**
     *
     * @param value
     * @param c
     * @param d
     * @param t
     * @returns {string}
     */
    var moneyFormat = function( value, c, d, t ) {
        c = isNaN( c = Math.abs( c ) ) ? 2 : c;
        d = _.isUndefined( d ) ? "," : d;
        t = _.isUndefined( t ) ? "." : t;
        var s = value < 0 ? "-" : "",
            i = parseInt( value = Math.abs( +value || 0 ).toFixed( c ) ) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr( 0, j ) + t : "") +
            i.substr( j ).replace( /(\d{3})(?=\d)/g, "$1" + t ) +
            (c ? d + Math.abs( value - i ).toFixed( c ).slice( 2 ) : "");
    };

    Handlebars.registerHelper( 'money', moneyFormat );
    Em.Handlebars.helper( 'money', moneyFormat );

    return moneyFormat;
} );

