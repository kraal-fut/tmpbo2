define( function ( require ) {
    'use strict';

    var Handlebars = require( 'hbs/handlebars' );

    var icons = {
        'orders': 'fa-list',
        'products': 'fa-tag',
        'shopmanagement': 'fa-shopping-cart',
        'statistics': 'fa-area-chart',
        'procurement': 'fa-euro'
    };

    var helper = function( value, options ) {
        if (icons[value]) {
            return '<i class="fa ' + icons[value] + '"></i>';
        }
        return '';
    };

    Handlebars.registerHelper( 'menuIcons', helper );

    return helper;
} );
