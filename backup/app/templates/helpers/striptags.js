define( function ( require ) {
    'use strict';

    var Handlebars = require( 'hbs/handlebars' ),
        _ = require( 'underscore' ),
        Em = require( 'ember-load' );


    var helper = function( value ) {
        if ( _.isUndefined( value ) || _.isNull( value ) ) {
            return '';
        }
        return value.replace( /<\/?([a-z][a-z0-9]*)\b[^>]*>?/gi, '' ).replace( /(&nbsp;)/gi, '' );
    };

    Handlebars.registerHelper( 'striptags', helper );

    Em.Handlebars.helper( 'striptags', helper );

    return helper;
} );
