/*global define:false*/

define(
    [
        'hbs/handlebars','./checked','./select',
        './equals','./futeach','./futeachrecursion',
        './round','./selected'
    ],
    function ( Handlebars, Checked, Colselect, Equals, Futeach, Futeachrecursion, Round, Selected ) {
        'use strict';

        return {
            checked:Checked,
            colselect:Colselect,
            equals:Equals,
            futeach: Futeach,
            futeachrecursion:Futeachrecursion,
            round:Round,
            selected:Selected
        };

    }
);