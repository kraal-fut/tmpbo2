define( function( require, exports ) {
    'use strict';

    /**
     * link-to is a built in ember helper, but requirejs doesn't know it yet.
     * So this is a dummy file
     */
    return undefined;
} );