/*global define:false*/

define('templates/helpers/selected', ['hbs/handlebars'], function ( Handlebars ) {
    'use strict';

    var selected = function ( option, value ) {
        if (option === value) {
            return new Handlebars.SafeString(' selected ');
        } else {
            return '';
        }
    };

    Handlebars.registerHelper( 'selected', selected );

    return selected;
});
