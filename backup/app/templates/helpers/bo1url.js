define( function ( require ) {
    'use strict';

    var Handlebars = require( 'hbs/handlebars' ),
        Em = require('ember-load');

    var settings = window.App ? window.App.settingsmodel : undefined;

    var getBaseUrl = function() {
        var s = settings ? settings : App.settingsmodel;
        return s.get( 'BASE_URL' );
    };

    var boLink = function( value, type ) {
        var action;
        var identifier;
        if ( type === 'order' ) {
            action = 'to_order';
            identifier = 'ordnu=';
        } else if ( type === 'customer' ) {
            action = 'client2';
            identifier = 'regid=';
        }

        return 'https://' + getBaseUrl() + 'backoffice/bo40.cgi?action=' + action + '&' + identifier + value;
    };

    var helper = function( value, options ) {
        var type = 'order';
        if ( options.hash && options.hash.type ) {
            type = options.hash.type;
        }

        return boLink( value, type );
    };

    Handlebars.registerHelper( 'bo1url', helper );

    Em.Handlebars.helper( 'bo1url', helper );

    return helper;
} );
