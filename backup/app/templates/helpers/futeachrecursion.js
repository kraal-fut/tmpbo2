/*global define:false*/

define('templates/helpers/futeachrecursion', ['hbs/handlebars'], function ( Handlebars ) {
    'use strict';
    function futeachrecursion ( a, context ) {
        var data;
        var parent_id = 'parent_id';
        var id = 'frontoffice_menu_id';
        data = typeof(a) === 'function' ? a() : a;


        var recursionMenu = function (data, item) {
            var resultHtml = '';
            var itemId = item[id];
            for (var _item in data) {
                if ( data.hasOwnProperty(_item) ) {
                    if ( data[_item][parent_id] === itemId) {
                        resultHtml += context.fn(recursionMenu(data, data[_item]));
                    }
                }
            }
            item.internal = resultHtml;
            return item;
        };

        var result = '';
        for (var i = 0; i < data.length; ++i) {
            if (data[i][parent_id] === 0) {
                result += context.fn(recursionMenu(data, data[i]));
            }
        }

        return result;
    }


    Handlebars.registerHelper('futeachrecursion', futeachrecursion);

    return futeachrecursion;

});
