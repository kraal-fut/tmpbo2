/*global define:false*/

define('templates/helpers/equals', ['hbs/handlebars'],
    function ( Handlebars ) {
    'use strict';
    var equals = function ( needle, haystack, options ) {
        var contextvalue = 0;
        var optionsvalue = 0;

        if (typeof needle === 'function') {
            contextvalue = needle();
        } else {
            contextvalue = needle;
        }

        if (typeof haystack === 'function') {
            optionsvalue = haystack();
        } else {
            optionsvalue = haystack;
        }

        if (contextvalue===optionsvalue) {
            return options.fn( this );
        } else if (options.inverse) {
            return options.inverse(this);
        }
    };

    Handlebars.registerHelper( 'equals', equals );

    return equals;

});
