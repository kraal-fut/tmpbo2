define( function ( require ) {
    'use strict';

    var Handlebars = require( 'hbs/handlebars' ),
        _ = require( 'underscore' ),
        Em = require( 'ember-load' );

    var helper = function ( value, context ) {
        if ( _.isUndefined( value ) ) {
            return '';
        }

        var options = context.hash || {};
        var type;

        if ( options.type ) {
            type = options.type;
        }

        var url = '';

        if ( type === 'product' ) {
            url += App.settingsmodel.get( 'PRODUCT_URL' ) + value;
        } else if ( type === 'category' ) {
            url += App.settingsmodel.get( 'BASE_URL') + '?action=search&category=' + value;
        } else if ( type === 'vendor' ) {
            url += App.settingsmodel.get( 'BASE_URL' ) + '?action=vendor&name=' + value;
        } else {
            if ( _.isObject( value ) && !_.isUndefined( value.Image ) ) {
                value = value.Image;
            }

            if ( value.indexOf( 'base64,' ) > -1 ) {
                return value;
            }
            url += App.settingsmodel.get( 'BASE_URL' ) + value;
        }

        if ( options.anchor ) {
            url += '#' + options.anchor;
        }

        return url;
    };

    Handlebars.registerHelper( 'fo2url', helper );

    Em.Handlebars.helper( 'fo2url', helper );

    return helper;
} );
