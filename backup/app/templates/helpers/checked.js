/*global define:false*/

define('templates/helpers/checked', ['hbs/handlebars'], function ( Handlebars ) {
    'use strict';

    var checked = function( context ) {
        if ( context === 1 || context === true || context === '1' ) {
            return 'checked="checked"';
        }
        return '';
    };

    Handlebars.registerHelper( 'checked', checked );

    return checked;
});

