/*global define:false*/

define('templates/helpers/futeach', ['hbs/handlebars'], function ( Handlebars ) {
    'use strict';
    function futeach ( a, context ) {
        var data;
        data = typeof(a) === 'function' ? a() : a;

        var result = '';
        for(var i = 0; i < data.length; ++i){
            result += context.fn(data[i]);
        }

        return result;
    }

    Handlebars.registerHelper('futeach', futeach);

    return futeach;

});