define( function( require ) {
    'use strict';

    var Handlebars = require( 'hbs/handlebars' ),
        _ = require( 'underscore' ),
        objToParams = require( 'components/helperObjectToParam' );

    var htmloptions = function ( original ) {
        var own = ['text'];
        return _.omit( original, own );
    };

    var helper = function ( context ) {
        var onOrOffText = function( checktext ) {
            return checktext || context.hash.text;
        };

        var getText = function() {
            return context.hash.value ? onOrOffText( context.hash.ontext ) : onOrOffText( context.hash.offtext );
        };

        var params = objToParams( htmloptions( context.hash ), this.editing );

        return new Handlebars.SafeString( '<button ' + params + '>' + getText() + '</button>' );
    };

    Handlebars.registerHelper( 'button', helper );

    return helper;
} );
