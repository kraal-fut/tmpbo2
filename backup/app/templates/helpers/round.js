define( function ( require ) {
    'use strict';

    var Handlebars = require( 'hbs/handlebars' );

    var helper = function( value ) {
        var rounded = parseFloat( value ).toFixed( 2 );
        return isNaN( rounded ) ? 'no valid price' : rounded;
    };

    Handlebars.registerHelper( 'round', helper );

    return helper;

});

