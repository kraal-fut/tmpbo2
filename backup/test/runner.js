/*global require:false*/

(function(window) {
    'use strict';

    var karma = window.__karma__;

    // Put Karma into an asynchronous waiting mode until we have loaded our
    // tests.
    karma.loaded = function() {};

    if ( window.chai ) {
        // Optionally use chai with Mocha.
        window.expect = window.chai.expect;
    }

    window.App ={};

    // Set the application endpoint and load the configuration.
    require.config({
        paths: {
            underscore: '../vendor/bower/underscore/underscore',
            'chai-as-promised': '../vendor/bower/chai-as-promised/lib/chai-as-promised',
            fixtures: '../test/fixtures'
        },

        baseUrl: 'base/app'
    });

    require([
        'config',
        'underscore',
        'chai-as-promised'
    ],
    function( config, _ , chaiAsPromised) {
        var specs = _.chain( karma.files )
            // Convert the files object to an array of file paths.
            .map( function( id, file ) { return file; })
            // Tests that end with `.spec.js' and existing either `app` or `test`
            // directories are automatically loaded.
            .filter( function( file ) {
                return ( /^\/base\/(app|test)\/.*\.spec\.js$/ ).test( file );
            })
            .value();

        chaiAsPromised.transferPromiseness = function (assertion, promise) {
            assertion.then = _.bind(promise.then, promise); // this is all you get by default
            assertion.finally = _.bind(promise.finally, promise);
            assertion.done = _.bind(promise.done, promise);
        };

        chai.use(chaiAsPromised);

        // Load all specs and start Karma.
        require( specs, karma.start );
    });
})(this);