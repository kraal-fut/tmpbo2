/*global describe, beforeEach, afterEach, it, expect, runs, waitsfor, sinon */

define ( function ( require ) {
    'use strict';
    var WordPasteParser = require( 'components/wordPasteParser' );

    describe('the Word Paste Parser', function() {
        var parser;

        it('strips inline styles', function(){
            parser = new WordPasteParser('<span style="font-weight:bold;text-decoration:underline">hello world</span>');

            var result = parser.parse();

            expect( result ).to.equal( '<span>hello world</span>' );
        });

        it('strips out word classes', function(){
            parser = new WordPasteParser( '<p class="MsoNoSpacing">foo <p class="MsoNormal">bar</p></p>' );

            var result = parser.parse();

            expect( result ).to.equal('foo <div>bar</div>\n');
        });

        it('strips out unneeded xml <o:p> tags ', function(){
            parser = new WordPasteParser( '<div>hello world<o:p></o:p></div><o:p></o:p>');

            var result = parser.parse();

            expect( result ).to.equal('<div>hello world</div>');
        });

        //it('strips out comments', function(){
        //    parser = new WordPasteParser( '<div>Foo Bar <!-- should be stripped --> World</div>');
        //
        //    var result = parser.parse();
        //
        //    expect( result ).to.equal( '<div>Foo Bar World</div>');
        //});
    });

} );