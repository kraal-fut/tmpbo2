define( function ( require ) {
    'use strict';

    var FutBinding = require( 'components/FutBinding' ),
        Fut = require( 'fut/entities' ),
        $ =  require( 'jquery' );

    var Dummy = Fut.Model.extend( {
        defaults: {
            othermodel: {},
            moremodels:[{},{}]
        },
        relations: [
            {type: Fut.One, key: 'othermodel', relatedModel: Fut.Model},
            {type: Fut.Many, key: 'moremodels', relatedModel: Fut.Model}
        ]
    } );

    describe( 'The Fut save binder', function() {
        describe( 'can get values from:', function() {
            var binder;

            beforeEach( function () {
                binder = new FutBinding( {}, {}, {} );
            } );

            it( 'input type text', function() {
                var $elem = $( '<input type="text">' ).val( 'foo' );
                assert.equal( binder.getValue( $elem ), 'foo' );
            } );

            it( 'input type checkbox', function() {
                var $elem = $( '<input type="checkbox" checked="checked">' );
                assert.equal( binder.getValue( $elem ), true );
            });

            it( 'input type radio', function () {
                var $elem = $( '<input type="radio" checked="checked">' );
                assert.equal( binder.getValue( $elem ), true );
            } );

            it( 'textarea', function () {
                var $elem = $( '<textarea>hello world</textarea>' );
                assert.equal( binder.getValue( $elem ), 'hello world' );
            } );

            it( 'select', function () {
                var $elem = $( '<select><option value="foo">foo</option><option value="bar" selected="selected"></option></select>' );
                assert.equal( binder.getValue( $elem ), 'bar' );
            } );
        } );

        describe( 'can set values to the model', function(){
            var model, binder;

            beforeEach( function () {
                model = new Dummy({});

                binder = new FutBinding( {
                    testfilter: function( value ) {return value + ' viewfilter';}
                }, model, {} );
            } );

            it( 'by simple attribute name', function(){
                binder.setModel( 'foo', 'bar' );
                assert.equal( model.get( 'foo' ), 'bar' );
            });

            it( 'by dot path to related attribute', function() {
                binder.setModel( 'othermodel.somekey', 'bar' );
                assert.equal( model.get( 'othermodel.somekey' ), 'bar' );

                binder.setModel( 'moremodels.1.someotherkey', 'cux' );
                assert.equal( model.get( 'moremodels' ).at( 1 ).get( 'someotherkey' ), 'cux' );
            } );

            it( 'by object with observe key', function() {
                binder.setModel( {observe:'cux'}, 'baz' );
                assert.equal( model.get( 'cux' ), 'baz' );
            } );

            it( 'by using a filter function', function() {
                var f = function( value ) {
                    return value + ' hello world';
                };

                binder.setModel( {observe: 'foo', filter:f}, 'bar' );
                assert.equal( model.get( 'foo' ), 'bar hello world' );
            } );

            it( 'by using a predefined filter as a string', function() {
                binder.setModel( {observe: 'foo', filter: 'parseint'}, '1337' );
                assert.equal( model.get( 'foo' ), 1337 );

                binder.setModel( {observe: 'bar', filter: 'bool'}, 1 );
                assert.equal( model.get( 'bar' ), true );
            });

            it( 'by using a view filter as a string', function() {
                binder.setModel( {observe: 'foo', filter: 'testfilter'}, 'hello' );
                assert.equal( model.get( 'foo' ), 'hello viewfilter' );
            });
        });
    } );
} );