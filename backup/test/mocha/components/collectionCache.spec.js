/*global describe, beforeEach, afterEach, it, expect, runs, waitsfor, sinon */

define ( function ( require ) {
    'use strict';

    var CollectionCache = require( 'components/collectionCache' ),
        fut = require( 'fut/entities' ),
        Backbone = require( 'backbone' ),
        BaseCollection = require( 'collections/base'),
        fixtures = require( 'fixtures/fixtures' );


    describe('the collectionCache', function(){
        var c, cc;

        beforeEach(function(){
            sinon.stub($, 'ajax').yieldsTo('success', fixtures.productcollection);

            c = new Backbone.Collection();
            cc = new CollectionCache( BaseCollection, new Backbone.Model(), 'foo');

            c.cc = cc;
        });

        afterEach(function(){
            $.ajax.restore();
        });

        it('can be attached to a collection', function(){
            expect( c.cc.getItems ).to.exist;
        });

        it('can return models', function(){
            var result = c.cc.getItems([1,2,3,4]);
            return result.should.eventually.be.fulfilled;
        });

        describe('makemodels', function(){
            it('can make an array of id objects', function(){
                var result = cc.makeModels([1,2,3,4]);
                expect( result[0].id ).to.equal(1);
                expect( result[1].id ).to.equal(2);
                expect( result[2].id ).to.equal(3);
                expect( result[3].id ).to.equal(4);
            });
        });
    });
});
