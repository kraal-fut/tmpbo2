/*global describe, beforeEach, afterEach, it, expect, runs, waitsfor, sinon */

define ( function ( require ) {
    'use strict';

    var Backbone = require( 'backbone' ),
        ImageUpload = require( 'components/base64ImageUpload' );

    describe(
        'base64 Image upload', function() {
            var requests = [], that;

            beforeEach ( function () {
                var xhr = sinon.useFakeXMLHttpRequest();

                that = {
                    model: new Backbone.Model ()
                };

                xhr.onCreate = function ( xhr ) {
                    requests.push( xhr );
                };
            });

            it('sets the correct fields on the model using the default setter', function(){
                var event = {
                    target: 'foo'
                };

                ImageUpload.fileSetter('base64data', event, that, 'testField');

                expect( that.model.get('testField' ) ).to.exist;
            });
        }
    );
} );