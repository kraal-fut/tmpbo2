define( function ( require ) {
    'use strict';

    var StickitHelpers = require( 'components/stickitHelpers' ),
        Backbone = require( 'backbone' );

    describe( 'stickithelpers', function(){
        describe( 'fit', function(){
            var f = StickitHelpers.fit;

            it( 'returns the input value when given no other parameters', function(){
                var result = f('De kat krabt de krullen van de trap');
                assert.equal(result, 'De kat krabt de krullen van de trap');
            });

            it( 'limits the length of a given string, based on the second parameter', function(){
                var result = f('De kat krabt de krullen van de trap', 5);
                assert.equal( result.length, 5 );
            });

            it( 'appends by default 3 dots at the end of the string to fill it up', function(){
                var result = f('De kat krabt de krullen van de trap', 7);
                assert.equal( result, 'De k...');
            });

            it( 'appends a custom replacement based on the third parameter to fill it up', function(){
                var result = f('De kat krabt de krullen van de trap', 10, '*');
                assert.equal( result, 'De kat kr*');
            });

            it( 'trims on the right side by default', function(){
                var result = f( 'De kat krabt de krullen van de trap', 4);
                assert.equal( result, 'D...');
            });

            it( 'trims on the left side is fourth parameter is "left"', function(){
                var result = f( 'De kat krabt de krullen van de trap', 5, null, 'left');
                assert.equal( result, '...ap');
            });

            it( 'trims in the middle if fourth parameter is "middle"', function() {
                var result = f( 'De kat krabt de krullen van de trap', 10, null, 'middle' );
                assert.equal( result, 'De k...rap');
            });
        });

        describe( 'striptags', function(){
            var f = StickitHelpers.striptags;

            it( 'strips some basic html stuff from strings', function(){
                var result = f('Hello <div>World</div>');
                assert.equal( result, 'Hello World' );
            });
        });

        describe( 'plucklist', function() {
            var f = StickitHelpers.pluckList;

            var dummy = [
                {
                    a: 'hello world',
                    b: 'foo'
                },
                {
                    a: 'All your base',
                    b: 'bar'
                }
            ];

            it( 'makes a comma seperated string list from values, given a attribute to pluck', function(){
                var result = f( dummy, 'a' );
                assert.equal( result, 'hello world,All your base' );
            });

            it( 'returns the pluck results from an object that implents pluck itself (like backbone collections)', function(){
                var col = new Backbone.Collection( dummy );
                var result = f( col, 'b' );
                assert.equal( result, 'foo,bar' );
            });
        });

        describe( 'bool', function() {
            var f = StickitHelpers.bool;

            it( 'transforms int values other than 0 to bool true', function() {
                assert.equal( f(1), true);
                assert.equal( f(2), true);
                assert.equal( f(10), true);
            });

            it( 'transforms int values of 0 to bool false', function() {
                assert.equal( f( 0 ), false );
            } );

            it( 'keeps original bool values in tact', function() {
                assert.equal( f( true ), true );
                assert.equal( f( false ), false );
            });

            it( 'converts string literals "true" and "false to their respective bool values', function() {
                assert.equal( f( 'true' ), true );
                assert.equal( f( 'false' ), false );
            } );
        });

        describe( 'yesno', function(){
            var f = StickitHelpers.yesno;

            it( 'transforms a boolean true to "yes"', function(){
                var result = f( true );
                assert.equal( result, 'Yes');
            });

            it( 'transforms a boolean false to "no"', function() {
                var result = f( false );
                assert.equal( result, 'No' );
            } );
        });

        describe( 'getBannerImageUrl', function(){
            var f = StickitHelpers.getBannerImageUrl;
            StickitHelpers.setSettings( new Backbone.Model( {'BASE_URL': 'http://fo2host/'} ) );

            it( 'returns an empty string when given an empty string', function() {
                var result = f('');
                assert.equal( result, '' );
            });

            it( 'returns a concatenated url when given a string', function(){
                var result = f( 'foobar.jpg' );
                assert.equal( result, 'http://fo2host/foobar.jpg' );
            });

            it( 'returns directly the url from an object with an "Image" property', function(){
                var result = f( {Image: 'http://www.foobar.nl/helloworld.png'} );
                assert.equal( result, 'http://www.foobar.nl/helloworld.png' );
            });
        });

        describe( 'getProductImages', function() {
            var f = StickitHelpers.getProductImages;
            beforeEach(function(){
                StickitHelpers.setSettings( new Backbone.Model( {'BASE_URL': 'http://fo2host/'} ) );
            });

            it( 'it returns an "image unavailable" image when geven an undefined value', function() {
                var result = f( undefined );
                assert.equal( result, 'http://fo2host/img/thumbs/unavail.jpg' );
            } );

            it( 'returns an array of images', function() {
                var dummyData = [
                    {filename: 'foo.jpg', prodlevid: '9040022'},
                    {filename: 'bar.png', prodlevid: '6124-0012-N0610'},
                    {filename: 'cux.gif', prodlevid: '6422-0002-N1309', situation: 'old'}

                ];

                var result = f( dummyData  );
                assert.deepEqual( result, [
                    'http://fo2host/img/products/9040022/thumb/foo.jpg',
                    'http://fo2host/img/products/6124-0012-N0610/thumb/bar.png',
                    'http://fo2host/img/thumbs/cux.gif'] );
            } );
        } );

        describe( 'boLink', function() {
            beforeEach( function() {
                StickitHelpers.setSettings( new Backbone.Model( {'BASE_URL': 'fo2host/'} ) );
            } );

            it( 'returns an order link when given type "order"', function() {
                var result = StickitHelpers.boLink( '123456', 'order' );
                assert.equal( result, 'https://fo2host/backoffice/bo40.cgi?action=to_order&ordnu=123456' );
            });

            it( 'returns an customer link when given type "customer"', function () {
                var result = StickitHelpers.boLink( '123456', 'customer' );
                assert.equal( result, 'https://fo2host/backoffice/bo40.cgi?action=client2&regid=123456' );
            } );
        });
    });
} );