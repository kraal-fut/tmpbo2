define( function ( require ) {
    'use strict';

    var helper = require( 'components/helpers/selecthelper' );

    describe('the select handlebars helper', function() {

        it( 'builds a basic select when at least given a value and context.hash', function() {
            var result = helper.helper( 'foo', {hash: undefined} );
            assert.equal( '<select></select>', result );
        } );

        it( 'adds options with default label/ids when given a simple array as collection', function() {
            var result = helper.helper( 'cux', {hash: {collection: ['foo', 'bar']}} );
            assert.equal( '<select><option value="foo">foo</option><option value="bar">bar</option></select>', result );
        });

        it ('makes the option with correct value selected', function() {
            var result = helper.helper( 'foo', {hash: {collection: ['foo', 'bar']}} );
            assert.equal( '<select><option value="foo" selected="selected">foo</option><option value="bar">bar</option></select>', result );
        });

        it ( 'makes options from a backbone collection with attribute id and label', function() {
            var c = new Backbone.Collection([
                {id: 'bar', label: 'foo'},
                {id: 'baz', label: 'cux'}
            ]);

            var result = helper.helper( 'foo', {hash: {collection: c}} );
            assert.equal( '<select><option value="bar">foo</option><option value="baz">cux</option></select>', result );
        } );

        it( 'allows to overrule the keys for id and label', function() {
            var c = new Backbone.Collection( [
                {someKey: 'bar', text: 'foo'},
                {someKey: 'baz', text: 'cux'}
            ] );

            var result = helper.helper( 'foo', {hash: {collection: c, label: 'text', idfield: 'someKey'}} );
            assert.equal( '<select><option value="bar">foo</option><option value="baz">cux</option></select>', result );
        });

        it( 'allows to define a default option for if nothing is selected', function() {
            var result = helper.helper( 'foo', {hash: {'default': 'select foo'}} );
            assert.equal( '<select><option>select foo</option></select>', result );
        });

        it( 'allows selecting numeric values even when they are given as string', function() {
            var result = helper.helper( '0', {hash: {collection: [0, 1]}} );
            assert.equal( '<select><option value="0" selected="selected">0</option><option value="1">1</option></select>', result );
        });
    });

} );