/*global describe, beforeEach, afterEach, it, expect, runs, waitsfor, spyOn*/

define ( function ( require ) {
    'use strict';
    var Backbone = require( 'backbone' ),
        Cocktail = require( 'cocktail' ),
        Marionette = require( 'marionette' ),
        Field = require( 'product/views/viewsfield/PopupField' );


    describe( 'The base field', function () {
            beforeEach( function () {
                sinon.spy( $, 'ajax' );

                var dummyApp = {
                    settingsmodel: new Backbone.Model()
                };

                var FieldView = Marionette.ItemView.extend({});
                Cocktail.mixin( FieldView, Field );

                this.testfield = new FieldView( {
                    App: dummyApp,
                    productView: new Backbone.View(),
                    fieldName: 'testField'
                } );
            } );

            afterEach( function () {
                $.ajax.restore();

                delete this.testfield;
            } );

            it( 'variables can not be empty', function () {
                expect( this.testfield.productView ).not.to.be.null;
                expect( this.testfield.fieldName ).not.to.be.null;
                expect( this.testfield.model ).not.to.be.null;
            } );

        }
    );

} );
