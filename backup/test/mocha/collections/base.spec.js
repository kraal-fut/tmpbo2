/*global jasmine, describe, beforeEach, it, expect */
define ( function ( require ) {
    'use strict';

    var Backbone = require( 'backbone' ),
        BaseCollection = require( 'collections/base' ),
        $ = require( 'jquery' );

    var baseCollection;

    describe(
        'The base collection', function () {
            beforeEach( function () {
                sinon.stub( $, 'ajax' ).yieldsTo( 'success', [] );

                baseCollection = new BaseCollection( {},{settings:new Backbone.Model()} );
                baseCollection.url = '/';

                baseCollection.resetCriteria();
            } );

            afterEach(function(){
                $.ajax.restore();
            });

            it('is able to be constructed', function(){
                assert.isDefined( baseCollection );
            });

            it('returns a promised when loading', function(){
                assert.isFunction( baseCollection.load().then );
            });

            it('triggers resetBreadcrumbs when calling persist', function(){
                var cb = sinon.spy();
                baseCollection.on('resetBreadcrumbs', cb);
                baseCollection.persistCriteria();

                expect(cb).to.have.been.called;
            });

            describe('criteria', function(){
                it('accepts criteria by passing an object', function(){
                    var crit = {'Vendor':{'in':['agu','shimano']}};

                    baseCollection.addCriteria( crit );


                    expect( baseCollection.getCriteriaAll() ).to.contain.keys('Vendor');
                });

                it('accepts criteria by passing a backbone model', function(){
                    var crit = new Backbone.Model({'Name':'Evil Eye'});

                    baseCollection.addCriteria( crit );

                    expect( baseCollection.getCriteriaAll() ).to.contain.keys('Name');
                });

                it('accepts criteria by passing a key/value pair', function(){
                    baseCollection.addCriteria({'in':['kleding','fietsen']}, 'cat');

                    expect( baseCollection.getCriteriaAll() ).to.contain.keys( 'cat' );
                });

                it('can remove a criterium base on a single fieldname', function(){
                    baseCollection.addCriteria({'in':['kleding','fietsen']}, 'cat');
                    expect( baseCollection.getCriteriaAll() ).to.contain.keys( 'cat' );

                    baseCollection.removeFieldCriteria('cat');

                    expect( baseCollection.getCriteriaAll() ).to.not.contain.keys('cat');
                });
            });

            describe('sorting', function(){
                it('returns a sort obect', function(){
                    baseCollection.addSort('name','asc');
                    assert.isObject( baseCollection.getSortAll() );
                });

                it('accepts sort parameters as an object', function(){
                    baseCollection.addSort({'name':'asc'});
                    expect( baseCollection.getSortAll() ).to.contain.keys( 'name' );

                    assert.equal( baseCollection.getSortAll().name, 'asc');
                });

                it('accepts sort parameters as seperate key and value pairs', function(){
                    baseCollection.addSort('Vendor','desc');

                    expect( baseCollection.getSortAll() ).to.contain.keys( 'Vendor' );
                    assert.equal( baseCollection.getSortAll().Vendor, 'desc' );
                });

                it('can return a sort specific for a single field', function(){
                    baseCollection.addSort('Vendor','asc');
                    assert.equal( baseCollection.getSort( 'Vendor' ), 'asc');
                });

                it('returns no sort specific for a single field, if not set', function() {
                    assert.equal( baseCollection.getSort( 'Vendor' ), '');
                });

                it('can remove a sort value based on a single field', function() {
                    baseCollection.addSort( 'Vendor','asc' );
                    assert.equal( baseCollection.getSort( 'Vendor' ), 'asc');

                    baseCollection.removeSort( 'Vendor' );
                    assert.equal( baseCollection.getSort( 'Vendor' ), '' );
                });
            });
        }
    );
});