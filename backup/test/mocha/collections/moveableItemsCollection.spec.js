/*global jasmine, describe, beforeEach, it, expect */
define ( function ( require ) {
    'use strict';

    var Backbone = require ( 'backbone' ),
        Cocktail = require ( 'cocktail' ),
        MoveableItemsCollection = require ( 'collections/moveableItemsCollection' );

    var c;

    describe ( 'The moveable model mixin', function () {
        beforeEach ( function () {
            c = new Backbone.Collection();
            c.comparator = 'quenumber';

            Cocktail.mixin( c, MoveableItemsCollection );
        } );

        it( 'does shift a model to the next place when calling moveModelDown', function() {
            c.add( {'quenumber':1, 'title':'item 1'} );
            c.add( {'quenumber':2, 'title':'item 2'} );
            c.add( {'quenumber':3, 'title':'item 3'} );
            var m = c.at(0);

            c.moveModelDown( m, 'quenumber' );
            assert.equal( m.get( 'quenumber'), 2);
            assert.equal( c.at( 0 ).get( 'title' ), 'item 2');
            assert.equal( c.at( 1 ).get( 'title' ), 'item 1');
        });

        it( 'does shift a model to the previous place when calling moveModelUp', function() {
            c.add( {'quenumber':1, 'title':'item 1'} );
            c.add( {'quenumber':2, 'title':'item 2'} );
            c.add( {'quenumber':3, 'title':'item 3'} );
            var m = c.at( 1 );

            c.moveModelUp( m, 'quenumber' );
            assert.equal( m.get( 'quenumber' ), 1 );
            assert.equal( c.at( 0 ).get( 'title' ), 'item 2' );
            assert.equal( c.at( 1 ).get( 'title' ), 'item 1' );
        });

        it ('moves models up with queue items that are strings', function(){
            c.add( {'quenumber':'1', 'title':'item 1'} );
            c.add( {'quenumber':'2', 'title':'item 2'} );
            c.add( {'quenumber':'3', 'title':'item 3'} );
            var m = c.at( 1 );

            c.moveModelUp( m, 'quenumber', true );

            assert.equal( m.get( 'quenumber' ), 1 );
            assert.equal( c.at( 0 ).get( 'title' ), 'item 2' );
            assert.equal( c.at( 1 ).get( 'title' ), 'item 1' );
        });

        it ('moves models down with queue items that are strings', function(){
            c.add( {'quenumber':'1', 'title':'item 1'} );
            c.add( {'quenumber':'2', 'title':'item 2'} );
            c.add( {'quenumber':'3', 'title':'item 3'} );
            var m = c.at( 0 );

            c.moveModelDown( m, 'quenumber', true );

            assert.equal( m.get( 'quenumber' ), 2 );
            assert.equal( c.at( 0 ).get( 'title' ), 'item 2' );
            assert.equal( c.at( 1 ).get( 'title' ), 'item 1' );
        });
    } );
});
