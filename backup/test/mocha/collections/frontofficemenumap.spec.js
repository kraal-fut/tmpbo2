define( function ( require, exports ) {
    'use strict';

    var FoMenuMap = require( 'collections/frontofficemenumap' );

    describe(
        'The fomenumap collection', function () {
            var foMenuMap = new FoMenuMap( [
                {parent_id: 10, frontoffice_menu_id: 6556, name:'home', isParent:true},
                {parent_id: 10, frontoffice_menu_id: 8145, name:'foo'},
                {parent_id: 12, frontoffice_menu_id: 1234, name:'bar'}
            ] );

            it('gets children by parent_id', function() {
                var result = foMenuMap.getChildren(10);
                assert.equal( 2, result.length );

                var singleresult = foMenuMap.getChildren( 12 );
                assert.equal( 1, singleresult.length );
            });

            it( 'gets its chilrens id by parentId', function() {
                var result = foMenuMap.getIdsByParent( 10 );
                assert.deepEqual( [6556, 8145], result );
            });

            it( 'augments its models to add a dot for labels that are parents', function(){
                var parent = foMenuMap.at(0);
                assert.equal( ' • home', parent.get( 'label' ) );
            });
        }
    );
} );