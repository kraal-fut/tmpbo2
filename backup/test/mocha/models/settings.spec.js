define( function ( require ) {
    'use strict';

    var Settingsmodel = require( 'models/settings' );

    describe( 'The settings model', function () {
        describe( 'when getting a setting', function() {

            it('accepts a single attribute to get the settings from', function() {
                var s = new Settingsmodel( {foo:'bar'} );
                assert.equal( s.getSetting( 'foo' ), 'bar' );
            });

            it('accepts a list of attributes to get the setting from', function() {
                var s = new Settingsmodel( {foo:'bar'} );
                assert.equal( s.getSetting( ['foo','cux'] ), 'bar');
            });

            it('picks the first setting that is available from the list', function() {
                var s = new Settingsmodel( {foo: 'bar', baz:undefined} );
                assert.equal( s.getSetting( ['baz', 'foo'] ), 'bar' );
            });

            it('returns a given default parameter when no setting was found', function() {
                var s = new Settingsmodel( {foo: 'bar', baz: undefined} );
                assert.equal( s.getSetting( 'notdefined', 'defaultfoo' ), 'defaultfoo' );
            });

            it( 'picks a setting first from userSettings', function() {
                var s = new Settingsmodel( {foo: 'bar' } );
                s.userSettings.set( 'foo', 'baz' );
                s.user.set( {'foo':'cux', 'brood':'kaas' } );

                assert.equal( s.getSetting( 'foo' ), 'baz' );
            });

            it( 'picks a setting second from user', function () {
                var s = new Settingsmodel( {foo: 'bar', brood: 'jam'} );
                s.userSettings.set( 'foo', 'baz' );
                s.user.set( {'foo': 'cux', 'brood': 'kaas'} );

                assert.equal( s.getSetting( 'brood' ), 'kaas' );
            } );
        });
    } );
});