/*global describe, beforeEach, afterEach, it, expect, runs, waitsfor */
define ( function ( require ) {
    'use strict';
    var Marionette = require('marionette'),
        Router = require( 'router');

    // Test that the Router exists.
    describe( 'Application Router', function() {
        it( 'exists', function() {
            assert.ok( Router );
        });

        it( 'is a Backbone.Router', function() {
            assert.instanceOf( new Router( {App: {}} ), Marionette.AppRouter );
        });

        it( 'builds module routers in the global object', function() {
            var router = new Router( {App: {}});

            assert.ok( window.orderrouter );
            assert.ok( window.productrouter );

        });
    });
});