define( function ( require ) {
    'use strict';

    var Model = require( 'order/ItPakket/models/order');

    describe('The IT Pakket Order model', function(){
        var m;
        beforeEach(function(){
            var lines = [
                '          3STQOV028026001                                           1-12-2014                      P. v. Foo\n',
                '          3662266                                                    1\n',
                '          03   - Pakket ptp, <30kg, verhoogd aansprakelijk, lineair\n',
                '                                                                                EUR 500,00         Barstreet    39\n',
                '                0,000                                                                              1234 XL          BAZ                              NL\n',
                '          FuturumShop.com bestelling\n'
            ];
            m = new Model({line:lines});
        });

        it( 'can extract a trackandtrace code, a date and a name', function () {
            m.getTracknoDateAndName( m.get( 'line' ) );

            assert.equal( m.get( 'trackno' ), '3STQOV028026001' );
            assert.equal( m.get( 'date' ), '1-12-2014' );
            assert.equal( m.get( 'name' ), 'P. v. Foo' );

        } );

        it( 'can extract an ordernumber', function() {
            m.getOrderNoAndColli( m.get( 'line' ) );

            assert.equal( m.get( 'orderno' ), '3662266' );
        });

        it( 'can extract an shipping code and shipping detail', function() {
            m.getShipcodeAndDetail( m.get( 'line' ) );

            assert.equal( m.get( 'shipcode' ), '03  ');
            assert.equal( m.get( 'shipdetail' ), 'Pakket ptp, <30kg, verhoogd aansprakelijk, lineair\n');
        });

        it( 'can extract insurance and address', function() {
            m.getInsuranceAndAddress( m.get( 'line' ) );

            assert.equal( m.get('insuranceamount'), '500,00');
            assert.equal( m.get('address'), 'Barstreet 39');
        });

        it( 'can extract weight, rembours and zipcode', function() {
            m.getWeightRembAndZip( m.get( 'line' ) );

            assert.equal( m.get('weight'), '0,000');
            assert.equal( m.get('rembammount'), undefined);
            assert.equal( m.get('zipetc'), '1234 XL BAZ NL');
        });

        it( 'can find see if shipment is to customer', function() {
            m.set('orderno', 12345);
            m.getSupplierOrCustomerShipment();
            assert.equal(m.get('receiver'), 'C');
            assert.equal(m.get('order'), true);

        });

        it( 'can find see if shipment is to customer', function () {
            m.getSupplierOrCustomerShipment();

            assert.equal( m.get( 'receiver' ), 'S' );
            assert.equal( m.get( 'order' ), false );

        } );
    });



} );