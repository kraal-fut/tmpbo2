/*global jasmine, describe, beforeEach, afterEach, it, expect */

define ( function ( require ) {
    'use strict';
    var PaymentModel = require('order/ProcessPayments/models/payment');

    describe( 'The payment model', function () {
        var model;
        var spy;
        beforeEach( function () {
            model = new PaymentModel( { line: '09/17/2014;BE59 3101 8949 3226;94,95;EUR;;;/TRCD/0150//RTRN///CREF/NOTPROVIDED//CNTP/BE98731028316493/KREDBEBB/BASTIAANSE IVO/7311    APELDOORN//REMI/USTD/3611106X//CATP///PURP///SVCL/SEPA//MARF///ULTC////CSID///ISDT//' } );
            //spy = sinon.stub($, 'ajax').yieldsTo('success', BundleFixture);
        } );

        it( 'fills date', function() {
            model.parseLine();
            assert.equal( model.get( 'date' ), '09/17/2014' );
        } );

        it( 'fills amount', function() {
            model.parseLine();
            assert.equal( model.get( 'amount' ), '94.95' );
        } );
        
        it( 'fills description', function() {
            model.parseLine();
            assert.equal( model.get( 'description' ), '/TRCD/0150//RTRN///CREF/NOTPROVIDED//CNTP/BE98731028316493/KREDBEBB/BASTIAANSE IVO/7311    APELDOORN//REMI/USTD/3611106X//CATP///PURP///SVCL/SEPA//MARF///ULTC////CSID///ISDT//');
        } );
        
        it( 'fills order', function() {
            model.parseLine();
            assert.equal( model.get( 'order' ), '3611106' );
        } );
        
        it( 'payment is invalid', function() {
            model.parseLine();
            assert.equal( model.get( 'valid' ), false );
        } );

        it( 'has payment type', function() {
            model.parseLine();
            assert.equal( model.get( 'paymenttype' ), 'C' );
        } );
    } );
} );