/*global jasmine, describe, beforeEach, afterEach, it, expect, runs, waitsFor, spyOn */
define ( function ( require ) {
    'use strict';

    var $ = require( 'jquery' ),
        Backbone = require( 'backbone' ),
        Payments = require( 'order/ProcessPayments/collections/payments' ),
        fixture = require( 'text!fixtures/payment.txt' );

    describe(
        'The payments collection', function () {

            beforeEach( function () {
                this.payments = new Payments( [] );
            } );

            it('splits file into lines', function () {
                this.payments.parseData('hello\nworld\nhello\nworld');
                assert.deepEqual( this.payments.getLines(), ['hello', 'world', 'hello', 'world'] );
            } );

            it('splits test file into lines', function () {
                this.payments.parseData(fixture);
                assert.deepEqual( this.payments.getLines(), [
                    'NL78ABNA0409991287 RA VERHOEF CJ Orderbevestiging Futurumshop order 3605524	EUR	20140917	0	0	20140917	140.75	NL78ABNA0409991287 RA VERHOEF CJ Orderbevestiging Futurumshop order 3605524												',
                    'NL95RABO0383738687 P.R. VAN CASPEL Ordernr 3592735x 	EUR	20140917	0	0	20140917	91.86	NL95RABO0383738687 P.R. VAN CASPEL Ordernr 3592735x 												',
                    'NL66RABO0366620649 B.W.WITZAND Order nr 3599582x 	EUR	20140917	0	0	20140917	74.95	NL66RABO0366620649 B.W.WITZAND Order nr 3599582x'
                ] );
            } );
        }
    );
});
