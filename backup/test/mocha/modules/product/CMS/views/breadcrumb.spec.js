/*global jasmine, describe, beforeEach, it, expect */
define( function ( require ) {
    'use strict';

    var Backbone = require( 'backbone' ),
        _ = require( 'underscore' ),
        Breadcrumbs = require( 'views/breadcrumbs' ),
        BaseCollection = require( 'collections/base' );

    describe( 'The breadcrumb view', function () {

        beforeEach( function () {
            window.App.settingsmodel = new Backbone.Model();
            this.breadCrumbs = new Breadcrumbs( {
                collection: new BaseCollection(),
                settings: {}
            } );
        } );

        it( 'is not null', function () {
            assert.isNotNull( this.breadcrumbs );
        } );

        describe( 'makeBreadcrumbObject function', function () {
            var tf;  //testfunction
            var col; //collection
            beforeEach( function () {
                tf = _.bind( this.breadCrumbs.makeBreadcrumbObject, this.breadCrumbs );
                col = this.breadCrumbs.otherCollection;
            } );

            it( 'returns an array', function () {
                assert.isArray( tf() );
            } );

            it( 'returns an not empty array when adding criteria', function () {
                col.addCriteria( {'Vendor': {'in': ['Knog', 'Agu']}} );
                assert.operator( tf().length, '>', 0);
            } );

            it( 'returns the correct format when adding criteria', function () {
                col.addCriteria( {'Vendor': {'in': ['Knog', 'Agu']}} );
                var test = tf()[0];

                assert.equal( test.id, 'Vendor' );
                assert.equal( test.name, 'Vendor' );
                assert.isDefined( test.value );
            } );
        } );
    } );

} );