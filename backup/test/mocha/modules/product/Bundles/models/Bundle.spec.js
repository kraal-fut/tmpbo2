/*global jasmine, describe, beforeEach, afterEach, it, expect */

define ( function ( require ) {
    'use strict';
    var BundleModel = require( 'product/Bundles/models/bundles' ),
        Backbone = require( 'backbone' ),
        BundleFixture = require( 'fixtures/bundleFixture' );

    describe( 'The bundle model', function () {
        var model;
        var spy;
        beforeEach ( function () {
            model = new BundleModel();
            spy = sinon.stub($, 'ajax').yieldsTo('success', BundleFixture);

        } );

        afterEach( function(){
            $.ajax.restore();
        });

        it('uses a normal id attribute', function(){
            model.set( 'id', 12345 );
            expect( model.id ).to.equal( 12345 );
        });

        it('parses the mongodb id on fetch in single view mode', function(){
            model.fetch({async:false});

            expect( model.id ).to.equal( '53ecc274d36653c340d63cd6' );
        });

        it('removes the "id" attribute when serializing', function(){
            model.set( 'id', 'foo bar' );

            var result = model.toJSON();

            expect( result.id ).to.not.exist;
        });

        it('can add a bundledProduct with a prodlevid', function(){
            model.addBundledProduct( '12345' );

            expect( model.get( 'bundledproducts' ).at( 0 )).to.exist;
            expect( model.get( 'bundledproducts' ).at( 0 ).get( 'prodlevid' ) ).to.equal( '12345' );
        });

        it('adds nothing when the prodlevid is empty', function() {
            model.addBundledProduct(' ');
            expect( model.get( 'bundledproducts').length ).to.equal(0);
        });

        it('can remove a bundled product by model', function(){
            model.addBundledProduct ( '12345' );
            var dummy = model.get ( 'bundledproducts' ).at ( 0 );
            expect ( model.get ( 'bundledproducts' ).length ).to.equal ( 1 );

            model.removeBundledProduct( dummy );
            expect( model.get( 'bundledproducts' ).length ).to.equal( 0 );
        });

        it('can update the productModel', function(){
            var dummyModel = new Backbone.Model({dummyAttribute:'foo bar'});
            model.update(dummyModel);

            expect( model.isProdlevidValid ).to.be.true;
            expect( model.productModel.get('dummyAttribute') ).to.equal('foo bar');
        });
        it ( 'is invalid when prodlevid is invalid (flag is set)', function () {
            model.set ( 'prodlevid', '12345' );
            model.isProdlevidValid = false;

            expect ( model.isValid () ).to.be.false;
        } );

        it('is invalid when prodlevid already exists (flag is set)', function(){
            model.set('prodlevid', '12345');
            model.alreadyExists = true;
            model.isProdlevidValid = true;

            expect( model.isValid() ).to.be.false;
        });
    });

} );