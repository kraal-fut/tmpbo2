/*global jasmine, describe, beforeEach, afterEach, it, expect, runs, waitsfor */

define ( function( require ) {
    'use strict';

    var Backbone = require( 'backbone' ),
        CouponListItemView = require( 'product/Discount/views/CouponListItemView' );

    var mockModel = new Backbone.Model({
        id:0,
        description: undefined,
        active:undefined,
        claimed:undefined
    });

    describe('The coupon listitem view', function () {
        beforeEach( function () {
            this.testView = new CouponListItemView( { model:mockModel } );
        });

        it( 'is not undefined', function () {
            assert.isDefined( this.testView );
        } );

        afterEach( function () {
            this.testView.destroy();
            delete this.testView;
        } );
    });
});