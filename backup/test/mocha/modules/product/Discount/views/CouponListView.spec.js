/*global jasmine, describe, beforeEach, afterEach, it, expect, runs, waitsfor */

define ( function ( require ) {
    'use strict';
    var CouponListView = require( 'product/Discount/views/CouponListView' );

    window.App.settingsmodel = new Backbone.Model();

    describe('The coupon list view', function () {
        beforeEach( function () {
            this.testView = new CouponListView();
        });

        it( 'is not undefined', function () {
            assert.isDefined( this.testView );
        } );

        it( 'has a collection', function() {
            assert.isDefined( this.testView.collection );
        });

        afterEach( function () {
            this.testView.destroy();
            delete this.testView;
        } );
    });
});