/*global jasmine, describe, beforeEach, afterEach, it, expect, runs, waitsFor, spyOn */
define ( function ( require ) {
    'use strict';

    var $ = require( 'jquery' ),
        Backbone = require( 'backbone' ),
        Products = require( 'product/collections/products' ),
        fixtures = require( 'fixtures/fixtures' );

    describe(
        'The products collection', function () {
            var settingsMock = new Backbone.Model();
            var appMock = {
                request: function() {
                    return settingsMock;
                }
            };

            beforeEach( function () {
                sinon.stub($, 'ajax').yieldsTo('success', fixtures.productcollection);

                this.products = new Products( [], {App: appMock, settings: settingsMock} );
                this.products.url = function(){
                    return '/';
                };
            } );
            afterEach(function(){
                $.ajax.restore();
            });

            it('uses the [/products] url search space', function () {
                expect(  this.products.url( ) ).to.equal( '/' );
            });

            it('find products collection like query MongoDB', function () {

                var params = {
                    'Vendor': {'in': ['Knog', 'Agu']}
                };

                this.products.addCriteria( params );
                this.products.addSort( 'name', 'asc' );
                this.products.setPageSize( 20 );

                this.products.fetch({async:false});

                expect( this.products.length ).to.be.above( 0 );

            });

            it('addition, replacement and remove of criterias in the search', function () {
                var params = {
                    'Name': 'lo',
                    'Vendor': {'$in': ['Knog', 'Agu']},
                    'sort.Name': 'asc'
                };
                this.products.addCriteria( params );
                expect( this.products.getCriteria( 'Name' ) ).to.equal( 'lo' );

                this.products.addCriteria( {'Name': 'new'} );
                expect( this.products.getCriteria( 'Name' ) ).to.equal( 'new' );

                this.products.addCriteria( {'sort.Name': 'desc'} );
                expect( this.products.getCriteria( 'sort.Name' ) ).to.equal( 'desc' );

                this.products.addCriteria( {'Vendor': {'$in': ['KMC']}} );
                var tmpVendorObj = this.products.getCriteria( 'Vendor' );
                expect( tmpVendorObj.$in ).to.contain( 'KMC' );

                this.products.addCriteria( {'ProdId': '1', 'sort.ProdId': 'desc'} );

                expect( this.products.getCriteria( 'ProdId' ) ).to.equal( '1' );
                expect( this.products.getCriteria( 'sort.ProdId' ) ).to.equal( 'desc' );

                expect( tmpVendorObj.$in ).to.exist;
                expect( tmpVendorObj.$in ).to.contain( 'KMC' );
                expect( tmpVendorObj.$in ).not.to.contain( 'Knog' );
                expect( tmpVendorObj.$in ).not.to.contain( 'Agu' );

                expect( this.products.getCriteria( 'sort.Name' ) ).to.equal( 'desc' );
                expect( this.products.getCriteria( 'Name' ) ).to.equal( 'new' );

                this.products.removeFieldCriteria( 'Vendor');
                expect( this.products.getCriteria( 'Vendor' ) ).to.equal( '' );

            });

            it('addition and reset criteria search', function () {
                var params = {
                    'product_special_price.end_date': {'lte': '2013/06/03', 'gte': '2013/06/07'},
                    'sort.product_special_price.end_date': 'desc'
                };
                this.products.addCriteria( params );

                expect( this.products.getCriteria( 'product_special_price.end_date' ) ).to.contain.keys( ['lte','gte'] );

                this.products.resetCriteria();
                expect( this.products.getCriteria( 'product_special_price.end_date' ) ).to.equal( '' );
            });

            it('data send params search is object', function () {
                expect( this.products.getCriteriaAll() ).to.be.an('object');
            });

            it('contains an extra product when duplicate is called', function () {
                this.products.fetch( {async: false} );

                var initialLength = this.products.length;
                this.products.duplicateProduct( '6004-0293-001-N1108' );

                expect( this.products.length ).to.equal( initialLength + 1 );
            });

            it('can filter products with an array of prodlevids', function(){
                this.products.fetch( {async: false} );
                var ids = ['6004-0293-001-N1108', '6040-0046-001-N1310', '6143-0714-N1307'];
                var filteredProducts = this.products.filterByIds(ids);

                expect( filteredProducts.length ).to.equal(3);
            });

            it('filtering products returns an array of product models', function(){
                this.products.fetch( {async: false} );
                var ids = ['6004-0293-001-N1108', '6040-0046-001-N1310', '6143-0714-N1307'];
                var filteredProducts = this.products.filterByIds( ids );

                expect( filteredProducts[0] ).to.be.an.instanceof( Backbone.Model );
            });
        }
    );
});
