/*global describe, beforeEach, afterEach, it, expect, runs, waitsfor, sinon */

define ( function ( require ) {
    'use strict';
    var Backbone = require( 'backbone' ),
        ProductImageUpload = require( 'product/components/productImageUpload' ),
        _ = require( 'underscore' );

    var callbacks = {
        failure: sinon.spy()
    };

    var files = {
        'file':{
            'size': 250,
            'type': 'image/jpg',
            'name': 'image1.jpg'
        },
        'file2':{
            'size': 500,
            'type': 'image/png',
            'name': 'image2.png'
        },
        'file3':{
            'size': 250,
            'type': 'image/tif',
            'name': 'wrongimagetype.tif'
        }
    };

    describe(
        'Image upload', function(){
            beforeEach( function() {
                var xhr = sinon.useFakeXMLHttpRequest();
                var requests = this.requests = [];

                xhr.onCreate = function ( xhr ) {
                    requests.push( xhr );
                };

                this.model = new Backbone.Model({'mediaType': 'product'});
                this.model.set('files', files);

                this.testComponent = new ProductImageUpload();
            } );

            it('should detect images with wrong filetype', function(){
                this.testComponent.handleDrop( this.model, callbacks );
                expect( callbacks.failure ).to.have.been.calledOnce;
            });

            it('should give progress', function() {
                this.testComponent.totalSize = 100;
                var xhr = {memo: 0};
                var result = this.testComponent.getProgress( 100, xhr );

                expect( result ).to.equal( 100 );
            });

            it('should give total size', function() {
                this.testComponent.totalSize = 0;
                this.testComponent.getTotalSize( files );

                expect( this.testComponent.totalSize ).to.equal( 1000 );
            });

            it('should call uploadImage twice', function() {
                this.testComponent.uploadImage = sinon.spy();
                this.testComponent.handleDrop( this.model );

                expect( this.testComponent.uploadImage ).to.have.been.calledTwice;
            });

            it('should do a request per image', function() {
                this.testComponent.handleDrop( this.model );

                expect( this.requests.length ).to.equal(2);
            });

            it('should do a request with the correct url', function() {
                this.testComponent.handleDrop( this.model );

                expect( this.requests[0].url ).to.equal( '/api/media/images/product' );
            });

            it('should set custom HTTP headers', function(){
                this.testComponent.handleDrop( this.model );

                expect( this.requests[0].requestHeaders['X-FILE-NAME'] ).to.equal( 'image1.jpg' );
                expect( this.requests[0].requestHeaders['X-FILE-OVERWRITE'] ).to.be.false;
            });

            it('should set overwrite HTTP headers for overwriting', function(){
                this.testComponent.uploadImage( _.toArray(files)[0], true, this.model);

                expect( this.requests[0].requestHeaders['X-FILE-OVERWRITE'] ).to.be.true;
            });
        }
    );
});