/*global describe, beforeEach, afterEach, it, expect, runs, waitsfor, sinon */
define ( function ( require ) {
    'use strict';
    var Backbone = require( 'backbone' ),
        Product = require( 'product/models/products' ),
        fixtureProduct = require( 'fixtures/completeproduct');

    describe(
        'The product model', function() {
            var spy;
            beforeEach( function() {
                spy = sinon.stub( $, 'ajax' ).yieldsTo(
                    'success',
                    JSON.stringify( fixtureProduct )
                );

                this.model = new Product( fixtureProduct );

                this.model.collection = {
                    url: '/'
                };

            });

            afterEach( function() {
                $.ajax.restore();
            });

            it('uses the correct url', function () {
                assert.equal( this.model.url(), '/6003-0085-N1011' );
            });

            it('adds the temporary state to url when temporary property is set', function () {
                this.model.set('temporary', 0);
                assert.equal( this.model.url(), '/6003-0085-N1011?temporary=false' );
            });

            it('should load from server given a ProdLevId', function () {
                this.model.fetch( {async:false} );

                expect( spy ).to.have.been.calledOnce;
                var args = $.ajax.getCall(0).args[0];

                assert.equal( args.type, 'GET');
            });

            it('should have a Name attribute', function () {
                assert.equal( this.model.get( 'Name' ), fixtureProduct.Name );
            });

            it('should not be editable when history', function () {
                this.model.set( 'frozen', 1 );
                assert.isFalse( this.model.isEditable() );
            });

            it('returns a promise when calling save', function(){
                var result = this.model.save();
                assert.isFunction( result.then );
            });

            it('should be editable when new', function () {
                this.model.unset( 'frozen' );
                assert.isTrue( this.model.isEditable() );
            });

            it('should be editable when not history', function () {
                this.model.set( 'frozen', 0 );
                assert.isTrue( this.model.isEditable() );
            });

            it ('should be editable when not "frozen"', function(){
                this.model.set( 'frozen', 1);
                assert.isFalse( this.model.isEditable() );
            });

            describe('working with marges', function(){
                it ('should return zero when prices are the same', function(){
                    this.model.set( 'pprice', 1 );
                    this.model.set( 'euprice', 1 );
                    assert.equal( parseFloat( this.model.getMarge() ), 0.00);
                });

                it('should calculate marge based on selling price and buying price', function () {
                    this.model.set( 'pprice', 1 );
                    this.model.set( 'euprice', 100 );
                    assert.equal( parseFloat( this.model.getMarge() ), 99.00 );

                    this.model.set( 'pprice', 50 );
                    this.model.set( 'euprice', 100 );
                    assert.equal( parseFloat( this.model.getMarge() ), 50.00 );

                    this.model.set( 'pprice', 100 );
                    this.model.set( 'euprice', 1 );
                    assert.equal( parseFloat( this.model.getMarge() ), -9900.00 );

                    this.model.set( 'pprice', 100 );
                    this.model.set( 'euprice', 50 );
                    assert.equal( parseFloat( this.model.getMarge() ), -100.00 );
                });
            });

            describe('parsing server result', function(){
                it('supports results without an entityname object', function(){
                    var result = this.model.parse({testdata:'test'});
                    assert.equal( result.testdata, 'test' );
                });

                it('supports results with an entityname object, as seen in jsonapi.org spec', function(){
                    var result = this.model.parse({doc:{testdata:'test'}});
                    assert.equal( result.testdata, 'test' );
                });

                it('sets an empty personal_notes object as default', function(){
                    var result = this.model.parse({testdata:'test'});
                    assert.isObject( result.personal_notes );
                });
            });

            describe('saving', function () {
                it('should call validate', function () {
                    this.model.validate = sinon.spy();

                    this.model.set( 'publish', 0 );
                    this.model.save();

                    expect( this.model.validate ).to.have.been.calledOnce;
                });

                it('should be valid with correct values', function () {
                    this.model.set( 'publish', 0 );
                    assert.isTrue( this.model.isValid() );
                });

                it('should be invalid without Name', function () {
                    this.model.set( 'publish', 0 );
                    this.model.unset( 'Name' );
                    assert.isFalse( this.model.isValid() );
                });

                it('should be invalid without ProdId', function () {
                    this.model.set( 'publish', 0 );
                    this.model.unset( 'ProdId' );
                    assert.isFalse( this.model.isValid() );
                });
            } );

            describe('publishing', function () {
                it('returns a promise', function(){
                    var result = this.model.save();
                    assert.isFunction( result.then );
                });

                it('should call validate', function () {
                    this.model.validate = sinon.spy();

                    this.model.publish();

                    this.model.validate.should.have.been.calledOnce;
                });

                it('should be valid with correct values', function () {
                    assert.isTrue( this.model.isPublishValid() );
                });

                it('should be invalid without Vendor', function () {
                    this.model.unset( 'Vendor' );
                    assert.isFalse( this.model.isPublishValid() );
                } );

                it('should be invalid without pprice', function () {
                    this.model.unset( 'pprice' );
                    assert.isFalse( this.model.isPublishValid() );
                });

                it('should be invalid without Sprice', function () {
                    this.model.unset( 'Sprice' );
                    assert.isFalse( this.model.isPublishValid() );
                });

                it('should be invalid without euprice', function () {
                    this.model.unset( 'euprice' );
                    assert.isFalse( this.model.isPublishValid() );
                });

                it('should be invalid without Cat', function () {
                    this.model.unset( 'Cat' );
                    assert.isFalse( this.model.isPublishValid() );
                });

                it('should be invalid without product_supplier', function () {
                    this.model.unset( 'product_supplier' );
                    assert.isFalse( this.model.isPublishValid() );
                });

                it('should be invalid without Name', function () {
                    this.model.unset( 'Name' );
                    assert.isFalse( this.model.isPublishValid() );
                });

                it('should be invalid without ProdId', function () {
                    this.model.unset( 'ProdId' );
                    assert.isFalse( this.model.isPublishValid() );
                });
            });

            describe('reverting', function() {
                var ctx;
                beforeEach(function(){
                    ctx = {
                        collection: new Backbone.Collection()
                    };
                });

                it('returns a promise', function(){
                    var result = this.model.revert();
                    assert.isFunction( result.then );
                });

                it('should unset the products temporary flag', function(){
                    this.model.revert( null, ctx );
                    assert.isUndefined( this.model.get( 'temporary' ) );
                } );

                it('should make a DELETE call to the server', function(){
                    this.model.revert( null, ctx );

                    expect( spy ).to.have.been.calledOnce;
                    var args = $.ajax.getCall(0).args[0];

                    assert.equal( args.type, 'DELETE');
                });
            });
        }
    );
});
