define( function ( require ) {
    'use strict';

    var TTC = require( 'order/TradeTracker/collections/affiliateorders' );

    describe( 'the TradeTracker affiliate orders collection', function() {

        it ( 'can find models which are checked', function() {
            var c = new TTC([
                {id:1, checked:true},
                {id:2, checked:false},
                {id:3, checked:'false'},
                {id:4, checked:true}
            ]);

            assert.equal( 2, c.getChecked().length );
        });
    });
} );