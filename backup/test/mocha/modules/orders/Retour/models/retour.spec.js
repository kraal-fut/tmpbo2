define( function ( require ) {
    'use strict';
    var Model = require( 'order/Retour/models/retour' );

    describe( 'The Retourmodel', function() {
        it( 'sets sets received for retours where status is less than 11', function() {
            var m = new Model();
            m.set( 'status', 1 );
            m.setReceived();

            assert.equal( m.get( 'status' ), 12);
        });

        it( 'does not set received for retours where status is more than 11', function(){
            var m = new Model();
            m.set( 'status', 13 );
            m.setReceived();

            assert.equal( m.get( 'status' ), 13);
        } );
    } );
});