/*global define:false, describe: false, it: false, expect: false */
define( [], function() {
    "use strict";
    return {
        "id":"6003-0085-N1011",
        "Shop":"store",
        "supplier":"eigen",
        "Vendor":"Adidas",
        "ProdId":"DE-A155\/006054",
        "Name":"T-sight S Red LST Contrast Silver Zonnebril",
        "Oms":"<font color=\"#AE0000\"><strong>Opruiming! Op=Op.<\/font><\/strong> Mooie sportbril van Adidas, speciaal ontworpen om precies te voldoen aan de behoeften van golfers en de verwachtingen die zij hebben van hun uitrusting.",
        "info":"fietsbrillen,zonnebrillen,zomeropruimingfietsen2",
        "Cat":"Kleding_D_01",
        "pprice":"79.500",
        "Sprice":"165.00",
        "euprice":"85.0800",
        "Stock":"uitverkocht",
        "MinUnits":null,
        "Units":null,
        "Updated":"2013-06-27 12:23:50",
        "Visible":"Y",
        "minimum_stock":"0",
        "maximum_stock":"0",
        "personal_notes":"",
        "stock_avg_depreciation":"0.00",
        "tax_rate":null,
        "own_stock":"0",
        "own_stock_updated":"2013-06-03 06:52:04",
        "created":"2010-11-26 15:49:51",
        "pricedrop":"0",
        "actual_euprice":"85.080",
        "pr":"1",
        "briefpost":"0",
        "price_management_euprice":"0.0000",
        "free_stock":"0",
        "blocked_stock":"0",
        "in_reserve_stock":"0",
        "active":"0",
        "stock_updated":"2012-07-27 12:01:01",
        "frozen":"1",
        "action_date_start":null,
        "action_date_end":null,
        "video":"5Q2A-0YTd5k",
        "exclude_vendor_open":"0",
        "exclude_vendor_close":"0",
        "exclude_vendor_open_date_start":null,
        "exclude_vendor_open_date_end":null,
        "exclude_vendor_close_date_start":null,
        "exclude_vendor_close_date_end":null,
        "fixed_min_stock":"0",
        "fixed_max_stock":"0",
        "fixed_stock_date_start":null,
        "fixed_stock_date_end":null,
        "fixed_stock_on":"0",
        "backorder_stock":"0",
        "backorder_stock_white_line":"0",
        "draft":"0",
        "publish":"1",
        "Type":"0",
        "bo2_cms_labels":'[{"label":1}]',
        "parentId":null,
        "variations":[],
        "alternative":[],
        "frontoffice_menu_product_pseudo":[
            {
                "id":"284",
                "prodlevid":"6003-0085-N1011",
                "frontoffice_menu_id":"6207"
            },
            {
                "id":"285",
                "prodlevid":"6003-0085-N1011",
                "frontoffice_menu_id":"6351"
            },{
                "id":"286",
                "prodlevid":"6003-0085-N1011",
                "frontoffice_menu_id":"6496"
            },
            {
                "id":"287",
                "prodlevid":"6003-0085-N1011",
                "frontoffice_menu_id":"6530"
            },
            {
                "id":"288",
                "prodlevid":"6003-0085-N1011",
                "frontoffice_menu_id":"6754"
            }
        ],
        "log_pr_update":[],
        "product_additional_quant":{
            "prodlevid":"6003-0085-N1011",
            "bundleid":"0",
            "min_quant":"1",
            "max_quant":"9999",
            "step_quant":"1"
        },
        "product_description":{
            "ProdLevId":"6003-0085-N1011",
            "dsupplier":"eigen",
            "Shop":"store",
            "dprodid":"DE-A155\/006054",
            "dvendor":"Adidas",
            "Image": {"filename": "6003-0085-N1011.jpg"},
            "Naam":"",
            "Koms":"<font color=\"#AE0000\"><strong>Opruiming! Op=Op. <\/font><\/strong>Mooie sportbril van Adidas, speciaal ontworpen om precies te voldoen aan de behoeften van golfers en de verwachtingen die zij hebben van hun uitrusting.",
            "Loms":"<p>Mooie sportbril van Adidas, speciaal ontworpen om precies te voldoen aan de behoeften van golfers en de verwachtingen die zij hebben van hun uitrusting.<br><br><h2>Specificaties Adidas T-sight S<\/h2><ul><li>Decentered Vision Advantage\u2122 PC Lens \u2013 sterke en krasbestendige lens <\/li><li>Quick-Change Lens System\u2122 - de lens is gemakkelijk te verwisselen<\/li><li>Double-Snap Nose Bridge\u2122 - individueel aan te passen neusstukjes<\/li><li>Auto Fit \u2013 uitstekende pasvorm<\/li><li>Grip Systems - non-slip en drukvrije pasvorm <\/li><li>Quick-Release Hinge\u2122 - brillenpootjes laten los bij hoge impact om beschadiging te voorkomen<\/li><li>Microfiber tasje<\/li><li>Hardcase <\/li><\/ul><strong>Beschrijving<\/strong><br><br>De T-sight, gebaseerd op de award winnaar, Supernova, is zorgvuldig aangepast voor gebruik tijden golf. De lens is ontworpen om precies te voldoen aan de behoeften van golfers en de verwachtingen die zij hebben van hun uitrusting. De T-sight is een uitermate betrouwbare sportbril. <br><br><strong>Technologie\u00ebn<\/strong> <br><br>Decentered Vision Advantage\u2122 PC Lens<br>Decentreren van het optische centrum resulteert in vervorming van het zicht. Echter in combinatie met sterk gebogen filters is het zicht optimaal en het oog optimaal omhuld. Vision Advantage \u2122 PC filters komen overeen met optische kwaliteit niveau 1 en garanderen totale bescherming tegen UVA-,-B en-C straling.<br><br>Quick-Change Lens System\u2122<br>Simpel en snel je lens verwisselen zodat je je supersnel aan kan passen aan alle licht- en weersomstandigheden. Geen Adidas drager hoeft ooit overrompeld te worden door veranderende lichtomstandigheden, met de vele lens tinten die beschikbaar zijn.<br><br>Double-Snap Nose Bridge \u2122<br>Voor het optimaliseren van de positie biedt de Double-Snap Nose Bridge \u2122 tweevoudige hoogteverstelling van de neusbrug om deze aan de individuele gebruiker aan te passen. De Double-Snap Nose Bridge \u2122 is ook gemaakt van een anti-slip, zacht materiaal dat het veilig en comfortabel aan doet voelen.<br><br>Auto Fit <ul><li>SPX \u2122 Flexibiliteit - SPX \u2122 is een extra licht, onbreekbaar materiaal met onovertroffen duurzaamheid, en is bestand tegen extreme temperaturen. Ook is het materiaal zeer flexibel. <\/li><li>Flex Zones \u2122 - De Flex Zones \u2122 opgenomen in het SPX \u2122 frame geven extra flexibiliteit waardoor de bril rekbaar is en zich optimaal aanpast aan de drager.<\/li><\/ul>Grip Systems <ul><li> Traction Grip \u2122 - Bestaat uit een zachte materiaal met grip, ge\u00efntegreerd in de brillenpootjes die een optimale non-slip, druk vrije en stabiele pasvorm bieden.<\/li><li>Geribd Grip System - De geribbelde grip systeem op brillenpootjes biedt een veilige drukvrije pasvorm en houdt de bril op zijn plaats.<\/li><\/ul>Quick-Release Hinge \u2122<br>In plaats van dat het frame beschadigd raakt, laten de brillenpootjes los bij hoge impact. Dit gepatenteerde release mechanisme voor de scharnieren van de brillenpootjes voorkomen dat het frame breekt of beschadigd. De brillenpootjes zijn gemakkelijk en snel opnieuw te bevestigen.<\/p>",
            "Updated":"2013-06-27 12:23:50"
        },
        "product_eancode":[
            {
                "ProdLevId":"6003-0085-N1011",
                "Eancode":"692740468198",
                "product_eancode_id":"27584",
                "supplier_id":"0"
            }
        ],
        "product_free":[],
        "product_supplier":[
            {
                "id":"34053",
                "prodlevid":"6003-0085-N1011",
                "supplier_id":"D1908c",
                "supplier_prodlevid":"DE-A155\/006054",
                "pprice":"79.5000"
            }
        ],
        "related":[],
        "vendor":{
            "name":"Adidas",
            "short_info":"",
            "info":"",
            "contact":"",
            "vendor_page":"",
            "vendor_logo":"",
            "vendor_banner":"",
            "vendor_banner_link":null,
            "vendor_banner_middle":"",
            "vendor_banner_middle_link":null,
            "size_table":"",
            "id":"6003",
            "country":"",
            "bron_name":"",
            "bron_href":"",
            "title":"Adidas fietsbril, skibril of zonnebril kopen?|Futurumshop.nl",
            "description":"Adidas brillen assortiment. O.a. adidas fietsbrillen, adidas zonnebrillen. Gratis Verzending | Gratis Retourneren bij Futurumshop.nl!","keyword":"Adidas fietsbril,adidas fietsbrillen,adidas skibril,adidas skibrillen,adidas zonnebril,adidas zonnebrillen,zonnebril adidas,zonnebrillen adidas",
            "status":"1",
            "stock_depreciation_period":null,
            "stock_min_multiplier":null,
            "stock_max_multiplier":null,
            "stock_min":null,
            "stock_max":null,
            "unit4_min_max_rule":"max",
            "unit4_equel_rule":"lower",
            "unit4_set_min_max_stock_rule":"0",
            "unit4_set_min_max_stock_value":"0"
        },
        "category":{
            "CatId":"Kleding_D_01",
            "shop":"store",
            "CatIdOwner":"Kleding_D",
            "Oms":"Sportbrillen",
            "Tax":"0.00",
            "Updated":"2013-06-27 12:23:50",
            "use_custom_tax_rate":"0",
            "Discount":"0.00",
            "unit4_id":"2047"
        },
        "product_features_pseudo":[
            {
                "id":"89807",
                "prodlevid":"6003-0085-N1011",
                "feature_id":"3",
                "updated":"2013-06-27 12:23:50",
                "value":"696"
            },
            {
                "id":"32555",
                "prodlevid":"6003-0085-N1011",
                "feature_id":"4",
                "updated":"2013-06-27 12:23:50",
                "value":"23"
            },
            {
                "id":"32556",
                "prodlevid":"6003-0085-N1011",
                "feature_id":"6",
                "updated":"2013-06-27 12:23:50",
                "value":"9"
            }
        ],
        "product_special_price":null,
        "product_specification":null,
        "seo_prodlevid":null
    };
});
