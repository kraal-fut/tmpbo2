/*global define:false, describe: false, it: false, expect: false */
define( ['fixtures/completeproduct','fixtures/product','fixtures/productcollection'], function(completeproduct,product,productcollection) {
    'use strict';
    return {
        completeproduct:completeproduct,
        product:product,
        productcollection:productcollection
    };
});