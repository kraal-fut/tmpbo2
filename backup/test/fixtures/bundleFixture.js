/*global define:false, describe: false, it: false, expect: false */
define( [], function() {
    "use strict";
    return {
        "_id": "53ecc274d36653c340d63cd6",
        "prodlevid":"6040-0002-004-N0701",
        "visibleInSearch":0,
        "enabled":0,
        "title":"dit is een test title",
        "updated":{
            "sec":1408046219,
            "usec":548000
        },
        "bundledproducts":[
            {
                "quenumber":1,
                "prodlevid":"6124-0012-N0610",
                "min":"1",
                "max":"9999",
                "per":"1",
                "discount":2,
                "discounttype":0,
                "tip":0,
                "enabled":1,
                "margin":{
                    "euro":15.456328,
                    "percentage":41.475317879454
                },
                "index":"16124-0012-N0610"
            }
        ]
    };
});