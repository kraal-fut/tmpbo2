/*global define:false, describe: false, it: false, expect: false */
define( [], function() {
    "use strict";
    return {
        "cms_visibleBlocks_obligated":true,
        "cms_visibleBlocks_info":true,
        "cms_visibleBlocks_content":true,
        "cms_visibleBlocks_pricemanager":true,
        "cms_visibleBlocks_stock":true,
        "cms_visibleBlocks_status":true,
        "cms_itemsPerPage":20,
        "frontofficemenu":{},
        "labels":[{"id":"1","Name":"Waiting for content","Color":"#FF0000"}],
        "INTERN_ORDER_STATUS":"20",
        "BASE_URL":"//fo2.patrickr.dev/",
        "PRODUCT_URL":"//fo2.patrickr.dev/?action=product&prodlevid="
    };
});
