BO2 [![Build Status](https://magnum.travis-ci.com/futurumshop/BO2.svg?token=YpxJpigcjZmKVMZjTmzF&branch=master)](https://magnum.travis-ci.com/futurumshop/BO2)
===

FuturumShop Backoffice2 based on Backbone

To install (assuming [Node.JS](http://nodejs.org/) and [NPM](https://www.npmjs.org/) are installed):

* Clone the repository:   `git clone https://github.com/futurumshop/BO2.git`
* `cd BO2`
* `npm install`


To run unittests in CI mode:     
* `npm test`


To run unittests in development mode:     
* `grunt karma:daemon`

To build the dist version:    
* `grunt`


To prevent merge conflicts. You should only commit a dist build, when it is actually going to be installed.
Don't forget to bump the version number in `package.json` when doing a release.
Run tests locally and on travis before installing.

Some important packages documentations we are using:
-------------------------------------

[Backbone](http://backbonejs.com)       Main framework   
[Marionette](http://marionettejs.com/docs/current)      Additional framework helpers   
[Backbone Epoxy](http://epoxyjs.org/documentation.html)     Model-binding    
[Backbone Validator](https://github.com/fantactuka/backbone-validator)  Validation    
[Summernote](https://github.com/HackerWins/summernote)  Wysiwyg editor    
[When.js](https://github.com/cujojs/when)  Javascript Promises, Deferreds and async tools    
[Require.js](http://requirejs.org/) Module loader system     
[Handlebars.js](http://handlebarsjs.com/) Template system, not so much used since we use model binding     


Testing packages:
-----------------

[Karma](http://karma-runner.github.io/) Test runner    
[Mocha](http://visionmedia.github.io/mocha/) Test framework    
[Chai](http://chaijs.com/) Assertion library     
[Sinon](http://sinonjs.org/) Stubs, Spies, Mocks     
[Chai-as-promised](https://github.com/domenic/chai-as-promised/) Testing promises     
