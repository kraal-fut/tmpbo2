module.exports = function( grunt ) {
    'use strict';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ['dist/', 'test/reports'],
        jshint: ['app/**/*.js'],

        requirejs: {
            release: {
                options: {
                    optimize: 'uglify2',
                    mainConfigFile: 'app/config.js',
                    include: ['main'],
                    out: 'dist/source.min.js',
                    findNestedDependencies: true,
                    name: 'almond',
                    baseUrl: 'app',
                    insertRequire: ['main'],
                    wrap: true,
                    wrapShim: true,
                    generateSourceMaps: true,
                    preserveLicenseComments: false
                }
            }
        },

        styles: {
            'dist/styles/styles.css': {
                src: 'app/styles/index.css',
                paths: ['/styles','/fonts'],
                forceRelative: '/'
            }
        },

        cssmin: {
            release: {
                files: {
                    'dist/styles/styles.min.css': ['dist/styles/styles.css']
                }
            }
        },

        processhtml: {
            release: {
                options: {
                    data: {
                        version: '<%= pkg.version %>'
                    }
                },
                files: {
                    'dist/index.html': ['index.html']
                }
            }
        },

        copy: {
            release: {
                files: [
                    {expand: true, cwd: 'app/fonts/', src: ['**'], dest: 'dist/fonts/'},
                    {expand: true, cwd: 'app/img/', src: ['**'], dest: 'dist/img/'},
                    { src: ['.htaccess'], dest: 'dist/.htaccess' }
                ]
            }
        },

        compress: {
            release: {
                options: {
                    archive: 'dist/source.min.js.gz'
                },

                files: ['dist/source.min.js']
            }
        },

        karma: {
            daemon: {
                configFile: 'karma.conf.js',
                options: {
                    singleRun: false
                }
            },
            run: {
                configFile: 'karma.conf.js',
                options: {
                    singleRun: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-requirejs');

    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-processhtml');

    grunt.loadNpmTasks('grunt-bbb-styles');
    grunt.registerTask('default', [
        //'clean',
        'jshint',
        'processhtml',
        'requirejs',
        'copy',
        'styles',
        'cssmin'
    ]);
};