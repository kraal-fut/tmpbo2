import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('fut-dataimport', 'Integration | Component | fut dataimport', {
  integration: true
});

test('it renders', function(assert) {
    //  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{fut-dataimport}}`);

  assert.equal(this.$().text().trim(), 'Drag file to process');

  // Template block usage:
  this.render(hbs`
    {{#fut-droparea}}
      template block text
    {{/fut-droparea}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
