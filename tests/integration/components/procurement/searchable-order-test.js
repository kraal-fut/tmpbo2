import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('procurement/searchable-order', 'Integration | Component | procurement/searchable order', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{procurement/searchable-order}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#procurement/searchable-order}}
      template block text
    {{/procurement/searchable-order}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
