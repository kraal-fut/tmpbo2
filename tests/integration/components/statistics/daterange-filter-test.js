import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('statistics/daterange-filter', 'Integration | Component | statistics/daterange filter', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(0);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  //this.render(hbs`{{statistics/daterange-filter}}`);

  //assert.equal(this.$().text().trim(), '');

  // Template block usage:
  //this.render(hbs`
  //  {{#statistics/daterange-filter}}
  //    template block text
  //  {{/statistics/daterange-filter}}
  //`);
  //
  //assert.equal(this.$().text().trim(), 'template block text');
});
