import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('shop/landing/image-sidebar', 'Integration | Component | shop/landing/image sidebar', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{shop/landing/image-sidebar}}`);

  assert.equal(this.$().text().trim(), 'Images');

  // Template block usage:
  this.render(hbs`
    {{#shop/landing/image-sidebar}}
      template block text
    {{/shop/landing/image-sidebar}}
  `);

  assert.equal(this.$().text().trim(), 'Images');
});
