import { moduleForModel, test } from 'ember-qunit';

moduleForModel('futurumclub/newproducts', 'Unit | Serializer | futurumclub/newproducts', {
  // Specify the other units that are required for this test.
  needs: ['serializer:futurumclub/newproducts', 'model:product/image']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  var record = this.subject();

  var serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
