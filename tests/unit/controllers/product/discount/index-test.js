import {moduleFor,test} from 'ember-qunit';

moduleFor('controller:product/discount/index', 'Unit | Controller | product/discount/index', {
  needs: ['controller:pagination']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});

test('it should return true after call loadingCode', function(assert) {
  var controller = this.subject();
  controller.loadingCode(true);
  assert.ok(controller.get('isLoadingCode'));
});

test('it should return false after call loadingCode', function(assert) {
  var controller = this.subject();
  controller.loadingCode(false);
  assert.ok(!controller.get('isLoadingCode'));
});
