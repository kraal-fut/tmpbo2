import { moduleFor, test } from 'ember-qunit';

moduleFor('controller:product/productquestions/edit', 'Unit | Controller | product/productquestions/edit', {
  // Specify the other units that are required for this test.
  // needs: ['controller:foo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});

test('it should computeds mailBody get correctly', function(assert) {
  var controller = this.subject();
  controller.set('model', {mail_body: "Mail body"});
  assert.equal(controller.get('mailBody'), "Mail body");
  controller.set('model.hasEmptyMailBody', true);
  controller.set('session', {userSettings:{emailSignature: "Some Signature"}});
  assert.equal(controller.get('mailBody'), "Some Signature");
});

test('it should computeds mailBody set correctly', function(assert) {
  var controller = this.subject();
  controller.set('mailBody', 'Mail Body');
  assert.equal(controller.get('mailBody'), "Mail Body");
  assert.equal(controller.get('email_answer'), "Mail Body");
});
