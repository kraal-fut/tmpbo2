import {
  moduleFor,
  test
} from 'ember-qunit';

moduleFor('controller:product/productquestions/index', 'Unit | Controller | product/productquestions/index', {
  needs: ['controller:pagination']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});
