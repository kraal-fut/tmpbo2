import {
  moduleFor,
  test
} from 'ember-qunit';

moduleFor('controller:product/content/index', 'Unit | Controller | product/content/index', {
   needs: ['controller:pagination']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});
