import {
  moduleFor,
  test
} from 'ember-qunit';

moduleFor('controller:product/vendors/index', 'Unit | Controller | product/vendors/index', {
  needs: ['controller:pagination']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});
