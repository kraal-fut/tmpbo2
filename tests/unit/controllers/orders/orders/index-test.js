import {moduleFor, test} from 'ember-qunit';

moduleFor('controller:orders/orders/index', 'Unit | Controller | orders/orders/index', {
  // Specify the other units that are required for this test.
   needs: ['controller:pagination']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});
