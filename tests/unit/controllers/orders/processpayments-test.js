import {moduleFor, test} from 'ember-qunit';

moduleFor('controller:orders/processpayments', 'Unit | Controller | orders/processpayments', {
   needs: []
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});

test('it should computeds greenOrders correctly', function(assert) {
  var controller = this.subject();
  controller.set('content', [{paymenttype: 'A', valid: 0}, {paymenttype: 'B', valid: 0}]);
  assert.equal(controller.get('greenOrders'), 0);
  controller.set('content', [{paymenttype: 'A', valid: 0}, {paymenttype: 'C', valid: 1}]);
  assert.equal(controller.get('greenOrders'), 1);
});

test('it should computeds redOrders correctly', function(assert) {
  var controller = this.subject();
  controller.set('content', [{paymenttype: 'C', valid: 1}, {paymenttype: 'C', valid: 1}]);
  assert.equal(controller.get('redOrders'), 0);
  controller.set('content', [{paymenttype: 'C', valid: 0}, {paymenttype: 'C', valid: 0}]);
  assert.equal(controller.get('redOrders'), 2);
});

test('it should computeds blackOrders correctly', function(assert) {
  var controller = this.subject();
  controller.set('content', [{paymenttype: 'D'}, {paymenttype: 'A'}]);
  assert.equal(controller.get('blackOrders'), 1);
});

test('it should computeds ordersToProcess correctly', function(assert) {
  var controller = this.subject();
  controller.set('content', [{process: true}, {process: false}, {process: true}, {process: false}]);
  assert.deepEqual(controller.get('ordersToProcess'), [{process:true}, {process: true}]);
});

test('it should computeds hasOrdersToProcess', function(assert) {
  var controller = this.subject();
  controller.set('content', [{process: true}, {process: false}, {process: true}, {process: false}]);
  assert.equal(controller.get('hasOrdersToProcess'), 2);
});
