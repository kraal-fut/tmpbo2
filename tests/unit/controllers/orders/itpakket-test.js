import {moduleFor, test} from 'ember-qunit';

moduleFor('controller:orders/itpakket', 'Unit | Controller | orders/itpakket', {
  // Specify the other units that are required for this test.
  // needs: ['controller:foo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});

test('it should computeds hasExportable correctly', function(assert) {
  var controller = this.subject();
  controller.set('model', {});
  assert.ok(!controller.get('hasExportable'));
  controller.set('model', [{},{}]);
  assert.ok(controller.get('hasExportable'));
});

test('it should computeds exportableOrders correctly', function(assert) {
  var controller = this.subject();
  controller.set('model', [{}, {}]);
  assert.equal(controller.get('exportableOrders'), 2);
});
