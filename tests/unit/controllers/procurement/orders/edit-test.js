import { moduleFor, test } from 'ember-qunit';

moduleFor('controller:procurement/orders/edit', 'Unit | Controller | procurement/orders/edit', {
  // Specify the other units that are required for this test.
  // needs: ['controller:foo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});

test('it should computeds mailBody correctly', function(assert) {
  var controller = this.subject();
  controller.set('model', {mail_body: "Mail Body"});
  assert.equal(controller.get('mailBody'), "Mail Body");
  controller.set('model.hasEmptyMailBody', true);
  controller.set('session', {userSettings: {emailSignature: 'Some Signature'}});
  assert.equal(controller.get('mailBody'), "Some Signature");
});

test('it should computeds getTotal correctly', function(assert) {
  var controller = this.subject();
  controller.set('model', {total: 10});
  assert.equal(controller.get('getTotal'), 10);
  controller.set('model.lines', true);
  controller.set('model.totalValue', 20);
  assert.equal(controller.get('getTotal'), 20);
});
