import {moduleFor, test} from 'ember-qunit';

moduleFor( 'controller:procurement/orders/item', 'Unit | Controller | procurement/orders/item', {
  // Specify the other units that are required for this test.
  // needs: ['controller:foo']
} );

// Replace this with your real tests.
test( 'it exists', function( assert ) {
  var controller = this.subject();
  assert.ok( controller );
});

test( 'it should computeds getSearched correctly', function( assert ) {
  var controller = this.subject();
  controller.set( 'model', {lines: [{prodlevid: '1111', prodid: '2222'}, {prodlevid: '3333', prodid: '4444'}]} );
  controller.set( 'parentController', {q: '1111'} );
  assert.deepEqual( controller.get( 'getSearched' ), [{prodlevid: '1111', prodid: '2222'}] );
  controller.set( 'parentController.q', '2222' );
  assert.deepEqual( controller.get( 'getSearched' ), [{prodlevid: '1111', prodid: '2222'}] );
  controller.set('parentController.q', '3333' );
  assert.deepEqual( controller.get( 'getSearched' ), [{prodlevid: '3333', prodid: '4444'}] );
  controller.set('parentController.q', '4444' );
  assert.deepEqual( controller.get( 'getSearched' ), [{prodlevid: '3333', prodid: '4444'}] );
  controller.set('parentController.q', '5555' );
  assert.deepEqual( controller.get( 'getSearched' ), [] );
});

test( 'it should computeds isSearched correctly', function( assert ) {
  var controller = this.subject();
  controller.set( 'model', {lines: [{prodlevid: '1111', prodid: '2222'}, {prodlevid: '3333', prodid: '4444'}]} );
  controller.set( 'parentController', {q: '5555'} );
  assert.ok( !controller.get( 'isSearched' ) );
  controller.set( 'parentController.q', '1111' );
  assert.ok( controller.get( 'isSearched' ) );
});
