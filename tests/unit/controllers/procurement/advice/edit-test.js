import {moduleFor, test} from 'ember-qunit';

moduleFor('controller:procurement/advice/edit', 'Unit | Controller | procurement/advice/edit', {
  // Specify the other units that are required for this test.
  // needs: ['controller:foo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});

test('it should computeds totalValue correctly', function(assert) {
  var controller = this.subject();
  controller.set('model', [{pprice: 1, advice: 3}, {pprice: 5, advice: 7}]);
  assert.equal(controller.get('totalValue'), 38);
});

test('it should computeds selectedValue correctly', function(assert) {
  var controller = this.subject();
  controller.set('model', [{pprice: 1, advice: 3, selected: true}, {pprice: 5, advice: 7, selected: true}]);
  assert.equal(controller.get('selectedValue'), 38);
});

test('it should computeds totalAdvice correctly', function(assert) {
  var controller = this.subject();
  controller.set('model', [{advice: 1}, {advice: 3}, {advice: 5}, {advice: 7}]);
  assert.equal(controller.get('totalAdvice'), 16);
});

test('it should computeds selectedAdvice correctly', function(assert) {
  var controller = this.subject();
  controller.set('model', [{advice: 1, selected: true}, {advice: 3, selected: true},
    {advice: 5, selected: true}, {advice: 7, selected: true}]);
  assert.equal(controller.get('selectedAdvice'), 16);
});
