import {moduleFor, test} from 'ember-qunit';

moduleFor('controller:procurement/advice/index', 'Unit | Controller | orders/advice/index', {
  // Specify the other units that are required for this test.
  // needs: ['controller:foo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});

test('it should selectAll return false', function(assert) {
  var controller = this.subject();
  controller.set('model', [{selected: false}, {selected: true}, {selected: false}]);
  controller.selectAll();
  assert.deepEqual(controller.get('model'), [{selected: false}, {selected:false}, {selected: false}]);

});

test('it should selectAll return true', function(assert) {
  var controller = this.subject();
  controller.set('model', [{selected: false}, {selected: false}, {selected: false}]);
  controller.selectAll();
  assert.deepEqual(controller.get('model'), [{selected: true}, {selected:true}, {selected: true}]);
});
