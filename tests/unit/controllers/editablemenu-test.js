import { moduleFor, test } from 'ember-qunit';
import Ember from 'ember';

moduleFor('controller:editablemenu', 'Unit | Controller | editablemenu', {
  // Specify the other units that are required for this test.
  // needs: ['controller:foo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});

test('it should computeds fomenu correctly', function(assert) {
  var controller = this.subject();
  controller.parentController = Ember.A([]);
  controller.parentController.set('model', {fomenu: 'Menu'});
  controller.set('id', 1);
  assert.equal(controller.get('fomenu'), 'Menu');
});
