import {
  moduleFor,
  test
} from 'ember-qunit';

moduleFor('controller:shop/actions/index', 'Unit | Controller | shop/actions/index', {
  needs: ['controller:pagination']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});
