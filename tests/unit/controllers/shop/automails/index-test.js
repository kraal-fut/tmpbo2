import {moduleFor, test} from 'ember-qunit';

moduleFor('controller:shop/automails/index', 'Unit | Controller | shop/automails/index', {
  needs: ['controller:pagination']
});

test( 'it exists', function( assert ) {
  var controller = this.subject();
  assert.ok( controller );
});
