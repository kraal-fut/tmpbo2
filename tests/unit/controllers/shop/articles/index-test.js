import {
  moduleFor,
  test
} from 'ember-qunit';

moduleFor('controller:shop/articles/index', 'Unit | Controller | shop/articles/index', {
  needs: ['controller:pagination']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});
