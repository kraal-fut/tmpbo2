import {
  moduleFor,
  test
} from 'ember-qunit';

moduleFor('controller:shop/banners', 'Unit | Controller | shop/banners', {
  needs: ['controller:pagination']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});
