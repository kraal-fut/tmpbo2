import { moduleFor, test } from 'ember-qunit';

moduleFor('route:FutEmberRoute', 'Unit | Route | FutEmberRoute', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
});

test('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
});

test('it parses direct data', function (assert) {
    let route = this.subject();
    let result = route.parse({foo: 'bar'});

    assert.ok(result);
    assert.equal(result.foo, 'bar');
});

test('it parses data with an entityname', function (assert) {
    let route = this.subject({entityName: 'somethingnested'});
    let result = route.parse({somethingnested: {foo: 'qux'}});
    assert.equal(result[0].foo, 'qux');
});

test('it uses the data from the top level "data" if needed', function(assert) {
    let route = this.subject({entityName: 'somethingnested'});
    let result = route.parse({data: {foo: 'qux'}});
    assert.equal(result.foo, 'qux');
});

test('it transforms the data when a map function is given', function (assert) {
    let mapper = function (item) {item.foo++; return item;};

    let route = this.subject({entityName: 'somethingnested'});
    route.map = mapper;

    let rawData = [{foo: 1}, {foo: 5}, {foo: 6}];
    let expected = [{foo: 2}, {foo: 6}, {foo: 7}];

    let result = route.parse(rawData);
    assert.deepEqual(result, expected);
});
