import { moduleFor, test } from 'ember-qunit';

moduleFor('route:product/productquestions/index', 'Unit | Route | product/productquestions/index');

test('it exists', function(assert) {
    var route = this.subject();
    assert.ok(route);
});

test('it makes a correct date format like 2015-04-30 13:16:59', function( assert ) {
    let route = this.subject();
    var dateString = route.makeDate( new Date() );

    assert.ok( /^\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}$/.test( dateString ) );
});
