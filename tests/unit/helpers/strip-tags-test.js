import {stripTags} from 'bo2/helpers/strip-tags';
import { module, test } from 'qunit';

module('StripTagsHelper');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = stripTags('hello <div>world</div>');
  assert.ok(result);
  assert.equal( result, 'hello world');
});
