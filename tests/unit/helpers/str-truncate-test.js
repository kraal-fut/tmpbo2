import { strTruncate } from '../../../helpers/str-truncate';
import { module, test } from 'qunit';

module('Unit | Helper | str truncate');

// Replace this with your real tests.
test('it truncates a string given a certain length', function(assert) {
  var result = strTruncate(['Some relatively long string', 6]);
  assert.equal('Some r', result);
});

test('it adds an omission if given', function (assert) {
    var result = strTruncate(['Some relatively long string', 9, '...']);
    assert.equal('Some r...', result);
});

test('it returns the unmodified string when it is short enough', function (assert) {
    var result = strTruncate(['Some relatively long string', 600, '...']);
    assert.equal('Some relatively long string', result);
});

test('it strips tags if 4th parameter is trueish', function (assert) {
    var result = strTruncate(['Some <strong>relatively</strong> long string', 6, undefined, true]);
    assert.equal('Some r', result);
});
