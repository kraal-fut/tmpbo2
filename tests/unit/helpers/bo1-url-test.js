import {bo1Url} from 'bo2/helpers/bo1-url';
import { module, test } from 'qunit';

module('Bo1UrlHelper');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = bo1Url(42);
  assert.ok(result);
});
