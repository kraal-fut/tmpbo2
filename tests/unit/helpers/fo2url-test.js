import {fo2url} from 'bo2/helpers/fo2-url';
import { module, test } from 'qunit';

module('Fo2urlHelper');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = fo2url('9040022');
  assert.ok(result);
});
