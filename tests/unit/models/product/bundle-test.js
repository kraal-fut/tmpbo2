import { moduleFor, test } from 'ember-qunit';

moduleFor('model:product/bundle', 'Unit | Model | product/bundle');

test('it exists', function(assert) {
    var model = this.subject();
    assert.ok(model);
});

test('it should not validate name attribute', function(assert) {
    var model = this.subject({name: 'adaj', discount: 2, discountType: 'euro'});

    return model.validate().then( () => {
        assert.ok(false, 'promise should not be fulfilled');
    }).catch(function (ignore) {
        assert.ok(true, 'promise should not be fulfilled');
    });
});

test('it should validate name attribute', function (assert) {
    var model = this.subject({name: 'Some product name', discount: 2, discountType: 'euro'});

    return model.validate().then(function () {
        assert.ok(true, 'promise should be fulfilled and is');
    }).catch(function (ignore) {
        assert.ok(false, "promise should be fulfilled but isn't");
    });
});

test('it shouldn\'t validate discount attribute', function (assert) {
    var model = this.subject({name: 'Some product name', discountType: 'euro'});

    return model.validate().then(function () {
        assert.ok(false, 'promise should not be fulfilled');
    }).catch(function (ignore) {
        assert.ok(true, 'promise should not be fulfilled');
    });
});

test('it should validate discount attribute', function (assert) {
    var model = this.subject({name: 'Some product name', discount: 2, discountType: 'euro'});
    return model.validate().then(function () {
        assert.ok(true, 'promise should be fulfilled and is');
    }).catch(function (ignore) {
        assert.ok(false, "promise should be fulfilled but isn't");
    });
});
