import { moduleFor, test } from 'ember-qunit';

moduleFor( 'model:product/discount', 'Unit | Model | product/discount' );

test('it exists', function(assert)  {
  var model = this.subject();
  assert.ok(model);
});

test('it should computeds hasMultipleCodes correctly', function(assert) {
  var model = this.subject();
  assert.ok(!model.get('hasMultipleCodes'));

  model.set('coupon_rule', [{isMultipleCodes: false}, {isMultipleCodes: false}]);
  assert.ok(!model.get('hasMultipleCodes'));

  model.set('coupon_rule', [{isMultipleCodes: true}, {isMultipleCodes: false}]);
  assert.ok(model.get('hasMultipleCodes'));
});
