import Ember from 'ember';
import { moduleFor, test } from 'ember-qunit';

moduleFor('model:product/couponrule', 'Unit | Model | product/couponrule');

test('it exists', function (assert) {
    var model = this.subject();
    assert.ok(model);
});

test('it should compute type attribute correctly', function (assert) {
    var model = this.subject();
    assert.equal(model.get('type'), '');

    model.setProperties({
        coupon_rule_type: '2',
        couponRuleTypes: [{value: 1, label: 'one'}, {value: 2, label: 'two'}, {value: 3, label: 'three'}]
    });
    assert.equal(model.get('type'), 'two');

    model.set('coupon_rule_type', '4');
    assert.equal(model.get('type'), '');
});

test('it should compute hasArguments correctly', function (assert) {
    var model = this.subject({'couponRuleArgument': 'arg'});
    assert.equal(true, model.get('hasArguments'));
});

test('it should compute isSelectableArgument correctly', function (assert) {
    var model = this.subject({'couponRuleArgument': {options: 'opt'}});
    assert.equal(true, model.get('isSelectableArgument'));
});

test('it should compute isInputArgument correctly', function (assert) {
    var model = this.subject();
    assert.equal(false, model.get('isInputArgument'));
    model.setProperties({coupon_rule_type: '1024'});
    assert.equal(true, model.get('isInputArgument'));
});

test('it should compute inputLabel correctly', function (assert) {
    var model = this.subject({'isInputArgument': ''});
    assert.equal(model.get('inputLabel'), '');

    model.setProperties({couponRuleArgument: {label: 'foo'}, isInputArgument: 1});
    assert.equal(model.get('inputLabel'), 'foo');
});

test('it should compute hasButtons correctly', function (assert) {
    var model = this.subject();
    assert.equal(false, model.get('hasButtons'));

    model.set('couponRuleArgument', {});
    assert.equal(false, model.get('hasButtons'));

    model.set('couponRuleArgument', {buttons: "yes"});
    assert.equal(true, model.get('hasButtons'));
});

test('it should compute isMultipleCodes correctly', function (assert) {
    var model = this.subject({'coupon_rule_type': 64});
    assert.equal(false, model.get('isMultipleCodes'));

    model.set('coupon_rule_type', 128);
    assert.equal(true, model.get('isMultipleCodes'));
});

test('it should ruleTypeChanged', function (assert) {
    var model = this.subject({coupon_rule_type: 1, isMultipleCodes: true, optional_arguments: 1});
    assert.equal(model.get('optional_arguments'), 1);

    model.setProperties({optional_arguments: '', coupon_rule_type: 2});
    var obj = Ember.Object.create({});
    assert.deepEqual(model.get('optional_arguments'), obj);
});
