import {moduleFor, test} from 'ember-qunit';

moduleFor('model:product/imageupload', 'Unit | Model | product/imageupload');

test('it exists', function (assert) {
    var m = this.subject();
    assert.ok(m);
});

test('it can make actions', function (assert) {
    var m = this.subject({isExtra: false, invalid: false});

    assert.equal(m.get('action'), 'create');

    m.set('isExtra', true);
    assert.equal(m.get('action'), 'add');

    m.set('invalid', true);
    assert.equal(m.get('action'), 'none');
});

test('it checks for already existing main, when given image is not an extra image', function (assert) {
    var m = this.subject({isExtra: false, images: []});
    assert.equal(m.get('notExtraButHasMain'), false);

    m.set('isExtra', true);
    assert.equal(m.get('notExtraButHasMain'), false);

    m.set('isExtra', false);

    m.set('images', [{filename: '1.jpg'}]);
    assert.equal(m.get('notExtraButHasMain'), true);

    m.set('images', []);
    m.get('images').pushObject({filename: '1.jpg'});
    assert.equal(m.get('notExtraButHasMain'), true);
});
