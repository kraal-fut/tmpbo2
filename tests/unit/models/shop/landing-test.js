import { moduleFor, test } from 'ember-qunit';
import ENV from 'bo2/config/environment';

moduleFor('model:shop/landing', 'Unit | Model | shop/landing');

test('it exists', function (assert) {
    var model = this.subject();
    assert.ok(model);
});

test('it should observe header correctly', function (assert) {
    var model = this.subject();
    model.set('header', 'value');
    assert.equal(model.get('footer'), 'value');
});

test('it should correctly get computed type', function (assert) {
    var model = this.subject();
    assert.equal(model.get('type'), 'landingpage');

    model.set('vendorpage', true);
    assert.equal(model.get('type'), 'vendorpage');
    model.set('themepage', true);
    assert.equal(model.get('type'), 'themepage');
});

test('it should correctly set computed type', function (assert) {
    var model = this.subject();
    model.set('type', 'landingpage');
    assert.equal(false, model.get('vendorpage'));
    assert.equal(false, model.get('themepage'));

    model.set('type', 'vendorpage');
    assert.equal(true, model.get('vendorpage'));
    assert.equal(false, model.get('themepage'));

    model.set('type', 'themepage');
    assert.equal(false, model.get('vendorpage'));
    assert.equal(true, model.get('themepage'));
});

test('it should correctly computed previewLink', function (assert) {
    var model = this.subject();
    model.set('type', 'landingpage');
    model.set('_id', 1);
    assert.equal(model.get('previewLink'), ENV.APP.fo2url + '?action=html/1&preview=true');

    model.set('type', 'vendorpage');
    model.set('name', 'agu');
    assert.equal(model.get('previewLink'), ENV.APP.fo2url + '?action=vendor&name=agu&preview=true');

    model.set('type', 'themepage');
    assert.equal(model.get('previewLink'), ENV.APP.fo2url + '?action=theme&element=agu&preview=true');
});
