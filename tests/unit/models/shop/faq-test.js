import { moduleFor, test } from 'ember-qunit';

moduleFor( 'model:shop/faq', 'Unit | Model | shop/faq' );

test('it exists', function(assert) {
  var model = this.subject();
  assert.ok(model);
});

test('it should correctly observed category.title', function(assert) {
  var model = this.subject();
  model.set('category', {title: 'some'});
  assert.equal(model.get('category.image'), '');
  assert.equal(model.get('category.queue'), '');
});
