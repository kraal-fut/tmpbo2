import { moduleFor, test } from 'ember-qunit';

moduleFor('model:shop/synonym', 'Unit | Model | shop/synonym');

test('it exists', function (assert) {
    var model = this.subject();
    assert.ok(model);
});

test('it should not validate synonym property', function (assert) {
    var model = this.subject();
    return model.validate().then(function () {
        assert.ok(false, 'promise should not be fulfilled');
    }).catch(function (ignore) {
        assert.ok(true, 'promise should not be fulfilled');
    });
});

test('it should validate synonym property', function (assert) {
    var model = this.subject();
    model.set('synonym', 'syn');
    return model.validate().then(function () {
        assert.ok(true, 'promise should not be fulfilled');
    }).catch(function (ignore) {
        assert.ok(false, 'promise should not be fulfilled');
    });
});
