import { moduleFor, test } from 'ember-qunit';

moduleFor( 'model:shop/automail', 'Unit | Model | shop/automail' );

test('it exists', function(assert) {
  var model = this.subject();
  assert.ok(model);
});

test('it should be invalid when subject is too short', function(assert) {
  var model = this.subject({
    subject: 'sub',
    body: 'Some body attribute existed',
    fromwho: 'someaddr@gmail.com',
    reply_to: 'someaddr@gmail.com'
  });

  return model.validate().then( function() {
    assert.ok(false, 'promise should not be fulfilled');
  } ).catch( function( ignore ) {
    assert.ok(true, 'promise should not be fulfilled');
  } );
});

test('it should validate subject attribute', function(assert) {
  var model = this.subject({
    subject: 'valid subject',
    body: 'Some body attribute existed',
    fromwho: 'someaddr@gmail.com',
    reply_to: 'someaddr@gmail.com'
  });

  return model.validate().then( function() {
    assert.ok(true, 'promise should not be fulfilled');
  } ).catch( function( ignore ) {
    assert.ok(false, 'promise should not be fulfilled');
  } );
});

test('it should not validate body attribute', function(assert) {
  var model = this.subject({
    subject: 'Subject subject',
    body: 'not valid',
    fromwho: 'someaddr@gmail.com',
    reply_to: 'someaddr@gmail.com'
  });

  return model.validate().then( function() {
    assert.ok(false, 'promise should not be fulfilled');
  } ).catch( function( ignore ) {
    assert.ok(true, 'promise should not be fulfilled');
  } );
});

test('it should validate body attribute', function(assert) {
  var model = this.subject({
    subject: 'Subject subject',
    body: 'Some body attribute existed',
    fromwho: 'someaddr@gmail.com',
    reply_to: 'someaddr@gmail.com'
  });

  return model.validate().then( function() {
    assert.ok(true, 'promise should not be fulfilled');
  } ).catch( function( ignore ) {
    assert.ok(false, 'promise should not be fulfilled');
  } );
});

test('it should not validate fromwho attribute', function(assert) {
  var model = this.subject({
    subject: 'Subject subject',
    body: 'Some body attribute existed',
    fromwho: 'invalid',
    reply_to: 'someaddr@gmail.com'
  });

  return model.validate().then( function() {
    assert.ok(false, 'promise should not be fulfilled');
  } ).catch( function( ignore ) {
    assert.ok(true, 'promise should not be fulfilled');
  } );
});

test('it should validate fromwho attribute', function(assert) {
  var model = this.subject({
    subject: 'Subject subject',
    body: 'Some body attribute existed',
    fromwho: 'valid@gmail.com',
    reply_to: 'someaddr@gmail.com'
  });

  return model.validate().then( function() {
    assert.ok(true, 'promise should not be fulfilled');
  } ).catch( function( ignore ) {
    assert.ok(false, 'promise should not be fulfilled');
  } );
});

test('it should not validate reply_to attribute', function(assert) {
  var model = this.subject({
    subject: 'valid subject',
    body: 'Some body attribute existed',
    fromwho: 'someaddr@gmail.com',
    reply_to: 'invalid'
  });

  return model.validate().then( function() {
    assert.ok(false, 'promise should not be fulfilled');
  } ).catch( function( ignore ) {
    assert.ok(true, 'promise should not be fulfilled');
  } );
});

test('it should validate reply_to attribute', function(assert) {
  var model = this.subject({
    subject: 'valid subject',
    body: 'Some body attribute existed',
    fromwho: 'someaddr@gmail.com',
    reply_to: 'valid@gmail.com'
  });

  return model.validate().then( function() {
    assert.ok(true, 'promise should not be fulfilled');
  } ).catch( function( ignore ) {
    assert.ok(false, 'promise should not be fulfilled');
  } );
});
