import { moduleForModel, test } from 'ember-qunit';

moduleForModel('futurumclub/newproducts', 'Unit | Model | futurumclub/newproducts', {
  needs: ['model:product/image']
});

test('it exists', function(assert) {
  let model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
