import { moduleFor, test } from 'ember-qunit';

moduleFor('model:order/tradetracker', 'Unit | Model | order/tradetracker');

test('it exists', function (assert) {
    var model = this.subject();
    assert.ok(model);
});

test('it correct computeds safe', function (assert) {
    var model = this.subject();
    model.set('received.orderAmount', 90);
    model.set('ourData.total', 90);
    assert.ok(model.get('safe'));

    model.set('ourData.total', 80);
    assert.ok(!model.get('safe'));
});

test('it correct computeds warning', function (assert) {
    var model = this.subject();
    model.set('safe', true);
    assert.ok(!model.get('warning'));

    model.set('safe', false);
    assert.ok(model.get('warning'));
});

test('it correct computeds label', function (assert) {
    var model = this.subject();
    model.set('danger', true);
    model.set('safe', false);
    model.set('warning', false);
    assert.equal(model.get('label'), 'label-danger');

    model.set('danger', false);
    model.set('warning', true);
    assert.equal(model.get('label'), 'label-warning');

    model.set('warning', false);
    model.set('safe', true);
    assert.equal(model.get('label'), 'label-success');
});

test('it correct computeds regDate', function (assert) {
    var model = this.subject();
    model.set('received.registrationDate', "2015-06-01 15:38:37");
    assert.equal(model.get('regDate'), '01-06-2015 15:38');

    model.set('received.registrationDate', 'aa-aa-aaaa');
    assert.equal(model.get('regDate'), 'Invalid date');
});
