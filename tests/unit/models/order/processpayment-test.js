import { moduleFor, test } from 'ember-qunit';

moduleFor( 'model:order/processpayment', 'Unit | Model | order/processpayment' );

test('it exists', function(assert) {
  var model = this.subject();
  assert.ok(model);
});

test('it correct computeds shouldSendMail', function(assert) {
  var model = this.subject();
  assert.ok(!model.get('shouldSendMail'));

  model.set('pin', 'yes');
  assert.ok(model.get('shouldSendMail'));

  model.set('pin', '');
  model.set('creditcard', 'yes');
  assert.ok(model.get('shouldSendMail'));

  model.set('creditcard', '');
  model.set('american', 'yes');
  assert.ok(model.get('shouldSendMail'));
});

test('it handles error', function(assert) {
  var model = this.subject();

  var error = {
    responseJSON: {error: {}},
    responseText: 'Message 1'
  };

  model.handleError(error);
  assert.equal(model.get('error'), 'Message 1');
  assert.ok(!model.get('valid'));

  error.responseJSON.error.message = 'Message 2';
  model.handleError(error);
  assert.equal(model.get('error'), 'Message 2');
});

