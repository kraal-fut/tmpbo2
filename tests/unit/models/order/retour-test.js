import { moduleFor, test } from 'ember-qunit';

moduleFor('model:order/retour', 'Unit | Model | order/retour');

test('it exists', function (assert) {
    var model = this.subject();
    assert.ok(model);
});

test('it correct computeds statusText', function (assert) {
    var model = this.subject();
    model.set('status', 10);
    assert.equal(model.get('statusText'), 'Complete');

    model.set('status', 0);
    assert.equal(model.get('statusText'), 'Incomplete');

    model.set('status', 11);
    assert.equal(model.get('statusText'), 'Waiting for receiving');

    model.set('status', 12);
    assert.equal(model.get('statusText'), 'Received');

    model.set('status', 9999);
    assert.equal(model.get('statusText'), 'Archived');
});
