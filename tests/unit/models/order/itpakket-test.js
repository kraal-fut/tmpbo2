import { moduleFor, test } from 'ember-qunit';

var lines = [
  '          3STQOV028026001                                           1-12-2014                      P. v. Foo\n',
  '          3662266                                                    1\n',
  '          03   - Pakket ptp, <30kg, verhoogd aansprakelijk, lineair\n',
  '                                                                                EUR 500,00         Barstreet    39\n',
  '                0,000                                                                              1234 XL          BAZ                              NL\n',
  '          FuturumShop.com bestelling\n'
];

moduleFor('model:order/itpakket', 'Unit | Model | order/itpakket');

test('it exists', function (assert) {
  var model = this.subject({line:lines});
  assert.ok(model);
});


test('it can extract a trackandtrace code, a date and a name', function (assert) {
  var model = this.subject({line: lines});
  model.getTracknoDateAndName(model.get('line'));

  assert.equal(model.get('trackno'), '3STQOV028026001');
  assert.equal(model.get('date'), '1-12-2014');
  assert.equal(model.get('name'), 'P. v. Foo');

});

test('it can extract an ordernumber', function (assert) {
  var model = this.subject({line: lines});

  model.getOrderNoAndColli(model.get('line'));

  assert.equal(model.get('orderno'), '3662266');
});

test('it can extract an shipping code and shipping detail', function (assert) {
  var model = this.subject({line: lines});

  model.getShipcodeAndDetail(model.get('line'));

  assert.equal(model.get('shipcode'), '03  ');
  assert.equal(model.get('shipdetail'), 'Pakket ptp, <30kg, verhoogd aansprakelijk, lineair\n');
});

test('it can extract insurance and address', function (assert) {
  var model = this.subject({line: lines});

  model.getInsuranceAndAddress(model.get('line'));

  assert.equal(model.get('insuranceamount'), '500,00');
  assert.equal(model.get('address'), 'Barstreet 39');
});

test('it can extract weight, rembours and zipcode', function (assert) {
  var model = this.subject({line: lines});

  model.getWeightRembAndZip(model.get('line'));

  assert.equal(model.get('weight'), '0,000');
  assert.equal(model.get('rembammount'), undefined);
  assert.equal(model.get('zipetc'), '1234 XL BAZ NL');
});

test('it can find see if shipment is to customer', function (assert) {
  var model = this.subject({line: lines});

  model.set('orderno', 12345);
  model.getSupplierOrCustomerShipment();
  assert.equal(model.get('receiver'), 'C');
  assert.equal(model.get('order'), true);

});

test('it can find see if shipment is to supplier', function (assert) {
  var model = this.subject({line: lines});

  model.getSupplierOrCustomerShipment();

  assert.equal(model.get('receiver'), 'S');
  assert.equal(model.get('order'), false);

});


///
test('it should getTracknoDateAndName', function (assert) {
  var model = this.subject();
  var lines = ['3STQOV0000000 11-22-3333 jhj '];
  model.getTracknoDateAndName(lines);
  assert.equal(model.get('trackno'), '3STQOV0000000');
  assert.equal(model.get('date'), '11-22-3333');
  assert.equal(model.get('name'), 'jhj');
});

test('it should getOrderNoAndColli', function (assert) {
  var model = this.subject();
  var lines = [];
  var anotherLines = [];
  lines.push('some line');
  anotherLines.push('some line');
  lines.push('1234567x 123');
  model.getOrderNoAndColli(lines);

  assert.equal(model.get('orderno'), '');
  assert.equal(model.get('colli'), '');

  anotherLines.push('1 1234567x 123');
  model.getOrderNoAndColli(anotherLines);
  assert.equal(model.get('orderno'), '1234567');
  assert.equal(model.get('colli'), '123');
});

test('it should getShipcodeAndDetail', function (assert) {
  var model = this.subject();
  var lines = [];
  var line = "Some line";
  lines.push(line, line);
  lines.push('1234 - 5678');

  model.getShipcodeAndDetail(lines);

  assert.equal(model.get('shipcode'), '1234');
  assert.equal(model.get('shipdetail'), '5678');
});

test('it should getInsuranceAndAddress', function (assert) {
  var model = this.subject();
  var lines = [];
  var anotherLines = [];
  var line = "Some line";
  lines.push(line, line, line);
  anotherLines.push(line, line, line);
  lines.push('EUR 1,2 a');

  model.getInsuranceAndAddress(lines);

  assert.equal(model.get('insuranceamount'), "1,2");
  assert.equal(model.get('address'), 'a');

  anotherLines.push('Some Text');
  model.getInsuranceAndAddress(anotherLines);
  assert.equal(model.get('address'), 'Some Text');
});

test('it should getWeightRembAndZip', function (assert) {
  var model = this.subject();
  var lines = [];
  var anotherLines = [];
  var anotherSecondLines = [];
  var line = 'Some line';
  lines.push(line, line, line, line);
  anotherLines.push(line, line, line, line);
  anotherSecondLines.push(line, line, line, line);
  lines.push('EUR 2,3 EUR 4,5 ABCDEF');

  model.getWeightRembAndZip(lines);

  assert.equal(model.get('weight'), '2,3');
  assert.equal(model.get('rembamount'), '4,5');
  assert.equal(model.get('zipetc'), 'ABCDEF');

  anotherLines.push('2,3333 ABCDEF');
  anotherSecondLines.push('2,3333 ABCDEF');
  anotherLines.push('Some text');

  model.getWeightRembAndZip(anotherLines);

  assert.equal(model.get('weight'), '2,3333');
  assert.equal(model.get('rembamount'), '');
  assert.equal(model.get('zipetc'), 'ABCDEF');
  anotherSecondLines.push('EUR 1,2');

  model.getWeightRembAndZip(anotherSecondLines);

  assert.equal(model.get('weight'), '2,3333');
  assert.equal(model.get('rembamount'), '1,2');
  assert.equal(model.get('zipetc'), 'ABCDEF');
});

test('it should getSupplierOrCustomerShipment', function (assert) {
  var model = this.subject();

  model.getSupplierOrCustomerShipment();

  assert.equal(model.get('receiver'), "S");
  assert.ok(!model.get('order'));

  model.set('orderno', 'val');
  model.getSupplierOrCustomerShipment();
  assert.equal(model.get('receiver'), "C");
  assert.ok(model.get('order'));
});
