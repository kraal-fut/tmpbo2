import { moduleFor, test } from 'ember-qunit';

moduleFor( 'model:procurement/line', 'Unit | Model | procurement/line' );

test('it exists', function(assert) {
  var model = this.subject();
  assert.ok(model);
});

test('it should detectChanged', function(assert) {
  var model = this.subject();
  model.set('qty', 'value');
  assert.ok(model.get('isChanged'));
});
