import { moduleFor, test } from 'ember-qunit';

moduleFor( 'model:procurement/order', 'Unit | Model | procurement/order' );

test('it exists', function(assert) {
  var model = this.subject();
  assert.ok(model);
});

test('it should computeds totalValue correctly', function(assert) {
  var model = this.subject();
  var lines = [];
  lines.push({qty:1, pprice: 2});
  lines.push({qty: 3, pprice: 4});
  model.set('lines', lines);
  assert.equal(model.get('totalValue'), 14);
});

test('it should computeds allQty correctly', function(assert) {
  var model = this.subject();
  var lines = [{qty: 1}, {qty: 2}, {qty: 3}, {qty: 4}];
  model.set('lines', lines);
  assert.deepEqual(model.get('allQty'), [1,2,3,4]);
});

test('it should computeds allScanned correctly', function(assert) {
  var model = this.subject();
  var lines = [{scanned_qty: 1}, {scanned_qty: 2}, {scanned_qty: 3}, {scanned_qty: 4}];
  model.set('lines', lines);
  assert.deepEqual(model.get('allScanned'), [1,2,3,4]);
});

test('it should setAsNew', function(assert) {
  var model = this.subject();
  model.set('status', 'draft');
  model.setAsNew();
  assert.equal(model.get('status'), 'new');
});
