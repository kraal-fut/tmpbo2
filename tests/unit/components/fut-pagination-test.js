import {moduleForComponent, test} from 'ember-qunit';

moduleForComponent('fut-pagination', 'Unit | Component | fut pagination', {
    needs: []
});

test('it has a perPage alias', function (assert) {
    let component = this.subject({data:{perPage: 1337}});
    assert.equal(component.get('perPage'), 1337);
});

test('it has a totalPages alias', function (assert) {
    let component = this.subject({data:{totalPages: 1337}});
    assert.equal(component.get('totalPages'), 1337);
});

test('it has a currentPage alias', function (assert) {
    let component = this.subject({data:{currentPage: 1337}});
    assert.equal(component.get('currentPage'), 1337);
});

test('it creates a pageSet when currentPage and totalPages are known', function (assert) {
    let ps;
    let component = this.subject({data:{totalPages:25, currentPage:1}});

    ps = component.get('pageSet');
    assert.propEqual(ps, [
        {num: 1, active: true},
        {num: 2, active: false},
        {num: 3, active: false},
        {num: 4, active: false},
        {num: 5, active: false}
    ]);

    component.set('data.currentPage', 2);
    ps = component.get('pageSet');
    assert.propEqual(ps, [
        {num: 1, active: false},
        {num: 2, active: true},
        {num: 3, active: false},
        {num: 4, active: false},
        {num: 5, active: false}
    ]);

    component.set('data.currentPage', 3);
    ps = component.get('pageSet');
    assert.propEqual(ps, [
        {num: 1, active: false},
        {num: 2, active: false},
        {num: 3, active: true},
        {num: 4, active: false},
        {num: 5, active: false}
    ]);

    component.set('data.currentPage', 4);
    ps = component.get('pageSet');
    assert.propEqual(ps, [
        {num: 2, active: false},
        {num: 3, active: false},
        {num: 4, active: true},
        {num: 5, active: false},
        {num: 6, active: false}
    ]);
});

test('it limits the pageSet to the totalPages', function (assert) {
    let component, ps;
    component = this.subject({data: {totalPages: 10, currentPage: 9}});

    ps = component.get('pageSet');

    assert.propEqual(ps, [
        {num: 7, active: false},
        {num: 8, active: false},
        {num: 9, active: true},
        {num: 10, active: false}
    ]);
});
