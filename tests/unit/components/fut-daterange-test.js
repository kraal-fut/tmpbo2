import {moduleForComponent, test} from 'ember-qunit';

moduleForComponent('fut-daterange', 'Unit | Component | fut daterange', {
   needs: ['component:fut-input']
});

test('it renders', function(assert) {
  assert.expect(1);

  // Creates the component instance
  var component = this.subject();
  assert.equal(component._state, 'preRender');

  // Renders the component to the page
  //this.render();
  //assert.equal(component._state, 'inDOM');
});
