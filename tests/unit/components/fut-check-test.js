import {moduleForComponent, test} from 'ember-qunit';
import Ember from 'ember';

moduleForComponent('fut-check', 'Unit | Component | fut check', {
    needs: []
});

test('it renders', function(assert) {
  assert.expect(2);

  // Creates the component instance
  var component = this.subject();
  assert.equal( component._state, 'preRender' );

  // Renders the component to the page
  this.render();
  assert.equal( component._state, 'inDOM' );
});

test( 'it does not change value when not enabled', function( assert ) {
  var component = this.subject();

  Ember.run( function() {
    component.set( 'enabled', false );
    component.set( 'checked', true);
  } );

  this.$().find( 'i' ).click();

  assert.equal( component.get( 'checked' ), true );
} );

test( 'it does change value when enabled', function( assert ) {
    var component = this.subject();

    Ember.run( function() {
        component.set( 'enabled', true );
        component.set( 'checked', true );
    } );

    this.$().find( 'i' ).click();

    assert.equal( component.get( 'checked' ), false );
} );