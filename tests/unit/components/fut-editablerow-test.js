import { moduleForComponent, test } from 'ember-qunit';

moduleForComponent('fut-editablerow', 'Unit | Component | fut editablerow', {
  // Specify the other units that are required for this test
  // needs: ['component:foo', 'helper:bar'],
  unit: true
});

test('it renders', function(assert) {
  assert.expect(2);

  // Creates the component instance
  var component = this.subject();
  assert.equal(component._state, 'preRender');

  // Renders the component to the page
  this.render();
  assert.equal(component._state, 'inDOM');
});

test('it shows a delete button when editing and delete action given', function (assert) {
    var component = this.subject();

    assert.equal(false, component.get('showDelete'));

    component.set('editing', true);
    assert.equal(false, component.get('showDelete'));

    component.set('delete', 'deleteactionname');
    assert.equal(true, component.get('showDelete'));

});
