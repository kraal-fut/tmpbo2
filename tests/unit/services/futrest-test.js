import Ember from 'ember';
import { moduleFor, test } from 'ember-qunit';
import {defineFixture} from 'ic-ajax';

moduleFor('service:futrest', 'Unit | Service | futrest');

test('it exists', function (assert) {
    let service = this.subject();
    assert.ok(service);
});

test('it can do a simple ajax GET request', function (assert) {
    assert.expect(2);

    defineFixture('/api/v3/test', {
        response: [
            {id: 1, color: 'red'},
            {id: 2, color: 'green'},
            {id: 3, color: 'blue'}
        ],
        jqXHR: {},
        textStatus: 'success'
    });

    return this.subject().load('/test').then(response => {
        assert.ok(response);
        assert.equal(3, response.length);
    });
});

test('it can load a model of some type', function (assert) {
    assert.expect(3);

    defineFixture('/api/v3/products', {
        response: [
            {id: 1, color: 'red'},
            {id: 2, color: 'green'},
            {id: 3, color: 'blue'}
        ],
        jqXHR: {},
        textStatus: 'success'
    });

    let DummyModel = Ember.Object.extend({foo: 'bar'});

    return this.subject().loadModel('product/product', DummyModel).then(response => {
        assert.ok(response[0]);
        assert.equal(3, response.length);
        assert.equal('bar', response[0].get('foo'));
    });
});

test('it parses an error when plain text response given', function(assert) {
    let response = {responseText: 'Something wrong. Futurumshop'};

    let result = this.subject().parseError(response);

    assert.equal(result, response.responseText);
});

test('it parses errors when json response is given', function(assert) {
    let response = {responseText: '{"errors":{"showOn.0.type":["The selected show on.0.type is invalid."]}}'};
    let result = this.subject().parseError(response);

    assert.ok(result.errors);
});

test('When a model is given and the error is a validation error it should try to set the errors to the model', function(assert) {
    let done;
    let model = Ember.Object.create({
        setServerValidationErrors(errors) {
            if (errors) {
                done = true;
            }
        }
    });

    let response = {status: 422, responseText: '{"errors":{"showOn.0.type":["The selected show on.0.type is invalid."]}}'};
    this.subject().parseError(response, model);

    assert.ok(done);
});

test('when a model is given that does not support setting errors, it should reformat the error more generally', function(assert) {
    let model = Ember.Object.create({});

    let response = {
        status: 422,
        responseText: '{"errors":{"showOn.0.type":["The selected show on.0.type is invalid."]}}'
    };

    let result = this.subject().parseError(response, model);

    assert.equal(result, 'Could not save, maybe not everything is filled in correctly?');
});
