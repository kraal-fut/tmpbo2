import Ember from 'ember';
import { moduleFor, test } from 'ember-qunit';

moduleFor('service:search', 'Unit | Service | search');

test('it exists', function(assert) {
  var service = this.subject();
  assert.ok(service);
});

test('it detects when a handler is registered', function(assert) {
    var service = this.subject();

    var Handler = Ember.Object.extend({
        search() {
            return 'foobar';
        }
    });

    service.setHandler(Handler.create({}));

    assert.equal(true, service.hasHandler());
});

test('it calls the search method on the registered handler', function(assert) {
    var service = this.subject();

    var Handler = Ember.Object.extend({
        search() {
            return 'foobar';
        }
    });

    service.setHandler(Handler.create({}));

    assert.equal('foobar', service.callhandler());

    service.unsetHandler();

    assert.equal(undefined, service.callhandler());
});
