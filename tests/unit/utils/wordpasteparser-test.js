import WordPasteParser from '../../../utils/wordpasteparser';
import { module, test } from 'qunit';

module('wordpasteparser', 'Unit | Utils | wordpasteparse');

test('it works', function(assert) {
  var parser = new WordPasteParser('hello world');
  assert.ok( parser );
});

test( 'strips inline styles', function( assert ) {
  var parser = new WordPasteParser( '<span style="font-weight:bold;text-decoration:underline">hello world</span>' );

  var result = parser.parse();
  assert.equal( result, '<span>hello world</span>' );
} );

test( 'strips out word classes', function( assert ) {
  var parser = new WordPasteParser( '<p class="MsoNoSpacing">foo <p class="MsoNormal">bar</p></p>' );

  assert.equal( parser.parse(), 'foo <div>bar</div>\n' );
} );

test( 'strips out unneeded xml <o:p> tags ', function( assert ) {
  var parser = new WordPasteParser( '<div>hello world<o:p></o:p></div><o:p></o:p>' );

  assert.equal( parser.parse(), '<div>hello world</div>' );
} );
