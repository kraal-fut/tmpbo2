import ImportCsv from 'bo2/utils/importCsv';
import { module, test } from 'qunit';
import Ember from 'ember';

module( 'utils:importCsv', 'Unit | Utils | importCsv' );

test( 'It instantiates', function( assert ) {
    let importer = new ImportCsv();

    assert.ok( importer );
} );

test( 'It parses a single correct line', function( assert ) {
    let importer = new ImportCsv();

    let line = '6040-0069-001-N1410;1;foobar;Polyester;80';

    importer.handleLine(line);

    let result = importer.importProducts;

    assert.equal(result[0].prodlevid, '6040-0069-001-N1410');
    assert.equal(result[0].part, '1');
    assert.equal(result[0].description, 'foobar');
    assert.equal(result[0].material, 'Polyester');
    assert.equal(result[0].percentage, '80');
} );

test( 'It parses a file with \\r\\n line endings', function( assert ) {
    let importer = new ImportCsv();
    let file = '6040-0069-001-N1410;1;foobar;Polyester;80\r\n6040-0069-002-N1410;1;baz;Nylon;20\r\n';

    return importer.parseData( file ). then( (data) => {
        assert.equal( data.length, 2 );

        assert.equal( data[0].prodlevid, '6040-0069-001-N1410' );
        assert.equal( data[1].prodlevid, '6040-0069-002-N1410' );
    });
} );

test( 'It skips a line of a file if it starts with headers', function( assert ) {
    let importer = new ImportCsv();
    let file = 'prodlevid;part;description;material;percentage\r\n6040-0069-002-N1410;1;baz;Nylon;20\r\nProduct\r\n';

    return importer.parseData( file ).then( ( data ) => {
        assert.equal( data.length, 1 );

        assert.equal( data[0].prodlevid, '6040-0069-002-N1410' );
        assert.equal( data[0].percentage, '20' );
    } );
});

test( 'It can use a different ColumnMap if given to the constructor', function( assert ) {
    let columnMap = {'0': 'prodlevid', '1': 'stock'};

    let importer = new ImportCsv( columnMap );
    let file = 'prodlevid;stock\r\n6040-0069-002-N1410;42\r\n6040-0069-001-N1410;76';

    return importer.parseData( file ).then( ( data ) => {
        assert.equal( data.length, 2);
        assert.equal( data[0].prodlevid, '6040-0069-002-N1410' );
        assert.equal( data[1].prodlevid, '6040-0069-001-N1410' );
        assert.equal( data[0].stock, 42 );
        assert.equal( data[1].stock, 76 );
    });
} );

test( 'It can use custom line seperator', function( assert ) {
    let columnMap = {'0': 'prodlevid', '1': 'stock'};
    let importer = new ImportCsv( columnMap, '|' );

    let file = 'prodlevid;stock|6040-0069-002-N1410;42|6040-0069-001-N1410;76';

    return importer.parseData( file ).then( ( data ) => {
        assert.equal( data.length, 2 );
        assert.equal( data[0].prodlevid, '6040-0069-002-N1410' );
        assert.equal( data[1].prodlevid, '6040-0069-001-N1410' );
        assert.equal( data[0].stock, 42 );
        assert.equal( data[1].stock, 76 );
    } );
});

test( 'It can use field line seperator', function( assert ) {
    let columnMap = {'0': 'prodlevid', '1': 'stock'};
    let importer = new ImportCsv( columnMap, undefined, '\t' );

    let file = 'prodlevid\tstock\r\n6040-0069-002-N1410\t42\r\n6040-0069-001-N1410\t76';

    return importer.parseData( file ).then( ( data ) => {
        assert.equal( data.length, 2 );
        assert.equal( data[0].prodlevid, '6040-0069-002-N1410' );
        assert.equal( data[1].prodlevid, '6040-0069-001-N1410' );
        assert.equal( data[0].stock, 42 );
        assert.equal( data[1].stock, 76 );
    } );
} );
