import LineParser from 'bo2/utils/LineParser';
import { module, test } from 'qunit';
import Ember from 'ember';

module('util:LineParser', 'Unit | Utils | LineParser');

let RABO_LINE = `"NL39RABO0177450959","EUR","20150410","C","9.95","NL95ABNA1234567890","P Radius","20150410","cb","","ordernummer 3779654x, 3x 111010510-","XLG","","","","","","",""`;
let RABO_ARRAY = ["NL39RABO0177450959", "EUR", "20150410", "C", "9.95", "NL95ABNA1234567890", "P Radius", "20150410", "cb", "", "ordernummer 3779654x, 3x 111010510-", "XLG", "", "", "", "", "", "", ""];

let DE_ING_LINE = `10-04-2015	DE77 5002 1000 0010 1291 46	5318,08	EUR			YYW5Q2228QTDYK7E PP.8644.PP PAYPALDE88500700100175526303/DEUTDEFFPAYPAL EUROPE S.A.R.L. ET CIE S.C.APP.8644.PP ABBUCHUNG VOM PAYPAL-KONTO`;
let DE_ING_ARRAY = ['10-04-2015', 'DE77 5002 1000 0010 1291 46', '5318,08', 'EUR', '', '', 'YYW5Q2228QTDYK7E PP.8644.PP PAYPALDE88500700100175526303/DEUTDEFFPAYPAL EUROPE S.A.R.L. ET CIE S.C.APP.8644.PP ABBUCHUNG VOM PAYPAL-KONTO'];

let BE_ING_LINE = `10-04-2015	BE59 3101 8949 3226	6,15	EUR			/TRCD/0150//RTRN///CREF/NOTPROVIDED//CNTP/BE01123456789012/ARSPBE22/Patrick/1234 Apeldoorn//REMI/USTD/ordernummer 3779654x//CATP///PURP///SVCL/SEPA//MARF///ULTC////CSID///ISDT//`;
let BE_ING_ARRAY = ['10-04-2015', 'BE59 3101 8949 3226', '6,15', 'EUR', '', '', '/TRCD/0150//RTRN///CREF/NOTPROVIDED//CNTP/BE01123456789012/ARSPBE22/Patrick/1234 Apeldoorn//REMI/USTD/ordernummer 3779654x//CATP///PURP///SVCL/SEPA//MARF///ULTC////CSID///ISDT//'];

let REMBOURS_LINE = `"NL39RABO0177450959","EUR","20150430","C","350.80","NL96INGB0008453491","POSTNL PAKKETSERVICE BENELUX BV REMBOURS","20150430","cb","","Geind E354,34/Kosten E3,54 KENMERK:","3788099","","","","","EBTR1","",""`;
let REMBOURS_ARRAY = ["NL39RABO0177450959", "EUR", "20150430", "C", "350.80", "NL96INGB0008453491", "POSTNL PAKKETSERVICE BENELUX BV REMBOURS", "20150430", "cb", "", "Geind E354,34/Kosten E3,54 KENMERK:", "3788099", "", "", "", "", "EBTR1", "", ""];

let NACHNAME_LINE = `29-04-2015	DE77 5002 1000 0010 1291 46	347,45	EUR			00340433836591037849DE12200100200060476204/PBNKDEFFABRECHNUNGS-/KONTROLLKONTO FORDERUNG/VERBINDLICHKEIT NACHNAHMECASHREMBOURS 3718889X/ULTC/00340433836591037849//ULTD/DEUTSCHE POST AG/`;
let NACHNAME_ARRAY = ['29-04-2015', 'DE77 5002 1000 0010 1291 46', '347,45', 'EUR', '', '', '00340433836591037849DE12200100200060476204/PBNKDEFFABRECHNUNGS-/KONTROLLKONTO FORDERUNG/VERBINDLICHKEIT NACHNAHMECASHREMBOURS 3718889X/ULTC/00340433836591037849//ULTD/DEUTSCHE POST AG/'];

test('It instantiates', function (assert) {
    let model = Ember.Object.create({});

    let parser = new LineParser(model);

    assert.ok(parser);

});

test('it returns a promise for a parser', function (assert) {
    let model = Ember.Object.create({});

    let parser = new LineParser(model);

    assert.ok(parser.detectParser(RABO_ARRAY).then);

    return parser.detectParser(RABO_ARRAY).then((parser) => {
        assert.ok(typeof parser === 'function');
    });
});

test('the rabo parser sets the correct attributes on the model', function (assert) {
    let model = Ember.Object.create({line: RABO_LINE});

    let parser = new LineParser(model);
    let expectedText = 'Klant heeft betaald EUR 9.95 (RABO) Kenmerk: ordernummer 3779654x, 3x 111010510-XLG Klant: P Radius Rekeningnummer: NL95ABNA1234567890';

    return parser.parseRabo(RABO_ARRAY).then(parser.setProperties.bind(parser)).then(function (m) {
        assert.equal(m.get('amount'), '9.95');
        assert.equal(m.get('text'), expectedText);
        assert.equal(m.get('description'), expectedText);
        assert.equal(m.get('paymenttype'), 'C');
        assert.equal(m.get('name'), 'P Radius');
        assert.equal(m.get('date'), '10-04-2015');
        assert.equal(m.get('bankaccount'), 'NL95ABNA1234567890');
    });
});

test('the DE ING parser sets the correct attributes on the model', function (assert) {
    let model = Ember.Object.create({line: DE_ING_LINE});

    let parser = new LineParser(model);
    let expectedText = 'Klant heeft betaald EUR 5318.08 (ING): YYW5Q2228QTDYK7E PP.8644.PP PAYPALDE88500700100175526303/DEUTDEFFPAYPAL EUROPE S.A.R.L. ET CIE S.C.APP.8644.PP ABBUCHUNG VOM PAYPAL-KONTO';
    let expectedDescription = 'YYW5Q2228QTDYK7E PP.8644.PP PAYPALDE88500700100175526303/DEUTDEFFPAYPAL EUROPE S.A.R.L. ET CIE S.C.APP.8644.PP ABBUCHUNG VOM PAYPAL-KONTO';

    return parser.parseIngDE(DE_ING_ARRAY).then(parser.setProperties.bind(parser)).then((m) => {
        assert.equal(m.get('amount'), '5318.08');
        assert.equal(m.get('text'), expectedText);
        assert.equal(m.get('description'), expectedDescription);
        assert.equal(m.get('paymenttype'), 'D');
        assert.equal(m.get('name'), undefined);
        assert.equal(m.get('date'), '10-04-2015');
        assert.equal(m.get('bankaccount'), 'DE88500700100175526303');
    });

});

test('the BE ING parser sets the correct attributes on the model', function (assert) {
    let model = Ember.Object.create({line: BE_ING_LINE});
    let expectedText = 'Klant heeft betaald EUR 6.15 (ING): Kenmerk:  ordernummer 3779654x Klant: Patrick Rekening: BE01123456789012 BIC: ARSPBE22';
    let expectedDescription = '/TRCD/0150//RTRN///CREF/NOTPROVIDED//CNTP/BE01123456789012/ARSPBE22/Patrick/1234 Apeldoorn//REMI/USTD/ordernummer 3779654x//CATP///PURP///SVCL/SEPA//MARF///ULTC////CSID///ISDT//';

    let parser = new LineParser(model);

    return parser.parseIngBE(BE_ING_ARRAY).then(parser.setProperties.bind(parser)).then((m)=> {
        assert.equal(m.get('amount'), '6.15');
        assert.equal(m.get('text'), expectedText);
        assert.equal(m.get('description'), expectedDescription);
        assert.equal(m.get('paymenttype'), 'C');
        assert.equal(m.get('name'), 'Patrick');
        assert.equal(m.get('date'), '10-04-2015');
        assert.equal(m.get('bankaccount'), 'BE01123456789012');
    });
});

test('the rembours parser sets the correct attributes on the model', function (assert) {
    let model = Ember.Object.create({line: REMBOURS_LINE});

    let parser = new LineParser(model);
    let expectedText = REMBOURS_LINE;

    return parser.parseRemb(REMBOURS_ARRAY).then(parser.setProperties.bind(parser)).then(function (m) {
        assert.equal(m.get('amount'), '354.34');
        assert.equal(m.get('text'), expectedText);
        assert.equal(m.get('description'), expectedText);
        assert.equal(m.get('paymenttype'), 'C');
        assert.equal(m.get('date'), '30-04-2015');
        assert.equal(m.get('bankaccount'), undefined);
    });
});

test('the nachname parser sets the correct attributes on the model', function (assert) {
    let model = Ember.Object.create({line: NACHNAME_LINE});

    let parser = new LineParser(model);
    let expectedText = NACHNAME_LINE;

    return parser.parseNachname(NACHNAME_ARRAY).then(parser.setProperties.bind(parser)).then(function (m) {
        assert.equal(m.get('amount'), '349.45');
        assert.equal(m.get('text'), expectedText);
        assert.equal(m.get('description'), expectedText);
        assert.equal(m.get('paymenttype'), 'C');
        assert.equal(m.get('date'), '29-04-2015');
        assert.equal(m.get('bankaccount'), undefined);
        assert.equal(m.get('order'), '3718889');
    });
});
