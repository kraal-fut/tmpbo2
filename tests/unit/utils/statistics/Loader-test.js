import Loader from 'bo2/utils/statistics/loader';
import { module, test } from 'qunit';

module( 'utils:statistics/LineParser', 'Unit | Utils | statistics/loader' );

test( 'It instantiates', function( assert ) {
    let loader = new Loader();

    assert.ok( loader );

} );

test( 'it can make a main query', function( assert ) {
    let loader = new Loader();
    let result = loader.getMainQuery();

    assert.ok( result );
    assert.ok( result.must );
    assert.ok( result.must_not );
} );

test( 'it can make a sameasbo1 query', function( assert ) {
    let expected = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69];

    let loader = new Loader( { 'status': ['sameasbo1'] } );
    let result = loader.getStatuses();

    assert.deepEqual( result, expected );
});

test( 'it can make a sameasbo1 with additional statuses query', function( assert ) {
    let expected = [1, 2, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69];
    let loader = new Loader( {'status': ['sameasbo1', 1, 2]} );
    let result = loader.getStatuses();

    assert.deepEqual( result, expected );
});

test( 'it can add a vendor to the vendorfilters', function( assert ) {
    let loader = new Loader();

    loader.addVendorFilter('nike');
    assert.deepEqual( ['nike'], loader.get( 'vendorFilter' ) );

    loader.addVendorFilter( 'assics' );
    assert.deepEqual( ['nike', 'assics'], loader.get( 'vendorFilter' ) );
});
