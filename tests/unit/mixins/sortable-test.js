import Ember from 'ember';
import SortableMixin from '../../../mixins/sortable';
import { moduleFor, test } from 'ember-qunit';

moduleFor('mixin:sortable', 'Unit | Mixins | Sortable', {
    needs: []
});
// Replace this with your real tests.
test('it works', function(assert) {
  var SortableObject = Ember.Object.extend(SortableMixin);
  var subject = SortableObject.create();
  assert.ok(subject);
});
