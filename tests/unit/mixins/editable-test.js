import Ember from 'ember';
import EditableMixin from '../../../mixins/editable';
import { moduleFor, test } from 'ember-qunit';

moduleFor('mixin:editable', 'Unit | Mixins | Editable', {
    needs: []
});

// Replace this with your real tests.
test('it works', function(assert) {
  var EditableObject = Ember.Object.extend(EditableMixin);
  var subject = EditableObject.create();
  assert.ok(subject);
});
