import Ember from 'ember';
import CansearchMixin from '../../../mixins/cansearch';
import { moduleFor, test } from 'ember-qunit';

moduleFor('mixin:cansearch', 'Unit | Mixins | CanSearch', {
    needs: []
});
// Replace this with your real tests.
test('it works', function(assert) {
  var CansearchObject = Ember.Object.extend(CansearchMixin);
  var subject = CansearchObject.create();
  assert.ok(subject);
});
