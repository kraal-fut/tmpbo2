import Ember from 'ember';
import FilterableMixin from '../../../mixins/filterable';
import { moduleFor, test } from 'ember-qunit';

moduleFor('mixin:filterable', 'Unit | Mixins | Filterable', {
    needs: []
});

// Replace this with your real tests.
test('it works', function(assert) {
  var FilterableObject = Ember.Object.extend(FilterableMixin);
  var subject = FilterableObject.create();
  assert.ok(subject);
});
