import Ember from 'ember';
import PaginatedMixin from '../../../mixins/paginated';
import { moduleFor, test } from 'ember-qunit';

moduleFor('mixin:paginated', 'Unit | Mixins | Paginated', {
    needs: []
});

// Replace this with your real tests.
test('it works', function(assert) {
  var PaginatedObject = Ember.Object.extend(PaginatedMixin);
  var subject = PaginatedObject.create();
  assert.ok(subject);
});
