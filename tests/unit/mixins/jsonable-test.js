import Ember from 'ember';
import JsonableMixin from '../../../mixins/jsonable';
import { moduleFor, test } from 'ember-qunit';

moduleFor('mixin:jsonable', 'Unit | Mixins | JsonAble', {
    needs: []
});
// Replace this with your real tests.
test('it works', function(assert) {
  var JsonableObject = Ember.Object.extend(JsonableMixin);
  var subject = JsonableObject.create();
  assert.ok(subject);
});
