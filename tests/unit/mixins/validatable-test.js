import Ember from 'ember';
import ValidatableMixin from '../../../mixins/validatable';
import {containsAny} from '../../../mixins/validatable';

import { moduleFor, test } from 'ember-qunit';

moduleFor('mixin:validatable', 'Unit | Mixins | Validatable', {
    needs: []
});

test('it considers validation pass when no validations given', (assert) => {
    var ValidatableObject = Ember.Object.extend(ValidatableMixin);
    var subject = ValidatableObject.create({});

    return subject.validate().then((model) => {
        assert.ok(model);
    });
});

test('it considers validation fail when not all rules are fulfilled', (assert) => {
    var ValidatableObject = Ember.Object.extend(ValidatableMixin);
    var subject = ValidatableObject.create({
        validations: {
            foo: {required: true}
        }
    });

    return subject.validate().then(() => {
        assert.ok(false, "promise should not be fulfilled");
    }).catch((err) => {
        assert.equal(err.get('length'), 1);
        assert.equal(subject.get('errors.0.field'), 'foo');
        assert.equal(subject.get('errors.0.reason'), 'required');
        assert.equal(subject.get('errors.0.message'), 'The field foo is required.');
    });
});

test('it resets validation after correcting them', (assert) => {
    var ValidatableObject = Ember.Object.extend(ValidatableMixin);
    var subject = ValidatableObject.create({
        validations: {
            foo: {required: true}
        }
    });

    var firstInvalid = function () {
        return subject.validate().then(() => {
            assert.ok(false, "promise should not be fulfilled");
        }).catch((err) => {
            assert.equal(err.get('length'), 1);
            return subject;
        });
    };

    return firstInvalid().then((model) => {
        model.set('foo', 'bar');

        return subject.validate().then((result) => {
            assert.ok(result);
        });
    });

});

test('it allows multiple rules to be set', (assert) => {
    var ValidatableObject = Ember.Object.extend(ValidatableMixin);
    var subject = ValidatableObject.create({
        validations: {
            foo: {required: true},
            bar: {min: 2},
            baz: {max: 4}
        },
        baz: '12345'
    });

    return subject.validate().then(() => {
        assert.ok(false, "promise should not be fulfilled");
    }).catch((err) => {
        assert.equal(err.get('length'), 3);
        assert.equal(subject.get('errors.0.field'), 'foo');
        assert.equal(subject.get('errors.0.reason'), 'required');
        assert.equal(subject.get('errors.0.message'), 'The field foo is required.');

        assert.equal(subject.get('errors.1.field'), 'bar');
        assert.equal(subject.get('errors.1.reason'), 'min');
        assert.equal(subject.get('errors.1.message'), 'The field bar should be at least 2 long.');

        assert.equal(subject.get('errors.2.field'), 'baz');
        assert.equal(subject.get('errors.2.reason'), 'max');
        assert.equal(subject.get('errors.2.message'), 'The field baz should be at most 4 long.');
    });
});


test('it validates a field for allowed params', (assert) => {
    var ValidatableObject = Ember.Object.extend(ValidatableMixin);
    var subject = ValidatableObject.create({
        validations: {
            foo: {oneOf: ['foo', 'bar', 42]},
            bar: {oneOf: ['foo', 'bar', 42]}
        }
    });

    subject.set('foo', 'wrong');
    subject.set('bar', 42);

    return subject.validate().then(() => {
        assert.ok(false, "promise should not be fulfilled");
    }).catch((err) => {
        assert.equal(err.get('length'), 1);
        assert.equal(subject.get('errors.0.field'), 'foo');
        assert.equal(subject.get('errors.0.reason'), 'oneOf');
        assert.equal(subject.get('errors.0.message'), 'The field foo does not have a correct value.');
    });
});

test('it can test intersection of tasks and whens', (assert) => {
    var t, w;
    t = ['foo', 'baz'];
    w = ['bar'];
    assert.ok(!containsAny(t, w));

    t = ['foo', 'bar'];
    w = ['bar'];
    assert.ok(containsAny(t, w));

    t = ['foo'];
    w = ['bar', 'foo'];
    assert.ok(containsAny(t, w));

    t = ['foo', 'foo'];
    w = ['foo', 'foo'];
    assert.ok(containsAny(t, w));
});

test('it validates related models', (assert) => {
    var ValidatableObject = Ember.Object.extend(ValidatableMixin, {
        validations: {
            foo: {required: true},
            bar: {hasRelateds: true}
        },
        bar: []
    });

    var RelatedObject = Ember.Object.extend(ValidatableMixin, {
        validations: {
            baz: {required: true}
        }
    });

    var subject = ValidatableObject.create({
        foo: 'Hello world'
    });

    var second = RelatedObject.create({baz: 'qux'});

    subject.get('bar').pushObject(RelatedObject.create({baz: 'yolo'}));
    subject.get('bar').pushObject(second);

    subject.validate().then((result)=>{
        assert.ok(result);
    }).catch((err) =>{
        console.log('err', err);
    });

    Ember.run(() => {
        second.set('baz', undefined);
    });

    return subject.validate().then((result)=> {
        assert.ok(false, "promise should not be fulfilled");
    }).catch((err)=>{
        assert.equal(err.get('length'), 1);
    });
});
