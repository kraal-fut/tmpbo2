import { moduleFor, test } from 'ember-qunit';

moduleFor('adapter:product/image', 'Unit | Adapter | product/image', {
    // Specify the other units that are required for this test.
    needs: ['service:firebase']
});

// Replace this with your real tests.
test('it exists', function (assert) {
    let adapter = this.subject();
    assert.ok(adapter);
});
