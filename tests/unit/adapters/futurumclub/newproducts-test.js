import { moduleFor, test } from 'ember-qunit';

moduleFor('adapter:futurumclub/newproducts', 'Unit | Adapter | futurumclub/newproducts', {
  // Specify the other units that are required for this test.
   needs: ['service:firebase']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var adapter = this.subject();
  assert.ok(adapter);
});
